import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import KeyboardVoiceIcon from '@material-ui/icons/KeyboardVoice';



export default function CustomizedInputBase() {
  

  return (
    <div component="form" className="w-4/12 flex border border-gray-200 rounded-full mx-auto p-2" >
      
      
     <SearchIcon className="ml-3 text-gray-300" />
     <input className="w-full outline-none bg-transparent wt text-center" type="text" placeholder="Serch Mobile, tablets, Tv &amp; Accessories" />
     <KeyboardVoiceIcon className="mr-3 text-gray-400" />
      
    </div>
  );
}
import React from 'react'
import Container from '@material-ui/core/Container';
import SearchIcon from '@material-ui/icons/Search';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import PersonIcon from '@material-ui/icons/Person';
import FileCopyRoundedIcon from '@material-ui/icons/FileCopyRounded';
import FavoriteBorderRoundedIcon from '@material-ui/icons/FavoriteBorderRounded';
import ContactMailSharpIcon from '@material-ui/icons/ContactMailSharp';
import CardGiftcardOutlinedIcon from '@material-ui/icons/CardGiftcardOutlined';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';
import QuestionAnswerOutlinedIcon from '@material-ui/icons/QuestionAnswerOutlined';
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import NotificationsNoneOutlinedIcon from '@material-ui/icons/NotificationsNoneOutlined';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';


export default function topheader() {
    return (
        <div>
           
           <div className="topbar p-1">
              <Container maxWidth="lg">
                 <div className="flex items-center justify-between">
                 
                 <div  className="w-36"><a href="/"><img  src="asset/poorvika-logo2.png"></img></a></div>

                  <div className="search-wrp flex items-center  bg-gray-50  rounded-full w-1/2">
                  <select id="cars" className="outline-none border-none mr-2 bg-transparent">
                  <option value="audi">All Categories</option>
                  <option value="volvo">Volvo</option>
                  <option value="saab">Saab</option>
                  <option value="vw">VW</option>
                  </select>
                  <input className="outline-none bg-transparent w-full" type="text" placeholder="Serch for product,brands and more" />
                  <SearchIcon/>
                  </div>


                  <div className="icon-wrp flex items-center ml-4 mr-5 text-white justify-between mt-2">
                      <div className="store-head">
                        <p className="font-light">Locate</p>
                        <h5 className="font-medium">Stores <ExpandMoreIcon/></h5>
                           
                            <ul className="Stores-dropdown">
                              <li className="pincode">
                              <span className="flex">
                              <input type="text" placeholder="Enter Pincode, City or State" value="" className="form-control" id="pincode" name="txtemail" />
                              <button className="check" type="submit" name="submit" >Check</button>
                              </span>
                              </li>

                              <li className="search-bar">
                              <select id="state_list" className="form-control option"><option value="">Select State</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Maharashtra">Maharashtra</option><option value="Pondicherry">Pondicherry</option><option value="Tamilnadu">Tamilnadu</option></select>
                              </li>

                              <li className="search-city">          
                              <select id="city_list" className="form-control option">
                              <option value="">Select City</option>
                              </select>
                              </li>

                              <li className="search-area">                    
                              <select id="area_list" className="form-control option">
                              <option value="">Select Area</option>
                              </select>               
                              </li>
                              </ul>
                                 
                      </div>
                      <div className="item-head">
                        <p className="font-light">(3)items</p>
                        <h5 className="font-medium"><span>24,458</span> <AddShoppingCartIcon/></h5>
                           <ul className="Stores-dropdown">
                              Your shopping cart is empty!
                           </ul>    
                      </div>
                      <div className="account-head">
                         <p className="font-light">Sebastin's</p>
                         <h5 className="font-medium"><span>Account</span> <PersonIcon/></h5>
                           <ul className="Stores-dropdown account-list w-52">
                                    <li id="new-loginbutton"><a href="#" className="order"> <FileCopyRoundedIcon/> My Order</a></li>
                                    <li id="newwishlist-loginbutton"><a href="#" className="wishlist"> <FavoriteBorderRoundedIcon/> Wishlist &amp; Saved</a></li>
                                    <li id="newsaved-loginbutton"><a href="#" className="address"> <ContactMailSharpIcon/> Saved Address</a></li>
                                    <li id="newgift-loginbutton"><a href="#" className="gift"> <CardGiftcardOutlinedIcon/> Gift Card</a></li>
                                    <li id="newcoupen-loginbutton"><a href="#" className="coupon"> <MapOutlinedIcon/> Coupons</a></li>
                                    <li id="newcontact-loginbutton"><a href="#" className="contact"> <QuestionAnswerOutlinedIcon/> Contact Us</a></li>
                                    <li id="newprofile-loginbutton"><a href="#" className="profile"> <PersonOutlineOutlinedIcon/> My Profile</a></li>
                                    <li id="newnotification-loginbutton"><a href="#" className="notification"> <NotificationsNoneOutlinedIcon/> Notification</a></li>
                                    <li id="new-loginbutton"><a href="#" className="logout"> <ExitToAppIcon/> Login</a></li>
                                    <li id="new-regbutton"><a href="#" className="logout"> <ExitToAppIcon/> Register</a></li> 	
                           </ul> 
                      </div>



                  </div>

                              

                 </div>
              </Container>    
           </div>    
        </div>
    )
}

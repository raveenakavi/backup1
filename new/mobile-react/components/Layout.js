
import NavBar from './Navbar'
import Topheader from './top-header';
import  Head  from "next/head";
import Footer from './Footer';

const layout=({children})=> {
    return (
        <div>
            <Head>
            {/* <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/> */}
            </Head>
            {/* <Topheader/>
            <NavBar/> */}
            {children}
            {/* <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> */}
            {/* <Footer/> */}
        </div>
    )
}

export default layout

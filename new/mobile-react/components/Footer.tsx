

import React from 'react';
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import CallIcon from '@material-ui/icons/Call';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import { green, lightBlue,indigo } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedInputBase from './searchbar';
import SearchIcon from '@material-ui/icons/Search';
import KeyboardVoiceIcon from '@material-ui/icons/KeyboardVoice';

const Footbar = () => {
  const responsive = {
    0: {
      items: 3
    },
    600: {
      items: 6
    },
    1024: {
      items: 9
    }
  };
  const useStyles = makeStyles((theme) => ({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 400,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  }));

const items = [
  <h4><WhatsAppIcon style={{ color: green[500] }}/> Whatsapp Us</h4>,
  <h4><CallIcon color="primary" /> Talk to us</h4>,
  <h4><InstagramIcon color="secondary" /> @poorvikamobiles</h4>,
  <h4><CallIcon  style={{ color: green[500] }}/> @poorvikamobiles</h4>,
  <h4><FacebookIcon style={{ color: indigo[500] }}/>@poorvikamobiles</h4>,
  <h4><TwitterIcon style={{ color: lightBlue[500] }}/> @poorvikamobiles</h4>,
  <h4><FacebookIcon style={{ color: indigo[500] }}/>@poorvikamobiles</h4>,
  <h4><TwitterIcon style={{ color: lightBlue[500] }}/> @poorvikamobiles</h4>,
  <h4><FacebookIcon style={{ color: indigo[500] }}/>@poorvikamobiles</h4>,
  <h4><TwitterIcon style={{ color: lightBlue[500] }}/> @poorvikamobiles</h4>,
  <h4><FacebookIcon style={{ color: indigo[500] }}/>@poorvikamobiles</h4>,
  <h4><TwitterIcon style={{ color: lightBlue[500] }}/> @poorvikamobiles</h4>,
];
const classes = useStyles();
  return (
    <div className="bg-white text-sm ">
      <div className="bg-white relative footbar">
        <div className="bg-white tag-footer top-0 p-3 flex justify-between items-center">
          <h5 className="font-bold">Not found it yet? <span className="pl-3 text-sm font-light">Check these most searched keywords</span></h5>
          <div  className="w-4/12 flex border border-gray-200 rounded-full p-2 bg-white" >
            <SearchIcon className="ml-3 text-gray-300" />
            <input className="w-full outline-none bg-transparent wt text-sm" type="text" placeholder="Serch Mobile, tablets, Tv &amp; Accessories" />
            <KeyboardVoiceIcon className="mr-3 text-gray-400" />
            </div>
        </div>
      
<div className="footer_icons pt-5">
        <AliceCarousel mouseTracking  items={items} responsive={responsive}  className="text-sm ml-3 mr-3 p-8 sbicon" />
</div>
           <div className="con-info grid grid-cols-5 px-2 py-2 text-black bg-white pt-10 pb-4">
           <div><h3 className="font-bold pl-3">Contact Information</h3>
           <h5 className="text-xs font-bold pl-3 pt-3">Head Office</h5>
           <p className="pl-3 leading-5">Poorvika Mobiles Pvt Ltd, <br />Admin Office No.30, Arcot Road,<br />Kodambakkam, Chennai- 600 024.</p>
           </div>
           <div><h3 className="font-bold pl-3">Support</h3>
           <ul className="leading-loose pl-3 pt-3">
             <li><a href="">Contact Us</a></li>
             <li><a href="">Site Map</a></li>
             <li><a href="">One Assistant</a></li></ul></div>
           <div><h3 className="font-bold pl-3">Policies</h3>
           <ul className="leading-loose pl-3 pt-3">
             <li><a href="">T &amp; C</a></li>
             <li><a href="">Privacy Policy</a></li>
             <li><a href="">About Us</a></li></ul></div>
           <div><h3 className="font-bold pl-3">Shop</h3>
           <ul className="leading-loose pl-3 pt-3">
             <li><a href="">Mobile</a></li>
             <li><a href="">Accessories</a></li>
             <li><a href="">Television</a></li></ul></div>
           <div><h3 className="font-bold pl-3">Opportunities</h3>
           <ul className="leading-loose pl-3 pt-3">
             <li><a href="">Be an Affiliate</a></li>
             <li><a href="">Be an influencer</a></li>
             <li><a href="">Create Content</a></li></ul></div>
           </div>
<div className="breadcrumbs-pad pt-10 pb-5 bg-white">
           <ul aria-label="Mobile" className="bg-white text-black pl-3">
  <li color="textPrimary"><span className="font-bold ">About Us : </span><span className="text-sm text-light about">Poorvika is the largest Omni channel retailer for smartphones, tablets and Accessories in Asia. The company has set up more than 390 one-stop mobile stores across various cities in Tamil Nadu, Karnataka, Maharastra, Kerala and Pondicherry where the customers can buy their preferred smartphones at ease. The company has set up more than 390 one-stop mobile stores across various cities in Tamil Nadu, karnataka, maharashtra, Kerala and Pondicherry where the customers can buy their preferred smartphones at ease.</span></li>
</ul>
</div>
             <div className="flex xs={12} bg-white breadcrumbs-pad">


                 <img src="asset/foot1.png" />
           
             </div>
            
      </div>
      </div>
  );
};


export default Footbar;
import React from 'react'
import Container from '@material-ui/core/Container';

export default function header() {
    return (
        <div className="top-header-menu shadow-sm bg-white">
            <Container maxWidth="lg">
                <ul className="menu_section flex justify-around p-2">
                   <a href="#" className="dropbtn">Mobiles & Accessories</a>
                   <a href="#" className="dropbtn">Computer & Tablets</a>
                   <a href="#" className="dropbtn">TV, Audio & Entertainment</a>
                   <a href="#" className="dropbtn">Smart Technology</a>
                   <a href="#" className="dropbtn">Personal & Health Care</a>
                   <a href="#" className="dropbtn">Offers</a>
                </ul>
            </Container>
        </div>
    )
}

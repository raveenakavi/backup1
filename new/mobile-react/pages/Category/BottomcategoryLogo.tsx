import React, { useState, useRef } from "react";
import Slider from "react-slick";


export default function BottomcategoryLogo() {

  const BottomcategoryLogoSlider = useRef();
  var categoryLogo = {
    dots: false,
    autoplay: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },

    {
      breakpoint: 476,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};
const next = () => {
  slider.slickNext();
};
const previous = () => {
  slider.slickPrev();
};


    return (
        <div className="category-bottom-logo bg-white relative">

            <Slider ref={(slider) => (BottomcategoryLogoSlider.current = slider)} {...categoryLogo}>

                <div className="category-bottom"><img src={'/asset/category-logo-1.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-2.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-3.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-4.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-1.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-2.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-3.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-4.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-1.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-2.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-3.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-4.png'} className="shadow-xl" /></div>
                <div className="category-bottom"><img src={'/asset/category-logo-1.png'} className="shadow-xl" /></div>
            </Slider>
        </div>
    )
}

import React, { useState, useRef } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import SyncAltIcon from '@material-ui/icons/SyncAlt';

const EarBuds = () => {

  const EarBudsSlider = useRef();
  var EarBuds = {
    dots: false,
    autoplay: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },

    {
      breakpoint: 476,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};
const next = () => {
  slider.slickNext();
};
const previous = () => {
  slider.slickPrev();
};

  const StyledRating = withStyles({
    iconFilled: {
      color: '#f11a08',
    },
    iconHover: {
      color: '#ff3d47',
    },
  })(Rating);
//   const  [toggleHeart, setToggleHeart] = useState(false)
    
//   changeColor = useCallback(() =>{
//    setToggleHeart(!toggleHeart)
//   },[])
//   <FavoriteIcon className={
//           toggleHeart ? 'heart active' : 'heart'
//         } onClick={changeColor}/>

const [divShow, setDivShow] = useState(false);
const [divHide, setDivHide] = useState(false);

const[add,minus]=useState(true);

const selectChange = (element) =>{
  if(element == add){
    setDivShow(false);
    setDivHide(true);
  }else if(element==minus){
    setDivShow(true);
    setDivHide(false);
  }
}
    
  return (
    <div className="bg-white app similar-products earbuds my-10 pb-5">
      {/*  Slider */}

      <h2 className="flex py-4 px-3 text-black text-md md:text-2xl font-bold">Water proof EarBuds

                    {/* <span className="text-xs pt-3 text-blue-600"> See all</span> */}
                    <span className="right-2 absolute">
                    <div className="slider-arrow">
                    <button  onClick={() => EarBudsSlider.current.slickPrev()} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button  onClick={() => EarBudsSlider.current.slickNext()} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
                                </div>                      </span>
                    
                    </h2>
      <Slider ref={(slider) => (EarBudsSlider.current = slider)} {...EarBuds}>
        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="element" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>


  <div className="element" id="surround">
        <div className="flex">
        <img src={'/asset/earbuds-2.png'} />
        <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element" id="surround">
        <div className="flex">
        <img src={'/asset/earbuds-3.jpeg'} />
        <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-4.jpeg'} />
        <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>


  <div className="element" id="surround">
        <div className="flex">
        <img src={'/asset/earbuds-2.png'} />
        <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element" id="surround">
        <div className="flex">
        <img src={'/asset/earbuds-3.jpeg'} />
        <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-4.jpeg'} />
        <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>
  
  <div className="element" id="surround">
        <div className="flex">
        <img src={'/asset/earbuds-3.jpeg'} />
        <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-4.jpeg'} />
        <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(Color and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>  
  
          {/* })} */}
      </Slider>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
    </div>
  );
};

export default EarBuds;

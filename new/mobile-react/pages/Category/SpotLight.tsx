import React from 'react';
import HeadsetMicOutlinedIcon from '@material-ui/icons/HeadsetMicOutlined';
import HomeWorkOutlinedIcon from '@material-ui/icons/HomeWorkOutlined';
import ContactlessOutlinedIcon from '@material-ui/icons/ContactlessOutlined';
import SportsEsportsOutlinedIcon from '@material-ui/icons/SportsEsportsOutlined';
import EqualizerOutlinedIcon from '@material-ui/icons/EqualizerOutlined';
import EmojiSymbolsOutlinedIcon from '@material-ui/icons/EmojiSymbolsOutlined';


export default function SpotLight() {
    return (
        <div>
                    <div className="bg-white spotlight pt-10 pb-5 relative">

<h3 className="uppercase text-center text-md md:text-lg font-bold">Choose</h3>
<h1 className="uppercase text-center text-2xl md:text-4xl font-bold relative">The Spot Light</h1>
{/* 
<div className="w-10/12 ml-auto mr-auto">
            <img src={'/asset/spotlight.png'} className="w-full h-auto" />
            </div> */}


            <section id="set-1" className="bg-white">
				<div className="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
					<a href="#" className="hi-icon hi-icon-screen"><HeadsetMicOutlinedIcon /></a>
					<a href="#" className="hi-icon hi-icon-screen"><HeadsetMicOutlinedIcon /></a>
					<a href="#" className="hi-icon hi-icon-earth"><HeadsetMicOutlinedIcon /></a>
					<a href="#" className="hi-icon hi-icon-support"><HeadsetMicOutlinedIcon /></a>
					<a href="#" className="hi-icon hi-icon-locked"><HomeWorkOutlinedIcon /></a>
				</div>
				<div className="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
					<a href="#" className="hi-icon hi-icon-screen"><ContactlessOutlinedIcon /></a>
					<a href="#" className="hi-icon hi-icon-screen"><SportsEsportsOutlinedIcon /></a>
					<a href="#" className="hi-icon hi-icon-earth"><EqualizerOutlinedIcon /></a>
					<a href="#" className="hi-icon hi-icon-support"><EmojiSymbolsOutlinedIcon /></a>
					<a href="#" className="hi-icon hi-icon-locked"><HomeWorkOutlinedIcon /></a>
				</div>
			</section>


</div>


</div>
    )
}

import React, { useState, useRef } from "react";
import Slider from "react-slick";


export default function categoryLogo() {

  const categoryLogoSlider = useRef();
  var categoryLogo = {
    dots: false,
    autoplay: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },

    {
      breakpoint: 476,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};
const next = () => {
  slider.slickNext();
};
const previous = () => {
  slider.slickPrev();
};

    return (
        <div className="bg-white category-series pt-10 pb-5 relative">

            <h3 className="uppercase text-center text-md md:text-lg font-bold">Our Series</h3>
            <h1 className="uppercase text-center text-2xl md:text-4xl font-bold relative"> choosing in one Style</h1>

            <Slider ref={(slider) => (categoryLogoSlider.current = slider)} {...categoryLogo}>

                <img src={'/asset/category-logo-1.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-2.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-3.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-4.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-1.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-2.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-3.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-4.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-1.png'} className="shadow-xl" />
                
            </Slider>
            <div className="slider-arrow">
                    <button  onClick={() => categoryLogoSlider.current.slickPrev()} className="previus-button text-bg-gray margin-2 padding-10">
      </button>
      <button  onClick={() => categoryLogoSlider.current.slickNext()} className="next-button text-bg-gray margin-2 padding-10">
      </button>
                                </div>  
        </div>
    )
}

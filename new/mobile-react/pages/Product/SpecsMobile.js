import Head from "next/head";
// import styles from "../styles/Home.module.css";
import Grid from "@material-ui/core/Grid";
import Select from "react-select";
import { useState, useEffect } from "react";
import Rating from "@material-ui/lab/Rating";
import SearchIcon from "@material-ui/icons/Search";
import { ToggleButton } from "primereact/togglebutton";
// import "primereact/resources/themes/saga-blue/theme.css";
// import "primereact/resources/primereact.min.css";
// import "primeicons/primeicons.css";
import Switch from "react-switch";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import Divider from "@material-ui/core/Divider";

const options = [
    { value: "Nokia", label: "Nokia" },
    { value: "Realme", label: "Realme" },
    { value: "Vivo", label: "Vivo" },
];
export default function SpecsMobile() {
    const [clientreview, setClientReview] = useState(2);
    const [clientreview1, setClientReview1] = useState(4);
    const [comparereview, setCompareReview] = useState(true);
    const [showModal, setShowModal] = useState(false);
    const [compareMobile, setCompareMobile] = useState(false);
    const [compareMobile1, setCompareMobile1] = useState(false);
    const [compareMobile2, setCompareMobile2] = useState(false);
    const [checked, setChecked] = useState(false);
    const [checked1, setChecked1] = useState(false);
    const [checked2, setChecked2] = useState(true);
    const [checked3, setChecked3] = useState(false);
    const [checked4, setChecked4] = useState(false);
    const [checked5, setChecked5] = useState(false);
    const [checked6, setChecked6] = useState(true);
    const [checked7, setChecked7] = useState(true);
    const handleChange = (nextChecked) => {
        setChecked(nextChecked);
    };
    const handleChange1 = (nextChecked) => {
        setChecked1(nextChecked);
    };
    const handleChange2 = (nextChecked) => {
        setChecked2(nextChecked);
    };
    const handleChange3 = (nextChecked) => {
        setChecked3(nextChecked);
    };
    const handleChange4 = (nextChecked) => {
        setChecked4(nextChecked);
    };
    const handleChange5 = (nextChecked) => {
        setChecked5(nextChecked);
    };
    const handleChange6 = (nextChecked) => {
        setChecked6(nextChecked);
    };
    const handleChange7 = (nextChecked) => {
        setChecked7(nextChecked);
    };
    const [scroll, setScroll] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            setScroll(window.scrollY > 10);
            console.log(scroll,"testing");
        });
    }, []);

    return (
        <div className="mobile-layout bg-white w-full md:w-3/5 ml-auto mr-auto shadow">
            <div className="flex flex-col">
                <div className="overflow-x-auto sm:overflow-visible md:overflow-visible lg:overflow-visible">
                    <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-4">
                        <div className="shadow border-b border-gray-200 sm:rounded-lg">
                        <div className="flex">
                            <div className="content-product items-center justify-center mt-5">
                                <img src="../asset/set2.png" className="w-16" />
                            </div>
                                <div className="prleft pl-8 py-5 md:py-8">
                                    <h6 className="font-semibold">Apple iPhone 11 Pro Max</h6>
                                    <span className="text-xs text-gray-400 flex mt-2">
                                        <p className="text-blue-400 mr-1">Rs. 1,19,249 </p> (PRODUCT CODE:31489)
                                    </span>
                                </div>
                                </div>

                                <Divider />

                            <table className="min-w-full divide-y divide-gray-200 table-auto sm:table-auto relative w-full border border-emerald-500">
                                <tbody className="">
                                            <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                                <Switch onChange={handleChange6} checked={checked6} className="react-switch pr-3" offColor="#f37021" onColor="#008800" offHandleColor="#e6e6fa" onHandleColor="#e6e6fa" />
                                                Key Specifications
                                            </div>
                                    {checked6 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line ">Display</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-xs font-light text-black p-1  whitespace-pre-line capitalize">6.5 Inches (16.51cm) <span className="text-gray-200">1080x2400px 405PPI</span></div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked6 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize whitespace-pre-line flex">Front Camera</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-xs font-light text-black p-1  capitalize whitespace-pre-line">12 Mp (Single Punch Hole)</div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked6 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">Rear camera</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-xs font-light text-black p-1  capitalize whitespace-pre-line">12MP+12Mp 
                                                    <span className="">(Primary)</span>
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked6 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim size</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked6 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">network</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <span className="block">4G available(supports indian bands)</span>
                                                    <span className="block">3G available</span>
                                                    <span className="block">2G available</span>
                                                </div>
                                            </td>

                                        </tr>
                                    ) : null}
                                    {checked6 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fingerprint sensor</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    available
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked6 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fast charging</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    available
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                            <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                                <Switch onChange={handleChange7} checked={checked7} className="react-switch pr-3" offColor="#f37021" onColor="#008800" offHandleColor="#e6e6fa" onHandleColor="#e6e6fa" />
                                               General features
                                            </div>
                                    {checked7 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">dimension</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <span className="block">height:155mm</span>
                                                    <span className="block">width:77.8mm</span>
                                                    <span className="block">thickness:8.1mm</span>
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked7 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">weight</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  whitespace-pre-line">189 g</div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked7 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">ruggedness</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    available
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked7 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">waterproof</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                                <Switch onChange={handleChange2} checked={checked2} className="react-switch" offColor="#f37021" onColor="#008800" offHandleColor="#e6e6fa" onHandleColor="#e6e6fa" />
                                                performance & storage
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border-none border border-emerald-500 sm:border sm:border-emerald-500 md:border md:border-emerald-500 lg:border lg:border-emerald-500 bg-gray-100">
                                            <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic hidden sm:flex md:flex lg:flex ">
                                                <p className="compare_rating">
                                                    <Rating name="read-only" value={clientreview} readOnly />
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                    {checked2 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim slot</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <span className="block">Dual sim, GSM+GSM</span>
                                                    <span className="block">Dual VOLTE</span>
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked2 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim size</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked2 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">network</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <span className="block">4G available(supports indian bands)</span>
                                                    <span className="block">3G available</span>
                                                    <span className="block">2G available</span>
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked2 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fingerprint sensor</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    available
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked2 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fast charging</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    available
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                                <Switch onChange={handleChange3} checked={checked3} className="react-switch" offColor="#f37021" onColor="#008800" offHandleColor="#e6e6fa" onHandleColor="#e6e6fa" />
                                                camera
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border-none border border-emerald-500 sm:border sm:border-emerald-500 md:border md:border-emerald-500 lg:border lg:border-emerald-500 bg-gray-100">
                                            <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic hidden sm:flex md:flex lg:flex ">
                                                <p className="compare_rating">
                                                    <Rating name="read-only" value={clientreview} readOnly />
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                    {checked3 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">dimension</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <span className="block">height:155mm</span>
                                                    <span className="block">width:77.8mm</span>
                                                    <span className="block">thickness:8.1mm</span>
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked3 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">weight</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  whitespace-pre-line">189 g</div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked3 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">ruggedness</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    available
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked3 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">waterproof</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                                <Switch onChange={handleChange4} checked={checked4} className="react-switch" offColor="#f37021" onColor="#008800" offHandleColor="#e6e6fa" onHandleColor="#e6e6fa" />
                                                battery
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border-none border border-emerald-500 sm:border sm:border-emerald-500 md:border md:border-emerald-500 lg:border lg:border-emerald-500 bg-gray-100">
                                            <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic hidden sm:flex md:flex lg:flex ">
                                                <p className="compare_rating">
                                                    <Rating name="read-only" value={clientreview} readOnly />
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                    {checked4 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim slot</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <span className="block">Dual sim, GSM+GSM</span>
                                                    <span className="block">Dual VOLTE</span>
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked4 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim size</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked4 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">network</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <span className="block">4G available(supports indian bands)</span>
                                                    <span className="block">3G available</span>
                                                    <span className="block">2G available</span>
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked4 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fingerprint sensor</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    available
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked4 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fast charging</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    available
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                                <Switch onChange={handleChange5} checked={checked5} className="react-switch" offColor="#f37021" onColor="#008800" offHandleColor="#e6e6fa" onHandleColor="#e6e6fa" />
                                                network & connectivity
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border-none border border-emerald-500 sm:border sm:border-emerald-500 md:border md:border-emerald-500 lg:border lg:border-emerald-500 bg-gray-100">
                                            <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic hidden sm:flex md:flex lg:flex ">
                                                <p className="compare_rating">
                                                    <Rating name="read-only" value={clientreview} readOnly />
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                    {checked5 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">dimension</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <span className="block">height:155mm</span>
                                                    <span className="block">width:77.8mm</span>
                                                    <span className="block">thickness:8.1mm</span>
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked5 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">weight</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  whitespace-pre-line">189 g</div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked5 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">ruggedness</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    available
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                    {checked5 ? (
                                        <tr>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                                <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">waterproof</div>
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                                <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                    <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                    water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                                </div>
                                            </td>
                                        </tr>
                                    ) : null}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

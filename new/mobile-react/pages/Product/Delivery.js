import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import AcUnitIcon from "@material-ui/icons/AcUnit";
import ArrowRightTwoToneIcon from '@material-ui/icons/ArrowRightTwoTone';
import SearchIcon from '@material-ui/icons/Search';
import HomeIcon from '@material-ui/icons/Home';
import Grid from '@material-ui/core/Grid';
import EditIcon from '@material-ui/icons/Edit';
const useStyles = makeStyles({
  list: {
    width: 450,
  },
  fullList: {
    width: 'auto',
  },
});

export default function TemporaryDrawer() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    
    right: false,
    
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
    >
      
      <div >
                <button className="float-left absolute z-10 bg-transparent text-2xl font-semibo0ld leading-none" onClick={toggleDrawer(anchor, false)}>
                    <span>×</span>
                </button>
            </div>

            <List className="delivery_background">
               <div className="mt-3">
               <div>
               <div class="pt-3 pb-3  flex space-x-4">
    <div class=" rounded-md font-semibold flex items-center justify-center py-1 px-1">
      <img src="deliveryperson.png" className="delivery_person"/>
    </div>
   
    <div class="rounded-md font-semibold items-center justify-center py-1 px-1">
       <p className="capitalize text-black font-bold text-base">delivery optins</p>
       <p className="capitalize text-black text-base delivery_address">select delivery preference</p>
    </div>
  </div>
               </div>
               <div className="bg-white">
               <p className="capitalize text-base font-bold text-black  w-max">delivery to:</p>
               <div class="mt-3 mb-3 flex space-x-4">
    <div class="flex-auto rounded-md font-semibold flex items-center justify-center py-1 px-1">
      <p className="flex text-black p-1 capitalize delivery_icondata"><HomeIcon className="delivery_iconcolor"/><span className="ml-1">home</span></p>
    </div>
    <div class="flex-auto rounded-md font-semibold flex items-center justify-center py-1 px-1">
      <p className="deliver_address leading-5 text-sm">Door no 143, 12th Floor, Phoenix Apartments, MG Road, Kurla West, Mumbai, Maharastra - 200027</p>
    </div>
    <div class="flex-auto rounded-md font-semibold flex items-center justify-center py-1 px-1">
       <p className="capitalize delivery_charge text-base"><EditIcon className="delivery_charge"/> edit</p>
    </div>
  </div>
               </div>
               <p className="capitalize text-base font-bold text-black delivery_border w-max">select deliver option:</p>
          <div class="exchange_borderbottom p-3 flex space-x-4">
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
      <label class="inline-flex items-center ">
                <input type="checkbox" class="form-checkbox h-5 w-5 text-gray-600"  /></label>
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
      
                <span class="text-gray-700 capitalize text-sm ">standard delivery rs <del>120 </del><span className="text-green-500 uppercase">free</span> <span className="capitalize"> expected delivery in 3 to 5 days</span></span>
           
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
       <p className="uppercase mt-1  text-sm delivery_color">t & c </p>
    </div>
  </div>
   <div class="exchange_borderbottom p-3 flex space-x-4">
    <div class="flex-auto rounded-md flex items-center justify-center py-1 px-1">
      <label class="inline-flex items-center ">
                <input type="checkbox" class="form-checkbox h-5 w-5 text-gray-600"  /></label>
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
      
               <span class="text-gray-700 capitalize text-sm "><span className="delivery_color">pickup @ store </span><span className="text-green-500 uppercase">free</span> - <span>nearest store is 5 mins away</span> view  stores near you </span>
            
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
       <p className="uppercase mt-1  text-sm delivery_color">t & c </p>
    </div>
  </div>
  <div class="exchange_borderbottom p-3 flex space-x-4">
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
      <label class="inline-flex items-center ">
                <input type="checkbox" class="form-checkbox h-5 w-5 text-gray-600"  /></label>
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
      
              <span class="text-gray-700 capitalize text-sm "><span className="delivery_hour">2 Hr Delivery</span> <span className="delivery_price">rs 120 </span><span className="delivery_charge">Delivery charges applicable </span>Orders before 5pm today</span>
            
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
       <p className="uppercase mt-1  text-sm delivery_color">t & c </p>
    </div>
  </div>   
  <div class="exchange_borderbottom p-3 flex space-x-4">
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
      <label class="inline-flex items-center ">
                <input type="checkbox" class="form-checkbox h-5 w-5 text-gray-600"  /></label>
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center py-1 px-1">
      
              <span class="text-gray-700 capitalize text-sm "><span className="delivery_sameday" >Same Day Delivery</span><span className="delivery_price"> rs50 </span><span className="delivery_charge">Delivery charges applicable </span>Orders between 12am to 10am only</span> 
           
    </div>
    <div class="flex-auto rounded-md flex items-center justify-center py-1 px-1">
       <p className="uppercase mt-1  text-sm delivery_color">t & c </p>
    </div>
  </div>   
     

               </div>
                
            </List>

    </div>
  );

  return (
    <div>
      {['bottom'].map((anchor) => (
        <React.Fragment key={anchor}>
         
         
                                       
                                            <span className="capitalize ml-2 cursor-pointer" onClick={toggleDrawer(anchor, true)}>delivery by tommorrow</span>
                                            
                                          
          <Drawer anchor={anchor} open={state[anchor]} >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}

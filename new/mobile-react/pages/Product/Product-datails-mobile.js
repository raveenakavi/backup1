import { React, useState } from "react";
import clsx from "clsx";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import Container from "@material-ui/core/Container";
import StarIcon from "@material-ui/icons/Star";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Checkbox from "@material-ui/core/Checkbox";
import Divider from "@material-ui/core/Divider";
import AddIcon from "@material-ui/icons/Add";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import AcUnitIcon from "@material-ui/icons/AcUnit";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ImageGallaryComponent from './image-gallery.component';
// import Setdelivery from "./Setdelivery";
// import Exchange from "./Exchange";
// import Delivery from "./Delivery";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
/* offer list style */
const useStyles = makeStyles({
    list: {
        width: 450,
    },
    emi: {
        width: 450,
    },
    fullList: {
        width: "auto",
    },
});
const Tabs = ({ color }) => {
    const [openTab, setOpenTab] = useState(1);
    const [emiScheme, setEmiScheme] = useState(false);
    const [axisScheme, setAxisScheme] = useState(false);
    return (
        <>
            <div className="flex flex-wrap">
                <div className="w-full">
               <div class="pt-6 pb-3  flex space-x-4  sptc  offer_border">
    <div class="flex rounded-md font-semibold items-center justify-center py-1 px-1">
       <p className="capitalize text-black font-bold text-base">Pay Now</p>
       <p className="absolute right-2 capitalize text-black text-base delivery_address">1 item : Rs. 1,19,249</p>
    </div>
  </div>



  <div class="p-1 flex space-x-4">
    <div class="flex-auto rounded-md  flex items-center justify-center px-1">
      <label class="inline-flex items-center ">
                <input type="checkbox" class="form-checkbox h-5 w-5 text-gray-600"  /></label>
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center px-1">
      
                <span class="text-gray-700 capitalize text-sm w-48 "><img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img></span>
           
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center px-8">
       <p className="uppercase mt-1  text-sm delivery_color">47****************345356</p>
    </div>
    
  </div>
  <div class="mt-1 mb-3 flex space-x-8">
    <div class="flex-auto rounded-md font-semibold flex items-center justify-center px-12">
      <p className="deliver_address leading-5 text-sm underline">enter CVV &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;?</p>
    </div>
    <div class="flex-auto rounded-md font-semibold flex px-1">
       <p className="capitalize delivery_charge text-base"><Button variant="contained" className="btn-orange w-full">Pay Now <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" /></Button></p>
    </div>
  </div>


  <div class="p-1 flex space-x-4">
    <div class="flex-auto rounded-md  flex items-center justify-center px-1">
      <label class="inline-flex items-center ">
                <input type="checkbox" class="form-checkbox h-5 w-5 text-gray-600"  /></label>
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center px-1">
      
                <span class="text-gray-700 capitalize text-sm w-48 "><img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img></span>
           
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center px-8">
       <p className="uppercase mt-1  text-sm delivery_color">47****************345356</p>
    </div>
    
  </div>


  <div class="p-1 flex space-x-4">
    <div class="flex-auto rounded-md  flex items-center justify-center px-1">
      <label class="inline-flex items-center ">
                <input type="checkbox" class="form-checkbox h-5 w-5 text-gray-600"  /></label>
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center px-1">
      
                <span class="text-gray-700 capitalize text-sm w-48 "><img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img></span>
           
    </div>
    <div class="flex-auto rounded-md  flex items-center justify-center px-8">
       <p className="uppercase mt-1  text-sm delivery_color">47****************345356</p>
    </div>
    
  </div>

                </div>
            </div>
        </>
    );
};
/* offer list style */
export default function Productdatails() {

    const images1 = [
        {
            original: "/asset/bestseller-2.png",
            thumbnail: "/asset/bestseller-2.png",
        },
        {
            original: "/asset/bestseller-3.png",
            thumbnail: "/asset/bestseller-3.png",
        },
        {
            original: "/asset/bestseller-4.png",
            thumbnail: "/asset/bestseller-4.png",
        },

    ];
    /* EMI list page */
    const [emilist, setEmiList] = useState({
        right: false,
    });

    const toggleEmi = (anchor, open) => (event) => {
        if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
            return;
        }

        setEmiList({ ...state, [anchor]: open });
    };
    const emi = (anchor) => (
        <div
            className={clsx(classes.emi, {
                [classes.fullList]: anchor === "top" || anchor === "bottom",
            })}
            role="presentation"
        >
            <div>
                <button className="float-left absolute z-10 bg-transparent text-2xl font-semibo0ld leading-none" onClick={toggleEmi(anchor, false)}>
                    <span>×</span>
                </button>
            </div>

            <List>
                <Tabs color="pink" />
            </List>
        </div>
    );
    /* EMI list page */
    /* offer list page */
    const classes = useStyles();
    const [state, setState] = useState({
        right: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === "top" || anchor === "bottom",
            })}
            role="presentation"
            
            
        >
            <div className="">
                <button className="float-left absolute z-10 bg-transparent text-2xl font-semibo0ld leading-none" onClick={toggleDrawer(anchor, false)}>
                    <span>×</span>
                </button>
            </div>

            <List>
                <div className="storage-wrp mt-3">
                    <h6 className="text-base  font-bold pb-4 offer_border text-center">Available Offers :</h6>
                    <div className="offerlist mt-3">
                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp  p-2  offerdetails_border flex md:items-center">
                                <img src="../asset/pec.png"></img>
                                <h5 class="ml-3">
                                    “Buy Now, Pay Later” : Min purchase of Rs.2000/- with HDFC-Flexipay (CCAvenue only) <span class="ml-2 sptc font-semibold">T &amp; C</span>
                                </h5>
                            </div>
                        </div>
                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp  p-2  offerdetails_border flex md:items-center">
                                <img src="../asset/pec.png"></img>
                                <h5 class="ml-3">
                                    Get Upto 5% Cashback on BOB Bank Credit Cards(through CC Avenue only) <span class="ml-2 sptc font-semibold">T &amp; C</span>
                                </h5>
                            </div>
                        </div>
                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp  p-2  offerdetails_border flex md:items-center">
                                <img src="../asset/pec.png"></img>
                                <h5 class="ml-3">
                                    Get Upto 5% Cashback on BOB Bank Credit Cards(through CC Avenue only) <span class="ml-2 sptc font-semibold">T &amp; C</span>
                                </h5>
                            </div>
                        </div>
                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp  p-2  offerdetails_border flex md:items-center">
                                <img src="../asset/pec.png"></img>
                                <h5 class="ml-3">
                                    Bank offer Flat 30% discount on minimum order value Rs 750 <span class="ml-2 sptc font-semibold">T &amp; C</span>
                                </h5>
                            </div>
                        </div>

                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp flex md:items-center">
                                <h5 className="flex pb-3 pt-2 offerdetails_border">
                                    <AcUnitIcon className="ml-2 offer_color" />
                                    <p className="ml-4">
                                        Bank offer Flat 30% discount on minimum order value Rs 750<span class="ml-2 sptc font-semibold">T &amp; C</span>
                                    </p>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </List>
        </div>
    );
    /* offer list page */
    return (
        <div className="mobile-layout bg-white w-full md:w-3/5 ml-auto mr-auto shadow">
            {/* <Container maxWidth="lg">
                <div className="prleft">
                    <p className="text-xs text-gray-400">
                        Home <ArrowForwardIosIcon className="arr-icon" /> Mobile & Accessories <ArrowForwardIosIcon className="arr-icon" /> Mobiles <ArrowForwardIosIcon className="arr-icon" /> Apple Mobiles
                    </p>
                </div>
            </Container> */}

            <div className="product-main-wrp pl-2 pr-2 md:flex gap-2 ">
                
                <div className="prod-detail-main">
                    <div className="md:flex gap-5">
                        <div className="product-price">
                            <div className="justify-between items-center">
                            <div className="flex">
                            <div className="content-product items-center justify-center mt-5">
                                <img src="../asset/set2.png" className="w-16" />
                            </div>
                                <div className="prleft pl-8 py-5 md:py-8">
                                    <h6 className="font-semibold">Apple iPhone 11 Pro (Green, 128 GB) </h6>
                                    <span className="text-xs text-gray-400 flex mt-2">
                                        <p className="text-blue-400 mr-1">From Apple store </p> (PRODUCT CODE:31489)
                                    </span>
                                </div>
                                </div>
                                <Divider />
                                <div className="">
                    <div className="pro-sticky">
                        <div className="p-5">
                        {/* <div className="title-head emi-store md:flex items-center mt-2 mb-2">
                                <p class="text-sm flex" ><span className="delivery_stock"> <CheckCircleIcon /><span class="MuiChip-label">In Stock</span></span></p>
                            </div> */}
      <ImageGallaryComponent />
                                  {/* <div className="content-product items-center justify-center mt-5">
                                <img src="../asset/storage1.jpg" className="h-96 ml-auto mr-auto"/>
                            </div> */}
                        </div>
                    </div>
                </div>
                            </div>




                        {/* <div className="storage-wrp">
                                <h6 className="text-base  font-bold pb-1">Color :</h6>

                                <div class="thumb-product-product mt-2 flex mb-3 ">
                                    <div class="imgwrap items-center">
                                        <div class="tumb-color border-solid border-gray-200 border border-r-0 p-2 items-center">
                                            <img src="../asset/storage1.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Glacier Green</p>
                                    </div>
                                    <div class="imgwrap">
                                        <div class="tumb-color border-solid border-gray-200 border border-r-0 p-2 items-center">
                                            <img src="../asset/storage2.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Magic Glow</p>
                                    </div>
                                    <div class="imgwrap">
                                        <div class="tumb-color border-solid border-gray-200 border border-r-0 p-2 items-center">
                                            <img src="../asset/storage3.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Sparky Blue</p>
                                    </div>
                                    <div class="imgwrap">
                                        <div class="tumb-color border-solid border-gray-200 border p-2 border-r-0 items-center">
                                            <img src="../asset/storage4.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Phonix Blue</p>
                                    </div>
                                    <div class="imgwrap">
                                        <div class="tumb-color border-solid border-gray-200 border p-2 items-center">
                                            <img src="../asset/storage5.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Classical Yellow</p>
                                    </div>
                                    
                                </div>
                            </div> */}

                            <div className="storage-wrp">
                                <h6 className="text-base  font-bold pb-1">Storage :</h6>
                                <div className="mt-3 flex mb-3 text-center">
                                    <div className="items-center  product_storage">
                                        <p class="text-xs font-bold">8GB + 128 GB</p>
                                        
                                    </div>
                                    <div className="items-center  product_storages">
                                        <p class="text-xs font-bold">8GB + 128 GB</p>
                                        
                                    </div>
                                    <div className="items-center  product_storages">
                                        <p class="text-xs font-bold">8GB + 128 GB</p>
                                        
                                    </div>
                                </div>
                            </div>

                            <div className="flex product-addcart p-2 shadow">


<div className="btn-wrp w-full mt-3">
{["bottom"].map((anchor) => (
                                     <div key={anchor}>
    <Button variant="contained" className="btn-orange w-full" onClick={toggleEmi(anchor, true)}>
        Buy Now
    </Button>
    <Drawer anchor={anchor} open={emilist[anchor]} >
                                                {emi(anchor)  }
                                            </Drawer>
                                        </div>
                                    
                                ))}
</div>

<div className="btn-wrp w-full mt-3">
    <Button variant="contained" className="btn-gray w-full">
        Add To Cart
    </Button>
</div>
</div>
                           
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    );
}

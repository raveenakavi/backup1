import React, { useState, useRef } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import AppleIcon from '@material-ui/icons/Apple';

const TOTAL_SLIDES = 5;

/**
 * It will return the JSX and register the callbacks for next and previous slide.
 * @param prevCallback {function} - Go back to the previous slide
 * @param nextCallback {function} - Go to the next slide
 * @param state {object} - Holds the state of your slider indexes
 * @param totalSlides {number} - Holds the total number of slides
 * @return {*} - Returns the JSX
 */
const renderArrows = (
  prevCallback,
  nextCallback,
  { currentIndex, slidesToScroll },
  totalSlides
) => {
  const cycleDots =
    currentIndex === 0 ? 1 : Math.ceil(totalSlides / slidesToScroll);
  return (
    <div className="mx-2 ...">
      <button disabled={currentIndex === 0} onClick={prevCallback} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button disabled={currentIndex > cycleDots} onClick={nextCallback} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
    </div>
  );
};

const TrendingCarousel = () => {
  const [state, setState] = useState({ currentIndex: 0, slidesToScroll: 0 });
  const sliderRef = useRef();
  const next = () => {
    sliderRef.current.slickNext();
  };

  const previous = () => {
    sliderRef.current.slickPrev();
  };

  const settings_3 = {
    slidesToShow: 3,
    dots: false,
    draggable: false,
    slidesToScroll: 3,
    arrows: false,
    speed: 1300,
    autoplay: false,
    centerMode: false,
    infinite: false,
    afterChange: indexOfCurrentSlide => {
      setState({
        currentIndex: indexOfCurrentSlide,
        slidesToScroll: 3
      });
    },
    responsive: [
      {
        breakpoint: 992,
        settings_3: {
          slidesToShow: 2,
          slidesToScroll: 2,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 2
            });
          }
        }
      },
      {
        breakpoint: 576,
        settings_3: {
          slidesToShow: 1,
          slidesToScroll: 1,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 1
            });
          }
        }
      }
    ]
  };

  return (
    <div className="app similar-products trending-carousel bg-white">
      {/*  Slider */}

      <h2 className="flex py-4 px-3 text-black text-md md:text-2xl font-bold"><span className="ml-0">Top Searched and trending</span>
                    
                    <p className="float-right w-4/12 right-0 md:right-2 absolute flex"><span className="text-xs pt-3 text-blue-600 invisible md:visible"> See all</span>  <span className="right-0 md:right-2 absolute">{renderArrows(previous, next, state, TOTAL_SLIDES - 5)}</span></p>
                    
                    </h2>
      <Slider {...settings_3} ref={slider => (sliderRef.current = slider)}>
        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="w-full trend-product px-6">
        <iframe className="rounded-xl" width="360" height="215" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
      <div className="w-full flex pt-3">
          <div className="w-2/6">
      <img src={'/asset/trending-1.jpeg'} className="w-24 h-24 rounded-xl pr-2" />
      </div>
      <div className="w-5/6">
      <h3 className="text-black text-sm pt-2">Best 5000 man battery SmartPhones</h3>
      <p className="font-normal text-gray-400">Top 10 Smart Phones</p>
      <p className="text-black text-sm pt-4"><span className="float-left pr-3 text-red-500 text-sm pr-5"> Above 4.2 <Grade /> <span className="float-right text-gray-600">Starting price: Rs.16,789</span></span></p>
      </div>
      </div>
  </div>

  <div className="w-full trend-product px-6">
        <iframe className="rounded-xl" width="360" height="215" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
      <div className="w-full flex pt-3">
          <div className="w-2/6">
      <img src={'/asset/trending-1.jpeg'} className="w-24 h-24 rounded-xl pr-2" />
      </div>
      <div className="w-5/6">
      <h3 className="text-black text-sm pt-2">Best 5000 man battery SmartPhones</h3>
      <p className="font-normal text-gray-400">Top 10 Smart Phones</p>
      <p className="text-black text-sm pt-4"><span className="float-left pr-3 text-red-500 text-sm pr-5"> Above 4.2 <Grade /> <span className="float-right text-gray-600">Starting price: Rs.16,789</span></span></p>
      </div>
      </div>
  </div>

  <div className="w-full trend-product px-6">
        <iframe className="rounded-xl" width="360" height="215" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
      <div className="w-full flex pt-3">
          <div className="w-2/6">
      <img src={'/asset/trending-1.jpeg'} className="w-24 h-24 rounded-xl pr-2" />
      </div>
      <div className="w-5/6">
      <h3 className="text-black text-sm pt-2">Best 5000 man battery SmartPhones</h3>
      <p className="font-normal text-gray-400">Top 10 Smart Phones</p>
      <p className="text-black text-sm pt-4"><span className="float-left pr-3 text-red-500 text-sm pr-5"> Above 4.2 <Grade /> <span className="float-right text-gray-600">Starting price: Rs.16,789</span></span></p>
      </div>
      </div>
  </div>

  <div className="w-full trend-product px-6">
        <iframe className="rounded-xl" width="360" height="215" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
      <div className="w-full flex pt-3">
          <div className="w-2/6">
      <img src={'/asset/trending-1.jpeg'} className="w-24 h-24 rounded-xl pr-2" />
      </div>
      <div className="w-5/6">
      <h3 className="text-black text-sm pt-2">Best 5000 man battery SmartPhones</h3>
      <p className="font-normal text-gray-400">Top 10 Smart Phones</p>
      <p className="text-black text-sm pt-4"><span className="float-left pr-3 text-red-500 text-sm pr-5"> Above 4.2 <Grade /> <span className="float-right text-gray-600">Starting price: Rs.16,789</span></span></p>
      </div>
      </div>
  </div>
             
             
        {/* })} */}
      </Slider>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
    </div>
  );
};

export default TrendingCarousel;

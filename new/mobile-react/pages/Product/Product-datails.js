import { React, useState } from "react";
import clsx from "clsx";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import Container from "@material-ui/core/Container";
import StarIcon from "@material-ui/icons/Star";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Checkbox from "@material-ui/core/Checkbox";
import Divider from "@material-ui/core/Divider";
import AddIcon from "@material-ui/icons/Add";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import AcUnitIcon from "@material-ui/icons/AcUnit";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import Setdelivery from "./Setdelivery";
import Exchange from "./Exchange";
import Delivery from "./Delivery";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
/* offer list style */
const useStyles = makeStyles({
    list: {
        width: 450,
    },
    emi: {
        width: 450,
    },
    fullList: {
        width: "auto",
    },
});
const Tabs = ({ color }) => {
    const [openTab, setOpenTab] = useState(1);
    const [emiScheme, setEmiScheme] = useState(false);
    const [axisScheme, setAxisScheme] = useState(false);
    return (
        <>
            <div className="flex flex-wrap">
                <div className="w-full">
                    <h6 className="text-base  font-bold text-center pb-2 sptc  offer_border"> EMI Option:</h6>
                    <ul className="flex mb-0 list-none flex-wrap pt-2 pb-2 flex-row" role="tablist">
                        <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                            <a
                                className={"text-xs font-bold uppercase shadow-lg px-2 py-2 rounded block " + (openTab === 1 ? "text-white bg-" + color + "-600" : "text-" + color + "-600 bg-white")}
                                onClick={(e) => {
                                    e.preventDefault();
                                    setOpenTab(1);
                                }}
                                data-toggle="tab"
                                href="#link1"
                                role="tablist"
                            >
                                Debit card EMI
                            </a>
                        </li>
                        <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                            <a
                                className={"text-xs font-bold uppercase shadow-lg px-2 py-2 rounded block " + (openTab === 2 ? "text-white bg-" + color + "-600" : "text-" + color + "-600 bg-white")}
                                onClick={(e) => {
                                    e.preventDefault();
                                    setOpenTab(2);
                                }}
                                data-toggle="tab"
                                href="#link2"
                                role="tablist"
                            >
                                No cost emi
                            </a>
                        </li>
                        <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                            <a
                                className={"text-xs font-bold uppercase shadow-lg px-2 py-2 rounded block " + (openTab === 3 ? "text-white bg-" + color + "-600" : "text-" + color + "-600 bg-white")}
                                onClick={(e) => {
                                    e.preventDefault();
                                    setOpenTab(3);
                                }}
                                data-toggle="tab"
                                href="#link3"
                                role="tablist"
                            >
                                emi
                            </a>
                        </li>
                        <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                            <a
                                className={"text-xs font-bold uppercase shadow-lg px-2 py-2 rounded block " + (openTab === 4 ? "text-white bg-" + color + "-600" : "text-" + color + "-600 bg-white")}
                                onClick={(e) => {
                                    e.preventDefault();
                                    setOpenTab(4);
                                }}
                                data-toggle="tab"
                                href="#link4"
                                role="tablist"
                            >
                                finance
                            </a>
                        </li>
                    </ul>
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
                        <div className="px-3 py-3 flex-auto">
                            <div className="tab-content tab-space">
                                <div className={openTab === 1 ? "block" : "hidden"} id="link1">
                                    <Grid container className={` ${emiScheme ? "show" : "hide"}`} onClick={() => setEmiScheme(!emiScheme)}>
                                        <Grid item xs={4}>
                                            <img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HDFC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    {emiScheme && (
                                        <div>
                                            <table class="table-auto text-sm w-full border-collapse border border-gray-100">
                                                <thead>
                                                    <tr>
                                                        <th className="border-collapse border border-gray-200 p-2">EMI plan</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Interest (Pa)</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Total Cost</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="emi_header">
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.21,833 * 3m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.1,603(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.65,504</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.11,833 * 6m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.2,825(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.66,724</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.7,833 * 9m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.4,060(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.67,897</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.5,768 * 12m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.5,303(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.69,290</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    )}
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`} onClick={() => setAxisScheme(!axisScheme)}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Axis_Bank_logo.svg/640px-Axis_Bank_logo.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">Axis Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    {axisScheme && (
                                        <div>
                                            <table class="table-auto text-sm w-full border-collapse border border-gray-100">
                                                <thead>
                                                    <tr>
                                                        <th className="border-collapse border border-gray-200 p-2">EMI plan</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Interest (Pa)</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Total Cost</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="emi_header">
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.21,833 * 3m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.1,603(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.65,504</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.11,833 * 6m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.2,825(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.66,724</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.7,833 * 9m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.4,060(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.67,897</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.5,768 * 12m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.5,303(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.69,290</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    )}
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://logo-logos.com/wp-content/uploads/2016/10/ICICI_bank_logo.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">ICICI Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/HSBC_logo_%282018%29.svg/1280px-HSBC_logo_%282018%29.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HSBC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HDFC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Axis_Bank_logo.svg/640px-Axis_Bank_logo.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">Axis Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                   <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://logo-logos.com/wp-content/uploads/2016/10/ICICI_bank_logo.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">ICICI Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/HSBC_logo_%282018%29.svg/1280px-HSBC_logo_%282018%29.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HSBC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    
                                </div>
                                <div className={openTab === 2 ? "block" : "hidden"} id="link2">
                                     <Grid container className={` ${emiScheme ? "show" : "hide"}`} onClick={() => setEmiScheme(!emiScheme)}>
                                        <Grid item xs={4}>
                                            <img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HDFC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    {emiScheme && (
                                        <div>
                                            <table class="table-auto text-sm w-full border-collapse border border-gray-100">
                                                <thead>
                                                    <tr>
                                                        <th className="border-collapse border border-gray-200 p-2">EMI plan</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Interest (Pa)</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Total Cost</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="emi_header">
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.21,833 * 3m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.1,603(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.65,504</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.11,833 * 6m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.2,825(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.66,724</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.7,833 * 9m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.4,060(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.67,897</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.5,768 * 12m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.5,303(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.69,290</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    )}
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`} onClick={() => setAxisScheme(!axisScheme)}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Axis_Bank_logo.svg/640px-Axis_Bank_logo.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">Axis Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    {axisScheme && (
                                        <div>
                                            <table class="table-auto text-sm w-full border-collapse border border-gray-100">
                                                <thead>
                                                    <tr>
                                                        <th className="border-collapse border border-gray-200 p-2">EMI plan</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Interest (Pa)</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Total Cost</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="emi_header">
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.21,833 * 3m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.1,603(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.65,504</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.11,833 * 6m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.2,825(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.66,724</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.7,833 * 9m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.4,060(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.67,897</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.5,768 * 12m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.5,303(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.69,290</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    )}
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://logo-logos.com/wp-content/uploads/2016/10/ICICI_bank_logo.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">ICICI Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/HSBC_logo_%282018%29.svg/1280px-HSBC_logo_%282018%29.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HSBC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HDFC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Axis_Bank_logo.svg/640px-Axis_Bank_logo.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">Axis Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                   <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://logo-logos.com/wp-content/uploads/2016/10/ICICI_bank_logo.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">ICICI Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/HSBC_logo_%282018%29.svg/1280px-HSBC_logo_%282018%29.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HSBC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                </div>
                                <div className={openTab === 3 ? "block" : "hidden"} id="link3">
                                    <Grid container className={` ${emiScheme ? "show" : "hide"}`} onClick={() => setEmiScheme(!emiScheme)}>
                                        <Grid item xs={4}>
                                            <img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HDFC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    {emiScheme && (
                                        <div>
                                            <table class="table-auto text-sm w-full border-collapse border border-gray-100">
                                                <thead>
                                                    <tr>
                                                        <th className="border-collapse border border-gray-200 p-2">EMI plan</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Interest (Pa)</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Total Cost</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="emi_header">
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.21,833 * 3m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.1,603(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.65,504</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.11,833 * 6m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.2,825(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.66,724</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.7,833 * 9m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.4,060(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.67,897</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.5,768 * 12m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.5,303(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.69,290</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    )}
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`} onClick={() => setAxisScheme(!axisScheme)}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Axis_Bank_logo.svg/640px-Axis_Bank_logo.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">Axis Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    {axisScheme && (
                                        <div>
                                            <table class="table-auto text-sm w-full border-collapse border border-gray-100">
                                                <thead>
                                                    <tr>
                                                        <th className="border-collapse border border-gray-200 p-2">EMI plan</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Interest (Pa)</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Total Cost</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="emi_header">
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.21,833 * 3m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.1,603(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.65,504</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.11,833 * 6m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.2,825(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.66,724</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.7,833 * 9m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.4,060(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.67,897</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.5,768 * 12m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.5,303(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.69,290</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    )}
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://logo-logos.com/wp-content/uploads/2016/10/ICICI_bank_logo.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">ICICI Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/HSBC_logo_%282018%29.svg/1280px-HSBC_logo_%282018%29.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HSBC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HDFC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Axis_Bank_logo.svg/640px-Axis_Bank_logo.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">Axis Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                   <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://logo-logos.com/wp-content/uploads/2016/10/ICICI_bank_logo.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">ICICI Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/HSBC_logo_%282018%29.svg/1280px-HSBC_logo_%282018%29.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HSBC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                </div>
                                <div className={openTab === 4 ? "block" : "hidden"} id="link4">
                                    <Grid container className={` ${emiScheme ? "show" : "hide"}`} onClick={() => setEmiScheme(!emiScheme)}>
                                        <Grid item xs={4}>
                                            <img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HDFC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    {emiScheme && (
                                        <div>
                                            <table class="table-auto text-sm w-full border-collapse border border-gray-100">
                                                <thead>
                                                    <tr>
                                                        <th className="border-collapse border border-gray-200 p-2">EMI plan</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Interest (Pa)</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Total Cost</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="emi_header">
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.21,833 * 3m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.1,603(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.65,504</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.11,833 * 6m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.2,825(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.66,724</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.7,833 * 9m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.4,060(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.67,897</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.5,768 * 12m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.5,303(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.69,290</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    )}
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`} onClick={() => setAxisScheme(!axisScheme)}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Axis_Bank_logo.svg/640px-Axis_Bank_logo.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">Axis Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    {axisScheme && (
                                        <div>
                                            <table class="table-auto text-sm w-full border-collapse border border-gray-100">
                                                <thead>
                                                    <tr>
                                                        <th className="border-collapse border border-gray-200 p-2">EMI plan</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Interest (Pa)</th>
                                                        <th className="border-collapse border border-gray-200 p-2">Total Cost</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="emi_header">
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.21,833 * 3m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.1,603(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.65,504</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.11,833 * 6m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.2,825(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.66,724</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.7,833 * 9m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.4,060(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.67,897</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="border-collapse border border-gray-200 text-left pl-3 p-1">
                                                            <label class="inline-flex items-center mt-3">
                                                                <input type="checkbox" class="form-checkbox h-3 w-5 text-gray-600" />
                                                                <span class="ml-2 text-gray-700">Rs.5,768 * 12m</span>
                                                            </label>
                                                        </td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.5,303(15%)</td>
                                                        <td className="border-collapse border border-gray-200 text-center p-1">Rs.69,290</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    )}
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://logo-logos.com/wp-content/uploads/2016/10/ICICI_bank_logo.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">ICICI Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/HSBC_logo_%282018%29.svg/1280px-HSBC_logo_%282018%29.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HSBC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                    <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://www.searchpng.com/wp-content/uploads/2019/01/Hdfc-Logo-PNG-715x186.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HDFC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Axis_Bank_logo.svg/640px-Axis_Bank_logo.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">Axis Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                   <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://logo-logos.com/wp-content/uploads/2016/10/ICICI_bank_logo.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">ICICI Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>

                                     <Grid container className={` ${axisScheme ? "show" : "hide"}`}>
                                        <Grid item xs={4}>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/HSBC_logo_%282018%29.svg/1280px-HSBC_logo_%282018%29.svg.png"></img>
                                        </Grid>
                                        <Grid item xs={7}>
                                            <p className="text-xs mt-2 font-bold text-black text-center">HSBC Bank Credit Cards</p>
                                        </Grid>

                                        <Grid item xs={1}>
                                            <p className="mt-2 text-sm">
                                                <ExpandMoreIcon className="emishowicon" />
                                                <ArrowForwardIosIcon className=" emi_showicon emi_onclickevent" />
                                            </p>
                                        </Grid>
                                    </Grid>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
/* offer list style */
export default function Productdatails() {
    /* EMI list page */
    const [emilist, setEmiList] = useState({
        right: false,
    });

    const toggleEmi = (anchor, open) => (event) => {
        if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
            return;
        }

        setEmiList({ ...state, [anchor]: open });
    };
    const emi = (anchor) => (
        <div
            className={clsx(classes.emi, {
                [classes.fullList]: anchor === "top" || anchor === "bottom",
            })}
            role="presentation"
        >
            <div>
                <button className="float-left absolute z-10 bg-transparent text-2xl font-semibo0ld leading-none" onClick={toggleEmi(anchor, false)}>
                    <span>×</span>
                </button>
            </div>

            <List>
                <Tabs color="pink" />
            </List>
        </div>
    );
    /* EMI list page */
    /* offer list page */
    const classes = useStyles();
    const [state, setState] = useState({
        right: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === "top" || anchor === "bottom",
            })}
            role="presentation"
            
            
        >
            <div className="">
                <button className="float-left absolute z-10 bg-transparent text-2xl font-semibo0ld leading-none" onClick={toggleDrawer(anchor, false)}>
                    <span>×</span>
                </button>
            </div>

            <List>
                <div className="storage-wrp mt-3">
                    <h6 className="text-base  font-bold pb-4 offer_border text-center">Available Offers :</h6>
                    <div className="offerlist mt-3">
                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp  p-2  offerdetails_border flex md:items-center">
                                <img src="../asset/pec.png"></img>
                                <h5 class="ml-3">
                                    “Buy Now, Pay Later” : Min purchase of Rs.2000/- with HDFC-Flexipay (CCAvenue only) <span class="ml-2 sptc font-semibold">T &amp; C</span>
                                </h5>
                            </div>
                        </div>
                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp  p-2  offerdetails_border flex md:items-center">
                                <img src="../asset/pec.png"></img>
                                <h5 class="ml-3">
                                    Get Upto 5% Cashback on BOB Bank Credit Cards(through CC Avenue only) <span class="ml-2 sptc font-semibold">T &amp; C</span>
                                </h5>
                            </div>
                        </div>
                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp  p-2  offerdetails_border flex md:items-center">
                                <img src="../asset/pec.png"></img>
                                <h5 class="ml-3">
                                    Get Upto 5% Cashback on BOB Bank Credit Cards(through CC Avenue only) <span class="ml-2 sptc font-semibold">T &amp; C</span>
                                </h5>
                            </div>
                        </div>
                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp  p-2  offerdetails_border flex md:items-center">
                                <img src="../asset/pec.png"></img>
                                <h5 class="ml-3">
                                    Bank offer Flat 30% discount on minimum order value Rs 750 <span class="ml-2 sptc font-semibold">T &amp; C</span>
                                </h5>
                            </div>
                        </div>

                        <div className="avamain md:flex">
                            <div class="avaoffer-wrp flex md:items-center">
                                <h5 className="flex pb-3 pt-2 offerdetails_border">
                                    <AcUnitIcon className="ml-2 offer_color" />
                                    <p className="ml-4">
                                        Bank offer Flat 30% discount on minimum order value Rs 750<span class="ml-2 sptc font-semibold">T &amp; C</span>
                                    </p>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </List>
        </div>
    );
    /* offer list page */
    return (
        <div className="bg-white">
            <Container maxWidth="lg">
                <div className="prleft">
                    <p className="text-xs text-gray-400">
                        Home <ArrowForwardIosIcon className="arr-icon" /> Mobile & Accessories <ArrowForwardIosIcon className="arr-icon" /> Mobiles <ArrowForwardIosIcon className="arr-icon" /> Apple Mobiles
                    </p>
                </div>
            </Container>

            <div className="product-main-wrp pl-2 pr-2 md:flex gap-2 ">
                <div className="product-img">
                    <div className="pro-sticky">
                        <div className="flex  p-5">
                            <div className="content-product items-center justify-center mt-5">
                                <img src="../asset/set2.png" />
                            </div>
                            <div className="hert-icon justify-end flex">
                                <FavoriteBorderIcon />
                            </div>
                        </div>

                        <div className="thumb-product border-solid border-gray-300  flex mt-5">
                            <div class="tumbline-prod border-gray-300 border-r  p-2 border">
                                <img src="../asset/realme1.jpeg" />
                            </div>
                            <div class="tumbline-prod border-gray-300 border-r  p-2 border">
                                <img src="../asset/realme1.jpeg" />
                            </div>
                            <div class="tumbline-prod border-gray-300 border-r  p-2 border">
                                <img src="../asset/realme1.jpeg" />
                            </div>
                            <div class="tumbline-prod border-gray-300 border-r  p-2 border">
                                <img src="../asset/realme1.jpeg" />
                            </div>
                            <div class="tumbline-prod border-gray-300 border-r  p-2 border">
                                <img src="../asset/realme1.jpeg" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="prod-detail-main">
                    <div className="md:flex gap-5">
                        <div className="product-price">
                            <div className="flex flex justify-between items-center">
                                <div className="prleft">
                                    <h6 className="font-semibold">Apple iPhone 11 Pro (Green, 128 GB) </h6>
                                    <span className="text-xs text-gray-400 flex mt-2">
                                        <p className="text-blue-400 mr-1">From Apple store </p> (PRODUCT CODE:31489)
                                    </span>
                                </div>
                                <div className="prright">
                                    <div className="point-rate text-center">
                                        <span className="text-sm previw">
                                            {" "}
                                            <StarIcon /> 4.7{" "}
                                        </span>
                                        <p className="text-sm">( 279 Reviews )</p>
                                    </div>
                                </div>
                            </div>

                            <div className="price-pro-details flex items-center mt-2">
                                <h4 className="font-semibold"> Rs. 1,19,249</h4>
                                <h6 className="ml-4">4%OFF</h6>
                                <span className="ml-4">In Stock</span>
                            </div>

                            <div className="save-product-price flex items-center">
                                <h4 className="line-through"> Rs. 1,19,249</h4>
                                <h6 className="ml-4">Save Rs.11,427</h6>
                            </div>

                            <div className="title-head emi-store md:flex items-center mt-2 mb-2">
                                <p class="text-sm">No Cost EMI Starts at 2,589.No Cost EMI available</p>

                                {["bottom"].map((anchor) => (
                                     <div key={anchor}>
                                     <a className="ml-3 flex items-center cursor-pointer font-bold text-sm cursor-pointer" onClick={toggleEmi(anchor, true)}>
                                       
                                            See EMI Option <ArrowForwardIosIcon />
                                            
                                            </a>
                                            <Drawer anchor={anchor} open={emilist[anchor]} >
                                                {emi(anchor)  }
                                            </Drawer>
                                        </div>
                                    
                                ))}
                            </div>

                            <div className="title-head emi-store md:flex items-center mt-2 mb-2">
                                <p class="text-sm flex" ><span className="delivery_stock"> <CheckCircleIcon /><span class="MuiChip-label">In Stock</span></span>Free <Delivery /></p>

                               
                                    
                                          
                                            
                                            
                                            
                              
                            </div>


                            

                            <div className="storage-wrp">
                                <h6 className="capitalize font-bold text-sm">Available Offers :</h6>
                                {["right"].map((anchor) => (
                                    <div key={anchor} className="mt-3 flex mb-3 text-center">
                                        <div className="items-center mr-2 p-1 offer_show rounded cursor-pointer" onClick={toggleDrawer(anchor, true)}>
                                            <div className="flex justify-between">
                                                <p className="flex-grow uppercase inline-flex items-center p-0 font-medium text-black text-xs sm:text-sm md:text-sm lg:text-xs">5% cashback</p>

                                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1">
                                                    <ArrowForwardIosIcon className="offer_detailswidth" />
                                                </p>
                                            </div>
                                            <div className="flex justify-between">
                                                <p className="flex-grow capitalize  offer_carddetails inline-flex items-center p-0 font-medium  text-xs sm:text-sm md:text-sm lg:text-xs">upto 1,500 on sbi card</p>

                                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1"></p>
                                            </div>
                                        </div>
                                        <div className="items-center mr-2 p-1 offer_show rounded" onClick={toggleDrawer(anchor, true)}>
                                            <div className="flex justify-between">
                                                <p className="flex-grow uppercase inline-flex items-center p-0 font-medium text-black text-xs sm:text-sm md:text-sm lg:text-xs">bank offer 10% </p>

                                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1">
                                                    <ArrowForwardIosIcon className="offer_detailswidth" />
                                                </p>
                                            </div>
                                            <div className="flex justify-between">
                                                <p className="flex-grow capitalize  offer_carddetails inline-flex items-center p-0 font-medium  text-xs sm:text-sm md:text-sm lg:text-xs">upto 13,500 off</p>

                                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1"></p>
                                            </div>
                                        </div>
                                        <div className="items-center mr-2 p-1 offer_show rounded" onClick={toggleDrawer(anchor, true)}>
                                            <div className="flex justify-between">
                                                <p className="flex-grow uppercase inline-flex items-center p-0 font-medium text-black text-xs sm:text-sm md:text-sm lg:text-xs">bank offer 10% </p>

                                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 offers_lastofferbefore">
                                                    <ArrowForwardIosIcon className="offer_detailswidth" />
                                                </p>
                                            </div>
                                            <div className="flex justify-between">
                                                <p className="flex-grow capitalize  offer_carddetails inline-flex items-center p-0 font-medium  text-xs sm:text-sm md:text-sm lg:text-xs">upto 13,500 off</p>

                                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 offers_lastofferafter"></p>
                                            </div>
                                        </div>
                                        <Drawer anchor={anchor} open={state[anchor]}>
                                            {list(anchor)}
                                        </Drawer>
                                    </div>
                                ))}
                            </div>
                                                          <div className="storage-wrp">
                                <h6 className="text-base  font-bold pb-1">Color :</h6>

                                <div class="thumb-product-product mt-2 flex mb-3 ">
                                    <div class="imgwrap items-center">
                                        <div class="tumb-color border-solid border-gray-200 border border-r-0 p-2 items-center">
                                            <img src="../asset/storage1.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Glacier Green</p>
                                    </div>
                                    <div class="imgwrap">
                                        <div class="tumb-color border-solid border-gray-200 border border-r-0 p-2 items-center">
                                            <img src="../asset/storage2.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Magic Glow</p>
                                    </div>
                                    <div class="imgwrap">
                                        <div class="tumb-color border-solid border-gray-200 border border-r-0 p-2 items-center">
                                            <img src="../asset/storage3.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Sparky Blue</p>
                                    </div>
                                    <div class="imgwrap">
                                        <div class="tumb-color border-solid border-gray-200 border p-2 border-r-0 items-center">
                                            <img src="../asset/storage4.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Phonix Blue</p>
                                    </div>
                                    <div class="imgwrap">
                                        <div class="tumb-color border-solid border-gray-200 border p-2 items-center">
                                            <img src="../asset/storage5.jpg" />
                                        </div>
                                        <p class="text-sm text-center leading-5 mt-1">Classical Yellow</p>
                                    </div>
                                    
                                </div>
                            </div>

                            <div className="storage-wrp">
                                <h6 className="text-base  font-bold pb-1">Storage :</h6>
                                <div className="mt-3 flex mb-3 text-center">
                                    <div className="items-center  product_storage">
                                        <p class="text-xs font-bold">8GB + 128 GB</p>
                                        
                                    </div>
                                    <div className="items-center  product_storages">
                                        <p class="text-xs font-bold">8GB + 128 GB</p>
                                        
                                    </div>
                                    <div className="items-center  product_storages">
                                        <p class="text-xs font-bold">8GB + 128 GB</p>
                                        
                                    </div>
                                </div>
                            </div>

                           
                        </div>
                        <div className="product-addcart p-2 shadow">
                            <div className="without-exch flex justify-between items-center bg-gray-100 pr-2">
                                <h6>
                                   
                                    <Checkbox /> Without Exchange
                                </h6>
                                <h6 className="text-green-700 font-semibold">Rs.1,28,249</h6>
                            </div>
                            <div className="with-exch flex justify-between items-center border pr-2 mt-3">
                                <div className="flex">
                                    <Checkbox className="ex-check" />
                                    <div className="leading-5 mt-1">
                                        <h6> With Exchange </h6>
                                        <span>upto 19,999</span>
                                    </div>
                                </div>
                                <div className="flex">
                                    <div className="leading-5 mt-1">
                                        <h6 className="font-normal">Rs.99,999</h6>
                                       <Exchange />
                                    </div>
                                </div>
                                
                                
                            </div>

                            <div className="buy-acc mt-5">
                                <div className="flex justify-between items-center w-full">
                                    <h6 className="underline">Buy Accessories</h6>
                                    <small>See All</small>
                                </div>

                                <div className="acc-exch flex justify-between items-center mt-3 w-full">
                                    <div className="flex w-full">
                                        <Checkbox className="ex-check" />
                                        <div className="flex w-full">
                                            <div>
                                                <img src="../asset/prz-img.png"></img>
                                            </div>

                                            <div className="w-full">
                                                <div className="flex justify-between items-center">
                                                    <h6 className="text-sm"> Apple AirPods </h6>
                                                    <small className="line-through">Rs.19,999</small>
                                                </div>
                                                <div className="flex justify-between items-center gesm">
                                                    <h6 className="text-sm"> Buy together for</h6>
                                                    <small>Rs.23,999</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="acc-exch flex justify-between items-center mt-3 w-full">
                                    <div className="flex w-full">
                                        <Checkbox className="ex-check" />
                                        <div className="flex w-full">
                                            <div>
                                                <img src="../asset/prz-img.png"></img>
                                            </div>

                                            <div className="w-full">
                                                <div className="flex justify-between items-center">
                                                    <h6 className="text-sm"> Transparent Black </h6>
                                                    <small className="line-through">Rs.19,999</small>
                                                </div>
                                                <div className="flex justify-between items-center gesm">
                                                    <h6 className="text-sm"> Buy together for</h6>
                                                    <small>Rs.23,999</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="mt-2">
                                <Divider />
                            </div>

                            <div className="wanttoproduct flex justify-between mt-5">
                                <h3>Want to Protect your Phone ?</h3>
                                <small>Know More</small>
                            </div>

                            <div className="flex items-center mt-5">
                                <AddIcon />
                                <p className="text-xs">Add Mobile Production for Just</p>
                                <small className="ml-2 add-mob">1,432</small>
                            </div>

                            <div className="flex  mt-5">
                                <AddIcon />
                                <p className="text-xs">1 Year Accident, Liquid and Screen Protection Plan for</p>
                                <small className="ml-2 add-mob">1,432</small>
                            </div>

                            <div className="mt-2">
                                <Divider />
                            </div>

                            <div className="flex items-center mt-5 justify-between">
                                <p>Order Quantity</p>
                                <p> 1Nos</p>
                            </div>

                            <div className="btn-wrp w-full mt-3">
                                <Button variant="contained" className="btn-orange w-full">
                                    Buy Now
                                </Button>
                            </div>

                            <div className="btn-wrp w-full mt-3">
                                <Button variant="contained" className="btn-gray w-full">
                                    Add To Cart
                                </Button>
                            </div>
                        </div>
                    </div>

                    <Setdelivery />
                    <div>
                        <p className="mt-10">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                            it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
                            Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>

                        <p className="mt-10">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                            it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
                            Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>

                        <p className="mt-10">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                            it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
                            Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>

                        <p className="mt-10">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                            it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
                            Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>

                        <p className="mt-10">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                            it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
                            Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import AcUnitIcon from "@material-ui/icons/AcUnit";
import ArrowRightTwoToneIcon from '@material-ui/icons/ArrowRightTwoTone';
import SearchIcon from '@material-ui/icons/Search';
import DescriptionIcon from '@material-ui/icons/Description';
import PostAddIcon from '@material-ui/icons/PostAdd';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
const useStyles = makeStyles({
  list: {
    width: 450,
  },
  fullList: {
    width: 'auto',
  },
});

export default function TemporaryDrawer() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    
    right: false,
    
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
    >
      
      <div className="">
                <button className="float-left absolute z-10 bg-transparent text-2xl font-semibo0ld leading-none" onClick={toggleDrawer(anchor, false)}>
                    <span>×</span>
                </button>
            </div>

            <List>
                <div className="storage-wrp mt-3">
                    <div className="exchange_border">
                    <h6 className="capitalize text-sm  w-3/4  pb-4 text-center">enter your mobile details to know the <b>exchange value</b></h6>
                    </div>
                    <div className="relative flex w-full flex-wrap items-stretch p-4">
  <span className="z-10  leading-snug font-normal absolute text-center absolute bg-transparent rounded text-base items-center justify-center w-8 pl-1 py-2">
    <ArrowRightTwoToneIcon />
  </span>
  <input type="text" placeholder="Select your phone to sell" className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-full pl-10"/>
  <span className="z-10  leading-snug font-normal absolute text-center text-blueGray-300 absolute bg-transparent rounded text-sm items-center justify-center w-8 right-0 pr-12 py-3">
    <SearchIcon />
  </span>
</div>
<div className="relative flex w-full flex-wrap items-stretch p-4 pb-2">
  <span className="z-10  leading-snug font-normal absolute text-center absolute bg-transparent rounded text-base items-center justify-center w-8 pl-1 py-2">
    <ArrowRightTwoToneIcon />
  </span>
  <input type="text" placeholder="Enter IMEI Number" className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-full pl-10"/>
  <span className="z-10  leading-snug font-normal absolute text-center text-gray-300 absolute bg-transparent rounded text-base items-center justify-center w-8 right-0 pr-20 py-3">
    verify
  </span>
</div>

<div className="exchange_borderbottom pb-12">
<p className="text-xs float-right pr-5  text-black text-gray-400">Dial *#06# to get IMEI Number</p>
  </div>
  <div className="flex justify-between pr-8 pl-8 pt-2 pb-0">
  <p className="capitalize text-base text-black">product price</p>
  <p className="capitalize text-base text-gray-500">rs 1,19,999</p>
  </div>
  <div className="flex justify-between pr-8 pl-8 pt-2 pb-0">
  <p className="capitalize text-base font-medium text-green-500">exchange discount</p>
  <p className="capitalize text-base text-gray-500">-</p>
  </div>
  <div className="flex justify-between pr-8 pl-8 pt-2 pb-0">
  <p className="capitalize text-base text-black">pickup charge</p>
  <p className="capitalize text-base text-gray-500">-</p>
  </div>
  
  
  <div className="p-4 pb-1">
  <div className="exchange_total flex justify-between pr-6 pl-6 pt-2 pb-2">
  <p className="capitalize grid text-base font-bold text-black">total<span className=" capitalize grid text-xs text-gray-300">including all taxes</span></p>
  <p className="capitalize text-base font-bold text-gray-500">rs 1,19,999</p>
  </div>
  <button className="exchange_mobile w-full font-bold uppercase text-white text-base px-8 py-3 rounded shadow-md  outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button"
      >
  buy with exchange
</button>
  </div>
  <div className="p-4 pt-1 pb-1">
  <button className="exchange_bgbutton w-full font-bold capitalize text-base px-8 py-3 rounded shadow-md  outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button"
      >
  How this exchange works?
</button>
  </div>
  <div className="m-4 exchange_condition">
  <div className="p-4 exchange_borderbottom">
              <dt>
                <div className="absolute flex items-center justify-center h-12 w-12 rounded-md text-gray-300">
                  <DescriptionIcon style={{ fontSize: 50 }}/>
                </div>
                <p className="ml-16 text-base text-gray-500"> your phone will be verified  at the doorstep by our delivery agent  </p>
              </dt>
              <dd className="exchange_termcondition ml-16 text-xs font-bold text-gray-500 ">
                Know all Guidelines             </dd>
            </div>
            <div className="p-4 ">
              <dt>
                <div className="absolute flex items-center justify-center h-12 w-12 rounded-md text-gray-300">
                  <PostAddIcon style={{ fontSize: 50 }}/>
                </div>
                <p className="ml-16 text-base text-justify text-gray-500"> Incase you change your mind or your Old phone doesn’t qualify for exchange, you can pay the difference amount via Cash/Card & get the delivery                           </p>
        
              </dt>
              <dd className="exchange_termcondition ml-16 font-bold text-xs text-gray-500">
                Know all Guidelines             </dd>
            </div>

            </div>
            <div className="p-4 exchange_question">
              <dt>
                <div className="absolute flex items-center justify-center h-12 w-12 rounded-md exchange_termcondition">
                  <LiveHelpIcon style={{ fontSize: 30 }}/>
                </div>
                <p className="ml-16 text-base text-justify capitalize text-gray-500"> question and answer on echange offer                           </p>
        
              </dt>
              
            </div>
            <div className="p-4 exchange_question">
              <dt>
                <div className="exchange_termcondition absolute flex items-center justify-center h-8 w-12 rounded-md ">
                  <QuestionAnswerIcon style={{ fontSize: 30 }}/>
                </div>
                <p className="ml-16 text-base text-justify capitalize text-gray-500"> we are ready help you.chat here                           </p>
        
              </dt>
              
            </div>
                </div>
            </List>

    </div>
  );

  return (
    <div>
      {['right'].map((anchor) => (
        <React.Fragment key={anchor}>
         
         
                                       
                                            <span className="capitalize text-green-500 text-xs font-medium cursor-pointer" onClick={toggleDrawer(anchor, true)}>check exchange</span>
                                            
                                          
          <Drawer anchor={anchor} open={state[anchor]} >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}

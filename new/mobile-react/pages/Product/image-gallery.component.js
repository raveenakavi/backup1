import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

class ImageGallaryComponent extends React.Component {
    render() {
        return (
            <div className="color-variant">
                <Carousel>
                    <div>
                    <p className="legend text-sm flex" ><span className="delivery_stock"> <CheckCircleIcon /><span class="MuiChip-label">In Stock</span></span></p>
                        <img src="../asset/storage1.jpg"/>
                    </div>
                    <div>
                    <p className="legend">2 Items left</p>

                        <img src="../asset/storage2.jpg" />
                    </div>
                    <div>
                    <p className="legend text-sm flex" ><span className="delivery_stock"> <CheckCircleIcon /><span class="MuiChip-label">In Stock</span></span></p>

                        <img src="../asset/storage3.jpg" />
                    </div>
                    <div>
                    <p className="legend">Fast Selleng</p>
                        <img src="../asset/storage4.jpg" />
                    </div>
                    <div>
                    <p className="legend">Fast Selleng</p>
                        <img src="../asset/storage5.jpg" />
                    </div>

                </Carousel>
                {/* <h2>My Gallery Image</h2> */}
                <div className="flex text-xs text-center m-4">
                <div>
                <p className="legend">Metalic gray</p>
                    </div>
                    <div>
                    <p className="legend">Midnight Black</p>
                    </div>
                    <div>
                    <p className="legend">Lemon Yellow</p>
                    </div>
                    <div>
                    <p className="legend">Pista green</p>
                    </div>
                    <div>
                    <p className="legend">Burgundy Red</p>
                    </div>
                    </div>
            </div>
        )
    };
}

export default ImageGallaryComponent;
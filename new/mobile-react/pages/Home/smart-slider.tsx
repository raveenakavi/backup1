import React from 'react'
import Slider from "react-slick";
import Grid from '@material-ui/core/Grid';
import StarIcon from '@material-ui/icons/Star';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Divider from '@material-ui/core/Divider';
export default function smartslider() {




  var settings = {

    infinite: false,
    speed: 500,
    slidesToShow: 5,
    autoplay: false,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
      {
        breakpoint:1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };



  return (
    <div className="slider-fl-wrp _6t1WkM bg-white">

      <div className="sm-grid grid grid-cols-12 gap-2">
        
        <div className="col-end-1">
          <div className="add-sl-img">
            <a href="#"><img className="sideimg" src="asset/sidebanner2.png"></img></a>
          </div>
        </div>



        <div className="col-span-12">
          <Tabs>

           <div className="modtitle flex justify-between items-center">
              <h3 className="font-medium text-2xl">Latest Launches </h3>
              <TabList>
                <Tab>under 15000</Tab>
                <Tab>under 10000</Tab>
                <Tab>under 25000</Tab>
                <Tab>Premium Phones</Tab>
              </TabList>

              <div>
              
              <p>see All</p>
              </div>

            </div>

            <div className="sm1-slider mt-3">
              <TabPanel className="border">
                <Slider {...settings}>


                  <div className="slider-wrp border-r">
                   <div className="smart-whlist">
                      <FormControlLabel 
                        control={<Checkbox icon={<FavoriteBorder />}
                          checkedIcon={<Favorite />} />}
                      />
                    </div>
                    <div className="product-image-container mt-2">
                      <a href="/Product"><img src="asset/smart.png"></img></a>
                    </div>
                    <div className="right-block">
                      <div className="p-1 leading-4 mb-2">
                      <h4>
                        <a className="font-normal text-sm" href="/Product" target="_self" title="Amazfit Bip Lite Smartwatch">Samsung Galaxy S20 Ultra (Green, 12 GB, 256 GB) </a>
                      </h4>
                      <div className="sli-product-details">
                        <span className="text-xs text-gray-400">(Green 12GB,256GB)</span>
                      </div>
                      </div>
                    
                     <div className="p-2">
                      <div className="sm-price flex justify-between items-center font-bold text-sm">
                        <p> ₹17,999 </p>
                        <span className="items-center"><StarIcon className="star-icon"/>3.9</span>
                      </div>
                      <div className="save-price-smart mt-1">
                        <p className="text-xs">Save Rs.1,249 (10% OFF)</p>
                      </div>
                      <div className="add-cart-wp">
                        <button>Add to Cart</button>
                      </div>
                      </div>
                    </div>
                  </div>




                  <div className="slider-wrp border-r">
                   <div className="smart-whlist">
                      <FormControlLabel 
                        control={<Checkbox icon={<FavoriteBorder />}
                          checkedIcon={<Favorite />} />}
                      />
                    </div>
                    <div className="product-image-container mt-2">
                    <a href="/Product"><img src="asset/smart.png"></img></a>
                    </div>
                    <div className="right-block">
                      <div className="p-1 leading-4 mb-2">
                      <h4>
                        <a className="font-normal text-sm" href="/Product" target="_self" title="Amazfit Bip Lite Smartwatch">Samsung Galaxy S20 Ultra (Green, 12 GB, 256 GB) </a>
                      </h4>
                      <div className="sli-product-details">
                        <span className="text-xs text-gray-400">(Green 12GB,256GB)</span>
                      </div>
                      </div>
                   
                     <div className="p-2">
                      <div className="sm-price flex justify-between items-center font-bold text-sm">
                        <p> ₹17,999 </p>
                        <span className="items-center"><StarIcon className="star-icon"/>3.9</span>
                      </div>
                      <div className="save-price-smart mt-1">
                        <p className="text-xs">Save Rs.1,249 (10% OFF)</p>
                      </div>
                      <div className="add-cart-wp">
                        <button>Add to Cart</button>
                      </div>
                      </div>
                    </div>
                  </div>

                  <div className="slider-wrp border-r">
                   <div className="smart-whlist">
                      <FormControlLabel 
                        control={<Checkbox icon={<FavoriteBorder />}
                          checkedIcon={<Favorite />} />}
                      />
                    </div>
                    <div className="product-image-container mt-2">
                    <a href="/Product"><img src="asset/smart.png"></img></a>
                    </div>
                    <div className="right-block">
                      <div className="p-1 leading-4 mb-2">
                      <h4>
                        <a className="font-normal text-sm" href="/Product" target="_self" title="Amazfit Bip Lite Smartwatch">Samsung Galaxy S20 Ultra (Green, 12 GB, 256 GB) </a>
                      </h4>
                      <div className="sli-product-details">
                        <span className="text-xs text-gray-400">(Green 12GB,256GB)</span>
                      </div>
                      </div>
                      
                     <div className="p-2">
                      <div className="sm-price flex justify-between items-center font-bold text-sm">
                        <p> ₹17,999 </p>
                        <span className="items-center"><StarIcon className="star-icon"/>3.9</span>
                      </div>
                      <div className="save-price-smart mt-1">
                        <p className="text-xs">Save Rs.1,249 (10% OFF)</p>
                      </div>
                      <div className="add-cart-wp">
                        <button>Add to Cart</button>
                      </div>
                      </div>
                    </div>
                  </div>


                  <div className="slider-wrp border-r">
                   <div className="smart-whlist">
                      <FormControlLabel 
                        control={<Checkbox icon={<FavoriteBorder />}
                          checkedIcon={<Favorite />} />}
                      />
                    </div>
                    <div className="product-image-container mt-2">
                    <a href="/Product"><img src="asset/smart.png"></img></a>
                    </div>
                    <div className="right-block">
                      <div className="p-1 leading-4 mb-2">
                      <h4>
                        <a className="font-normal text-sm" href="/Product" target="_self" title="Amazfit Bip Lite Smartwatch">Samsung Galaxy S20 Ultra (Green, 12 GB, 256 GB)</a>
                      </h4>
                      <div className="sli-product-details">
                        <span className="text-xs text-gray-400">(Green 12GB,256GB)</span>
                      </div>
                      </div>
                    
                     <div className="p-2">
                      <div className="sm-price flex justify-between items-center font-bold text-sm">
                        <p> ₹17,999 </p>
                        <span className="items-center"><StarIcon className="star-icon"/>3.9</span>
                      </div>
                      <div className="save-price-smart mt-1">
                        <p className="text-xs">Save Rs.1,249 (10% OFF)</p>
                      </div>
                      <div className="add-cart-wp">
                        <button>Add to Cart</button>
                      </div>
                      </div>
                    </div>
                  </div>

                  <div className="slider-wrp border-r">
                   <div className="smart-whlist">
                      <FormControlLabel 
                        control={<Checkbox icon={<FavoriteBorder />}
                          checkedIcon={<Favorite />} />}
                      />
                    </div>
                    <div className="product-image-container mt-2">
                    <a href="/Product"><img src="asset/smart.png"></img></a>
                    </div>
                    <div className="right-block">
                      <div className="p-1 leading-4 mb-2">
                      <h4>
                        <a className="font-normal text-sm" href="/Product" target="_self" title="Amazfit Bip Lite Smartwatch">Samsung Galaxy S20 Ultra (Green, 12 GB, 256 GB) </a>
                      </h4>
                      <div className="sli-product-details">
                        <span className="text-xs text-gray-400">(Green 12GB,256GB)</span>
                      </div>
                      </div>
                     
                     <div className="p-2">
                      <div className="sm-price flex justify-between items-center font-bold text-sm">
                        <p> ₹17,999 </p>
                        <span className="items-center"><StarIcon className="star-icon"/>3.9</span>
                      </div>
                      <div className="save-price-smart mt-1">
                        <p className="text-xs">Save Rs.1,249 (10% OFF)</p>
                      </div>
                      <div className="add-cart-wp">
                        <button>Add to Cart</button>
                      </div>
                      </div>
                    </div>
                  </div>


                  <div className="slider-wrp border-r">
                   <div className="smart-whlist">
                      <FormControlLabel 
                        control={<Checkbox icon={<FavoriteBorder />}
                          checkedIcon={<Favorite />} />}
                      />
                    </div>
                    <div className="product-image-container mt-2">
                    <a href="/Product"><img src="asset/smart.png"></img></a>
                    </div>
                    <div className="right-block">
                      <div className="p-1 leading-4 mb-2">
                      <h4>
                        <a className="font-normal text-sm" href="/Product" target="_self" title="Amazfit Bip Lite Smartwatch">Samsung Galaxy S20 Ultra (Green, 12 GB, 256 GB)</a>
                      </h4>
                      <div className="sli-product-details">
                        <span className="text-xs text-gray-400">(Green 12GB,256GB)</span>
                      </div>
                      </div>
                     
                     <div className="p-2">
                      <div className="sm-price flex justify-between items-center font-bold text-sm">
                        <p> ₹17,999 </p>
                        <span className="items-center"><StarIcon className="star-icon"/>3.9</span>
                      </div>
                      <div className="save-price-smart mt-1">
                        <p className="text-xs">Save Rs.1,249 (10% OFF)</p>
                      </div>
                      <div className="add-cart-wp">
                        <button>Add to Cart</button>
                      </div>
                      </div>
                    </div>
                  </div>

                  <div className="slider-wrp border-r">
                   <div className="smart-whlist">
                      <FormControlLabel 
                        control={<Checkbox icon={<FavoriteBorder />}
                          checkedIcon={<Favorite />} />}
                      />
                    </div>
                    <div className="product-image-container mt-2">
                    <a href="/Product"><img src="asset/smart.png"></img></a>
                    </div>
                    <div className="right-block">
                      <div className="p-1 leading-4 mb-2">
                      <h4>
                        <a className="font-normal text-sm" href="/Product" target="_self" title="Amazfit Bip Lite Smartwatch">Samsung Galaxy S20 Ultra (Green, 12 GB, 256 GB)</a>
                      </h4>
                      <div className="sli-product-details">
                        <span className="text-xs text-gray-400">(Green 12GB,256GB)</span>
                      </div>
                      </div>
                     
                     <div className="p-2">
                      <div className="sm-price flex justify-between items-center font-bold text-sm">
                        <p> ₹17,999 </p>
                        <span className="items-center"><StarIcon className="star-icon"/>3.9</span>
                      </div>
                      <div className="save-price-smart mt-1">
                        <p className="text-xs">Save Rs.1,249 (10% OFF)</p>
                      </div>
                      <div className="add-cart-wp">
                        <button>Add to Cart</button>
                      </div>
                      </div>
                    </div>
                  </div>


  
                  
                </Slider>
              </TabPanel>


              <TabPanel>
                slider2
            </TabPanel>
              <TabPanel>
                slider3
            </TabPanel>
              <TabPanel>
                slider4
            </TabPanel>




            </div>
          </Tabs>
        </div>
      </div>



    </div>
  )
}

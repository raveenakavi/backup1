import React from 'react'

export default function freeshipping() {
    return (
        <div className="_6t1WkM bg-white mt-2 p-7">
            <div className="flex justify-around items-center text-center object-center">
                <div className="freeship-icon">
                    <img src="asset/shipped.svg"></img>
                    <p>Free Shipping & Return</p>
                    <small>Free shipping on orders</small>
                </div>
                <div className="freeship-icon">
                    <img src="asset/card.svg"></img>
                    <p>Over 50 Payment Options</p>
                    <small>Secure System</small>
                </div>
                <div className="freeship-icon">
                    <img src="asset/customer.svg"></img>
                    <p>Online Support</p>
                    <small>24 hours a day</small>
                </div>
                <div className="freeship-icon">
                    <img src="asset/Group.svg"></img>
                    <p>100% Safe</p>
                    <small>Secure Shopping</small>
                </div>
            </div>
        </div>
    )
}

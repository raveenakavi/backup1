import React from 'react'
import Grid from '@material-ui/core/Grid';

export default function bannershop() {
    return (
        <div className="_6t1WkM">
            <Grid container spacing={1}>
            <Grid item md={8}>
            <a href=""><img className="w-full" src="asset/watch-banner.png"></img></a>
            </Grid>
            <Grid item md={4}>
            <div className="bannerstop">
            <div className="item1">
            <a href=""><img className="w-full" alt="image" src="asset/headset-banner.png"></img></a>
            </div>
            </div>

            <div className="bannerstop mt-2">
            <div className="item1">
            <a href=""><img className="w-full" alt="image" src="asset/oppo-banner.png"></img></a>
            </div>
            </div>

            </Grid>
            </Grid>
        </div>
    )
}

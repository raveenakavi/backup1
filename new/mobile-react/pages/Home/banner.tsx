import React from 'react'
import Slider from "react-slick";

export default function Mainbanner() {

    var settings = {

        infinite: true,
        speed: 500,
        slidesToShow: 1,
        autoplay: true,
        slidesToScroll: 1,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };

    return (
        <div className="banner-wrp  _6t1WkM">
      
      <Slider {...settings} className="w-full">
          <div>
            <img className="w-full" src="asset/slide1.png"></img>
          </div>
          <div>
            <img className="w-full" src="asset/slide1.png"></img>
          </div>
          <div>
            <img className="w-full" src="asset/slide1.png"></img>
          </div>
       
        </Slider>
       
      </div>
    )
}

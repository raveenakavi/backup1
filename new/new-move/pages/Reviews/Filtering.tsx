import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import dummyReview from "./dummyreview";
import LatestReview from "./LatestReview";
import FilterReviewProductDetail from "./FilterReviewProductDetail";
import {Tabs} from "antd"

export default function Filtering() {
  const [ratingStar, setRatingStar] = useState();

  useEffect(() => {
    setRatingStar("");
  }, []);
  const dataDummy = dummyReview.data;

  const filterReviewRating = dataDummy.filter(review => {
    if (!ratingStar) return true;
    return review.rating == ratingStar;
  });

  function actionChangeSelectFilter(e) {
    setRatingStar(e);
  }

  console.log(filterReviewRating);

  return (
    <div className="mp-review-product-detail">
      <div>
        <div >
          <div className="mp-review-product-detail__filter">
            <FilterReviewProductDetail
              actionChangeSelectFilter={actionChangeSelectFilter}
            />
          </div>
        </div>
      </div>

          {filterReviewRating.map((review, i) => {
            return <LatestReview key={i} review={review} />;
          })}

    </div>
  );
}


import React from 'react'
import Ratings from './Ratings';

export default function index() {
    return (
        <div>
            <Ratings />
        </div>
    )
}

const dummyReview = {
  code: "200",
  message: "OK",
  data: [
    {
      rating: 5,
      message: "its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.",
      isAnonymous: false,
      images: [],
      reviewer: {
        name: "Sathish",
        imageUrl:
          "https://material-ui.com/static/images/avatar/1.jpg",
        id: "",
        
      },
      anonymous: false,
      id: ""
    },
    {
      rating: 4,
      message: "its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.",
      isAnonymous: false,
      images: [],
      reviewer: {
        name: "Kavitha",
        imageUrl:
          "https://material-ui.com/static/images/avatar/1.jpg",
        id: "",
        
      },
      anonymous: false,
      id: ""
    },
    {
      rating: 4,
      message: "its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.",
      isAnonymous: false,
      images: [],
      reviewer: {
        name: "Sebastian Abraham",
        imageUrl:
          "https://material-ui.com/static/images/avatar/1.jpg",
        id: "",
        
      },
      anonymous: false,
      id: ""
    },
    {
      rating: 3,
      message: "its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.",
      isAnonymous: false,
      images: [],
      reviewer: {
        name: "Sebastian Abraham",
        imageUrl:
          "https://material-ui.com/static/images/avatar/1.jpg",
        id: "",
        
      },
      anonymous: false,
      id: ""
    },
    {
      rating: 2,
      message: "its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.",
      isAnonymous: false,
      images: [],
      reviewer: {
        name: "Viji",
        imageUrl:
          "https://material-ui.com/static/images/avatar/1.jpg",
        id: "",
        
      },
      anonymous: false,
      id: ""
    },
    {
      rating: 1,
      message: "its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.",
      isAnonymous: false,
      images: [],
      reviewer: {
        name: "Sebastian Abraham",
        imageUrl:
          "https://material-ui.com/static/images/avatar/1.jpg",
        id: "",
        
      },
      anonymous: false,
      id: ""
    }
  ],
  responseTime: 0
};

export default dummyReview;

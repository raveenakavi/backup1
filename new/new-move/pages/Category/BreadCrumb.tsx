import React from 'react'
import ViewCompactIcon from '@material-ui/icons/ViewCompact';
import AppsIcon from '@material-ui/icons/Apps';
import StorageIcon from '@material-ui/icons/Storage';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

// import Reviews from './Reviews';
import HeadPhones from './HeadPhoness';
import Category from './Category';

export default function BreadCrumb() {
    return (
        <div className="breadcrumb w-full grid grid-flow-col grid-col-2 pt-3 pb-3 tabs">
                        <Tabs>
            <TabList>
            <div className="absolute left-6 text-sm">Home &#62; Mobile Accessories &#62; Accessories &#62; HeadPhones</div>
            <div className="absolute right-6">

            <Tab>
                <ViewCompactIcon /></Tab>
                <Tab>
                <AppsIcon /> </Tab>
                <Tab><StorageIcon /></Tab>
                
                <div className="content w-full">
                <TabPanel>
                <div id="content1">
                    <Category />
                    </div>
                    </TabPanel>
                    <TabPanel>
                    <div id="content2">
                        <HeadPhones />
                    </div>
                    </TabPanel>
                    <TabPanel>
                    <div id="content3">
                        fkgfkg
                    </div>
                    </TabPanel>
                    </div>


<br /><br /><br />
                </div>
                </TabList>
</Tabs>
        </div>
    )
}

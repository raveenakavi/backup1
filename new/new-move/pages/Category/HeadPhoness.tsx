import React, { useState, useRef } from "react";
// import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import SearchIcon from '@material-ui/icons/Search';
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import RangeSlider from 'reactrangeslider';
import Sliders from "react-slick";
import Slider from "@material-ui/core/Slider";
import Grid from "@material-ui/core/Grid";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";



export default function HeadPhones() {

//   const [value, setValue] = React.useState([10, 50]);
  const [values, setValues] = useState([15, 75]);
  const rangeSelector = (event, newValue) => {
    setValues(newValue);
    console.log(newValue);
};
    const StyledRating = withStyles({
        iconFilled: {
          divor: '#f11a08',
        },
        iconHover: {
          divor: '#ff3d47',
        },
      })(Rating);
    
    const [divShow, setDivShow] = useState(false);
    const [divHide, setDivHide] = useState(false);
    const [showText, setShowText] = useState(true);
    const [showFilter, setShowFilter] = useState(true);
    const [showScreen, setShowScreen] = useState(true);
    const [showPrimary, setShowPrimary] = useState(true);
    const [showSecondary, setShowSecondary] = useState(true);
    const [showProcessor, setShowProcessor] = useState(true);
    
    const[add,minus]=useState(true);
    
    const selectChange = (element) =>{
      if(element == add){
        setDivShow(false);
        setDivHide(true);
      }else if(element==minus){
        setDivShow(true);
        setDivHide(false);
      }
    }

    // function Example2() {
    //   const [value, setValue] = React.useState([10, 50]);
    //   return (
    //     <div>
    //       <div>
    //         <RangeSlider
    //           progress
    //           style={{ marginTop: 16 }}
    //           value={value}
    //           onChange={value => {
    //             setValue(value);
    //           }}
    //         />
    //       </div>
    //       <div>
    //         <input>
    //           <input
    //             min={0}
    //             max={100}
    //             value={value[0]}
    //             onChange={nextValue => {
    //               const [start, end] = value;
    //               if (nextValue > end) {
    //                 return;
    //               }
    //               setValue([nextValue, end]);
    //             }}
    //           />
    //           <p>to</p>
    //           <input
    //             min={0}
    //             max={100}
    //             value={value[1]}
    //             onChange={nextValue => {
    //               const [start, end] = value;
    //               if (start > nextValue) {
    //                 return;
    //               }
    //               setValue([start, nextValue]);
    //             }}
    //           />
    //         </input>
    //       </div>
    //     </div>
    //   );
    // }

    return (
        <div>
        <div>
                <Grid container className="w-full  category_padalignment">
                    <Grid item xs={3} sm={2} md={2} lg={2}>
                        <div>
                            <div className="flex justify-between ">
                                <div className="p-1">
                                    <div className="relative">
                                        <span className="category_inputdropdown z-10 h-full leading-snug font-normal absolute text-center text-blueGray-300 absolute bg-transparent rounded text-base items-center justify-center w-8 pl-0 py-2">
                                            <LocationOnOutlinedIcon />
                                        </span>

                                        <select className="block capitalize appearance-none w-full border border-gray-200 text-gray-700 py-2 px-2 pr-8 pl-7 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                            <option className="capitalize text-base">pick up at store </option>
                                            <option className="capitalize text-base">next day delivery</option>
                                            <option className="capitalize text-base">can be deliver later</option>
                                            <option className="capitalize text-base">2 hours delivery</option>
                                        </select>
                                        <div className="absolute flex inset-y-0 items-center px-3 right-0 text-gray-700 rounded-r pointer-events-none">
                                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>

                                <p className="capitalize text-2xl font-bold p-1 text-black-500 mt-1" >
                                    <FontAwesomeIcon className="category_inputdropdown" icon="filter" />
                                </p>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={9} sm={10} md={10} lg={10}>
                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                            <div className="category_filterdesign flex overflow-y-auto overflow-x-auto">
                                <span>
                                    <a>showing all</a>
                                </span>
                                <span>
                                    <a>android</a>
                                </span>
                                <span>
                                    <a>ios</a>
                                </span>
                                <span>
                                    <a>top rating</a>
                                </span>
                                <span>
                                    <a>wireless charging</a>
                                </span>
                                <span>
                                    <a>water resistant</a>
                                </span>
                                <span>
                                    <a>free delivery</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                            </div>
                            <div className="p-2">
                                <p className="capitalize  mt-1 text-sm font-bold text-blue-600 min-w-max"> clear all</p>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
        <div className="container md:gap-8 flex similar-products earbuds py-10 px-6">


<div className="w-4/12 md:w-2/12">
                <div className={`${showText ? "col-start-1 col-span-2 sm:col-start-1 sm:col-span-1 md:col-start-1 md:col-span-1 lg:col-start-1 lg:col-span-1 ..." : "test"}`}>
                    {showText && (
                        <div className="category_filterheader rounded">
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_background">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">filters</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1">
                                    clear all <FontAwesomeIcon className="category_inputdropdown category_filterclose" icon="times-circle" />
                                </p>
                            </div>
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_filterclose">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">price range</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1"></p>
                            </div>
                            <div className="flex pl-0 sm:pl-10 md:pl-10 lg:pl-10">
                                <div className="flex-1">Rs. {values[0]}</div>
                                <div className="flex-1 capitalize">to</div>
                                <div className="flex-1">Rs. {values[1]}</div>
                            </div>
                            <div className="pr-4 pl-4 pt-2 pb-2">
                                <Slider value={values} onChange={rangeSelector} valueLabelDisplay="auto" className="category_filterclose" />
                            </div>

                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1 ">
                                <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">brand</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 category_filterclose">47 brands</p>
                            </div>
                            <div className="pt-2 pb-0 category_background">
                                <div className="pr-3 pl-3 relative flex w-full flex-wrap items-stretch mb-1">
                                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-300 absolute bg-transparent rounded text-base items-center justify-center w-10 pl-1 py-1">
                                        <FontAwesomeIcon icon="search" />
                                    </span>
                                    <input
                                        type="text"
                                        placeholder="Search"
                                        className=" w-20 md:w-28 sm:w-full md:w-full lg:w-full px-2 py-1 category_searchmethod placeholder-blueGray-300 text-blueGray-600 relative rounded text-sm border border-blueGray-300 outline-none focus:w-full md:pl-10 pl-5"
                                    />
                                </div>
                                <p className="mb-1">
                                    <input type="radio" id="apple" name="brand" value="apple" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        apple
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="celkon" name="brand" value="celkon" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        celkon
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="karbon" name="brand" value="karbon" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        karbon
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="micromax" name="brand" value="micromax" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        micromax
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="samsung" name="brand" value="samsung" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        samsung
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="nokia" name="brand" value="nokia" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        nokia
                                    </label>
                                </p>
                            </div>

                            <div className="pt-0 pb-2 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1 ${showFilter ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">ram</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowFilter(!showFilter)} />
                                        <RemoveIcon onClick={() => setShowFilter(!showFilter)} className="category_removeicon" />
                                    </p>
                                </div>

                                {showFilter && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="8gb ram" name="ram" value="8gb ram" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                8GB & Above
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="6gb ram" name="ram" value="6gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6GB
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4gb ram" name="ram" value="4gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="3gb ram" name="ram" value="3gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                3GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="2gb ram" name="ram" value="2gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB-1GB" name="ram" value="512MB-1GB" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                512MB-1GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB" name="ram" value="512MB" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than- 512MB
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1 ${showScreen ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">screen size</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowScreen(!showScreen)} />
                                        <RemoveIcon onClick={() => setShowScreen(!showScreen)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showScreen && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 3inch" name="screensize" value="lessthan 3inch" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 3 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4inch" name="screensize" value="4inch" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 inch - 4.99 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="5inch" name="screensize" value="5inch" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                5 inch - 5.99 inch
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="6inch" name="screensize" value="6inch" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6 inch & Above
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1 ${showPrimary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">primary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowPrimary(!showPrimary)} />
                                        <RemoveIcon onClick={() => setShowPrimary(!showPrimary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showPrimary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="primarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="primarycamera" value="12mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="primarycamera" value="20mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="primarycamera" value="40mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1 ${showSecondary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">secondary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowSecondary(!showSecondary)} />
                                        <RemoveIcon onClick={() => setShowSecondary(!showSecondary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showSecondary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="secondarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="secondarycamera" value="12mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="secondarycamera" value="20mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="secondarycamera" value="40mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1 ${showProcessor ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">Processor</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowProcessor(!showProcessor)} />
                                        <RemoveIcon onClick={() => setShowProcessor(!showProcessor)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showProcessor && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="dualcore" name="processor" value="dualcore" className="ml-3" />
                                            <label className="ml-3  text-sm font-medium">
                                                Dual Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="hexacore" name="processor" value="hexacore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Hexa Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="octacore" name="processor" value="octacore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Octa Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="quadcore" name="processor" value="quadcore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Quad Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="singlecore" name="processor" value="singlecore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Single Core
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                        </div>
                    )}
                </div>
                </div>
<div className="w-8/12 md:w-10/12 mx-4 md:mx-0">
<div className="md:grid grid-cols-3 gap-2 ">
            <div className="element headphones my-2 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/tab-image-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character  </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/tab-image-2.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/tab-image-3.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/tab-image-4.jpg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

            <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/category-headphone-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>
  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/category-headphone-2.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>
  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/category-headphone-3.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/category-headphone-4.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-2.png'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-3.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones my-4 md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-4.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones md:my-4 bg-white" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-3 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>
  </div>
  </div>
  </div>
  </div>
    )
}

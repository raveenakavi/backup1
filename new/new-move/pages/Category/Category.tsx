import React from 'react'
import BestSeller from './BestSeller'
import BottomcategoryLogo from './BottomcategoryLogo'
import BreadCrumb from './BreadCrumb'
import CategoryHeadphones from './CategoryHeadphones'
import CategoryLogo from './categoryLogo'
import CategoryTab from './CategoryTab'
import EarBuds from './EarBuds'
import MostRecommended from './MostRecommended'
import SpotLight from './SpotLight'

export default function Category() {
    return (
        <div>
            {/* <BreadCrumb /> */}
        <div className="Category bg-no-repeat bg-center p-5">

            <img src={'/asset/category-ban.png'} className="w-full h-auto my:5 md:my-1" />

            <div className="categories pt-5 grid md:grid-rows-2 md:grid-flow-col gap-4">

            <div className="row-span-2 md:row-span-1 col-span-2 ..."><a href=""><img src={'/asset/category-1.png'} className="md:h-full" /></a></div>
            <div className="row-span-2 col-span-2 ..."><a href=""><img src={'/asset/category-2.png'} /></a></div>
            <div className="row-span-2 md:row-span-1 col-span-2 ..."><a href=""><img src={'/asset/category-3.png'} className="md:h-full" /></a></div>
            <div className="row-span-2 col-span-2 ..."><a href=""><img src={'/asset/category-4.png'} /></a></div>

            <div className="row-span-2 md:row-span-3 col-span-4 md:col-span-0 ..."><img src={'/asset/category-5.png'} className="fit-content"/></div>
                        <div className="row-span-2 md:row-span-1 col-span-2 ..."><a href=""><img src={'/asset/category-1.png'} className="md:h-full" /></a></div>
            <div className="row-span-2 col-span-2 ..."><a href=""><img src={'/asset/category-2.png'} /></a></div>
            <div className="row-span-2 md:row-span-1 col-span-2 ..."><a href=""><img src={'/asset/category-3.png'} className="md:h-full" /></a></div>
            <div className="row-span-2 col-span-2 ..."><a href=""><img src={'/asset/category-4.png'} /></a></div>

            </div>

            <CategoryLogo />
            <EarBuds />
            <CategoryHeadphones />
            <div className="my-5 md:my-10">
            <img src={'/asset/sony-ban.png'} className="w-full h-auto" />
            </div>

            <CategoryTab />

            <BestSeller />
            <SpotLight />
            <MostRecommended />
            <EarBuds />
            <CategoryHeadphones />

            <div className="my-5 md:my-10">
            <img src={'/asset/category-mid-ban.png'} className="w-full h-auto" />
            </div>
            
            <BottomcategoryLogo />
            

            <div className="my-5 md:my-10">
            <img src={'/asset/category-bottom-ban.png'} className="w-full h-auto" />
            </div>
        </div>
        </div>
    )
}

import React, { useState, useRef } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import SyncAltIcon from '@material-ui/icons/SyncAlt';


const BestSeller = () => {

  const BestSellerSlider = useRef();
  var BestSeller = {
    dots: false,
    autoplay: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },

    {
      breakpoint: 476,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};
const next = () => {
  slider.slickNext();
};
const previous = () => {
  slider.slickPrev();
};


  const StyledRating = withStyles({
    iconFilled: {
      color: '#f11a08',
    },
    iconHover: {
      color: '#ff3d47',
    },
  })(Rating);
//   const  [toggleHeart, setToggleHeart] = useState(false)
    
//   changeColor = useCallback(() =>{
//    setToggleHeart(!toggleHeart)
//   },[])
//   <FavoriteIcon className={
//           toggleHeart ? 'heart active' : 'heart'
//         } onClick={changeColor}/>

const [divShow, setDivShow] = useState(false);
const [divHide, setDivHide] = useState(false);

const[add,minus]=useState(true);

const selectChange = (element) =>{
  if(element == add){
    setDivShow(false);
    setDivHide(true);
  }else if(element==minus){
    setDivShow(true);
    setDivHide(false);
  }
}
    
  return (
    <div className="bg-white app similar-products bestseller my-10 pb-5">
      {/*  Slider */}

      <h2 className="flex py-4 px-3 text-black text-md md:text-2xl font-bold">Today's Best Seller

                    {/* <span className="text-xs pt-3 text-blue-600"> See all</span> */}
                    <span className="right-2 absolute">
                    <div className="slider-arrow">
                    <button  onClick={() => BestSellerSlider.current.slickPrev()} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button  onClick={() => BestSellerSlider.current.slickNext()} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
                                </div>
                      </span>
                    
                    </h2>
                    <Slider ref={(slider) => (BestSellerSlider.current = slider)} {...BestSeller}>
        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="element bg-pink-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-4.png'} />
            </div>
      <h3 className="text-black text-md pt-2 px-3">Boat </h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>


  <div className="element bg-yellow-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-2.png'} />
        </div>
      <h3 className="text-black text-md pt-2 px-3">Sennheiser</h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>

  <div className="element bg-blue-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-3.png'} />
        </div>
      <h3 className="text-black text-md pt-2 px-3">JBL</h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>


  <div className="element bg-green-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-4.png'} />
        </div>
      <h3 className="text-black text-md pt-2 px-3">SONY </h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>

  <div className="element bg-pink-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-4.png'} />
            </div>
      <h3 className="text-black text-md pt-2 px-3">Boat </h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>


  <div className="element bg-yellow-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-2.png'} />
        </div>
      <h3 className="text-black text-md pt-2 px-3">Sennheiser</h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>

  <div className="element bg-blue-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-3.png'} />
        </div>
      <h3 className="text-black text-md pt-2 px-3">JBL</h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>


  <div className="element bg-green-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-4.png'} />
        </div>
      <h3 className="text-black text-md pt-2 px-3">SONY </h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>
  
  <div className="element bg-pink-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-4.png'} />
            </div>
      <h3 className="text-black text-md pt-2 px-3">Boat </h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>


  <div className="element bg-yellow-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-2.png'} />
        </div>
      <h3 className="text-black text-md pt-2 px-3">Sennheiser</h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>

  <div className="element bg-blue-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-3.png'} />
        </div>
      <h3 className="text-black text-md pt-2 px-3">JBL</h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>


  <div className="element bg-green-100 border border-red-200 rounded-md" id="surround">
        <div className="flex pt-5">
        <img src={'/asset/bestseller-4.png'} />
        </div>
      <h3 className="text-black text-md pt-2 px-3">SONY </h3>
      <p className="font-normal text-gray-400 text-sm px-3">Blue Rockerz 400</p>
      <p className="text-gray-600 text-xs py-2 px-3"> Rs. 1495 EMI Starts From <span className="text-yellow-600">Save Rs.1,249</span></p>
  </div>
            {/* })} */}
      </Slider>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
    </div>
  );
};

export default BestSeller;

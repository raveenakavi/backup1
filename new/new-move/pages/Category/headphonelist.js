import Head from "next/head";
// import styles from "../styles/Home.module.css";
import Grid from "@material-ui/core/Grid";
import Select from "react-select";
import { useState, useEffect, useRef } from "react";
import Rating from "@material-ui/lab/Rating";
import SearchIcon from "@material-ui/icons/Search";
// import { ToggleButton } from "primereact/togglebutton";
import { ToggleButton } from '@material-ui/lab';
import Switch from "react-switch";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import StarIcon from "@material-ui/icons/Star";
import Sliders from "react-slick";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { ButtonBase } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
import TocIcon from "@material-ui/icons/Toc";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import { withStyles } from "@material-ui/core/styles";
import FavoriteIcon from "@material-ui/icons/Favorite";
import SyncAltIcon from "@material-ui/icons/SyncAlt";
import SyncAltOutlinedIcon from "@material-ui/icons/SyncAltOutlined";
import { Grade } from "@material-ui/icons";
import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from "react-image-gallery";
export default function Home() {
    const images = [
        {
            original: "/asset/earbuds-2.png",
            thumbnail: "/asset/earbuds-2.png",
        },
        {
            original: "/asset/earbuds-3.jpeg",
            thumbnail: "/asset/earbuds-3.jpeg",
        },
        {
            original: "https://s1.poorvikamobile.com/image/data/AAA%20Acc/bose/Bose%20Sport%20Earbuds/Glacier%20White/Bose-Sport-Earbuds-Glacier-White-3.jpg",
            thumbnail: "https://s1.poorvikamobile.com/image/data/AAA%20Acc/bose/Bose%20Sport%20Earbuds/Glacier%20White/Bose-Sport-Earbuds-Glacier-White-3.jpg",
        },

    ];
    const images1 = [
        {
            original: "/asset/bestseller-2.png",
            thumbnail: "/asset/bestseller-2.png",
        },
        {
            original: "/asset/bestseller-3.png",
            thumbnail: "/asset/bestseller-3.png",
        },
        {
            original: "/asset/bestseller-4.png",
            thumbnail: "/asset/bestseller-4.png",
        },

    ];
    const images2 = [
        {
            original: "/asset/earbuds-1.jpeg",
            thumbnail: "/asset/earbuds-1.jpeg",
        },
        {
            original: "/asset/earbuds-4.jpeg",
            thumbnail: "/asset/earbuds-4.jpeg",
        },
        {
            original: "/asset/earbuds-1.jpeg",
            thumbnail: "/asset/earbuds-1.jpeg",
        },

    ];

    const [showText, setShowText] = useState(true);
    const [showFilter, setShowFilter] = useState(true);
    const [showScreen, setShowScreen] = useState(true);
    const [showPrimary, setShowPrimary] = useState(true);
    const [showSecondary, setShowSecondary] = useState(true);
    const [showProcessor, setShowProcessor] = useState(true);
    // price range filter
    const [values, setValues] = useState([15, 75]);
    const rangeSelector = (event, newValue) => {
        setValues(newValue);
        console.log(newValue);
    };

    const [value, setValue] = useState(2);

    const StyledRating = withStyles({
        iconFilled: {
            color: "#f11a08",
        },
        iconHover: {
            color: "#ff3d47",
        },
    })(Rating);

    return (
        <div>
            <div>
                <Grid container className="w-full  category_padalignment">
                    <Grid item xs={3} sm={2} md={2} lg={2}>
                        <div>
                            <div className="flex justify-between ">
                                <div className="p-1">
                                    <div className="relative">
                                        <span className="category_inputdropdown z-10 h-full leading-snug font-normal absolute text-center text-blueGray-300 absolute bg-transparent rounded text-base items-center justify-center w-8 pl-0 py-2">
                                            <LocationOnOutlinedIcon />
                                        </span>

                                        <select className="block capitalize appearance-none w-full border border-gray-200 text-gray-700 py-2 px-2 pr-8 pl-7 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                            <option className="capitalize text-base">pick up at store </option>
                                            <option className="capitalize text-base">next day delivery</option>
                                            <option className="capitalize text-base">can be deliver later</option>
                                            <option className="capitalize text-base">2 hours delivery</option>
                                        </select>
                                        <div className="absolute flex inset-y-0 items-center px-3 right-0 text-gray-700 rounded-r pointer-events-none">
                                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>

                                <p className="capitalize text-2xl font-bold p-1 text-black-500 mt-1" >
                                    <FontAwesomeIcon className="category_inputdropdown" icon="filter" />
                                </p>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={9} sm={10} md={10} lg={10}>
                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                            <div className="category_filterdesign flex overflow-y-auto overflow-x-auto">
                                <span>
                                    <a>showing all</a>
                                </span>
                                <span>
                                    <a>android</a>
                                </span>
                                <span>
                                    <a>ios</a>
                                </span>
                                <span>
                                    <a>top rating</a>
                                </span>
                                <span>
                                    <a>wireless charging</a>
                                </span>
                                <span>
                                    <a>water resistant</a>
                                </span>
                                <span>
                                    <a>free delivery</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                            </div>
                            <div className="p-2">
                                <p className="capitalize  mt-1 text-sm font-bold text-blue-600 min-w-max"> clear all</p>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>

            <div className="grid grid-cols-6 gap-1">
                <div className={`${showText ? "col-start-1 col-span-2 sm:col-start-1 sm:col-span-1 md:col-start-1 md:col-span-1 lg:col-start-1 lg:col-span-1 ..." : "test"}`}>
                    {showText && (
                        <div className="category_filterheader rounded">
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_background">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">filters</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1">
                                    clear all <FontAwesomeIcon className="category_inputdropdown category_filterclose" icon="times-circle" />
                                </p>
                            </div>
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_filterclose">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">price range</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1"></p>
                            </div>
                            <div className="flex pl-0 sm:pl-10 md:pl-10 lg:pl-10">
                                <div className="flex-1">Rs. {values[0]}</div>
                                <div className="flex-1 capitalize">to</div>
                                <div className="flex-1">Rs. {values[1]}</div>
                            </div>
                            <div className="pr-4 pl-4 pt-2 pb-2">
                                <Slider value={values} onChange={rangeSelector} valueLabelDisplay="auto" className="category_filterclose" />
                            </div>

                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1 ">
                                <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">brand</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 category_filterclose">47 brands</p>
                            </div>
                            <div className="pt-2 pb-0 category_background">
                            <div className="pr-3 pl-3 relative flex w-full flex-wrap items-stretch mb-1">
                                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-300 absolute bg-transparent rounded text-base items-center justify-center w-10 pl-1 py-1">
                                        <FontAwesomeIcon icon="search" />
                                    </span>
                                    <input
                                        type="text"
                                        placeholder="Search"
                                        className=" w-20 md:w-28 sm:w-full md:w-full lg:w-full px-2 py-1 category_searchmethod placeholder-blueGray-300 text-blueGray-600 relative rounded text-sm border border-blueGray-300 outline-none focus:w-full md:pl-10 pl-5"
                                    />
                                </div>
                                <p className="mb-1">
                                    <input type="radio" id="apple" name="brand" value="apple" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        apple
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="celkon" name="brand" value="celkon" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        celkon
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="karbon" name="brand" value="karbon" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        karbon
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="micromax" name="brand" value="micromax" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        micromax
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="samsung" name="brand" value="samsung" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        samsung
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="nokia" name="brand" value="nokia" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        nokia
                                    </label>
                                </p>
                            </div>

                            <div className="pt-0 pb-2 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showFilter ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">ram</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowFilter(!showFilter)} />
                                        <RemoveIcon onClick={() => setShowFilter(!showFilter)} className="category_removeicon" />
                                    </p>
                                </div>

                                {showFilter && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="8gb ram" name="ram" value="8gb ram" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                8GB & Above
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="6gb ram" name="ram" value="6gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6GB
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4gb ram" name="ram" value="4gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="3gb ram" name="ram" value="3gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                3GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="2gb ram" name="ram" value="2gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB-1GB" name="ram" value="512MB-1GB" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                512MB-1GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB" name="ram" value="512MB" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than- 512MB
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showScreen ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">screen size</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowScreen(!showScreen)} />
                                        <RemoveIcon onClick={() => setShowScreen(!showScreen)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showScreen && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 3inch" name="screensize" value="lessthan 3inch" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 3 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4inch" name="screensize" value="4inch" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 inch - 4.99 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="5inch" name="screensize" value="5inch" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                5 inch - 5.99 inch
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="6inch" name="screensize" value="6inch" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6 inch & Above
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showPrimary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">primary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowPrimary(!showPrimary)} />
                                        <RemoveIcon onClick={() => setShowPrimary(!showPrimary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showPrimary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="primarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="primarycamera" value="12mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="primarycamera" value="20mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="primarycamera" value="40mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showSecondary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">secondary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowSecondary(!showSecondary)} />
                                        <RemoveIcon onClick={() => setShowSecondary(!showSecondary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showSecondary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="secondarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="secondarycamera" value="12mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="secondarycamera" value="20mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="secondarycamera" value="40mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showProcessor ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">Processor</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowProcessor(!showProcessor)} />
                                        <RemoveIcon onClick={() => setShowProcessor(!showProcessor)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showProcessor && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="dualcore" name="processor" value="dualcore" className="ml-3" />
                                            <label className="ml-3  text-sm font-medium">
                                                Dual Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="hexacore" name="processor" value="hexacore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Hexa Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="octacore" name="processor" value="octacore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Octa Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="quadcore" name="processor" value="quadcore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Quad Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="singlecore" name="processor" value="singlecore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Single Core
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                        </div>
                    )}
                </div>

                <div className={`${showText ? "col-end-7 col-span-4 sm:col-end-7 sm:col-span-5 md:col-end-7 md:col-span-5 lg:col-end-7 lg:col-span-5" : "col-start-1 col-end-7 ..."}`}>
                    <div className={`grid gap-1 ${showText ? "grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-1" : "grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-1"}`}>
                        <div className="product_listdata grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-2">
                        {/* <div className="vc text-xs">New</div> */}
                            <div className="... text-center pt-2 pl-2 pr-2 pb-1 sm:pl-10 sm:pr-10 sm:pb-1 md:pl-10 md:pr-10 md:pb-1 lg:pl-10 lg:pr-10 lg:pb-1">
                                
                                <ImageGallery items={images} />
                            </div>
                            <div className="pt-7 pr-3 ...">
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-1">
                                    <p className="capitalize inline-flex text-black items-center p-0  font-bold text-xl "> bose sports boat earbuds (baltic blue)</p>

                                    <p className="capitalize text-xs font-bold p-1 bg-black text-white rounded-sm categorylist_headtitle"> free 2 hour delivery</p>
                                </div>
                                <div className="  pl-0 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-1">
                                    <div className="...">
                                        <p className="capitalize text-base text-gray-400 p-1 ">sports earbuds true wireless</p>
                                    </div>
                                    <div className="pl-1 sm:pl-10 md:pl-10 lg:pl-10 p-1 text-left ...">
                                        <StarIcon className="brand_staricon" />
                                        <span className="text-gray-600 font-medium text-base">
                                            4.5 <b>(276 reviews)</b>
                                        </span>
                                    </div>
                                </div>
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-2 pl-3 sm:pl-0 md:pl-0 lg:pl-1">
                                    <p className="capitalize inline-flex text-black items-center p-0  font-bold text-sm sm:text-xl md:text-xl lg:text-xl ">
                                        
                                        Rs.1,04,999<del className="pl-4 capitalize inline-flex items-center text-gray-300 text-sm sm:text-xl md:text-xl lg:text-xl">rs. 1,25,000</del>
                                    </p>

                                    <p className="capitalize text-xs font-bold p-2 bg-black text-white rounded-sm w-max"> 45% OFF</p>
                                </div>
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-0 pl-3 sm:pl-0 md:pl-0 lg:pl-1 mb-20">
                                    <p className="capitalize inline-flex text-black items-center font-medium pl-4 sm:pl-0 md:pl-0 lg:pl-0 sm:p-0 md:p-0 lg:p-0 text-sm category_emi">
                                        
                                        emi from<span className="pl-2 capitalize inline-flex items-center text-black text-sm">rs. 25,000</span>
                                    </p>

                                    <p className="capitalize text-sm font-bold pl-4 sm:p-1 md:p-1 lg:p-1 text-white rounded-md category_saveprice"> save rs 11,000</p>
                                </div>
                                <div >
                                    <ul className="float-left break-normal text-sm text-gray-500">
                                        <li className="pl-3 sm:pl-0 md:pl-0 lg:pl-1 inline-block leading-8 font-bold  text-base">
                                            <span className="underline">Key Specs :</span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <FiberManualRecordIcon className="category_keyspec" />
                                            <span >Noice Cancelling</span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Acclaimed Lifelike, Sound
                                            </span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Secure &amp; Comfortable fit
                                            </span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Simple Touch Controls
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="product_listdata grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-2">
                            <div className="... text-center pt-2 pl-2 pr-2 pb-1 sm:pl-10 sm:pr-10 sm:pb-1 md:pl-10 md:pr-10 md:pb-1 lg:pl-10 lg:pr-10 lg:pb-1">
                                
                                <ImageGallery items={images1} />
                            </div>
                            <div className="pt-7 pr-3 ...">
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-1">
                                    <p className="capitalize inline-flex text-black items-center p-0  font-bold text-xl "> bose sports boat earbuds (baltic blue)</p>

                                    <p className="capitalize text-xs font-bold p-1 bg-black text-white rounded-sm categorylist_headtitle"> free 2 hour delivery</p>
                                </div>
                                <div className="  pl-0 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-1">
                                    <div className="...">
                                        <p className="capitalize text-base text-gray-400 p-1 ">sports earbuds true wireless</p>
                                    </div>
                                    <div className="pl-1 sm:pl-10 md:pl-10 lg:pl-10 p-1 text-left ...">
                                        <StarIcon className="brand_staricon" />
                                        <span className="text-gray-600 font-medium text-base">
                                            4.5 <b>(276 reviews)</b>
                                        </span>
                                    </div>
                                </div>
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-2 pl-3 sm:pl-0 md:pl-0 lg:pl-1">
                                    <p className="capitalize inline-flex text-black items-center p-0  font-bold text-sm sm:text-xl md:text-xl lg:text-xl ">
                                        
                                        Rs.1,04,999<del className="pl-4 capitalize inline-flex items-center text-gray-300 text-sm sm:text-xl md:text-xl lg:text-xl">rs. 1,25,000</del>
                                    </p>

                                    <p className="capitalize text-xs font-bold p-2 bg-black text-white rounded-sm w-max"> 45% OFF</p>
                                </div>
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-0 pl-3 sm:pl-0 md:pl-0 lg:pl-1 mb-20">
                                    <p className="capitalize inline-flex text-black items-center font-medium pl-4 sm:pl-0 md:pl-0 lg:pl-0 sm:p-0 md:p-0 lg:p-0 text-sm category_emi">
                                        
                                        emi from<span className="pl-2 capitalize inline-flex items-center text-black text-sm">rs. 25,000</span>
                                    </p>

                                    <p className="capitalize text-sm font-bold pl-4 sm:p-1 md:p-1 lg:p-1 text-white rounded-md category_saveprice"> save rs 11,000</p>
                                </div>
                                <div >
                                    <ul className="float-left break-normal text-sm text-gray-500">
                                        <li className="pl-3 sm:pl-0 md:pl-0 lg:pl-1 inline-block leading-8 font-bold  text-base">
                                            <span className="underline">Key Specs :</span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <FiberManualRecordIcon className="category_keyspec" />
                                            <span >Noice Cancelling</span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Acclaimed Lifelike, Sound
                                            </span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Secure &amp; Comfortable fit
                                            </span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Simple Touch Controls
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="product_listdata grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-2">
                            <div className="... text-center pt-2 pl-2 pr-2 pb-1 sm:pl-10 sm:pr-10 sm:pb-1 md:pl-10 md:pr-10 md:pb-1 lg:pl-10 lg:pr-10 lg:pb-1">
                                
                                <ImageGallery items={images2} />
                            </div>
                            <div className="pt-7 pr-3 ...">
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-1">
                                    <p className="capitalize inline-flex text-black items-center p-0  font-bold text-xl "> bose sports boat earbuds (baltic blue)</p>

                                    <p className="capitalize text-xs font-bold p-1 bg-black text-white rounded-sm categorylist_headtitle"> free 2 hour delivery</p>
                                </div>
                                <div className="  pl-0 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-1">
                                    <div className="...">
                                        <p className="capitalize text-base text-gray-400 p-1 ">sports earbuds true wireless</p>
                                    </div>
                                    <div className="pl-1 sm:pl-10 md:pl-10 lg:pl-10 p-1 text-left ...">
                                        <StarIcon className="brand_staricon" />
                                        <span className="text-gray-600 font-medium text-base">
                                            4.5 <b>(276 reviews)</b>
                                        </span>
                                    </div>
                                </div>
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-2 pl-3 sm:pl-0 md:pl-0 lg:pl-1">
                                    <p className="capitalize inline-flex text-black items-center p-0  font-bold text-sm sm:text-xl md:text-xl lg:text-xl ">
                                        
                                        Rs.1,04,999<del className="pl-4 capitalize inline-flex items-center text-gray-300 text-sm sm:text-xl md:text-xl lg:text-xl">rs. 1,25,000</del>
                                    </p>

                                    <p className="capitalize text-xs font-bold p-2 bg-black text-white rounded-sm w-max"> 45% OFF</p>
                                </div>
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-0 pl-3 sm:pl-0 md:pl-0 lg:pl-1 mb-20">
                                    <p className="capitalize inline-flex text-black items-center font-medium pl-4 sm:pl-0 md:pl-0 lg:pl-0 sm:p-0 md:p-0 lg:p-0 text-sm category_emi">
                                        
                                        emi from<span className="pl-2 capitalize inline-flex items-center text-black text-sm">rs. 25,000</span>
                                    </p>

                                    <p className="capitalize text-sm font-bold pl-4 sm:p-1 md:p-1 lg:p-1 text-white rounded-md category_saveprice"> save rs 11,000</p>
                                </div>
                                <div >
                                    <ul className="float-left break-normal text-sm text-gray-500">
                                        <li className="pl-3 sm:pl-0 md:pl-0 lg:pl-1 inline-block leading-8 font-bold  text-base">
                                            <span className="underline">Key Specs :</span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <FiberManualRecordIcon className="category_keyspec" />
                                            <span >Noice Cancelling</span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Acclaimed Lifelike, Sound
                                            </span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Secure &amp; Comfortable fit
                                            </span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Simple Touch Controls
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="product_listdata grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-2">
                            <div className="... text-center pt-2 pl-2 pr-2 pb-1 sm:pl-10 sm:pr-10 sm:pb-1 md:pl-10 md:pr-10 md:pb-1 lg:pl-10 lg:pr-10 lg:pb-1">
                                
                                <ImageGallery items={images} />
                            </div>
                            <div className="pt-7 pr-3 ...">
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-1">
                                    <p className="capitalize inline-flex text-black items-center p-0  font-bold text-xl "> bose sports boat earbuds (baltic blue)</p>

                                    <p className="capitalize text-xs font-bold p-1 bg-black text-white rounded-sm categorylist_headtitle"> free 2 hour delivery</p>
                                </div>
                                <div className="  pl-0 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-1">
                                    <div className="...">
                                        <p className="capitalize text-base text-gray-400 p-1 ">sports earbuds true wireless</p>
                                    </div>
                                    <div className="pl-1 sm:pl-10 md:pl-10 lg:pl-10 p-1 text-left ...">
                                        <StarIcon className="brand_staricon" />
                                        <span className="text-gray-600 font-medium text-base">
                                            4.5 <b>(276 reviews)</b>
                                        </span>
                                    </div>
                                </div>
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-2 pl-3 sm:pl-0 md:pl-0 lg:pl-1">
                                    <p className="capitalize inline-flex text-black items-center p-0  font-bold text-sm sm:text-xl md:text-xl lg:text-xl ">
                                        
                                        Rs.1,04,999<del className="pl-4 capitalize inline-flex items-center text-gray-300 text-sm sm:text-xl md:text-xl lg:text-xl">rs. 1,25,000</del>
                                    </p>

                                    <p className="capitalize text-xs font-bold p-2 bg-black text-white rounded-sm w-max"> 45% OFF</p>
                                </div>
                                <div className="inline-block sm:flex md:flex lg:flex justify-between p-0 pl-3 sm:pl-0 md:pl-0 lg:pl-1 mb-20">
                                    <p className="capitalize inline-flex text-black items-center font-medium pl-4 sm:pl-0 md:pl-0 lg:pl-0 sm:p-0 md:p-0 lg:p-0 text-sm category_emi">
                                        
                                        emi from<span className="pl-2 capitalize inline-flex items-center text-black text-sm">rs. 25,000</span>
                                    </p>

                                    <p className="capitalize text-sm font-bold pl-4 sm:p-1 md:p-1 lg:p-1 text-white rounded-md category_saveprice"> save rs 11,000</p>
                                </div>
                                <div >
                                    <ul className="float-left break-normal text-sm text-gray-500">
                                        <li className="pl-3 sm:pl-0 md:pl-0 lg:pl-1 inline-block leading-8 font-bold  text-base">
                                            <span className="underline">Key Specs :</span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <FiberManualRecordIcon className="category_keyspec" />
                                            <span >Noice Cancelling</span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Acclaimed Lifelike, Sound
                                            </span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Secure &amp; Comfortable fit
                                            </span>
                                        </li>
                                        <li className="inline-block pl-5 leading-8">
                                            <span >
                                                <FiberManualRecordIcon className="category_keyspec" />
                                                Simple Touch Controls
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>




















                    </div>
                </div>
            </div>
        </div>
    );
}

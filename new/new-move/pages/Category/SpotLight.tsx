import React from 'react';
import HeadsetMicOutlinedIcon from '@material-ui/icons/HeadsetMicOutlined';
import HomeWorkOutlinedIcon from '@material-ui/icons/HomeWorkOutlined';
import ContactlessOutlinedIcon from '@material-ui/icons/ContactlessOutlined';
import SportsEsportsOutlinedIcon from '@material-ui/icons/SportsEsportsOutlined';
import EqualizerOutlinedIcon from '@material-ui/icons/EqualizerOutlined';
import EmojiSymbolsOutlinedIcon from '@material-ui/icons/EmojiSymbolsOutlined';
import Grid from "@material-ui/core/Grid";
import HeadsetIcon from '@material-ui/icons/Headset';
import SpeakerIcon from '@material-ui/icons/Speaker';
import QueueMusicIcon from '@material-ui/icons/QueueMusic';
import GraphicEqIcon from '@material-ui/icons/GraphicEq';
import CellWifiIcon from '@material-ui/icons/CellWifi';
export default function SpotLight() {
    return (
        <div>
                    <div className="bg-white spotlights pt-10 pb-5 relative">
<Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                        <Grid item xs={12}>
                            <div className="text-center">
                                <div className="pt-1 pb-1 pr-1 pl-1">
                                    <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-lg md:text-lg lg:text-lg text-lg italic "> choose</p>
                                    <h1 className="uppercase items-center p-0 cart_flexing font-bold sm:text-3xl md:text-3xl lg:text-3xl text-lg italic "> the spotlight</h1>
                                </div>
                            </div>
                        </Grid>
                    </Grid>


                <div className="w-full sm:w-/12 md:w-/12 lg:w-9/12 grid grid-cols-1 sm:grid-cols-5 md:grid-cols-5 lg:grid-cols-5 gap-4 text-center m-auto">
  <div className="..."><p><HeadsetIcon className="spotlight_icon"/></p><p className="mt-2 uppercase items-center text-center text-base m-auto"> wireless headphone</p></div>
  <div className="..."><SpeakerIcon className="spotlight_icon"/><p className="mt-2 uppercase items-center text-center text-base m-auto"> wireless earphones</p></div>
  <div className="..."><QueueMusicIcon className="spotlight_icon"/><p className="mt-2 uppercase items-center text-center text-base m-auto"> true wireless earbuds</p></div>
  <div className="..."><HeadsetMicOutlinedIcon className="spotlight_icon"/><p className="mt-2 uppercase items-center text-center text-base m-auto"> in ears</p></div>
  <div className="..."><HomeWorkOutlinedIcon className="spotlight_icon"/><p className="mt-2 uppercase items-center text-center text-base m-auto"> work from home</p></div>
   
</div>
 <div className="w-full mt-6 sm:w-/12 md:w-/12 lg:w-9/12 grid grid-cols-1 sm:grid-cols-5 md:grid-cols-5 lg:grid-cols-5 gap-4 text-center m-auto">
  <div className="..."><p><ContactlessOutlinedIcon className="spotlight_icon"/></p><p className="mt-2 uppercase items-center text-center text-base m-auto"> extra bass</p></div>
  <div className="..."><SportsEsportsOutlinedIcon className="spotlight_icon"/><p className="mt-2 uppercase items-center text-center text-base m-auto"> studio & professional</p></div>
  <div className="..."><EqualizerOutlinedIcon className="spotlight_icon"/><p className="mt-2 uppercase items-center text-center text-base m-auto"> gaming</p></div>
  <div className="..."><GraphicEqIcon className="spotlight_icon"/><p className="mt-2 uppercase items-center text-center text-base m-auto"> Noise cancellation</p></div>
  <div className="..."><CellWifiIcon className="spotlight_icon"/><p className="mt-2 uppercase items-center text-center text-base m-auto"> wireless speakers</p></div>
   
</div>
				
		


</div>


</div>
    )
}

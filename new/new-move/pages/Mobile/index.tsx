import React from 'react'
import ProductVariant from './ProductVariant.mobile'

export default function index() {
    return (
        <div>
            <ProductVariant />
        </div>
    )
}

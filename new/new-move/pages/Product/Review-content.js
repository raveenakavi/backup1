import React from 'react'
import GradeIcon from '@material-ui/icons/Grade';
import {Grid, Button, InputBase} from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import Avatar from '@material-ui/core/Avatar';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';

export default function Reviewcontent() {
    return (
        <div>
            <div class="md:w-full reviews-wrp border-b border-gray-400">
                   
                   <div className="pl-2 pr-2">
                   <h4 class="text-center font-bold bg-gradient-to-r from-gray-400 via-gray-400 to-gray-400 text-white p-3">Ratings &amp; Reviews</h4>
                   </div>

                   <div class="grid grid-cols-2 md:grid md:grid-cols-3">
                   <div className="border-r border-gray-400 md:pb-7">
                       <h2 className="md:flex md:p-10 text-5xl font-bold ml-3 mr-1">
                       <div className="p-8 md:border-r border-gray-500 pr-4"><span className="flex startreview"><GradeIcon />  4.6 </span>
                       <span className="font-normal text-base  md:pl-3">279 Reviews</span></div>
                       <div className="review-divider p-8 md:pl-4"><span className="flex percentreview">78 % </span>
                       <span className="font-normal text-base text-black md:float-right pt-6">Recommends</span>
                       </div></h2>
                       <div>
                       <Button className="finder-button topbar" variant="contained">
                           <a href="/Product-Info" className="font-bold">Write Reviewss</a>
                       </Button>
                       </div>
                   </div>
                  
                  
                   <div className="p-10">
                       <div className="mb-5">
                       <p className="flex md:w-4/5 text-xs"><span className="float-left w-5/12 md:w-9/12">5 Star</span><span className="float-right text-gray-500">124 Reviews</span></p>
                       <div className="p-0 w-4/5 h-2 overflow-hidden bg-gray-200 rounded">
                       <div className="relative float-left h-full bg-green-400 w-5/6 rounded"></div>
                       </div>
                       </div>

                       <div className="mb-5">
                       <p className="flex w-4/5 text-xs"><span className="float-left w-7/12 md:w-9/12">4 Star</span><span className="float-right text-gray-500">34 Reviews</span></p>
                       <div className="p-0 w-4/5 h-2 overflow-hidden bg-gray-200 rounded">
                       <div className="relative float-left h-full bg-blue-300 w-4/6 rounded"></div>
                       </div>
                       </div>

                       <div className="mb-5">
                       <p className="flex w-4/5 text-xs"><span className="float-left w-7/12 md:w-9/12">3 Star</span><span className="float-right text-gray-500">18 Reviews</span></p>
                       <div className="p-0 w-4/5 h-2 overflow-hidden bg-gray-200 rounded">
                       <div className="relative float-left h-full bg-yellow-400 w-3/6 rounded"></div>
                       </div>
                       </div>

                       <div className="mb-5">
                       <p className="flex w-4/5 text-xs"><span className="float-left w-7/12 md:w-9/12">2 Star</span><span className="float-right text-gray-500">8 Reviews</span></p>
                       <div className="p-0 w-4/5 h-2 overflow-hidden bg-gray-200 rounded">
                       <div className="relative float-left h-full bg-red-300 w-2/6 rounded"></div>
                       </div>
                       </div>

                       <div className="mb-5">
                       <p className="flex w-4/5 text-xs"><span className="float-left w-7/12 md:w-9/12">1 Star</span><span className="float-right text-gray-500">3 Reviews</span></p>
                       <div className="p-0 w-4/5 h-2 overflow-hidden bg-gray-200 rounded">
                       <div className="relative float-left h-full bg-red-500 w-1/6 rounded"></div>
                       </div>
                       </div>
                   </div>

                   <div className="md:border-l border-gray-400 p-10 w-full">
                   <div> 
                   <h4 className="text-sm font-bold">Frequently Mentioned</h4>
                  
                   <ul className="review-list float-left break-normal text-sm pt-6 text-gray-500">
                       <li className="text-green-500">Camera (120)</li>
                       <li className="text-green-500">Value for Money (5)</li>
                       <li>Photography (12)</li>
                       <li>Battery Life (22)</li>
                       <li>Look (10)</li>
                       <li>Performance (34)</li>
                       <li className="text-red-500">Heating Issue (4)</li>
                   </ul>

                   </div> 
                   <div className="clear-both pt-5"><h3 className="text-center text-base font-bold bg-gray-200 p-2 mt-10">Read all Reviews</h3></div>
                   </div>

                   

                   </div>
               </div> 


               <div className=" w-full col-span-2">
                        <div className="review-icon mt-10 pl-10 pr-6">
<h3 className="text-2xl font-bold">Customer Reviews </h3>
<div className="review pb-3">
<h4 className="pt-5 text-l font-bold text-green-600 sm:flex items-center"> <span className="float-left pr-6 text-left">Loving It !</span><Rating name="half-rating" defaultValue={4.5} precision={0.5} size="large"/></h4>
<div className="w-4/5 md:w-2/5 review-status inline-block pt-2">

<div className="inline-block">
    <span className="text-sm">By John Lewis 31 Oct 2019</span>
   
    </div>
</div>

    </div>
                            <p className="pt-2 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>
                            
                            <div className="w-full md:flex pb-8 border-b border-gray-200"><span className="w-3/5 flex md:w-4/5 flex space-x-2 float-left pr-52"><img src="asset/1.jpg" />
                            <img src="asset/1.jpg" />
                            <img src="asset/1.jpg" />
                            <img src="asset/1.jpg" /></span>
                            <span className="float-right pt-9 pb-0 inline flex md:space-x-16 text-xs"><span className="fill-current text-yellow-600"><ThumbUpIcon size="small"/> 21</span><ThumbDownIcon /> 1 </span>
                            </div>

                        </div>

                        

                    
                        
                    </div>



        </div>
    )
}

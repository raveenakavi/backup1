import React from 'react'
import Typography from "@material-ui/core/Typography";
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const protecphone = [
    {
        Check:<input type="checkbox"/>,
        image:"../asset/production.png",
        name:"1 Year Accidental, Liquid and Screen Production Plan 2",
        price:"3,847",
        oldPrice:"6,800",
        details:"Brand authorised ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum"
    },
    {
        Check:<input type="checkbox"/>,
        image:"../asset/production.png",
        name:"1 Year Accidental, Liquid and Screen Production Plan 2",
        price:"3,847",
        oldPrice:"6,800",
        details:"Brand authorised ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum"
    },
    {
        Check:<input type="checkbox"/>,
        image:"../asset/production.png",
        name:"1 Year Accidental, Liquid and Screen Production Plan 2",
        price:"3,847",
        oldPrice:"6,800",
        details:"Brand authorised ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum"
    }
    
]



function Protection() {
    return (
     
                           <div className="pro-title p-4 bg-white">
                           <h4  className="font-extrabold p-2"> Want to Protect Your Phone ? </h4>
                           <div className="protection-main md:flex pb-4 mt-5">
                           {protecphone.map((item, index) => (
                           <div className="inner-wrp p-2">
                           <div className="pro-wrp p-3">
                            {item.Check}
                           <div className="protection-content flex">
                            <div className="prot-first flex text-center">
                            <img src={item.image} alt="" />
                            </div>
                           <div className="prot-second">
                            <Typography variant="h6" gutterBottom>{item.name}</Typography>
                            <h5> <FontAwesomeIcon className="rupay mr-1" icon="rupee-sign" /> {item.price}<span className="ml-1"><strike>{item.oldPrice}</strike></span> </h5>
                           </div>
                           </div>
                           <Divider/> 
                           <div className="readmore">
                            <Typography variant="body2" gutterBottom>
                            {item.details}<Link>...Know More</Link>
                            </Typography>
                           </div>
                           </div>
                           </div>
                            ))}
                           </div>
                           </div>
        
    )
}

export default Protection
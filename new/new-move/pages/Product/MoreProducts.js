import React, { useState, useRef } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import AppleIcon from '@material-ui/icons/Apple';

const MoreProducts = () => {

  const moreProductSlider = useRef();
  var moreProducts = {
    dots: false,
    autoplay: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },

    {
      breakpoint: 476,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};
const next = () => {
  slider.slickNext();
};
const previous = () => {
  slider.slickPrev();
};

  return (
    <div className="app similar-products">
      {/*  Slider */}

      <h2 className="flex py-4 px-3 border-b border-gray-300 text-black text-md md:text-2xl font-bold"><AppleIcon style={{ fontSize: 33}} /> <span className="ml-4">More from Apple</span>
            <ul className="px-8 md:flex similar-list break-normal text-sm pb-2 pt-2 text-gray-500">
                        <li className="text-green-500 mx-5 px-2 border border-gray-300 rounded-lg">iPhone</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">iMac</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Airpods</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">iWatch</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">iPad</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">TV</li>

                    </ul> 
                    
                    <span className="right-2 absolute">
                    <div className="slider-arrow"><span className="text-xs pt-3 text-blue-600"> See all</span> 
                    <button  onClick={() => moreProductSlider.current.slickPrev()} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button  onClick={() => moreProductSlider.current.slickNext()} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
                                </div>                      </span>
                    
                    </h2>
      <Slider ref={(slider) => (moreProductSlider.current = slider)} {...moreProducts}>
        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
             
             
        {/* })} */}
      </Slider>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
    </div>
  );
};

export default MoreProducts;

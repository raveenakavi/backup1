import React from 'react'
import ProductDetail from '../Product/Product-datails';
import Productlist from '../Product/Product-list';

export default function index() {
    return (
        <div>
            <ProductDetail/>
            <Productlist/>
        </div>
    )
}

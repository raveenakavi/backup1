import '../styles/globals.css'
import 'tailwindcss/tailwind.css'
import '../styles/Responsive.css'
import Layout from '../components/Layout'
import "../node_modules/slick-carousel/slick/slick-theme.css"; 
import "../node_modules/slick-carousel/slick/slick.css"; 
function MyApp({ Component, pageProps }) {
  return (
    <Layout>
   <Component {...pageProps} />
   </Layout> 
   )
}

export default MyApp

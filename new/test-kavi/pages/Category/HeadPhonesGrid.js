import React, { useState, useEffect } from "react";
import { Grade } from '@material-ui/icons';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import SearchIcon from '@material-ui/icons/Search';
import RemoveIcon from '@material-ui/icons/Remove';
// import RangeSlider from 'reactrangeslider';
// import Sliders from "@material-ui/core/Slider";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import SliderApp from "./SliderApp";

export default function HeadPhones() {

  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  const [slider1, setSlider1] = useState(null);
  const [slider2, setSlider2] = useState(null);

  useEffect(() => {

    setNav1(slider1);
    setNav2(slider2);

  });


  const settingsMain = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    centerMode: true,
    fade: true,
    asNavFor: '.slider-nav'
  };

  const settingsThumbs = {
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    arrows:false,
    swipeToSlide: true,
    focusOnSelect: true,
    centerPadding: '10px'
  };

  const slidesData = [
    {
      id: 1,
      title: 'repellendus id ullam',
      label: 'Dolorem officiis temporibus.'
    }, {
      id: 2,
      title: 'excepturi consequatur est',
      label: 'Officia non provident dolor esse et neque.'
    }, {
      id: 3,
      title: 'eius doloribus blanditiis',
      label: 'Ut recusandae vel vitae molestiae id soluta.'
    }, {
      id: 4,
      title: 'nihil voluptates delectus',
      label: 'Qui vel consequatur recusandae illo repellendus.'
    }, {
      id: 5,
      title: 'nemo dolorem necessitatibus',
      label: 'Placeat odit velit itaque voluptatem.'
    }, {
      id: 6,
      title: 'dolorem quibusdam quasi',
      label: 'Adipisci officiis repudiandae.'
    },
  ];


  const [values, setValues] = useState([15, 75]);
  const rangeSelector = (event, newValue) => {
      setValues(newValue);
      console.log(newValue);
  };
  const [value, setValue] = React.useState([10, 50]);
  
    const StyledRating = withStyles({
        iconFilled: {
          divor: '#f11a08',
        },
        iconHover: {
          divor: '#ff3d47',
        },
      })(Rating);
    
    const [divShow, setDivShow] = useState(false);
    const [divHide, setDivHide] = useState(false);
    
    const[add,minus]=useState(true);
    
    const selectChange = (element) =>{
      if(element == add){
        setDivShow(false);
        setDivHide(true);
      }else if(element==minus){
        setDivShow(true);
        setDivHide(false);
      }
    }

    // function Example2() {
    //   const [value, setValue] = React.useState([10, 50]);
    //   return (
    //     <div>
    //       <div>
    //         <RangeSlider
    //           progress
    //           style={{ marginTop: 16 }}
    //           value={value}
    //           onChange={value => {
    //             setValue(value);
    //           }}
    //         />
    //       </div>
    //       <div>
    //         <input>
    //           <input
    //             min={0}
    //             max={100}
    //             value={value[0]}
    //             onChange={nextValue => {
    //               const [start, end] = value;
    //               if (nextValue > end) {
    //                 return;
    //               }
    //               setValue([nextValue, end]);
    //             }}
    //           />
    //           <p>to</p>
    //           <input
    //             min={0}
    //             max={100}
    //             value={value[1]}
    //             onChange={nextValue => {
    //               const [start, end] = value;
    //               if (start > nextValue) {
    //                 return;
    //               }
    //               setValue([start, nextValue]);
    //             }}
    //           />
    //         </input>
    //       </div>
    //     </div>
    //   );
    // }

    return (
        <div className="container flex gap-x-3 md:gap-8 py-10 px-6">
            <div className="w-2/6 md:w-1/4 headphones row-span-6">
            <h3 className="text-lg font-bold border-b border-yellow-500 pb-2 pt-1 px-2">Filters <span className="float-right text-xs font-normal text-gray-500 pt-2 pr-2">Clear All </span></h3>

            {/* <RangeSlider
      value={ value }
      onChange={ onChange }
      min={ 20 }
      max={ 100 }
      step={ 5 }
    /> */}

<div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_filterclose">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">price range</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1"></p>
                            </div>
                            <div className="flex pl-0 sm:pl-10 md:pl-10 lg:pl-10">
                                <div className="flex-1">Rs. {values[0]}</div>
                                <div className="flex-1 capitalize">to</div>
                                <div className="flex-1">Rs. {values[1]}</div>
                            </div>
                            <div className="pr-4 pl-4 pt-2 pb-2">
                                <Slider value={values} onChange={rangeSelector} valueLabelDisplay="auto" className="category_filterclose" />
                            </div>

<div className="text-xs md:text-lg px-2 md:px-0 py-4 border-b broder-yellow-500">
            <h3 className="text-sm md:text-lg font-bold pb-2 pt-1 px-2">BRAND <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2">47 Brands </span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Apple
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            Celelkon
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Karbonn
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            LAVA
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Micromax
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Samsung
          </label>
        </div>        
                      </form>
            </div>

            <div className="text-xs md:text-lg px-2 md:px-0 py-4 border-b broder-yellow-500">
            <h3 className="text-sm md:text-lg font-bold pb-2 pt-1 px-2">RAM </h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            6 GB &amp; Above
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            4 GB
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            3 GB
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            2 GB
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            512 MB - 1 GB
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Less than 512 MB
          </label>
        </div>        
                      </form>
            </div>
            
            <div className="text-xs md:text-lg px-2 md:px-0 py-4 border-b broder-yellow-500">
            <h3 className="text-sm md:text-lg font-bold pb-2 pt-1 px-2">Scrren Size <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Less than 3 inch
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            4 inch - 4.99 inch
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            5 inch - 5.99 inch
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            6 inch &amp; Above
          </label>
        </div>
                      </form>
            </div>

<div className="text-xs md:text-lg px-2 md:px-0 py-4 border-b broder-yellow-500">
            <h3 className="text-sm md:text-lg font-bold pb-2 pt-1 px-2">Primary Camera <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Less than 2 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            2 Mp - 3.99 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            4 Mp - 11.99 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            12 Mp - 19.99 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            20 Mp - 39.99 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Above 40 MP
          </label>
        </div>        
                      </form>
            </div>   

            <div className="text-xs md:text-lg px-2 md:px-0 py-4 border-b broder-yellow-500">
            <h3 className="text-sm md:text-lg font-bold pb-2 pt-1 px-2">Secondary Camera <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Less than 2 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            2 Mp - 3.99 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            4 Mp - 11.99 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            12 Mp - 19.99 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            20 Mp - 39.99 MP
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Above 40 MP
          </label>
        </div>        
                      </form>
            </div>  


            <div className="text-xs md:text-lg px-2 md:px-0 py-4 border-b broder-yellow-500">
            <h3 className="text-sm md:text-lg font-bold pb-2 pt-1 px-2">PROCESSOR <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Dual Core
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            Hexa Core
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Octa Core
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Quad Core
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Single Core
          </label>
        </div>
      
                      </form>
            </div>  


            <div className="text-xs md:text-lg px-2 md:px-0 py-4 border-b broder-yellow-500">
            <h3 className="text-sm md:text-lg font-bold pb-2 pt-1 px-2">FEATURES <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Bluetooth
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            Wi-Fi
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            NFC
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            FM Radio
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Finger Print Scanner
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            In-Display Finger Print Scanner
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            3.5mm Audio Jack
          </label>
        </div>        
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Without Camera (Front &amp; Back)
          </label>
        </div>                      </form>
            </div>  

            <div className="text-xs md:text-lg px-2 md:px-0 py-4 border-b broder-yellow-500">
            <h3 className="text-sm md:text-lg font-bold pb-2 pt-1 px-2">OPERATING SYSTEM <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Android
          </label>
        </div>
        <div className="radio md:px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            IOS
          </label>
        </div>
                      </form>
            </div> 



                        </div>
<div className="w-4/6 flex md:w-3/4 mx-2 md:mx-0">
  <div className="shadow-lg">
    <div className="w-full md:w-2/6 element md:flex headphonesgrid rounded-lg my-4">
            <div className=" py-2 my-2">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<SliderApp />
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-4/6 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
</div>
  </div>


  {/* <div className="md:flex headphonesgrid rounded-lg my-4">
            <div className="w-full md:w-2/5 element py-2 my-2" id="surround">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<Sliders
  {...settingsMain}
  asNavFor={nav2}
  ref={slider => (setSlider1(slider))}
>

  {slidesData.map((slide) =>
    <div className="slick-slide" key={slide.id}>
      <img className="slick-slide-image" src={`/asset/earbuds-3.jpeg?img=${slide.id}`} />
    </div>

  )}

</Sliders>
<div className="thumbnail-slider-wrap">
  <Sliders
    {...settingsThumbs}
    asNavFor={nav1}
    ref={slider => (setSlider2(slider))}>

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <img className="slick-slide-image" src={`/asset/earbuds-3.jpeg?img=${slide.id}`} />
      </div>

    )}

  </Sliders>
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-3/5 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
  </div> */}


  {/* <div className="md:flex headphonesgrid rounded-lg my-4">
            <div className="w-full md:w-2/5 element py-2 my-2" id="surround">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<Sliders
  {...settingsMain}
  asNavFor={nav2}
  ref={slider => (setSlider1(slider))}
>

  {slidesData.map((slide) =>
    <div className="slick-slide" key={slide.id}>
      <img className="slick-slide-image" src={`/asset/earbuds-2.png?img=${slide.id}`} />
    </div>

  )}

</Sliders>
<div className="thumbnail-slider-wrap">
  <Sliders
    {...settingsThumbs}
    asNavFor={nav1}
    ref={slider => (setSlider2(slider))}>

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <img className="slick-slide-image" src={`/asset/earbuds-2.png?img=${slide.id}`} />
      </div>

    )}

  </Sliders>
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-3/5 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
  </div>


  <div className="md:flex headphonesgrid rounded-lg my-4">
            <div className="w-full md:w-2/5 element py-2 my-2" id="surround">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<Sliders
  {...settingsMain}
  asNavFor={nav2}
  ref={slider => (setSlider1(slider))}
>

  {slidesData.map((slide) =>
    <div className="slick-slide" key={slide.id}>
      <img className="slick-slide-image" src={`/asset/bestseller-5.jpeg?img=${slide.id}`} />
    </div>

  )}

</Sliders>
<div className="thumbnail-slider-wrap">
  <Sliders
    {...settingsThumbs}
    asNavFor={nav1}
    ref={slider => (setSlider2(slider))}>

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <img className="slick-slide-image" src={`/asset/bestseller-5.jpeg?img=${slide.id}`} />
      </div>

    )}

  </Sliders>
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-3/5 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
  </div>



  <div className="md:flex headphonesgrid rounded-lg my-4">
            <div className="w-full md:w-2/5 element py-2 my-2" id="surround">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<Sliders
  {...settingsMain}
  asNavFor={nav2}
  ref={slider => (setSlider1(slider))}
>

  {slidesData.map((slide) =>
    <div className="slick-slide" key={slide.id}>
      <img className="slick-slide-image" src={`/asset/bestseller-4.jpeg?img=${slide.id}`} />
    </div>

  )}

</Sliders>
<div className="thumbnail-slider-wrap">
  <Sliders
    {...settingsThumbs}
    asNavFor={nav1}
    ref={slider => (setSlider2(slider))}>

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <img className="slick-slide-image" src={`/asset/bestseller-4.jpeg?img=${slide.id}`} />
      </div>

    )}

  </Sliders>
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-3/5 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
  </div>




 */}



https://stackoverflow.com/questions/49617161/hover-link-text-and-icon








  </div>
        // </div>
    )
}

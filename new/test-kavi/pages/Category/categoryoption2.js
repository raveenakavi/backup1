import Head from "next/head";
import styles from "../styles/Home.module.css";
import Grid from "@material-ui/core/Grid";
import Select from "react-select";
import { useState, useEffect, useRef } from "react";
import Rating from "@material-ui/lab/Rating";
import SearchIcon from "@material-ui/icons/Search";
import { ToggleButton } from "primereact/togglebutton";
import Switch from "react-switch";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import StarIcon from "@material-ui/icons/Star";
import Sliders from "react-slick";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { ButtonBase } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
import TocIcon from "@material-ui/icons/Toc";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import { CountdownCircleTimer } from "react-countdown-circle-timer";

export default function Home() {
    const renderTime = (dimension, time) => {
        return (
            <div className="time-wrapper">
                <div className="time">{time}</div>
                <div>{dimension}</div>
            </div>
        );
    };
    const [showText, setShowText] = useState(false);
    const [showFilter, setShowFilter] = useState(true);
    const [showScreen, setShowScreen] = useState(true);
    const [showPrimary, setShowPrimary] = useState(true);
    const [showSecondary, setShowSecondary] = useState(true);
    const [showProcessor, setShowProcessor] = useState(true);
    //start count timer
    const minuteSeconds = 60;
    const hourSeconds = 3600;
    const daySeconds = 86400;

    const timerProps = {
        isPlaying: true,
        size: 110,
        strokeWidth: 6,
    };
    const getTimeSeconds = (time) => (minuteSeconds - time) | 0;
    const getTimeMinutes = (time) => ((time % hourSeconds) / minuteSeconds) | 0;
    const getTimeHours = (time) => ((time % daySeconds) / hourSeconds) | 0;
    const getTimeDays = (time) => (time / daySeconds) | 0;

    const stratTime = Date.now() / 1000; // use UNIX timestamp in seconds
    const endTime = stratTime + 243248; // use UNIX timestamp in seconds

    const remainingTime = endTime - stratTime;
    const days = Math.ceil(remainingTime / daySeconds);
    const daysDuration = days * daySeconds;
    // price range filter
    const [values, setValues] = useState([15, 75]);
    const rangeSelector = (event, newValue) => {
        setValues(newValue);
        console.log(newValue);
    };
    let interval = useRef();
    const latest = useRef();

    const customSlider = useRef();
    const bcamera = useRef();
    const [value, setValue] = useState(2);
    var categoryslider = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };
    var brandslider = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                brandslider: {
                    slidesToShow: 10,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: true,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                brandslider: {
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    arrows: true,
                    dots: false,
                },
            },
            {
                breakpoint: 480,
                brandslider: {
                    slidesToShow: 5,
                    arrows: true,
                    slidesToScroll: 1,
                    dots: false,
                },
            },
        ],
    };

    var shopbrand = {
        dots: false,
        infinite: true,
        speed: 500,
        autoplay: true,
        slidesToShow: 13,
        slidesToScroll: 1,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 10,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: true,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    arrows: true,
                    dots: false,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 5,
                    arrows: true,
                    slidesToScroll: 1,
                    dots: false,
                },
            },
        ],
    };
    var iphones = {
        dots: false,
        autoplay: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: false,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    slidesToScroll: 1,
                },
            },
        ],
    };
    var bestcamera = {
        dots: false,
        autoplay: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: false,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    slidesToScroll: 1,
                },
            },
        ],
    };
    const next = () => {
        slider.slickNext();
    };
    const previous = () => {
        slider.slickPrev();
    };
    var latests = {
        dots: false,
        autoplay: false,
        infinite: true,
        speed: 500,
        arrows: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: false,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    arrows: false,
                    slidesToScroll: 2,
                },
            },
        ],
    };

    return (
        <div>
            <div>
                <Grid container className="w-full  category_padalignment">
                    <Grid item xs={3} sm={2} md={2} lg={2}>
                        <div>
                            <div className="flex justify-between ">
                                <div className="p-1">
                                    <div className="relative">
                                        <span className="category_inputdropdown z-10 h-full leading-snug font-normal absolute text-center text-blueGray-300 absolute bg-transparent rounded text-base items-center justify-center w-8 pl-0 py-2">
                                            <LocationOnOutlinedIcon />
                                        </span>

                                        <select className="block capitalize appearance-none w-full border border-gray-200 text-gray-700 py-2 px-2 pr-8 pl-7 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                            <option className="capitalize text-base">pick up at store </option>
                                            <option className="capitalize text-base">next day delivery</option>
                                            <option className="capitalize text-base">can be deliver later</option>
                                            <option className="capitalize text-base">2 hours delivery</option>
                                        </select>
                                        <div className="absolute flex inset-y-0 items-center px-3 right-0 text-gray-700 rounded-r pointer-events-none">
                                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>

                                <p className="capitalize text-2xl font-bold p-1 text-black-500 mt-1" onClick={() => setShowText(!showText)}>
                                    <FontAwesomeIcon className="category_inputdropdown" icon="filter" />
                                </p>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={9} sm={10} md={10} lg={10}>
                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                            <div className="category_filterdesign flex overflow-y-auto overflow-x-auto">
                                <span>
                                    <a>showing all</a>
                                </span>
                                <span>
                                    <a>android</a>
                                </span>
                                <span>
                                    <a>ios</a>
                                </span>
                                <span>
                                    <a>top rating</a>
                                </span>
                                <span>
                                    <a>wireless charging</a>
                                </span>
                                <span>
                                    <a>water resistant</a>
                                </span>
                                <span>
                                    <a>free delivery</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                            </div>
                            <div className="p-2">
                                <p className="capitalize  mt-1 text-sm font-bold text-blue-600 min-w-max"> clear all</p>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>

            <div className="grid grid-cols-6 gap-1">
                <div className={`${showText ? "col-start-1 col-span-2 sm:col-start-1 sm:col-span-1 md:col-start-1 md:col-span-1 lg:col-start-1 lg:col-span-1 ..." : "test"}`}>
                    {showText && (
                        <div className="category_filterheader rounded">
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_background">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">filters</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1">
                                    clear all <FontAwesomeIcon className="category_inputdropdown category_filterclose" icon="times-circle" />
                                </p>
                            </div>
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_filterclose">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">price range</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1"></p>
                            </div>
                            <div class="flex pl-0 sm:pl-10 md:pl-10 lg:pl-10">
                                <div className="flex-1">Rs. {values[0]}</div>
                                <div className="flex-1 capitalize">to</div>
                                <div className="flex-1">Rs. {values[1]}</div>
                            </div>
                            <div className="pr-4 pl-4 pt-2 pb-2">
                                <Slider value={values} onChange={rangeSelector} valueLabelDisplay="auto" className="category_filterclose" />
                            </div>

                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1 ">
                                <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">brand</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 category_filterclose">47 brands</p>
                            </div>
                            <div className="pt-2 pb-0 category_background">
                                <div className="pr-3 pl-3 relative flex w-full flex-wrap items-stretch mb-1">
                                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-300 absolute bg-transparent rounded text-base items-center justify-center w-10 pl-1 py-1">
                                        <FontAwesomeIcon icon="search" />
                                    </span>
                                    <input
                                        type="text"
                                        placeholder="Search"
                                        className=" w-28  sm:w-full md:w-full lg:w-full px-2 py-1 category_searchmethod placeholder-blueGray-300 text-blueGray-600 relative bg-white rounded text-sm border border-blueGray-300 outline-none focus:w-full pl-10"
                                    />
                                </div>
                                <p className="mb-1">
                                    <input type="radio" id="apple" name="brand" value="apple" className="ml-3" />
                                    <label for="apple" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        apple
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="celkon" name="brand" value="celkon" className="ml-3" />
                                    <label for="celkon" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        celkon
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="karbon" name="brand" value="karbon" className="ml-3" />
                                    <label for="karbon" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        karbon
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="micromax" name="brand" value="micromax" className="ml-3" />
                                    <label for="micromax" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        micromax
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="samsung" name="brand" value="samsung" className="ml-3" />
                                    <label for="samsung" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        samsung
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="nokia" name="brand" value="nokia" className="ml-3" />
                                    <label for="nokia" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        nokia
                                    </label>
                                </p>
                            </div>

                            <div className="pt-0 pb-2 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showFilter ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">ram</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowFilter(!showFilter)} />
                                        <RemoveIcon onClick={() => setShowFilter(!showFilter)} className="category_removeicon" />
                                    </p>
                                </div>

                                {showFilter && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="8gb ram" name="ram" value="8gb ram" className="ml-3" />
                                            <label for="8gb ram" className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                8GB & Above
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="6gb ram" name="ram" value="6gb ram" className="ml-3" />
                                            <label for="6gb ram" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6GB
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4gb ram" name="ram" value="4gb ram" className="ml-3" />
                                            <label for="4gb ram" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="3gb ram" name="ram" value="3gb ram" className="ml-3" />
                                            <label for="3gb ram" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                3GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="2gb ram" name="ram" value="2gb ram" className="ml-3" />
                                            <label for="2gb ram" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB-1GB" name="ram" value="512MB-1GB" className="ml-3" />
                                            <label for="512MB-1GB" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                512MB-1GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB" name="ram" value="512MB" className="ml-3" />
                                            <label for="512MB" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than- 512MB
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showScreen ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">screen size</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowScreen(!showScreen)} />
                                        <RemoveIcon onClick={() => setShowScreen(!showScreen)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showScreen && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 3inch" name="screensize" value="lessthan 3inch" className="ml-3" />
                                            <label for="lessthan 3inch" className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 3 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4inch" name="screensize" value="4inch" className="ml-3" />
                                            <label for="4inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 inch - 4.99 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="5inch" name="screensize" value="5inch" className="ml-3" />
                                            <label for="5inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                5 inch - 5.99 inch
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="6inch" name="screensize" value="6inch" className="ml-3" />
                                            <label for="6inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6 inch & Above
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showPrimary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">primary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowPrimary(!showPrimary)} />
                                        <RemoveIcon onClick={() => setShowPrimary(!showPrimary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showPrimary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="primarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label for="lessthan 2mp" className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label for="4inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label for="4mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="primarycamera" value="12mp" className="ml-3" />
                                            <label for="12mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="primarycamera" value="20mp" className="ml-3" />
                                            <label for="20mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="primarycamera" value="40mp" className="ml-3" />
                                            <label for="40mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showSecondary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">secondary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowSecondary(!showSecondary)} />
                                        <RemoveIcon onClick={() => setShowSecondary(!showSecondary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showSecondary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="secondarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label for="lessthan 2mp" className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label for="4inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label for="4mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="secondarycamera" value="12mp" className="ml-3" />
                                            <label for="12mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="secondarycamera" value="20mp" className="ml-3" />
                                            <label for="20mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="secondarycamera" value="40mp" className="ml-3" />
                                            <label for="40mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showProcessor ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">Processor</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowProcessor(!showProcessor)} />
                                        <RemoveIcon onClick={() => setShowProcessor(!showProcessor)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showProcessor && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="dualcore" name="processor" value="dualcore" className="ml-3" />
                                            <label for="dualcore" className="ml-3  text-sm font-medium">
                                                Dual Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="hexacore" name="processor" value="hexacore" className="ml-3" />
                                            <label for="hexacore" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Hexa Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="octacore" name="processor" value="octacore" className="ml-3" />
                                            <label for="octacore" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Octa Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="quadcore" name="processor" value="quadcore" className="ml-3" />
                                            <label for="quadcore" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Quad Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="singlecore" name="processor" value="singlecore" className="ml-3" />
                                            <label for="singlecore" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Single Core
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                        </div>
                    )}
                </div>

                <div className={`${showText ? "col-end-7 col-span-4 sm:col-end-7 sm:col-span-5 md:col-end-7 md:col-span-5 lg:col-end-7 lg:col-span-5" : "col-start-1 col-end-7 ..."}`}>
                    <div>
                        <Sliders {...categoryslider}>
                            <div>
                                <img src="https://s1.poorvikamobile.com/image/data/Banner/Home_banner/may/32/Realem-X7-Max-5G-Prebook-now-at-Poorvika.jpg" />
                            </div>

                            <div>
                                <img src="https://s1.poorvikamobile.com/image/data/Banner/Home_banner/may/22/vivo-cashback-offer-available-at-Poorvika.jpg" />
                            </div>
                            <div>
                                <img src="https://s1.poorvikamobile.com/image/data/Banner/Home_banner/may/18/Cashback-offer-available-at-poorvika.jpg" />
                            </div>
                        </Sliders>
                    </div>
                    <div>
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="https://s1.poorvikamobile.com/image/data/Banner/Middle_banner/May/Redmi-Note-10s-available-at-poorvika.jpg" className="rounded" />
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="https://s1.poorvikamobile.com/image/data/Banner/Middle_banner/April/14/Oneplus%209%20series%205G%20availalble%20at%20Poorvika.jpg" className="rounded" />
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="https://s1.poorvikamobile.com/image/data/Banner/Home_banner/April/24/29/Buy-Asus-Rog-Zephyrus-g14--Laptop-&-get-free-gifts-in-poorvika-2-Recovered-1..jpg" className="rounded" />
                                </div>
                            </Grid>
                        </Grid>
                    </div>

                    <div className="pb-12">
                        <Grid container className="sm:p-0 md:p-0 lg:p-0 p-0">
                            <Grid item xs={12}>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow uppercase text-center items-center p-0 cart_flexing font-bold   sm:text-2xl md:text-2xl lg:text-2xl text-xl italic brand_heads"> </p>

                                        <p className="capitalize text-sm font-bold brand_seeall"> see all </p>
                                        <div className="slider-arrow">
                                            <ButtonBase className="arrow-btn prev" onClick={() => latest.current.slickPrev()}>
                                                <ArrowBackIosIcon style={{ fontSize: 20 }} />
                                            </ButtonBase>
                                            <ButtonBase className="arrow-btn next" onClick={() => latest.current.slickNext()}>
                                                <ArrowForwardIosIcon style={{ fontSize: 20 }} />
                                            </ButtonBase>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                        <Sliders ref={(sliders) => (latest.current = sliders)} {...latests}>
                            <div className="p-2 under2k-slide categoryoption2_slider">
                                <div>
                                    <div className="flex justify-between ">
                                        <p className="uppercase items-center p-2 text-white rounded-full categoryoption2_backtitle font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block"> best price</p>

                                        <p className="capitalize text-xs font-bold  m-0">
                                            <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" /> 4.3
                                        </p>
                                    </div>
                                </div>

                                <div className="relative bg-images">
                                    <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/samsung/Samsung%20Galaxy%20Buds%20Pro/Phantom%20Violet/Samsung-Galaxy-Buds-Pro-Phantom-Violet-1.jpg" />
                                </div>
                                <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="capitalize inline-flex items-center font-medium text-sm w-1/2"> Rockerz 600 Kings XI</p>

                                            <p className="capitalize text-xs font-bold p-0 mt-4">
                                                <label class="inline-flex items-center ml-1 category2_textgreen">
                                                    <input type="radio" class="form-radio h-3 w-3 " checked />
                                                </label>
                                                <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                    <input type="radio" class="form-radio h-3 w-3 " checked />
                                                </label>
                                                <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                    <input type="radio" class="form-radio h-3 w-3 " checked />
                                                </label>
                                            </p>
                                        </div>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                    Rs.1,495 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.2,495</span>
                                                </p>

                                                <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 under2k-slide categoryoption2_slider">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="uppercase  items-center p-2 text-white rounded-full categoryoption2_backtitle1 font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block"> hot sale</p>

                                            <p className="capitalize text-xs font-bold  m-0">
                                                <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" /> 4.3
                                            </p>
                                        </div>
                                    </div>
                                    <div className="relative bg-images">
                                        <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/samsung/Samsung%20Galaxy%20Buds%20Pro/Phantom%20Silver/Samsung-Galaxy-Buds-Pro-Phantom-Silver-1.jpg" />
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="capitalize inline-flex items-center font-medium text-sm w-1/2"> Rockerz 600 Kings XI</p>

                                                <p className="capitalize text-xs font-bold p-0 mt-4">
                                                    <label class="inline-flex items-center ml-1 category2_textgreen">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                </p>
                                            </div>
                                            <div>
                                                <div className="flex justify-between ">
                                                    <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                        Rs.1,495 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.2,495</span>
                                                    </p>

                                                    <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div className="p-2 under2k-slide categoryoption2_slider">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="uppercase  items-center p-2 text-white rounded-full categoryoption2_backtitle2 font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block">
                                                {" "}
                                                special discount
                                            </p>

                                            <p className="capitalize text-xs font-bold  m-0">
                                                <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" /> 4.3
                                            </p>
                                        </div>
                                    </div>
                                    <div className="relative bg-images">
                                        <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/samsung/Samsung%20Galaxy%20Buds%20Pro/Phantom%20Black/Samsung-Galaxy-Buds-Pro-Phantom-Black-1.jpg" />
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="capitalize inline-flex items-center font-medium text-sm w-1/2"> Rockerz 600 Kings XI</p>

                                                <p className="capitalize text-xs font-bold p-0 mt-4">
                                                    <label class="inline-flex items-center ml-1 category2_textgreen">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                </p>
                                            </div>
                                            <div>
                                                <div className="flex justify-between ">
                                                    <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                        Rs.1,495 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.2,495</span>
                                                    </p>

                                                    <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 under2k-slide categoryoption2_slider">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="uppercase  items-center p-2 text-white rounded-full categoryoption2_backtitle3 font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block"> price drop</p>

                                            <p className="capitalize text-xs font-bold  m-0">
                                                <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" /> 4.3
                                            </p>
                                        </div>
                                    </div>
                                    <div className="relative bg-images">
                                        <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/Boat/Boat%20Airdopes%20138%20Wireless%20Earbuds/Active%20Black/Boat-Airdopes-138-Wireless-Earbuds-Active-Black-1.jpg" />
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="capitalize inline-flex items-center font-medium text-sm w-1/2"> Rockerz 600 Kings XI</p>

                                                <p className="capitalize text-xs font-bold p-0 mt-4">
                                                    <label class="inline-flex items-center ml-1 category2_textgreen">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                </p>
                                            </div>
                                            <div>
                                                <div className="flex justify-between ">
                                                    <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                        Rs.1,495 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.2,495</span>
                                                    </p>

                                                    <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 under2k-slide categoryoption2_slider">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="uppercase  items-center p-2 text-white rounded-full categoryoption2_backtitle3 font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block"> price drop</p>

                                            <p className="capitalize text-xs font-bold  m-0">
                                                <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" /> 4.3
                                            </p>
                                        </div>
                                    </div>
                                    <div className="relative bg-images">
                                        <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/reailme/Realme%20Buds%20Air%202%20True%20Wireless/Closer%20Black/Realme-Buds-Air-2-True-Wireless-Closer-Black-1.jpg" />
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="capitalize inline-flex items-center font-medium text-sm w-1/2"> Rockerz 600 Kings XI</p>

                                                <p className="capitalize text-xs font-bold p-0 mt-4">
                                                    <label class="inline-flex items-center ml-1 category2_textgreen">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                </p>
                                            </div>
                                            <div>
                                                <div className="flex justify-between ">
                                                    <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                        Rs.1,495 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.2,495</span>
                                                    </p>

                                                    <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 under2k-slide categoryoption2_slider">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="uppercase  items-center p-2 text-white rounded-full categoryoption2_backtitle1 font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block"> best price</p>

                                            <p className="capitalize text-xs font-bold  m-0">
                                                <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" /> 4.3
                                            </p>
                                        </div>
                                    </div>
                                    <div className="relative bg-images">
                                        <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/OnePlus/OnePlus%20Buds%20Overseas%20Version-White/OnePlus-Buds-Overseas-Version-White-1.jpg" />
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="capitalize inline-flex items-center font-medium text-sm w-1/2"> Rockerz 600 Kings XI</p>

                                                <p className="capitalize text-xs font-bold p-0 mt-4">
                                                    <label class="inline-flex items-center ml-1 category2_textgreen">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                </p>
                                            </div>
                                            <div>
                                                <div className="flex justify-between ">
                                                    <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                        Rs.1,495 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.2,495</span>
                                                    </p>

                                                    <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Sliders>
                    </div>

                    <div className="categoryoption2_brandhead pt-8 pb-8">
                        <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={12}>
                                <div className="text-center">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-lg md:text-lg lg:text-lg text-lg italic brand_heads"> our series</p>
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-3xl md:text-3xl lg:text-3xl text-lg italic brand_heads"> choose in one style</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                        <div className="pb-3">
                            <Sliders {...brandslider}>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand1.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand2.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand3.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand4.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand5.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand6.png" />
                                    </p>
                                </div>
                            </Sliders>
                        </div>
                    </div>
                    <div className="mt-5 mb-5">
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="https://s1.poorvikamobile.com/image/data/Banner/Middle_banner/middle/Watch-series-6-available-at-poorvika.jpg" className="rounded" />
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="https://s1.poorvikamobile.com/image/data/Banner/Middle_banner/middle/Philips-Grooming-gadgets-available-at-poorvika.jpg" className="rounded" />
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="https://s1.poorvikamobile.com/image/data/Banner/Middle_banner/middle/Sony-Festive-sale-offer%20at%20poorvika.jpg" className="rounded" />
                                </div>
                            </Grid>
                        </Grid>
                    </div>

                    <div className="mt-5 mb-5">
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={3}>
                                <div>
                                <div className="p-2 categoryoption_image categoryoption2_slider">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="uppercase  items-center p-2 text-white rounded-full categoryoption2_backtitle3 font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block"> price drop</p>

                                            <p className="capitalize text-xs font-bold  m-0">
                                                <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" /> 3.6
                                            </p>
                                        </div>
                                    </div>
                                    <div className="relative">
                                        <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/Fingers%20/Fingers%20Rock-n-Roll%20Lounge%20Boom%20Headset%20/Rich%20Black%20Wine/Fingers-Rock-n-Roll-Lounge-Boom-Headset-Rich-Black+Wine-1.jpg" />
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="capitalize inline-flex items-center font-medium text-sm w-1/2">oopo reno free true wireless headphone</p>

                                                <p className="capitalize text-xs font-bold p-0 mt-4">
                                                    <label class="inline-flex items-center ml-1 category2_textgreen">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                </p>
                                            </div>
                                            <div>
                                                <div className="flex justify-between ">
                                                    <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                        Rs.5,990 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.9,990</span>
                                                    </p>

                                                    <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </Grid>
                            <Grid item xs={3}>
                                <div>
                                <div className="p-2 categoryoption_image categoryoption2_slider">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="uppercase  items-center p-2 text-white rounded-full categoryoption2_backtitle1 font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block"> price drop</p>

                                            <p className="capitalize text-xs font-bold  m-0">
                                                <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" />3.5
                                            </p>
                                        </div>
                                    </div>
                                    <div className="relative ">
                                        <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/Boat/Boat%20Rockerz%20510%20(Bluetooth)%20Boom%20Headset/new/Rockerz%20510%20Boom%20Headset%20-shop%20online-nearby%20poorvika%20stores-%202.webp" />
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="capitalize inline-flex items-center font-medium text-sm w-1/2">oopo reno free true wireless headphone</p>

                                                <p className="capitalize text-xs font-bold p-0 mt-4">
                                                    <label class="inline-flex items-center ml-1 category2_textgreen">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                </p>
                                            </div>
                                            <div>
                                                <div className="flex justify-between ">
                                                    <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                        Rs.5,990 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.9,990</span>
                                                    </p>

                                                    <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </Grid>
                            <Grid item xs={3}>
                                <div>
                                <div className="p-2 categoryoption_image categoryoption2_slider">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="uppercase  items-center p-2 text-white rounded-full categoryoption2_backtitle2 font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block"> price drop</p>

                                            <p className="capitalize text-xs font-bold  m-0">
                                                <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" /> 3.9
                                            </p>
                                        </div>
                                    </div>
                                    <div className="relative ">
                                        <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/Beats/Beats%20Studio3-The%20Beats%20Decade%20Collection%20Wireless%20Headphone%20Midnight%20Black/Beats-Studio3-The-Beats-Decade-Collection-Wireless-Headphone-Midnight-Black-1.jpg" />
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="capitalize inline-flex items-center font-medium text-sm w-1/2"> oopo reno free true wireless headphone</p>

                                                <p className="capitalize text-xs font-bold p-0 mt-4">
                                                    <label class="inline-flex items-center ml-1 category2_textgreen">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                </p>
                                            </div>
                                            <div>
                                                <div className="flex justify-between ">
                                                    <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                        Rs.5,990 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.9,990</span>
                                                    </p>

                                                    <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </Grid>

                            <Grid item xs={3}>
                            <div>
                                <div className="p-2 categoryoption_image categoryoption2_slider">
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="uppercase  items-center p-2 text-white rounded-full categoryoption2_backtitle font-bold text-xs overflow-hidden whitespace-nowrap overflow-ellipsis inline-block"> price drop</p>

                                            <p className="capitalize text-xs font-bold  m-0">
                                                <StarIcon className="capitalize font-bold m-2 mt-1 mr-0 brand_staricon" /> 3.7
                                            </p>
                                        </div>
                                    </div>
                                    <div className="relative">
                                        <img src="https://s1.poorvikamobile.com/image/data/AAA%20Acc/Boat/Rockerz%20410%20Boom%20Headset/boat-rockerz-410-boom-headset-4166.webp" />
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="capitalize inline-flex items-center font-medium text-sm w-1/2"> oopo reno free true wireless headphone</p>

                                                <p className="capitalize text-xs font-bold p-0 mt-4">
                                                    <label class="inline-flex items-center ml-1 category2_textgreen">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen1">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                    <label class="inline-flex items-center ml-1 category2_textgreen2">
                                                        <input type="radio" class="form-radio h-3 w-3 " checked />
                                                    </label>
                                                </p>
                                            </div>
                                            <div>
                                                <div className="flex justify-between ">
                                                    <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-xs">
                                                        Rs.5,990 <span className="items-center p-0 ml-3 text-gray-400 text-xs">Rs.9,990</span>
                                                    </p>

                                                    <p className="capitalize text-xs font-bold p-1 m-1 categoryoption2_coloroffer"> (40% off)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </Grid>
                        </Grid>
                    </div>
                    <div className="categoryoption2_brandhead pb-6">
                        <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={12}>
                                <div className="text-center">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-lg md:text-lg lg:text-lg text-lg italic brand_heads"> offer</p>
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-3xl md:text-3xl lg:text-3xl text-lg italic brand_heads"> special discount sale</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={12} sm={6} md={6} lg={6}>
                                <div className="p-2">
                                    <img src="https://cdna.artstation.com/p/assets/images/images/031/534/448/large/murugendra-hiremath-11111.jpg?1603893338" className="rounded-2xl pr-1 pl-1 m-auto" />
                                </div>
                            </Grid>

                            <Grid item xs={12} sm={6} md={6} lg={6}>
                                <div className="ml-12">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-3xl md:text-3xl lg:text-3xl text-lg italic brand_heads w-1/2">
                                            {" "}
                                            headphones klm <span className="text-green-400">+</span> leather case <span className="text-green-400">+</span> free delivery
                                        </p>
                                    </div>
                                    <div>
                                        <div className="flex justify-between p-2">
                                            <p className="flex-grow capitalize inline-flex items-center p-0 font-bold text-3xl">
                                                Rs.34,999 <span className="items-center p-0 ml-3 mt-1 text-gray-400 text-xl">Rs.40,000</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="App">
                                        <CountdownCircleTimer {...timerProps} colors={[["#7E2E84"]]} duration={daysDuration} initialRemainingTime={remainingTime}>
                                            {({ elapsedTime }) => renderTime("days", getTimeDays(daysDuration - elapsedTime))}
                                        </CountdownCircleTimer>
                                        <CountdownCircleTimer
                                            {...timerProps}
                                            colors={[["#D14081"]]}
                                            duration={daySeconds}
                                            initialRemainingTime={remainingTime % daySeconds}
                                            onComplete={(totalElapsedTime) => [remainingTime - totalElapsedTime > hourSeconds]}
                                        >
                                            {({ elapsedTime }) => renderTime("hours", getTimeHours(daySeconds - elapsedTime))}
                                        </CountdownCircleTimer>
                                        <CountdownCircleTimer
                                            {...timerProps}
                                            colors={[["#EF798A"]]}
                                            duration={hourSeconds}
                                            initialRemainingTime={remainingTime % hourSeconds}
                                            onComplete={(totalElapsedTime) => [remainingTime - totalElapsedTime > minuteSeconds]}
                                        >
                                            {({ elapsedTime }) => renderTime("minutes", getTimeMinutes(hourSeconds - elapsedTime))}
                                        </CountdownCircleTimer>
                                        <CountdownCircleTimer
                                            {...timerProps}
                                            colors={[["#218380"]]}
                                            duration={minuteSeconds}
                                            initialRemainingTime={remainingTime % minuteSeconds}
                                            onComplete={(totalElapsedTime) => [remainingTime - totalElapsedTime > 0]}
                                        >
                                            {({ elapsedTime }) => renderTime("seconds", getTimeSeconds(elapsedTime))}
                                        </CountdownCircleTimer>
                                    </div>

                                    <div className="pt-10 text-gray-500 text-base text-justify">
                                        <p>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                            type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                                        </p>
                                    </div>
                                    <div className="pt-10">
                                        <button
                                            className="bg-green-500 text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-12 py-5 rounded-full shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                            type="button"
                                        >
                                            learn more
                                        </button>
                                        <button
                                            className="bg-black text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-12 py-5  rounded-full shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                            type="button"
                                        >
                                            add to cart
                                        </button>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                    </div>

                    <div>
                    <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={12}>
                                <div className="text-center">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-lg md:text-lg lg:text-lg text-lg italic brand_heads"> choose</p>
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-3xl md:text-3xl lg:text-3xl text-lg italic brand_heads"> the spotlight</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                    
                    <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={1}>

                            </Grid>
                            <Grid item xs={2}>
                                <p className="categoryoption2_spotlight"><FontAwesomeIcon className="text-white text-5xl text-gray-200 mt-6 ml-8" icon="volume-mute" />
</p>
<p className="capitalize text-base text-black font-medium text-center p-2">noise cancellation</p>
<p className="capitalize text-sm font-medium text-center p-0 text-blue-400">view more <ArrowForwardIosIcon className="categoryoption2_spotlighticon" /></p>
                            </Grid>
                            <Grid item xs={2}>
                              <p className="categoryoption2_spotlight"><FontAwesomeIcon className="text-white text-5xl text-gray-200 mt-6 ml-8" icon="tint" />
</p>
<p className="capitalize text-base text-black font-medium text-center p-2">sweat proof</p>
<p className="capitalize text-sm font-medium text-center p-0 text-blue-400">view more <ArrowForwardIosIcon className="categoryoption2_spotlighticon" /></p>
                            
                            </Grid>
                            <Grid item xs={2}>
                                <p className="categoryoption2_spotlight"><FontAwesomeIcon className="text-white text-5xl text-gray-200 mt-6 ml-8" icon={["fab", "bluetooth-b"]} />
</p>
<p className="capitalize text-base text-black font-medium text-center p-2">bluetooth</p>
<p className="capitalize text-sm font-medium text-center p-0 text-blue-400">view more <ArrowForwardIosIcon className="categoryoption2_spotlighticon" /></p>
                            
                            </Grid>
                            <Grid item xs={2}>
                                <p className="categoryoption2_spotlight"><FontAwesomeIcon className="text-white text-5xl text-gray-200 mt-6 ml-6" icon="battery-full" />
</p>
<p className="capitalize text-base text-black font-medium text-center p-2">battery</p>
<p className="capitalize text-sm font-medium text-center p-0 text-blue-400">view more <ArrowForwardIosIcon className="categoryoption2_spotlighticon" /></p>
                            
                            </Grid>
                            <Grid item xs={2}>
                                <p className="categoryoption2_spotlight"><FontAwesomeIcon className="text-white text-5xl text-gray-200 mt-6 ml-8" icon="microphone" />
</p>
<p className="capitalize text-base text-black font-medium text-center p-2">microphone</p>
<p className="capitalize text-sm font-medium text-center p-0 text-blue-400">view more <ArrowForwardIosIcon className="categoryoption2_spotlighticon" /></p>
                            
                            </Grid>
                            <Grid item xs={1}>
                              
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                     <Grid item xs={12}>
                               <img src="../brand/category-ban.png" className="w-full rounded-3xl p-2" />
                            </Grid>
                    </div>
                    <div>
                    </div>
                    <div className="mt-5 mb-5">
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="https://s1.poorvikamobile.com/image/data/Banner/Middle_banner/Sandisk/Sandisk-Festive-offer-in-Poorvika.jpg" className="rounded" />
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="https://s1.poorvikamobile.com/image/data/Banner/Middle_banner/middle/Feedback-Contest-Participate-Win.jpg" className="rounded" />
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="https://s1.poorvikamobile.com/image/data/Banner/Middle_banner/April/14/Oneplus%209%20series%205G%20availalble%20at%20Poorvika.jpg" className="rounded" />
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                    <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={12}>
                                <div className="text-center">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-lg md:text-lg lg:text-lg text-lg italic brand_heads"> budget</p>
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-3xl md:text-3xl lg:text-3xl text-lg italic brand_heads"> which fits your pocket</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                    
                    <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={1}>

                            </Grid>
                            <Grid item xs={2}>
                                <div className="mb-12">
                                    <div className="category_serviceBox">
                                        <div className="category_service-icon pl-3">
                                           <p className="text-sm font-medium uppercase  text-white text-center pt-8 pb-0"> upto</p>
                                            <p className="text-sm font-medium uppercase  text-white text-center  pb-2"> rs.1,000</p> 
                                        </div>
                                    </div>
                                </div></Grid>
                            <Grid item xs={2}>
                             <div className="mb-12">
                                    <div className="category_serviceBox">
                                        <div className="category_service-icon pl-3">
                                            
                                            <p className="text-sm font-medium uppercase  text-white text-center pt-8 pb-0"> rs.1,000</p>
                                             <p className="text-xs font-medium uppercase  text-white text-center  pb-0"> to</p>
                                            <p className="text-sm font-medium uppercase  text-white text-center  pb-2"> rs.2,000</p>
                                        </div>
                                    </div>
                                </div> </Grid>
                            <Grid item xs={2}>
                                <div className="mb-12">
                                    <div className="category_serviceBox">
                                        <div className="category_service-icon pl-3">
                                            
                                            <p className="text-sm font-medium uppercase  text-white text-center pt-8 pb-0"> rs.2,000</p>
                                             <p className="text-xs font-medium uppercase  text-white text-center  pb-0"> to</p>
                                            <p className="text-sm font-medium uppercase  text-white text-center  pb-2"> rs.5,000</p>
                                        </div>
                                    </div>
                                </div></Grid>
                            <Grid item xs={2}>
                               <div className="mb-12">
                                    <div className="category_serviceBox">
                                        <div className="category_service-icon pl-3">
                                            
                                            <p className="text-sm font-medium uppercase  text-white text-center pt-8 pb-0"> rs.5,000</p>
                                             <p className="text-xs font-medium uppercase  text-white text-center  pb-0"> to</p>
                                            <p className="text-sm font-medium uppercase  text-white text-center  pb-2"> rs.10,000</p>
                                        </div>
                                    </div>
                                </div></Grid>
                            <Grid item xs={2}>
                               <div className="mb-12">
                                    <div className="category_serviceBox">
                                        <div className="category_service-icon pl-3">
                                            
                                            <p className="text-sm font-medium uppercase  text-white text-center pt-8 pb-0"> rs.10,000</p>
                                             <p className="text-xs font-medium uppercase  text-white text-center  pb-0"> &</p>
                                            <p className="text-sm font-medium uppercase  text-white text-center  pb-2"> above</p>
                                        </div>
                                    </div>
                                </div></Grid>
                            <Grid item xs={1}>
                              
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                     <Grid item xs={12}>
                               <img src="https://www.cycelectronica.com/media/banner_audifonos1-1000.jpg" className="w-full rounded-3xl p-2" />
                            </Grid>
                    </div>
                    
                    <div className="categoryoption2_brandhead pt-8 pb-8">
                        <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={12}>
                                <div className="text-center">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-lg md:text-lg lg:text-lg text-lg italic brand_heads"> our series</p>
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-3xl md:text-3xl lg:text-3xl text-lg italic brand_heads"> choose in one style</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                        <div className="pb-3">
                            <Sliders {...brandslider}>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand1.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand2.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand3.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand4.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand5.png" />
                                    </p>
                                </div>
                                <div className="pr-1 pl-1">
                                    <p className="bg-white rounded-2xl">
                                        <img src="../brand/brand6.png" />
                                    </p>
                                </div>
                            </Sliders>
                        </div>
                    </div>
                    <div className="mt-5 mb-5">
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={6}>
                                <div className="p-2">
                                    <img src="https://i.pinimg.com/originals/2f/b3/b3/2fb3b315173ac39d9bad5751f00bfe36.jpg" className="rounded" />
                                </div>
                            </Grid>
                            <Grid item xs={6}>
                                <div className="p-2">
                                    <img src="https://cdn.mos.cms.futurecdn.net/XoNd9Vgj5xRCtGdhshnJt8.jpg" className="rounded" />
                                </div>
                            </Grid>
                            
                        </Grid>
                    </div>
                </div>
            </div>
        </div>
    );
}

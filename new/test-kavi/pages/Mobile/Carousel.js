import React, { useState, useRef } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

    // <div className="mx-2 ...">
    //   <button disabled={currentIndex === 0} onClick={prevCallback} className="previus-button text-bg-gray margin-2 padding-10">
    //     <ArrowBackIosIcon />
    //   </button>
    //   <button disabled={currentIndex > cycleDots} onClick={nextCallback} className="next-button text-bg-gray margin-2 padding-10">
    //   <ArrowForwardIosIcon />
    //   </button>
    // </div>

const Carousel1 = () => {

  const similarProductSlider = useRef();
  var similarProducts = {
    dots: false,
    autoplay: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },

    {
      breakpoint: 476,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};
const next = () => {
  slider.slickNext();
};
const previous = () => {
  slider.slickPrev();
};
  return (
    <div className="similar-products bg-white">
      {/*  Slider */}
      <Tabs>
      <h2 className="flex py-4 px-3 border-b border-gray-300 text-black text-md md:text-2xl font-bold">Compare similar Products 
      <TabList>
            <ul className="md:flex similar-list break-normal text-sm pb-2 pt-2 text-gray-500">
                        <li className="text-green-500 mx-5 px-2">Comparing:</li>
                        <Tab><span className="mx-2 px-2 py-1 border border-gray-300 rounded-lg">Front Camera</span></Tab>
                        <Tab><span className="mx-2 px-2 py-1 border border-gray-300 rounded-lg">Main Camera</span></Tab>
                        <Tab><span className="mx-2 px-2 py-1 border border-gray-300 rounded-lg">RAM</span></Tab>
                        <Tab><span className="mx-2 px-2 py-1 border border-gray-300 rounded-lg">Battery</span></Tab>
                        <Tab><span className="mx-2 px-2 py-1 border border-gray-300 rounded-lg">Performance</span></Tab>
                        <Tab><span className="mx-2 px-2 py-1 border border-gray-300 rounded-lg">price</span></Tab>

                    </ul> 
                    </TabList>
                    <span className="text-xs pt-3 text-blue-600 invisible md:visible"> See all</span>
                    <span className="right-2 absolute">  <div className="slider-arrow">
                    <button  onClick={() => similarProductSlider.current.slickPrev()} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button  onClick={() => similarProductSlider.current.slickNext()} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
                                </div></span>
                    
                    </h2>
                    <TabPanel>
    <Slider ref={(slider) => (similarProductSlider.current = slider)} {...similarProducts}>
      
        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-red-600 pt-1 pr-3"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pt-1 pr-3"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-red-600 pt-1 pr-3"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pt-1 pr-3"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-red-600 pt-1 pr-3"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pt-1 pr-3"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-red-600 pt-1 pr-3"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pt-1 pr-3"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-red-600 pt-1 pr-3"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pt-1 pr-3"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-red-600 pt-1 pr-3"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pt-1 pr-3"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-red-600 pt-1 pr-3"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pt-1 pr-3"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3 iconli">
          <li><span className="text-red-600 pt-1 pr-3"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pt-1 pr-3"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pt-1 pr-3"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
             
             
             
        {/* })} */}
      </Slider>
      </TabPanel>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
            </Tabs>
    </div>
  );
};

export default Carousel1;

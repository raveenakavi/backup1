import Grid from '@material-ui/core/Grid';
import Slider from "react-slick";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Checkbox from '@material-ui/core/Checkbox';
import StarIcon from '@material-ui/icons/Star';
export default function Trendingvideo() {

    var settings = {

        infinite: true,
        speed: 500,
        slidesToShow:3,
        autoplay: false,
        slidesToScroll: 1,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };



    return (
       
     <div>
         <div className="slider-fl-wrp _6t1WkM bg-white">

<Grid container className="sm-grid">
  



  <Grid item xs={12} className="p-2">
  <div className="modtitle flex justify-between items-center">
        <h3 className="font-semibold text-2xl">Trending Product </h3>
       
        <p>icon</p>
      </div>

      <div className="sm1-slider">
       
          <Slider {...settings}>


                <div className="trending-wrp p-2">
                <div className="product-video-container">

                <a href="#"><iframe  className="rounded-2xl" width="100%" src="https://www.youtube.com/embed/NXB4up9Cixc?autoplay=0&amp;mute=0&amp;controls=1&amp;origin=http%3A%2F%2F172.105.58.32%3A4000&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;iv_load_policy=3&amp;modestbranding=1&amp;enablejsapi=1&amp;widgetid=9"></iframe></a>
                
                <div className="video-home-content mt-2 flex">
                     <div className="may-wrp text-center">
                         <img src="asset/vicon.png"></img>
                     </div>
                     <div className="video-inner-con ml-3 w-full">
                        <h6 className="text-sm font-medium">Best 5000 man Battery Smartphone </h6>
                        <p  className="text-xs mt-0">Top 10 SmartPhone</p>

                        <div className="flex justify-between items-center mt-1">
                            <span className="text-xs">Above <StarIcon className="vidstar"/> </span>
                            <span className="text-xs">Starting price Rs.13,000</span>
                        </div>
                     </div>
                     
                </div>
                </div>
                </div>




                <div className="trending-wrp p-2">
                <div className="product-video-container">

                <a href="#"><iframe  className="rounded-2xl" width="100%" src="https://www.youtube.com/embed/NXB4up9Cixc?autoplay=0&amp;mute=0&amp;controls=1&amp;origin=http%3A%2F%2F172.105.58.32%3A4000&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;iv_load_policy=3&amp;modestbranding=1&amp;enablejsapi=1&amp;widgetid=9"></iframe></a>
                
                <div className="video-home-content mt-2 flex">
                     <div className="may-wrp text-center">
                         <img src="asset/vicon.png"></img>
                     </div>
                     <div className="video-inner-con ml-3 w-full">
                        <h6 className="text-sm font-medium">Best 5000 man Battery Smartphone </h6>
                        <p  className="text-xs mt-0">Top 10 SmartPhone</p>

                        <div className="flex justify-between items-center mt-1">
                            <span className="text-xs">Above <StarIcon className="vidstar"/> </span>
                            <span className="text-xs">Starting price Rs.13,000</span>
                        </div>
                     </div>
                     
                </div>
                </div>
                </div>


                <div className="trending-wrp p-2">
                <div className="product-video-container">

                <a href="#"><iframe  className="rounded-2xl" width="100%" src="https://www.youtube.com/embed/NXB4up9Cixc?autoplay=0&amp;mute=0&amp;controls=1&amp;origin=http%3A%2F%2F172.105.58.32%3A4000&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;iv_load_policy=3&amp;modestbranding=1&amp;enablejsapi=1&amp;widgetid=9"></iframe></a>
                
                <div className="video-home-content mt-2 flex">
                     <div className="may-wrp text-center">
                         <img src="asset/vicon.png"></img>
                     </div>
                     <div className="video-inner-con ml-3 w-full">
                        <h6 className="text-sm font-medium">Best 5000 man Battery Smartphone </h6>
                        <p  className="text-xs mt-0">Top 10 SmartPhone</p>

                        <div className="flex justify-between items-center mt-1">
                            <span className="text-xs">Above <StarIcon className="vidstar"/> </span>
                            <span className="text-xs">Starting price Rs.13,000</span>
                        </div>
                     </div>
                     
                </div>
                </div>
                </div>


                <div className="trending-wrp p-2">
                <div className="product-video-container">

                <a href="#"><iframe  className="rounded-2xl" width="100%" src="https://www.youtube.com/embed/NXB4up9Cixc?autoplay=0&amp;mute=0&amp;controls=1&amp;origin=http%3A%2F%2F172.105.58.32%3A4000&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;iv_load_policy=3&amp;modestbranding=1&amp;enablejsapi=1&amp;widgetid=9"></iframe></a>
                
                <div className="video-home-content mt-2 flex">
                     <div className="may-wrp text-center">
                         <img src="asset/vicon.png"></img>
                     </div>
                     <div className="video-inner-con ml-3 w-full">
                        <h6 className="text-sm font-medium">Best 5000 man Battery Smartphone </h6>
                        <p  className="text-xs mt-0">Top 10 SmartPhone</p>

                        <div className="flex justify-between items-center mt-1">
                            <span className="text-xs">Above <StarIcon className="vidstar"/> </span>
                            <span className="text-xs">Starting price Rs.13,000</span>
                        </div>
                     </div>
                     
                </div>
                </div>
                </div>


                <div className="trending-wrp p-2">
                <div className="product-video-container">

                <a href="#"><iframe  className="rounded-2xl" width="100%" src="https://www.youtube.com/embed/NXB4up9Cixc?autoplay=0&amp;mute=0&amp;controls=1&amp;origin=http%3A%2F%2F172.105.58.32%3A4000&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;iv_load_policy=3&amp;modestbranding=1&amp;enablejsapi=1&amp;widgetid=9"></iframe></a>
                
                <div className="video-home-content mt-2 flex">
                     <div className="may-wrp text-center">
                         <img src="asset/vicon.png"></img>
                     </div>
                     <div className="video-inner-con ml-3 w-full">
                        <h6 className="text-sm font-medium">Best 5000 man Battery Smartphone </h6>
                        <p  className="text-xs mt-0">Top 10 SmartPhone</p>

                        <div className="flex justify-between items-center mt-1">
                            <span className="text-xs">Above <StarIcon className="vidstar"/> </span>
                            <span className="text-xs">Starting price Rs.13,000</span>
                        </div>
                     </div>
                     
                </div>
                </div>
                </div>

               

          </Slider>
     




      </div>
  
  </Grid>
</Grid>



</div>
     </div>
       
    )
}

import React from 'react'
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import Grid from '@material-ui/core/Grid';

export default function stripbanner() {
    return (
        <div className="_6t1WkM bg-white">
             <div className="mapwrp">
                <div className="ms-Grid" dir="ltr">
                <Grid container>  
                    <Grid item xs={4} className="res-pad">
                        <div>
                        <AliceCarousel autoPlay autoPlayInterval={2000} autoPlayControls infinite >
                        <img src="asset/strip1.png" className="sliderimg shadow border w-full"/>
                        <img src="asset/strip1.png" className="sliderimg shadow border w-full"/>
                        
                        </AliceCarousel>
                        </div>
                        <div className="pt-2">
                        <AliceCarousel autoPlay autoPlayInterval={2000} autoPlayControls infinite>
                       
                            <img src="asset/strip2.png" className="sliderimg shadow border w-full"/>
                            <img src="asset/strip2.png" className="sliderimg shadow border w-full"/>

                        </AliceCarousel>
                        </div>
                    </Grid>  


                    <Grid item xs={4} className="res-pad pl-2 pr-2">
                    <div >
                    <AliceCarousel autoPlay autoPlayInterval={3000} autoPlayControls infinite >
                    
                        <img src="asset/strip5.png" className="sliderimg shadow border w-full"/>
                        <img src="asset/strip5.png" className="sliderimg shadow border w-full"/>
                    </AliceCarousel>
                    </div>
                    </Grid>  

                    <Grid item xs={4} className="res-pad">
                    <div>
                    <AliceCarousel autoPlay autoPlayInterval={1000} autoPlayControls infinite >
                   
                        <img src="asset/strip3.png" className="sliderimg shadow border w-full"/>
                        <img src="asset/strip3.png" className="sliderimg shadow border w-full"/>
                       
                    </AliceCarousel>
                    </div>
                    <div className="pt-2">
                    <AliceCarousel  autoPlay autoPlayInterval={1000}  autoPlayControls infinite>
                        <img src="asset/strip4.png" className="sliderimg shadow border w-full"/>
                        <img src="asset/strip4.png" className="sliderimg shadow border w-full"/>
                       
                    </AliceCarousel>
                    </div>
                    </Grid>
                    </Grid>  
        </div>
                    </div>
        </div>
    )
}

import React from 'react'
import Banner from './banner';
import Strip from './stripbanner';
import Smartslider from './smart-slider';
import Addbanner from './addbanner';
import Treadingvideo from './trending-video';
import Advertisement from './advertisement';
import Freeshipping from './freeshipping';



export default function index() {
    return (
        <div>
         
            <Banner/>
            <Strip/>
            <Smartslider/>
            <Freeshipping/>
            <Addbanner/>
            <Treadingvideo/>
            <Advertisement/>
            
        </div>
    )
}

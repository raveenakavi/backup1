import React from 'react';
import { Grade } from '@material-ui/icons';
import CloseIcon from '@material-ui/icons/Close';

export default function Accessories() {
    return (
        <div className="block">
           <div className="accessories rounded-t-lg w-full p-5">
            <h2 className="py-4 px-3 border-b border-gray-300 text-yellow-500 text-md md:text-2xl font-bold">Buy an accessory with this <span className="text-xs right-10 absolute pt-2"> View All Options</span></h2>
            <div className="grid md:grid-flow-col">
                <div className="bg-white shadow-md my-5 mx-1 p-2 flex md:block">
                <div className="flex">
                <img src={'/asset/accessories-1.png'} className="md:h-48" />
                </div>
                <div className="pt-2 pl-2 relative">
                    <h3 className="text-black text-md">Apple iphone 11 pro </h3>
                    <p className="font-normal text-gray-400">(Green, 128 GB)</p>
                    <p className="text-black pt-4">Rs.62,899 <span className="right-0 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
                    <p className="py-2 text-sm"><span className="text-xs text-yellow-600 font-medium">Save Rs.11, 427, (4% OFF)</span></p>
                </div>
                
                </div>
                <div className="bg-white shadow-md my-5 mx-1 p-2 flex md:block">
                <div className="flex">
                <span className="text-xs text-red-400 font-bold"><CloseIcon /></span>
                <img src={'/asset/accessories-2.png'} className="md:h-48" />
                </div>
                <div className="pt-2 pl-2 relative">
                    <h3 className="text-black text-md">Transparent back Cover Clear</h3>
                    <p className="font-normal text-gray-400">(Green, 128 GB)</p>
                    <p className="text-black pt-4 md:text-center">Rs.62,899 </p>
                    <p className="md:text-center text-green-400 py-2 text-sm"><span className="text-xs text-yellow-600 font-medium">Buy Together for </span>Rs.999</p>
                </div>
                </div>
                <div className="bg-white shadow-md my-5 mx-1 p-2 flex md:block">
                <div className="flex">
                <span className="text-xs text-red-400 font-bold"><CloseIcon /></span>
                <img src={'/asset/accessories-3.png'} className="md:h-48" />
                </div>
                <div className="pt-2 pl-2 relative">
                    <h3 className="text-black text-md">Apple Airpods pro </h3>
                    <p className="font-normal text-gray-400">(Green, 128 GB)</p>
                    <p className="text-black pt-4 md:text-center">Rs.62,899</p>
                    <p className="md:text-center py-2 pl-2 text-sm"><span className="text-xs text-yellow-600 font-medium">Save Rs.11, 427, (4% OFF)</span></p>
                </div>
                </div>

                <div className="bg-white shadow-md my-5 mx-1 p-2  px-8 relative">
                    <table className="w-full relative leading-10 text-sm">
                        <tr>
                            <td className="w-1/2">3 Items Total :</td>
                            <td className="right-0 absolute">1,19,249</td>
                        </tr>
                        <tr>
                            <td>Buy Together :</td>
                            <td className="right-0 absolute">3, 889</td>
                        </tr>
                        <tr className="text-red-500">
                            <td>Discount :</td>
                            <td className="right-0 absolute">3, 889</td>
                        </tr>
                        <tr className="border-t border-b border-gray-300 pt-10">
                            <td>To be Paid :</td>
                            <td className=" right-0 absolute">1,23,128</td>
                        </tr>
                    </table>
                    <button className="w-full my-16 bottom-0 rounded-full protection-button px-10 py-2 my-2 text-white text-xs">Add 3 items to cart</button>

                </div>

                

            </div>
        </div>
        <h5 className="accessories-bg text-center rounded-b-lg py-3">Buy all 3 items together &nbsp; get <b>FLAT 2% OFF</b> on your next Purchase of any Apple devices</h5>
        </div>
    )
}

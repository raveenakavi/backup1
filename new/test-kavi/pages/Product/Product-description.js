import React from 'react'

export default function Productdescription() {
    return (
        <div className="Productdescription md:flex justify-evenly">
            <div className="description-wrp justify-center text-center">
                <h1 class="text-6xl">Lead with Speed</h1>
                <p>From  ₹44,999</p>
            </div>
            <div className="section-kv__text bgwrp">
            <img src="../asset/diskbg.png"></img>
            </div>
        </div>
    )
}

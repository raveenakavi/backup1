import React, { useState, useRef, useCallback } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { render } from 'react-dom';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';

const img1 = '/asset/similar-1.png'
const img2 = '/asset/Frequent-2.png'
const img3 = '/asset/Frequent-3.png'
const img4 = '/asset/Frequent-4.png'

const ImageToggleOnMouseOver = ({primaryImg, secondaryImg}) => {
  const imageRef = useRef(null);
  const [divShow, setDivShow] = useState(false);
  const [divHide, setDivHide] = useState(false);
  return (
    <img 
      onMouseOver={() => {
        imageRef.current.src = secondaryImg;
        setDivShow(false);
    setDivHide(true);
      }}
      onMouseOut={() => {
        imageRef.current.src= primaryImg;
        setDivShow(true);
    setDivHide(false);
      }}
      src={primaryImg} 
      alt=""
      ref={imageRef}
    />
  )
}

const Trending = () => {

  const frequentCarouselSlider = useRef();
  var frequentCarousel = {
    dots: false,
    autoplay: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },

    {
      breakpoint: 476,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};
const next = () => {
  slider.slickNext();
};
const previous = () => {
  slider.slickPrev();
};

  const StyledRating = withStyles({
    iconFilled: {
      color: '#f11a08',
    },
    iconHover: {
      color: '#ff3d47',
    },
  })(Rating);
//   const  [toggleHeart, setToggleHeart] = useState(false)
    
//   changeColor = useCallback(() =>{
//    setToggleHeart(!toggleHeart)
//   },[])
//   <FavoriteIcon className={
//           toggleHeart ? 'heart active' : 'heart'
//         } onClick={changeColor}/>

const [divShow, setDivShow] = useState(false);
const [divHide, setDivHide] = useState(false);

const[add,minus]=useState(true);

const selectChange = (element) =>{
  if(element == add){
    setDivShow(false);
    setDivHide(true);
  }else if(element==minus){
    setDivShow(true);
    setDivHide(false);
  }
}
    
  return (
    <div className="frequent app similar-products bg-white">
      {/*  Slider */}

      <h2 className="flex py-4 px-3 border-b border-gray-300 text-black text-md md:text-2xl font-bold">Frequently Bought Together
            <ul className="md:flex similar-list break-normal text-sm pb-2 pt-2 text-gray-500 ml-12">
                        <li className="text-green-500 mx-5 px-2 border border-gray-300 rounded-lg">ALL</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Cases &nbsp; Covers</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">AirPods</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">iWatch</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Pouches</li>
                    </ul> 
                    <span className="text-xs pt-3 text-blue-600 invisible md:visible"> See all</span>
                    <span className="right-2 absolute">
                    <div className="slider-arrow">
                    <button  onClick={() => frequentCarouselSlider.current.slickPrev()} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button  onClick={() => frequentCarouselSlider.current.slickNext()} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
                                </div>
                                </span>
                    
                    </h2>
      <Slider ref={(slider) => (frequentCarouselSlider.current = slider)} {...frequentCarousel} onChange="selectChange">

        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="px-2 element" id="surround">
        <div className="flex">
        <iframe className="rounded-xl" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-4 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
        <iframe className="rounded-xl" width="360" height="215" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-4 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>

  <div className="px-2 element" id="surround">
        <div className="flex">
        <iframe className="rounded-xl" width="360" height="215" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-4 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
        <iframe className="rounded-xl" width="360" height="215" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-4 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
        <iframe className="rounded-xl" width="360" height="215" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-4 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
        <iframe className="rounded-xl" width="360" height="215" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-4 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
        {/* })} */}
      </Slider>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
    </div>
  );
};

export default Trending;

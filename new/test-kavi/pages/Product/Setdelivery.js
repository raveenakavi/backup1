import { Box } from '@material-ui/core'
import React from 'react'
import RoomIcon from '@material-ui/icons/Room';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


function DeliveryInfo() {
    return (
        <div className="wrapper mt-2">
            <Box  mx="auto"  className="flex justify-center">
               
                <div  className="container">
                    <Box  className="item-wrapper flex" textAlign="center">
                        <Box className="flex-item del_address items-center text-center">
                            <div className="del-box bg-white text-left">
                                <h4 className="-mt-0">Delivering to:</h4>
                                <FormControl className="inpu-wrap">

                                    <Input
                                    className="address_input"
                                        id="input-with-icon-adornment"
                                        startAdornment={
                                            <InputAdornment position="start">
                                                <RoomIcon />
                                            </InputAdornment>
                                        }
                                        endAdornment={<InputAdornment position="end"> <Box className="change_address cursor-pointer" fontWeight="500">Change</Box></InputAdornment>}
                                        value="M.K.N Road,Alandur"
                                    />
                                   
                                </FormControl>
                                <div className="input-note flex">
                                <p className="font-semibold gre"> In Stock</p>
                                <p className="ml-2 text-gray-400"> Poorvika Assured </p>
                                </div>
                            </div>
                            <h3 className="seller_details text-left">Sold By <span className="text-black ml-2">Fortunes Communications</span></h3>
                        </Box>
                        <div className="flex-item">
                            <div className="content-wrap text-center setdel">
                                <img src="../asset/standard-delivery.png" alt="Standard Delivery" />
                                <h4>Standard Delivery</h4>
                                <p className="text-gray-400">Before 7th Aug</p>
                                <span className="font-semibold gre">Free</span>
                            </div>
                        </div>
                        <div className="flex-item">
                            <div className="content-wrap two_hrs_del text-center setdel">
                                <img src="../asset/two-hrs-delivery-icon.png" alt="2 Hours Delivery" />
                                <h4>2 Hours Delivery</h4>
                                <p className="text-gray-400">Available for</p>
                                <span className="font-semibold org"><FontAwesomeIcon className="rupay mr-1" icon="rupee-sign" />120</span>
                            </div>
                        </div>
                                <div className="flex-item">
                                <Box className="content-wrap pickup-at-store text-center setdel">
                                <img src="../asset/pickup-at-store.png" alt="Pickup at Store" />
                                <h4 className="text-gray-400">Pickup at Store</h4>
                                <p className="text-gray-400">Not Available </p>
                                </Box>
                        </div>
                    </Box>
                </div>
            </Box>
        </div>
    )
}

export default DeliveryInfo
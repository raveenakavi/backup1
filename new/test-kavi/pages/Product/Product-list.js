import React from 'react'
import Reviewscontent from './Review-content';
import Productdescription from './Product-description';
import ProtectMobile from './Protection';
import Accessories from './Accessories.js';
import Carousel1 from './Carousel';
import MoreProducts from './MoreProducts.js'
import FrequentlyCarousel1 from './FrequentlyCarousel1.js'
import TrendingCarousel from './TrendingCarousel1.js'

export default function Productlist() {
    return (
        <div className="mt-5">
            <nav class="over-wrp">
        <ul>
          <li><a href="#1">Overview</a></li>
          <li><a href="#2">Reviews</a></li>
          <li><a href="#3">Media</a></li>
          <li><a href="#4">Specs</a></li>
          <li><a href="#5">Compare Products</a></li>
          <li><a href="#6">Q&A</a></li>
          <li><a href="#7">Blog</a></li>
          <li><a href="#8">Combo Offer</a></li>
        </ul>
      </nav>
      
      <div class="sections">
       
        <div class="prohead" id="1"><h1><Productdescription/></h1></div>
        <ProtectMobile/>
        <div class="prohead" id="2"><h1><Reviewscontent/></h1></div>
        <Accessories />
        <Carousel1 />
        <MoreProducts />
        <FrequentlyCarousel1 />
        <TrendingCarousel />
      </div>
        </div>
    )
}

import React, { useState, useRef } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import AppleIcon from '@material-ui/icons/Apple';

const TrendingCarousel = () => {

  const similarProductSlider = useRef();
  var similarProducts = {
    dots: false,
    autoplay: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },

    {
      breakpoint: 476,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};
const next = () => {
  slider.slickNext();
};
const previous = () => {
  slider.slickPrev();
};

  return (
    <div className="app similar-products trending-carousel bg-white">
      {/*  Slider */}

      <h2 className="flex py-4 px-3 text-black text-md md:text-2xl font-bold"><span className="ml-0">Top Searched and trending</span>
                    
                    <p className="float-right w-4/12 right-0 md:right-2 absolute flex"><span className="text-xs pt-3 text-blue-600 invisible md:visible"> See all</span>  <span className="right-0 md:right-2 absolute">
                    <div className="slider-arrow">
                    <button  onClick={() => similarProductSlider.current.slickPrev()} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button  onClick={() => similarProductSlider.current.slickNext()} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
                                </div>                      </span></p>
                    
                    </h2>
                    <Slider ref={(slider) => (similarProductSlider.current = slider)} {...similarProducts}>
        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="w-full trend-product px-6">
        <iframe className="rounded-xl" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
      <div className="w-full flex pt-3">
          <div className="w-2/6">
      <img src={'/asset/trending-1.jpeg'} className="w-24 h-24 rounded-xl pr-2" />
      </div>
      <div className="w-5/6">
      <h3 className="text-black text-sm pt-2">Best 5000 man battery SmartPhones</h3>
      <p className="font-normal text-gray-400">Top 10 Smart Phones</p>
      <p className="text-black text-sm pt-4"><span className="float-left pr-3 text-red-500 text-sm pr-5"> Above 4.2 <Grade /> <span className="float-right text-gray-600">Starting price: Rs.16,789</span></span></p>
      </div>
      </div>
  </div>

  <div className="w-full trend-product px-6">
        <iframe className="rounded-xl" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
      <div className="w-full flex pt-3">
          <div className="w-2/6">
      <img src={'/asset/trending-1.jpeg'} className="w-24 h-24 rounded-xl pr-2" />
      </div>
      <div className="w-5/6">
      <h3 className="text-black text-sm pt-2">Best 5000 man battery SmartPhones</h3>
      <p className="font-normal text-gray-400">Top 10 Smart Phones</p>
      <p className="text-black text-sm pt-4"><span className="float-left pr-3 text-red-500 text-sm pr-5"> Above 4.2 <Grade /> <span className="float-right text-gray-600">Starting price: Rs.16,789</span></span></p>
      </div>
      </div>
  </div>

  <div className="w-full trend-product px-6">
        <iframe className="rounded-xl" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
      <div className="w-full flex pt-3">
          <div className="w-2/6">
      <img src={'/asset/trending-1.jpeg'} className="w-24 h-24 rounded-xl pr-2" />
      </div>
      <div className="w-5/6">
      <h3 className="text-black text-sm pt-2">Best 5000 man battery SmartPhones</h3>
      <p className="font-normal text-gray-400">Top 10 Smart Phones</p>
      <p className="text-black text-sm pt-4"><span className="float-left pr-3 text-red-500 text-sm pr-5"> Above 4.2 <Grade /> <span className="float-right text-gray-600">Starting price: Rs.16,789</span></span></p>
      </div>
      </div>
  </div>

  <div className="w-full trend-product px-6">
        <iframe className="rounded-xl" src="https://www.youtube.com/embed/abYXpfWemdk" title="YouTube video player"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
      <div className="w-full flex pt-3">
          <div className="w-2/6">
      <img src={'/asset/trending-1.jpeg'} className="w-24 h-24 rounded-xl pr-2" />
      </div>
      <div className="w-5/6">
      <h3 className="text-black text-sm pt-2">Best 5000 man battery SmartPhones</h3>
      <p className="font-normal text-gray-400">Top 10 Smart Phones</p>
      <p className="text-black text-sm pt-4"><span className="float-left pr-3 text-red-500 text-sm pr-5"> Above 4.2 <Grade /> <span className="float-right text-gray-600">Starting price: Rs.16,789</span></span></p>
      </div>
      </div>
  </div>
             
             
        {/* })} */}
      </Slider>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
    </div>
  );
};

export default TrendingCarousel;

import Head from "next/head";
import styles from "../styles/Home.module.css";
import Grid from "@material-ui/core/Grid";
import Select from "react-select";
import { useState, useEffect, useRef } from "react";
import Rating from "@material-ui/lab/Rating";
import SearchIcon from "@material-ui/icons/Search";
import { ToggleButton } from "primereact/togglebutton";
import Switch from "react-switch";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import StarIcon from "@material-ui/icons/Star";
import Sliders from "react-slick";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { ButtonBase } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
import TocIcon from "@material-ui/icons/Toc";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
export default function Home() {
    const [showText, setShowText] = useState(false);
    const [showFilter, setShowFilter] = useState(true);
    const [showScreen, setShowScreen] = useState(true);
    const [showPrimary, setShowPrimary] = useState(true);
    const [showSecondary, setShowSecondary] = useState(true);
    const [showProcessor, setShowProcessor] = useState(true);
    const [timerDays, setTimerDays] = useState("00");
    const [timerHours, setTimerHours] = useState("00");
    const [timerMinutes, setTimerMinutes] = useState("00");
    const [timerSeconds, setTimerSeconds] = useState("00");
    // price range filter
    const [values, setValues] = useState([15, 75]);
    const rangeSelector = (event, newValue) => {
        setValues(newValue);
        console.log(newValue);
    };
    let interval = useRef();
    const latest = useRef();
    const startTimer = () => {
        const countdownDate = new Date("June 30 2021 00:00:00").getTime();

        interval = setInterval(() => {
            const now = new Date().getTime();

            const distance = countdownDate - now;

            const days = Math.floor(distance / (1000 * 60 * 60 * 24));
            const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((distance % (1000 * 60)) / 1000);
            if (distance < 0) {
                clearInterval(interval.current);
            } else {
                setTimerDays(days);
                setTimerHours(hours);
                setTimerMinutes(minutes);
                setTimerSeconds(seconds);
            }
        }, 1000);
    };
    useEffect(() => {
        startTimer();
        return () => {
            clearInterval(interval.current);
        };
    });
    const customSlider = useRef();
    const bcamera = useRef();
    const [value, setValue] = useState(2);
    var categoryslider = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };

    var shopbrand = {
        dots: false,
        infinite: true,
        speed: 500,
        autoplay: true,
        slidesToShow: 13,
        slidesToScroll: 1,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 10,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: true,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    arrows: true,
                    dots: false,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 5,
                    arrows: true,
                    slidesToScroll: 1,
                    dots: false,
                },
            },
        ],
    };
    var iphones = {
        dots: false,
        autoplay: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: false,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    slidesToScroll: 1,
                },
            },
        ],
    };
    var bestcamera = {
        dots: false,
        autoplay: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: false,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    slidesToScroll: 1,
                },
            },
        ],
    };
    const next = () => {
        slider.slickNext();
    };
    const previous = () => {
        slider.slickPrev();
    };
    var latests = {
        dots: false,
        autoplay: false,
        infinite: true,
        speed: 500,
        arrows: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: false,
                    dots: false,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    arrows: false,
                    slidesToScroll: 2,
                },
            },
        ],
    };

    return (
        <div>
            <div>
                <Grid container className="w-full  category_padalignment">
                    <Grid item xs={3} sm={2} md={2} lg={2}>
                        <div>
                            <div className="flex justify-between ">
                                <div className="p-1">
                                    <div className="relative">
                                        <span className="category_inputdropdown z-10 h-full leading-snug font-normal absolute text-center text-blueGray-300 absolute bg-transparent rounded text-base items-center justify-center w-8 pl-0 py-2">
                                            <LocationOnOutlinedIcon />
                                        </span>

                                        <select className="block capitalize appearance-none w-full border border-gray-200 text-gray-700 py-2 px-2 pr-8 pl-7 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                            <option className="capitalize text-base">pick up at store </option>
                                            <option className="capitalize text-base">next day delivery</option>
                                            <option className="capitalize text-base">can be deliver later</option>
                                            <option className="capitalize text-base">2 hours delivery</option>
                                        </select>
                                        <div className="absolute flex inset-y-0 items-center px-3 right-0 text-gray-700 rounded-r pointer-events-none">
                                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>

                                <p className="capitalize text-2xl font-bold p-1 text-black-500 mt-1" onClick={() => setShowText(!showText)}>
                                    <FontAwesomeIcon className="category_inputdropdown" icon="filter" />
                                </p>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={9} sm={10} md={10} lg={10}>
                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                            <div className="category_filterdesign flex overflow-y-auto overflow-x-auto">
                                <span>
                                    <a>showing all</a>
                                </span>
                                <span>
                                    <a>android</a>
                                </span>
                                <span>
                                    <a>ios</a>
                                </span>
                                <span>
                                    <a>top rating</a>
                                </span>
                                <span>
                                    <a>wireless charging</a>
                                </span>
                                <span>
                                    <a>water resistant</a>
                                </span>
                                <span>
                                    <a>free delivery</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                                <span>
                                    <a>with offer</a>
                                </span>
                                <span>
                                    <a>voice assistant</a>
                                </span>
                                <span>
                                    <a>face unlock</a>
                                </span>
                            </div>
                            <div className="p-2">
                                <p className="capitalize  mt-1 text-sm font-bold text-blue-600 min-w-max"> clear all</p>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>

            <div className="grid grid-cols-6 gap-1">
                <div className={`${showText ? "col-start-1 col-span-2 sm:col-start-1 sm:col-span-1 md:col-start-1 md:col-span-1 lg:col-start-1 lg:col-span-1 ..." : "test"}`}>
                    {showText && (
                        <div className="category_filterheader rounded">
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_background">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">filters</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1">
                                    clear all <FontAwesomeIcon className="category_inputdropdown category_filterclose" icon="times-circle" />
                                </p>
                            </div>
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_filterclose">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">price range</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1"></p>
                            </div>
                            <div class="flex pl-0 sm:pl-10 md:pl-10 lg:pl-10">
                                <div className="flex-1">Rs. {values[0]}</div>
                                <div className="flex-1 capitalize">to</div>
                                <div className="flex-1">Rs. {values[1]}</div>
                            </div>
                            <div className="pr-4 pl-4 pt-2 pb-2">
                                <Slider value={values} onChange={rangeSelector} valueLabelDisplay="auto" className="category_filterclose" />
                            </div>

                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1 ">
                                <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">brand</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 category_filterclose">47 brands</p>
                            </div>
                            <div className="pt-2 pb-0 category_background">
                                <div className="pr-3 pl-3 relative flex w-full flex-wrap items-stretch mb-1">
                                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-300 absolute bg-transparent rounded text-base items-center justify-center w-10 pl-1 py-1">
                                        <FontAwesomeIcon icon="search" />
                                    </span>
                                    <input
                                        type="text"
                                        placeholder="Search"
                                        className=" w-28  sm:w-full md:w-full lg:w-full px-2 py-1 category_searchmethod placeholder-blueGray-300 text-blueGray-600 relative bg-white rounded text-sm border border-blueGray-300 outline-none focus:w-full pl-10"
                                    />
                                </div>
                                <p className="mb-1">
                                    <input type="radio" id="apple" name="brand" value="apple" className="ml-3" />
                                    <label for="apple" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        apple
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="celkon" name="brand" value="celkon" className="ml-3" />
                                    <label for="celkon" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        celkon
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="karbon" name="brand" value="karbon" className="ml-3" />
                                    <label for="karbon" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        karbon
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="micromax" name="brand" value="micromax" className="ml-3" />
                                    <label for="micromax" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        micromax
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="samsung" name="brand" value="samsung" className="ml-3" />
                                    <label for="samsung" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        samsung
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="nokia" name="brand" value="nokia" className="ml-3" />
                                    <label for="nokia" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        nokia
                                    </label>
                                </p>
                            </div>

                            <div className="pt-0 pb-2 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showFilter ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">ram</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowFilter(!showFilter)} />
                                        <RemoveIcon onClick={() => setShowFilter(!showFilter)} className="category_removeicon" />
                                    </p>
                                </div>

                                {showFilter && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="8gb ram" name="ram" value="8gb ram" className="ml-3" />
                                            <label for="8gb ram" className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                8GB & Above
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="6gb ram" name="ram" value="6gb ram" className="ml-3" />
                                            <label for="6gb ram" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6GB
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4gb ram" name="ram" value="4gb ram" className="ml-3" />
                                            <label for="4gb ram" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="3gb ram" name="ram" value="3gb ram" className="ml-3" />
                                            <label for="3gb ram" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                3GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="2gb ram" name="ram" value="2gb ram" className="ml-3" />
                                            <label for="2gb ram" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB-1GB" name="ram" value="512MB-1GB" className="ml-3" />
                                            <label for="512MB-1GB" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                512MB-1GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB" name="ram" value="512MB" className="ml-3" />
                                            <label for="512MB" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than- 512MB
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showScreen ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">screen size</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowScreen(!showScreen)} />
                                        <RemoveIcon onClick={() => setShowScreen(!showScreen)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showScreen && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 3inch" name="screensize" value="lessthan 3inch" className="ml-3" />
                                            <label for="lessthan 3inch" className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 3 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4inch" name="screensize" value="4inch" className="ml-3" />
                                            <label for="4inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 inch - 4.99 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="5inch" name="screensize" value="5inch" className="ml-3" />
                                            <label for="5inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                5 inch - 5.99 inch
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="6inch" name="screensize" value="6inch" className="ml-3" />
                                            <label for="6inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6 inch & Above
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showPrimary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">primary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowPrimary(!showPrimary)} />
                                        <RemoveIcon onClick={() => setShowPrimary(!showPrimary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showPrimary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="primarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label for="lessthan 2mp" className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label for="4inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label for="4mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="primarycamera" value="12mp" className="ml-3" />
                                            <label for="12mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="primarycamera" value="20mp" className="ml-3" />
                                            <label for="20mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="primarycamera" value="40mp" className="ml-3" />
                                            <label for="40mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showSecondary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">secondary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowSecondary(!showSecondary)} />
                                        <RemoveIcon onClick={() => setShowSecondary(!showSecondary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showSecondary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="secondarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label for="lessthan 2mp" className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label for="4inch" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label for="4mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="secondarycamera" value="12mp" className="ml-3" />
                                            <label for="12mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="secondarycamera" value="20mp" className="ml-3" />
                                            <label for="20mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="secondarycamera" value="40mp" className="ml-3" />
                                            <label for="40mp" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showProcessor ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">Processor</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowProcessor(!showProcessor)} />
                                        <RemoveIcon onClick={() => setShowProcessor(!showProcessor)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showProcessor && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="dualcore" name="processor" value="dualcore" className="ml-3" />
                                            <label for="dualcore" className="ml-3  text-sm font-medium">
                                                Dual Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="hexacore" name="processor" value="hexacore" className="ml-3" />
                                            <label for="hexacore" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Hexa Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="octacore" name="processor" value="octacore" className="ml-3" />
                                            <label for="octacore" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Octa Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="quadcore" name="processor" value="quadcore" className="ml-3" />
                                            <label for="quadcore" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Quad Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="singlecore" name="processor" value="singlecore" className="ml-3" />
                                            <label for="singlecore" className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Single Core
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                        </div>
                    )}
                </div>

                <div className={`${showText ? "col-end-7 col-span-4 sm:col-end-7 sm:col-span-5 md:col-end-7 md:col-span-5 lg:col-end-7 lg:col-span-5" : "col-start-1 col-end-7 ..."}`}>
                    <div>
                        <Sliders {...categoryslider}>
                            <div>
                                <img src="category/banner11.png" />
                            </div>

                            <div>
                                <img src="category/banner13.jpg" />
                            </div>
                            <div>
                                <img src="category/banner14.png" />
                            </div>
                        </Sliders>
                    </div>
                    <div>
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="category/sidebanner1.jpg" className="rounded" />
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="category/sidebanner2.jpg" className="rounded" />
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="category/sidebanner3.jpg" className="rounded" />
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                        <Grid container>
                            <Grid item xs={12} sm={3} md={3} lg={3}>
                                <div className="special_phone">
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow uppercase text-black font-bold inline-flex items-center p-0 cart_flexing font-bold text-lg"> special offer</p>

                                        <p className="capitalize text-xs font-bold p-1 text-gray-600 special_offer">
                                            <span className="block">save</span>
                                            <span className="block">2000</span>
                                        </p>
                                    </div>
                                    <div>
                                        <img src="category/iphonese2020.jpg" />
                                    </div>

                                    <div>
                                        <h1 className="uppercase font-bold text-black mt-1 text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">iphone se 2020</h1>
                                    </div>
                                    <div>
                                        <h4 className="catgory_modadetails">(Black, 256 GB)</h4>
                                    </div>
                                    <div className="relative pt-1">
                                        <div className="flex mb-2 items-center justify-between">
                                            <div>
                                                <span className="text-xs font-semibold inline-block py-1 px-2 capitalize rounded-full text-black">available:60</span>
                                            </div>
                                            <div className="text-right">
                                                <span className="text-xs capitalize font-semibold inline-block text-black">sold:200</span>
                                            </div>
                                        </div>
                                        <div className="overflow-hidden h-4 mb-4 text-xs flex rounded bg-gray-200">
                                            <div style={{ width: "50%" }} className="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg_offer"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <h1 className="capitalize font-bold text-black mt-1 text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">hurry up offer ends in </h1>
                                    </div>
                                    <div className="flex ml-12 mr-3 mt-3 mb-12">
                                        <section>
                                            <p className="p-3 m-1 rounded text-center bg_countcolor">{timerDays}</p>
                                            <p className="capitalize text-base text-center">
                                                <small>Days</small>
                                            </p>
                                        </section>
                                        <span className="mt-4">:</span>
                                        <section>
                                            <p className="p-3 m-1 rounded text-center bg_countcolor">{timerHours}</p>
                                            <p className="capitalize text-base text-center">
                                                <small>hours</small>
                                            </p>
                                        </section>
                                        <span className="mt-4">:</span>
                                        <section>
                                            <p className="p-3 m-1 rounded text-center bg_countcolor">{timerMinutes}</p>
                                            <p className="capitalize text-base text-center">
                                                <small>mins</small>
                                            </p>
                                        </section>
                                        <span className="mt-4">:</span>
                                        <section>
                                            <p className="p-3 m-1 rounded text-center bg_countcolor">{timerSeconds}</p>
                                            <p className="capitalize text-base text-center">
                                                <small>sec</small>
                                            </p>
                                        </section>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={9} md={9} lg={9}>
                                <Tabs className="text-center">
                                    <TabList>
                                        <Tab>Camera</Tab>
                                        <Tab>Gaming</Tab>
                                        <Tab>Processor</Tab>
                                    </TabList>

                                    <TabPanel>
                                        <div>
                                            <Sliders ref={(sliders) => (customSlider.current = sliders)} {...iphones}>
                                                <div className="p-2 vivo_phone">
                                                    <div>
                                                        <img src="https://s1.poorvikamobile.com/image/data/AAAAA/realme/Realme%206%20Pro/New/Blue/realme-6-pro-blue-64gb-6gb-ram-front_ios.jpeg" />
                                                    </div>
                                                    <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                            </div>
                                                        </div>
                                                        <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                            Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                        </h1>
                                                        <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                <p className="capitalize text-xs font-bold"> 4.3</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/realme/realme%20x50%20pro/New/Green/realme-x50-pro-5g-moss-green-128gb-6gb-ram-front_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/realme/Realme%20X7%205G/Space%20Silver/8GB%20Jpeg/Realme-X7-5G-Space-Silver-128GB-8GB-RAM-front.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/realme/Realme%20X3/New/White/realme-x3-arctic-white-128gb-6gb-ram-front_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/realme/Realme%20X7%20Pro/Mystic%20Black/Realme-X7-Pro-5G-Mystic-Black-128GB-8GB-RAM-front.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/realme/Realme%207%20Pro/Mirror%20Blue/6GB%20RAM/Realme-7-Pro-Mirror-Blue-128GB-6GB-RAM-front_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/realme/Realme%206%20Pro/New/Orange/realme-6-pro-orange-64gb-6gb-ram-back_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Sliders>
                                            <Sliders className="category_bottomslider" ref={(sliders) => (customSlider.current = sliders)} {...iphones}>
                                                <div className="p-2 vivo_phone">
                                                    <div>
                                                        <img src="category/vivomobile1.jpg" />
                                                    </div>
                                                    <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                            </div>
                                                        </div>
                                                        <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                            Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                        </h1>
                                                        <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                <p className="capitalize text-xs font-bold"> 4.3</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="category/vivomobile2.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="category/vivomobile3.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/ONEPLUS/OnePlus%209%20Pro%205G/Morning%20Mist/12GB/OnePlus-9-Pro-5G-Morning-Mist-256GB-12GB-RAM-back.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/ONEPLUS/OnePlus%209%20Pro%205G/Pine%20Green/12GB/OnePlus-9-Pro-5G-Pine-Green-256GB-12GB-RAM-back.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/ONEPLUS/OnePlus%209%205G/Astral%20Black/12GB/OnePlus-9-5G-Astral-Black-256GB-12GB-RAM-front.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/ONEPLUS/ONEPLUS-8Pro/Onyx%20Black/8%20GB/OnePlus-8-Pro-Onyx-Black-128GB-8GB-RAM-front.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Sliders>
                                        </div>
                                    </TabPanel>
                                    <TabPanel>
                                        <div>
                                            <Sliders ref={(sliders) => (customSlider.current = sliders)} {...iphones}>
                                                <div className="p-2 vivo_phone">
                                                    <div>
                                                        <img src="https://s1.poorvikamobile.com/image/data/AAAAA/vivo/Vivo%20V20%202021/Sunset%20Melody/Vivo-V20-2021-Sunset-Melody-128GB-8GBp-RAM-back.jpg" />
                                                    </div>
                                                    <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                            </div>
                                                        </div>
                                                        <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                            Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                        </h1>
                                                        <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                <p className="capitalize text-xs font-bold"> 4.3</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/vivo/Vivo%20V20%202021/Moonlight%20Sonata/Vivo-V20-2021-Moonlight-Sonata-128GB-8GB-RAM-front.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/vivo/VIVO%20V19/New/Mystic%20Silver/vivo-v19-mystic-silver-128gb-8gb-ram-back_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/vivo/Vivo%20V21%205G/Arctic%20White/128GB%208GB/Vivo-V21-5G-Arctic-White-128GB-8GB-RAM-back.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/vivo/Vivo%20X50/New/Frost%20Blue/vivo-x50-frost-blue-128gb-8gb-ram-back_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/vivo/Vivo%20X50/New/Glaze%20Black/vivo-x50-frost-blue-128gb-8gb-ram-front_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/vivo/ViVO%20X60%20Pro/Shimmer%20Blue/Vivo-X60-Pro-5G-Shimmer-Blue-256GB-12GB-RAM-back.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Sliders>
                                            <Sliders className="category_bottomslider" ref={(sliders) => (customSlider.current = sliders)} {...iphones}>
                                                <div className="p-2 vivo_phone">
                                                    <div>
                                                        <img src="https://s1.poorvikamobile.com/image/data/AAAAA/Samsung/Samsung%20Galaxy%20A72%205G/Violet/256GB-8GB/Samsung-Galaxy-A72-5G-Violet-256GB-8GB-RAM-back.jpg" />
                                                    </div>
                                                    <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                            </div>
                                                        </div>
                                                        <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                            Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                        </h1>
                                                        <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                <p className="capitalize text-xs font-bold"> 4.3</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/Samsung/Samsung%20Galaxy%20A72%205G/Blue/256GB-8GB/Samsung-Galaxy-A72-5G-Blue-256GB-8GB-RAM-back.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/Samsung/Samsung%20Galaxy%20S20%20FE%205G/Cloud%20Lavender/Samsung-Galaxy-S20-FE-5G-Cloud-Lavender-128GB-8GB-RAM-back.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/Samsung/Samsung%20Galaxy%20Note20%205G/%20Mystic%20Green/256-8GB%20RAM/Samsung-Galaxy-Note20-5G-Mystic-Green-256GB-8GB-RAM-back_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/Samsung/Samsung%20Galaxy%20S21%205G/S21%20Ultra%20Phantom%20Silver/Samsung-Galaxy-S21-5G-Ultra%20-Phantom-Silver-5.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/Samsung/Samsung%20Galaxy%20Z%20Fold%202/Mystic%20Bronze/256GB/Samsung-Galaxy-Z-Fold-2-Mystic-Bronze-256GB-12GB-RAM-back_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="compare3.png" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Sliders>
                                        </div>
                                    </TabPanel>
                                    <TabPanel>
                                        <div>
                                            <Sliders ref={(sliders) => (customSlider.current = sliders)} {...iphones}>
                                                <div className="p-2 vivo_phone">
                                                    <div>
                                                        <img src="category/vivomobile1.jpg" />
                                                    </div>
                                                    <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                            </div>
                                                        </div>
                                                        <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                            Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                        </h1>
                                                        <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                <p className="capitalize text-xs font-bold"> 4.3</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="category/vivomobile2.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="category/vivomobile3.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="category/vivomobile1.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="category/vivomobile2.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="category/vivomobile3.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="compare3.png" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Sliders>
                                            <Sliders className="category_bottomslider" ref={(sliders) => (customSlider.current = sliders)} {...iphones}>
                                                <div className="p-2 vivo_phone">
                                                    <div>
                                                        <img src="https://s1.poorvikamobile.com/image/data/AAAAA/NOKIA/Nokia%205-4/Polar%20Night/6GB/Nokia-5-4-Polar-Night-128GB-6GB-RAM-back.jpg" />
                                                    </div>
                                                    <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                            </div>
                                                        </div>
                                                        <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                            Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                        </h1>
                                                        <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                        <div>
                                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                <p className="capitalize text-xs font-bold"> 4.3</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/NOKIA/Nokia%203-4/DUSK/Nokia-3-4-Dusk-64GB-4GB-RAM-back.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/NOKIA/2.4/Dusk/Nokia-2-4-Dusk-64GB-3GB-RAM-front.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/NOKIA/2.4/Dusk/Nokia-2-4-Dusk-64GB-3GB-RAM-front.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="https://s1.poorvikamobile.com/image/data/AAAAA/NOKIA/2.3/New/Sand/nokia-2-3-sand-32gb-2gb-%20ram-front_ios.jpeg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="category/vivomobile3.jpg" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="p-2 vivo_phone">
                                                        <div>
                                                            <img src="compare3.png" />
                                                        </div>
                                                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-sm"> Rs.74,999</p>

                                                                    <p className="capitalize text-xs font-bold p-1 text-blue-500">save Rs 3,001</p>
                                                                </div>
                                                            </div>
                                                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                                Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                            </h1>
                                                            <h4 className="text-xs text-left text-gray-600">(Gold Chrome, 256 GB,8GB…)</h4>

                                                            <div>
                                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                                    <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                                    <p className="capitalize text-xs font-bold"> 4.3</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Sliders>
                                        </div>
                                    </TabPanel>
                                </Tabs>
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                        <Grid container className="sm:p-0 md:p-0 lg:p-0 p-0">
                            <Grid item xs={12}>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize mb-10 text-center items-center p-0 cart_flexing font-bold   sm:text-2xl md:text-2xl lg:text-2xl text-xl italic brand_heads"> view by price</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                        <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={6} sm={1} md={1} lg={1} className="category_hidden"></Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12 category_border ">
                                    <div className="view_price pt-3 m-3 mt-0">
                                        <div className="service-icon pl-3">
                                            <p className="rounded text-xl bg-white text-center capitalize font-bold m-auto w-1/2"> below</p>
                                            <p className="text-4xl uppercase font-bold text-white text-center pt-3  pb-3"> 5,000</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12  category_border">
                                    <div className="view_price1 pt-3 m-3 mt-0">
                                        <div className="service-icon pl-3">
                                            <div className="grid grid-cols-2 ">
                                                <div className="rounded text-xl bg-white capitalize font-bold w-full  p-0 text-center ">from </div>
                                                <div className="text-2xl uppercase font-bold text-white text-center ">5,000</div>
                                            </div>
                                            <div className="grid grid-cols-1 ">
                                                <div className="rounded text-base  capitalize font-bold w-full  p-0 text-center ">to </div>
                                            </div>
                                            <p className="text-4xl uppercase font-bold text-white text-center  pb-3"> 10,000</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12 category_border">
                                    <div className="view_price2 pt-3 m-3 mt-0">
                                        <div className="service-icon pl-3">
                                            <div className="grid grid-cols-2 ">
                                                <div className="rounded text-xl bg-white capitalize font-bold w-full  p-0 text-center ">from </div>
                                                <div className="text-2xl uppercase font-bold text-white text-center ">10,000</div>
                                            </div>
                                            <div className="grid grid-cols-1 ">
                                                <div className="rounded text-base  capitalize font-bold w-full  p-0 text-center ">to </div>
                                            </div>
                                            <p className="text-4xl uppercase font-bold text-white text-center  pb-3"> 20,000</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12 category_border">
                                    <div className="view_price3 pt-3 m-3 mt-0">
                                        <div className="service-icon pl-3">
                                            <div className="grid grid-cols-2 ">
                                                <div className="rounded text-xl bg-white capitalize font-bold w-full  p-0 text-center ">from </div>
                                                <div className="text-2xl uppercase font-bold text-white text-center ">20,000</div>
                                            </div>
                                            <div className="grid grid-cols-1 ">
                                                <div className="rounded text-base  capitalize font-bold w-full  p-0 text-center ">to </div>
                                            </div>
                                            <p className="text-4xl uppercase font-bold text-white text-center  pb-3"> 35,000</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12">
                                    <div className="view_price4 pt-3 m-3 mt-0">
                                        <div className="service-icon pl-3">
                                            <p className="rounded text-xl bg-white text-center capitalize font-bold m-auto w-1/2"> above</p>
                                            <p className="text-4xl uppercase font-bold text-white text-center pt-3  pb-3"> 35,000</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={1} md={1} lg={1} className="invisible sm:visible md:visible lg:visible"></Grid>
                        </Grid>
                    </div>
                    <div className="pb-12">
                        <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={12}>
                                <div className="text-center">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="capitalize items-center p-0 cart_flexing font-bold sm:text-2xl md:text-2xl lg:text-2xl text-lg italic brand_heads"> top selling smartphones-2021</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                        <Sliders ref={(sliders) => (latest.current = sliders)} {...latests}>
                            <div className="p-2 category-slide">
                                <div className="relative bg-images">
                                    <img src="category/smartphone1.jpg" />
                                    <div className="selling_smart absolute text-xs text-center text-white">
                                        <p className="mt-3">Save</p>
                                        <p className="font-bold">Rs 4,001</p>
                                    </div>
                                </div>
                                <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                    <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                    <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                    <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>

                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                            <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                            <p className="capitalize font-bold m-2  category_staricon">
                                                <StarIcon className="brand_staricon" />
                                            </p>
                                            <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 category-slide">
                                    <div className="relative bg-images">
                                        <img src="category/smartphone2.jpg" />
                                        <div className="selling_smart1 absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div className="p-2 category-slide">
                                    <div className="relative bg-images">
                                        <img src="category/smartphone3.jpg" />
                                        <div className="selling_smart2 absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 category-slide">
                                    <div className="relative bg-images">
                                        <img src="category/smartphone1.jpg" />
                                        <div className="selling_smart3 absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 category-slide">
                                    <div className="relative bg-images">
                                        <img src="category/smartphone2.jpg" />
                                        <div className="selling_smart4 absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 category-slide">
                                    <div className="relative bg-images">
                                        <img src="category/smartphone3.jpg" />
                                        <div className="selling_smart absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Sliders>
                    </div>
                    <div className="pb-12">
                        <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={12}>
                                <div className="text-center">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="capitalize items-center p-0 cart_flexing font-bold sm:text-2xl md:text-2xl lg:text-2xl text-lg italic brand_heads"> best camera phone under 20k</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                        <Sliders ref={(sliders) => (latest.current = sliders)} {...latests}>
                            <div className="p-2 under2k-slide">
                                <div className="relative bg-images">
                                    <img src="category/bestsmartphone1.jpeg" />
                                    <div className="selling_smart absolute text-xs text-center text-white">
                                        <p className="mt-3">Save</p>
                                        <p className="font-bold">Rs 4,001</p>
                                    </div>
                                </div>
                                <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                    <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                    <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                    <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                    <div>
                                        <div className="flex justify-between ">
                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                            <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                            <p className="capitalize font-bold m-2  category_staricon">
                                                <StarIcon className="brand_staricon" />
                                            </p>
                                            <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 under2k-slide">
                                    <div className="relative bg-images">
                                        <img src="category/bestsmartphone2.jpeg" />
                                        <div className="selling_smart1 absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div className="p-2 under2k-slide">
                                    <div className="relative bg-images">
                                        <img src="category/bestsmartphone3.jpeg" />
                                        <div className="selling_smart2 absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 under2k-slide">
                                    <div className="relative bg-images">
                                        <img src="category/bestsmartphone4.jpeg" />
                                        <div className="selling_smart3 absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 under2k-slide">
                                    <div className="relative bg-images">
                                        <img src="category/bestsmartphone5.jpeg" />
                                        <div className="selling_smart4 absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="p-2 under2k-slide">
                                    <div className="relative bg-images">
                                        <img src="category/bestsmartphone1.jpeg" />
                                        <div className="selling_smart absolute text-xs text-center text-white">
                                            <p className="mt-3">Save</p>
                                            <p className="font-bold">Rs 4,001</p>
                                        </div>
                                    </div>
                                    <div className="pl-0 sm:pl-3 md:pl-3 lg:pl-3">
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>
                                        <h1 className="text-sm text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Galaxy Note 20 Ultra</h1>

                                        <h4 className="catgory_modadetails">(Gold Chrome, 256 GB, 8GB RAM, 500…)</h4>
                                        <div>
                                            <div className="flex justify-between ">
                                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                                <p className="uppercase best_phoneoffer  m-2"> 4% off</p>
                                                <p className="capitalize font-bold m-2  category_staricon">
                                                    <StarIcon className="brand_staricon" />
                                                </p>
                                                <p className="capitalize text-xs font-bold p-1 m-1"> 4.3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Sliders>
                    </div>
                    <div className="pb-6">
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={8}>
                                <div className="p-2">
                                    <img src="category/oppobanner.jpg" className="rounded-2xl pr-1 pl-1" />
                                </div>
                            </Grid>

                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="category/redmi.png" className="rounded-2xl " />
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                        <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={12}>
                                <div className="text-center">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-2xl md:text-2xl lg:text-2xl text-lg italic brand_heads"> shop by brand</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                        <div>
                            <Sliders {...shopbrand}>
                                <div>
                                    <img src="category/brand/apple.png" />
                                </div>

                                <div>
                                    <img src="category/brand/Celkon.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Huawei.png" />
                                </div>
                                <div>
                                    <img src="category/brand/itel.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Jio.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Karbonn.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Lava.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Lenovo.png" />
                                </div>

                                <div>
                                    <img src="category/brand/LG.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Meizu.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Mi.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Micromax.png" />
                                </div>

                                <div>
                                    <img src="category/brand/Moto.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Nokia.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Oneplus.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Oppo.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Realme.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Samsung.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Tecno.png" />
                                </div>
                                <div>
                                    <img src="category/brand/Vivo.png" />
                                </div>
                            </Sliders>
                        </div>
                    </div>
                    <div>
                        <Grid container className="pr-3 pl-3 mt-16">
                            <Grid item xs={12} sm={3} md={3} lg={3}>
                                <div className="p-2">
                                    <img src="category/vivo1.jpg" className="rounded-xl" />
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={9} md={9} lg={9}>
                                <div>
                                    <Grid container className="sm:p-0 md:p-0 lg:p-0 p-0">
                                        <Grid item xs={12}>
                                            <div>
                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                    <p className="flex-grow uppercase text-center items-center p-0 cart_flexing font-bold   sm:text-2xl md:text-2xl lg:text-2xl text-xl italic brand_heads"> best in camera</p>

                                                    <p className="capitalize text-sm font-bold brand_seeall"> see all </p>
                                                    <div className="slider-arrow">
                                                        <ButtonBase className="arrow-btn prev" onClick={() => bcamera.current.slickPrev()}>
                                                            <ArrowBackIosIcon style={{ fontSize: 20 }} />
                                                        </ButtonBase>
                                                        <ButtonBase className="arrow-btn next" onClick={() => bcamera.current.slickNext()}>
                                                            <ArrowForwardIosIcon style={{ fontSize: 20 }} />
                                                        </ButtonBase>
                                                    </div>
                                                </div>
                                            </div>
                                        </Grid>
                                    </Grid>
                                    <Sliders ref={(slider) => (bcamera.current = slider)} {...bestcamera}>
                                        <div className="p-2 vivo_phone">
                                            <div>
                                                <img src="category/vivomobile1.jpg" />
                                            </div>
                                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                <div>
                                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                        <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                        <p className="capitalize text-xs font-bold"> 4.3</p>
                                                    </div>
                                                </div>
                                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                    Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                </h1>
                                                <h4 className="catgory_modadetails">(Gold Chrome, 256 GB,8GB…)</h4>
                                                <div>
                                                    <List component="nav" className="p-0" aria-label="contacts">
                                                        <ListItem className="list_datas">
                                                            <ExpandLessIcon style={{ fontSize: 15 }} />

                                                            <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                                        </ListItem>
                                                        <ListItem className="list_datas">
                                                            <ExpandLessIcon style={{ fontSize: 15 }} />
                                                            <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                                        </ListItem>
                                                    </List>
                                                </div>
                                                <div>
                                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>

                                                        <p className="capitalize text-xs font-bold p-1">save Rs 3,001</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div>
                                                    <img src="category/vivomobile2.jpg" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                            <p className="capitalize text-xs font-bold"> 4.3</p>
                                                        </div>
                                                    </div>
                                                    <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                        Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                    </h1>
                                                    <h4 className="catgory_modadetails">(Gold Chrome, 256 GB,8GB…)</h4>
                                                    <div>
                                                        <List component="nav" className="p-0" aria-label="contacts">
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />

                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                                            </ListItem>
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />
                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                                            </ListItem>
                                                        </List>
                                                    </div>
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>

                                                            <p className="capitalize text-xs font-bold p-1">save Rs 3,001</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div>
                                                    <img src="category/vivomobile3.jpg" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                            <p className="capitalize text-xs font-bold"> 4.3</p>
                                                        </div>
                                                    </div>
                                                    <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                        Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                    </h1>
                                                    <h4 className="catgory_modadetails">(Gold Chrome, 256 GB,8GB…)</h4>
                                                    <div>
                                                        <List component="nav" className="p-0" aria-label="contacts">
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />

                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                                            </ListItem>
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />
                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                                            </ListItem>
                                                        </List>
                                                    </div>
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>

                                                            <p className="capitalize text-xs font-bold p-1">save Rs 3,001</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div>
                                                    <img src="category/vivomobile1.jpg" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                            <p className="capitalize text-xs font-bold"> 4.3</p>
                                                        </div>
                                                    </div>
                                                    <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                        Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                    </h1>
                                                    <h4 className="catgory_modadetails">(Gold Chrome, 256 GB,8GB…)</h4>
                                                    <div>
                                                        <List component="nav" className="p-0" aria-label="contacts">
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />

                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                                            </ListItem>
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />
                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                                            </ListItem>
                                                        </List>
                                                    </div>
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>

                                                            <p className="capitalize text-xs font-bold p-1">save Rs 3,001</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div>
                                                    <img src="category/vivomobile2.jpg" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                            <p className="capitalize text-xs font-bold"> 4.3</p>
                                                        </div>
                                                    </div>
                                                    <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                        Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                    </h1>
                                                    <h4 className="catgory_modadetails">(Gold Chrome, 256 GB,8GB…)</h4>
                                                    <div>
                                                        <List component="nav" className="p-0" aria-label="contacts">
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />

                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                                            </ListItem>
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />
                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                                            </ListItem>
                                                        </List>
                                                    </div>
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>

                                                            <p className="capitalize text-xs font-bold p-1">save Rs 3,001</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div>
                                                    <img src="category/vivomobile3.jpg" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                            <p className="capitalize text-xs font-bold"> 4.3</p>
                                                        </div>
                                                    </div>
                                                    <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                        Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                    </h1>
                                                    <h4 className="catgory_modadetails">(Gold Chrome, 256 GB,8GB…)</h4>
                                                    <div>
                                                        <List component="nav" className="p-0" aria-label="contacts">
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />

                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                                            </ListItem>
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />
                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                                            </ListItem>
                                                        </List>
                                                    </div>
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>

                                                            <p className="capitalize text-xs font-bold p-1">save Rs 3,001</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div>
                                                    <img src="compare3.png" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <Rating name="ratingvalue" style={{ fontSize: 20 }} value={value} />

                                                            <p className="capitalize text-xs font-bold"> 4.3</p>
                                                        </div>
                                                    </div>
                                                    <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">
                                                        Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)
                                                    </h1>
                                                    <h4 className="catgory_modadetails">(Gold Chrome, 256 GB,8GB…)</h4>
                                                    <div>
                                                        <List component="nav" className="p-0" aria-label="contacts">
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />

                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                                            </ListItem>
                                                            <ListItem className="list_datas">
                                                                <ExpandLessIcon style={{ fontSize: 15 }} />
                                                                <p className="ml-3 leading-7 text-sm whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                                            </ListItem>
                                                        </List>
                                                    </div>
                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>

                                                            <p className="capitalize text-xs font-bold p-1">save Rs 3,001</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Sliders>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                        <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={12}>
                                <div className="text-center">
                                    <div className="pt-1 pb-1 pr-1 pl-1">
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-lg md:text-lg lg:text-lg text-lg italic brand_heads"> poorvika</p>
                                        <p className="uppercase items-center p-0 cart_flexing font-bold sm:text-2xl md:text-2xl lg:text-2xl text-lg italic brand_heads"> exclusive deals</p>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                        <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12">
                                    <div className="serviceBox">
                                        <div className="service-icon pl-3">
                                            <p className="text-4xl uppercase font-bold"> 60%</p>
                                            <p className="text-4xl uppercase font-bold"> off</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12">
                                    <div className="serviceBox1">
                                        <div className="service-icon1 pl-3">
                                            <p className="text-4xl uppercase font-bold"> 50%</p>
                                            <p className="text-4xl uppercase font-bold"> off</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12">
                                    <div className="serviceBox2">
                                        <div className="service-icon2 pl-3">
                                            <p className="text-4xl uppercase font-bold"> 40%</p>
                                            <p className="text-4xl uppercase font-bold"> off</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12">
                                    <div className="serviceBox3">
                                        <div className="service-icon3 pl-3">
                                            <p className="text-4xl uppercase font-bold"> 30%</p>
                                            <p className="text-4xl uppercase font-bold"> off</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12">
                                    <div className="serviceBox4">
                                        <div className="service-icon4 pl-3">
                                            <p className="text-4xl uppercase font-bold"> 20%</p>
                                            <p className="text-4xl uppercase font-bold"> off</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={6} sm={2} md={2} lg={2}>
                                <div className="mb-12">
                                    <div className="serviceBox5">
                                        <div className="service-icon5 pl-3">
                                            <p className="text-4xl uppercase font-bold"> 10%</p>
                                            <p className="text-4xl uppercase font-bold"> off</p>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                    <div className="pb-6">
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={8}>
                                <div className="p-2">
                                    <img src="category/applewatch.jpg" className="rounded-2xl pr-1 pl-1" />
                                </div>
                            </Grid>

                            <Grid item xs={4}>
                                <div className="p-2">
                                    <img src="category/samsungtv.jpg" className="rounded-2xl s_tv" />
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                    <div className="pb-6">
                        <Grid container className="pr-3 pl-3">
                            <Grid item xs={12} sm={2} md={2} lg={2}>
                                <div className="p-2">
                                    <img src="category/samsung_test.png" className="rounded-2xl pr-1 pl-1 m-auto" />
                                </div>
                            </Grid>

                            <Grid item xs={12} sm={10} md={10} lg={10}>
                                <div>
                                    <Grid container className="sm:p-0 md:p-0 lg:p-0 p-0">
                                        <Grid item xs={12}>
                                            <div>
                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                    <p className="flex-grow uppercase mb-10 text-center items-center p-0 cart_flexing font-bold   sm:text-2xl md:text-2xl lg:text-2xl text-xl italic brand_heads"> hot deals</p>

                                                    <p className="capitalize text-sm font-bold brand_seeall"> see all </p>
                                                </div>
                                            </div>
                                        </Grid>
                                    </Grid>
                                    <Sliders ref={(sliders) => (customSlider.current = sliders)} {...iphones}>
                                        <div className="p-2 vivo_phone">
                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                <p className="bg-red-600 text-white p-1 rounded uppercase text-xs font-bold"> 4 % off</p>
                                            </div>
                                            <div>
                                                <img src="category/vivomobile1.jpg" />
                                            </div>
                                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                <div className="flex  ml-16  sm:ml-0 md:ml-0 lg:ml-0">
                                                    <section className="p-2 m-1 rounded text-center hot_deals">
                                                        <p>{timerDays}</p>
                                                        <p className="uppercase text-base">
                                                            <small>Days</small>
                                                        </p>
                                                    </section>
                                                    <section className="p-2 m-1 rounded text-center hot_deals">
                                                        <p>{timerHours}</p>
                                                        <p className="uppercase text-base">
                                                            <small>hours</small>
                                                        </p>
                                                    </section>
                                                    <section className="p-2 m-1 rounded text-center hot_deals">
                                                        <p>{timerMinutes}</p>
                                                        <p className="uppercase text-base">
                                                            <small>mins</small>
                                                        </p>
                                                    </section>
                                                    <section className="p-2 m-1 rounded text-center hot_deals">
                                                        <p>{timerSeconds}</p>
                                                        <p className="uppercase text-base">
                                                            <small>sec</small>
                                                        </p>
                                                    </section>
                                                </div>
                                                <h1 className="mt-1 text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>

                                                <h1 className="capitalize text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">galaxy note 20 Ultra</h1>

                                                <div>
                                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.1,03,999</p>

                                                        <p className="capitalize text-xs font-bold p-1 text-gray-600">
                                                            <del>Rs 1,13,999</del>
                                                        </p>
                                                    </div>
                                                    <div>
                                                        <button className="uppercase hot_button text-xs rounded text-white w-full">add to cart </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                    <p className="bg-red-600 text-white p-1 rounded uppercase text-xs font-bold"> 4 % off</p>
                                                </div>
                                                <div>
                                                    <img src="category/vivomobile2.jpg" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div className="flex ml-16  sm:ml-0 md:ml-0 lg:ml-0">
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerDays}</p>
                                                            <p className="uppercase text-base">
                                                                <small>Days</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerHours}</p>
                                                            <p className="uppercase text-base">
                                                                <small>hours</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerMinutes}</p>
                                                            <p className="uppercase text-base">
                                                                <small>mins</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerSeconds}</p>
                                                            <p className="uppercase text-base">
                                                                <small>sec</small>
                                                            </p>
                                                        </section>
                                                    </div>
                                                    <h1 className="mt-1 text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>

                                                    <h1 className="capitalize text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">galaxy note 20 Ultra</h1>

                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.1,03,999</p>

                                                            <p className="capitalize text-xs font-bold p-1 text-gray-600">
                                                                <del>Rs 1,13,999</del>
                                                            </p>
                                                        </div>
                                                        <div>
                                                            <button className="uppercase hot_button text-xs rounded text-white w-full">add to cart </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                    <p className="bg-red-600 text-white p-1 rounded uppercase text-xs font-bold"> 4 % off</p>
                                                </div>
                                                <div>
                                                    <img src="category/vivomobile3.jpg" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div className="flex ml-16  sm:ml-0 md:ml-0 lg:ml-0">
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerDays}</p>
                                                            <p className="uppercase text-base">
                                                                <small>Days</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerHours}</p>
                                                            <p className="uppercase text-base">
                                                                <small>hours</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerMinutes}</p>
                                                            <p className="uppercase text-base">
                                                                <small>mins</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerSeconds}</p>
                                                            <p className="uppercase text-base">
                                                                <small>sec</small>
                                                            </p>
                                                        </section>
                                                    </div>
                                                    <h1 className="mt-1 text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>

                                                    <h1 className="capitalize text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">galaxy note 20 Ultra</h1>

                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.1,03,999</p>

                                                            <p className="capitalize text-xs font-bold p-1 text-gray-600">
                                                                <del>Rs 1,13,999</del>
                                                            </p>
                                                        </div>
                                                        <div>
                                                            <button className="uppercase hot_button text-xs rounded text-white w-full">add to cart </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="p-2 vivo_phone">
                                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                <p className="bg-red-600 text-white p-1 rounded uppercase text-xs font-bold"> 4 % off</p>
                                            </div>
                                            <div>
                                                <img src="category/vivomobile1.jpg" />
                                            </div>
                                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                <div className="flex  ml-16  sm:ml-0 md:ml-0 lg:ml-0">
                                                    <section className="p-2 m-1 rounded text-center hot_deals">
                                                        <p>{timerDays}</p>
                                                        <p className="uppercase text-base">
                                                            <small>Days</small>
                                                        </p>
                                                    </section>
                                                    <section className="p-2 m-1 rounded text-center hot_deals">
                                                        <p>{timerHours}</p>
                                                        <p className="uppercase text-base">
                                                            <small>hours</small>
                                                        </p>
                                                    </section>
                                                    <section className="p-2 m-1 rounded text-center hot_deals">
                                                        <p>{timerMinutes}</p>
                                                        <p className="uppercase text-base">
                                                            <small>mins</small>
                                                        </p>
                                                    </section>
                                                    <section className="p-2 m-1 rounded text-center hot_deals">
                                                        <p>{timerSeconds}</p>
                                                        <p className="uppercase text-base">
                                                            <small>sec</small>
                                                        </p>
                                                    </section>
                                                </div>
                                                <h1 className="mt-1 text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>

                                                <h1 className="capitalize text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">galaxy note 20 Ultra</h1>

                                                <div>
                                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.1,03,999</p>

                                                        <p className="capitalize text-xs font-bold p-1 text-gray-600">
                                                            <del>Rs 1,13,999</del>
                                                        </p>
                                                    </div>
                                                    <div>
                                                        <button className="uppercase hot_button text-xs rounded text-white w-full">add to cart </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                    <p className="bg-red-600 text-white p-1 rounded uppercase text-xs font-bold"> 4 % off</p>
                                                </div>
                                                <div>
                                                    <img src="category/vivomobile2.jpg" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div className="flex  ml-16  sm:ml-0 md:ml-0 lg:ml-0">
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerDays}</p>
                                                            <p className="uppercase text-base">
                                                                <small>Days</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerHours}</p>
                                                            <p className="uppercase text-base">
                                                                <small>hours</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerMinutes}</p>
                                                            <p className="uppercase text-base">
                                                                <small>mins</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerSeconds}</p>
                                                            <p className="uppercase text-base">
                                                                <small>sec</small>
                                                            </p>
                                                        </section>
                                                    </div>
                                                    <h1 className="mt-1 text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>

                                                    <h1 className="capitalize text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">galaxy note 20 Ultra</h1>

                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.1,03,999</p>

                                                            <p className="capitalize text-xs font-bold p-1 text-gray-600">
                                                                <del>Rs 1,13,999</del>
                                                            </p>
                                                        </div>
                                                        <div>
                                                            <button className="uppercase hot_button text-xs rounded text-white w-full">add to cart </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div className="p-2 vivo_phone">
                                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                    <p className="bg-red-600 text-white p-1 rounded uppercase text-xs font-bold"> 4 % off</p>
                                                </div>
                                                <div>
                                                    <img src="category/vivomobile3.jpg" />
                                                </div>
                                                <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                                    <div className="flex ml-16  sm:ml-0 md:ml-0 lg:ml-0">
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerDays}</p>
                                                            <p className="uppercase text-base">
                                                                <small>Days</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerHours}</p>
                                                            <p className="uppercase text-base">
                                                                <small>hours</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerMinutes}</p>
                                                            <p className="uppercase text-base">
                                                                <small>mins</small>
                                                            </p>
                                                        </section>
                                                        <section className="p-2 m-1 rounded text-center hot_deals">
                                                            <p>{timerSeconds}</p>
                                                            <p className="uppercase text-base">
                                                                <small>sec</small>
                                                            </p>
                                                        </section>
                                                    </div>
                                                    <h1 className="mt-1 text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">Samsung</h1>

                                                    <h1 className="capitalize text-base text-center overflow-hidden whitespace-nowrap overflow-ellipsis">galaxy note 20 Ultra</h1>

                                                    <div>
                                                        <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                                            <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.1,03,999</p>

                                                            <p className="capitalize text-xs font-bold p-1 text-gray-600">
                                                                <del>Rs 1,13,999</del>
                                                            </p>
                                                        </div>
                                                        <div>
                                                            <button className="uppercase hot_button text-xs rounded text-white w-full">add to cart </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Sliders>
                                </div>
                                <Grid container>
                                    <Grid item xs={6}>
                                        <div className="p-2">
                                            <img src="https://www.bajajfinserv.in/New_BannerProductImage_3_Motor_Image_3_Moto_Destop_Banner_03.jpg" className="rounded" />
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className="p-2">
                                            <img src="https://fdn.gsmarena.com/imgroot/news/19/10/huawei-mate-30-over-1-million/-1220x526/gsmarena_000.jpg" className="rounded align_samsung" />
                                        </div>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                        <Grid container>
                            <Grid item xs={12}>
                                <img src="category/bottombanner2.jpg" />
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
        </div>
    );
}

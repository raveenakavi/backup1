import Head from "next/head";
import styles from "../styles/Home.module.css";
import Grid from "@material-ui/core/Grid";
import Select from "react-select";
import { useState, useEffect, useRef } from "react";
import Rating from "@material-ui/lab/Rating";
import SearchIcon from "@material-ui/icons/Search";
import { ToggleButton } from "primereact/togglebutton";
import Switch from "react-switch";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import StarIcon from "@material-ui/icons/Star";
import Slider from "react-slick";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { ButtonBase } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
export default function Home() {
    const [clientreview, setClientReview] = useState(2);
    const customSlider = useRef();
    const customSliders = useRef();
    const latest = useRef();
    const customipad = useRef();
    var latests = {
        dots: false,
        autoplay: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]
    };
    var brands = {
        dots: false,
        autoplay: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]
    };
    var iphones = {
        dots: false,
        autoplay: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]
    };
    const next = () => {
        slider.slickNext();
    };
    const previous = () => {
        slider.slickPrev();
    };
    return (
        <div>
            <div className="flex flex-wrap">
                <div className="w-full fixed brand_header">
                    <nav className="relative flex flex-wrap items-center justify-between px-2 py-3 bg-black">
                        <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
                            <div className="w-full relative flex justify-between lg:w-auto  lg:static lg:block lg:justify-start">
                                
                            </div>
                            <div className="flex lg:flex-grow items-center" id="example-navbar-info">
                                <ul className=" flex sm:flex md:flex lg:flex flex-row lg:flex-row list-none ml-auto text-center m-auto">
                                    <li className="nav-item">
                                        <a className=" brand_menu px-1 py-1 sm:px-5 sm:py-2 md:px-12 md:py-2 lg:px-12 lg:py-2 flex items-center text-sm capitalize font-medium leading-snug text-white hover:opacity-75" href="#pablo">
                                            Apple store
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="brand_menu px-1 py-1 sm:px-5 sm:py-2 md:px-12 md:py-2 lg:px-12 lg:py-2 flex items-center text-sm capitalize font-medium leading-snug text-white hover:opacity-75" href="#pablo">
                                            mac
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="brand_menu px-1 py-1 sm:px-5 sm:py-2 md:px-12 md:py-2 lg:px-12 lg:py-2 flex text-sm capitalize font-medium leading-snug text-white hover:opacity-75" href="#pablo">
                                            ipod
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="brand_menu px-1 py-1 sm:px-5 sm:py-2 md:px-12 md:py-2 lg:px-12 lg:py-2 flex items-center text-sm capitalize font-medium leading-snug text-white hover:opacity-75" href="#pablo">
                                            iphone
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="brand_menu px-1 py-1 sm:px-5 sm:py-2 md:px-12 md:py-2 lg:px-12 lg:py-2 flex items-center text-sm capitalize font-medium leading-snug text-white hover:opacity-75" href="#pablo">
                                            watch
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="brand_menu px-1 py-1 sm:px-5 sm:py-2 md:px-12 md:py-2 lg:px-12 lg:py-2 flex items-center text-sm capitalize font-medium leading-snug text-white hover:opacity-75" href="#pablo">
                                            tv
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="brand_menu px-1 py-1 sm:px-5 sm:py-2 md:px-12 md:py-2 lg:px-12 lg:py-2 flex items-center text-sm capitalize font-medium leading-snug text-white hover:opacity-75" href="#pablo">
                                            accessories
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>

            <div className="mt-12">
                <Grid container className="p-0">
                    <Grid item xs={12}>
                        <img src="banner.png" />
                    </Grid>
                </Grid>
            </div>

            <Grid container className="p-5 sm:p-12 md:p-12 lg:p-12">
                <Grid item xs={12} lg={6}>
                    <p className="website_brands  capitalize font-bold pl-0 pr-10 sm:pl-10 sm:pr-10 md:pr-10 md:pl-10 lg:pr-10 lg:pl-10 pt-0 right pr-0  pb-0">discover a new world</p>
                </Grid>
                <Grid item xs={12} lg={6}>
                    <p className="text-left text-base p-5 sm:p-10 md:p-10 lg:p-10  text-sm left pl-0 pr-0 sm:pl-0 md:pl-0 lg:pl-0 website_description pb-0 ">We at Apple, we create technology that's never been done before, so you can do what you can't.</p>
                </Grid>
            </Grid>

            <div>
                <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                    <Grid item xs={12}>
                        <img src="ipad.jpg" />
                    </Grid>
                </Grid>
            </div>
            <div>
                <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                    <Grid item xs={12}>
                        <div>
                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold sm:text-2xl md:text-2xl lg:text-2xl text-lg italic brand_heads"> Latest mac & imac</p>

                                <p className="capitalize text-sm font-bold brand_seeall"> see all </p>
                                <div className="slider-arrow">
                                    <ButtonBase className="arrow-btn prev" onClick={() => latest.current.slickPrev()}>
                                        <ArrowBackIosIcon style={{ fontSize: 20 }} />
                                    </ButtonBase>
                                    <ButtonBase className="arrow-btn next" onClick={() => latest.current.slickNext()}>
                                        <ArrowForwardIosIcon style={{ fontSize: 20 }} />
                                    </ButtonBase>
                                </div>
                            </div>
                        </div>
                    </Grid>
                </Grid>
                <Slider ref={(sliders) => (latest.current = sliders)} {...latests}>
                    <div className="p-4 brand-slide">
                        <div>
                            <img src="laptop/laptop1.jpg" />
                        </div>
                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)</h1>
                            <div>
                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                    <p className="capitalize text-xs font-bold p-1">
                                        <StarIcon className="brand_staricon" />
                                    </p>
                                    <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                </div>
                            </div>
                            <div>
                                <List component="nav" aria-label="contacts">
                                    <ListItem className="list_datas">
                                        <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                        <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                    </ListItem>
                                    <ListItem className="list_datas">
                                        <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                        <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                    </ListItem>
                                    <ListItem className="list_datas">
                                        <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                        <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                    </ListItem>
                                </List>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop2.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop3.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i3 10th Gen Mac OS Catalina Laptop MWTL2HN/A (8GB RAM, 256GB SSD,13 inch, Gold,1.29 kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop4.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop5.jpeg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop6.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop1.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                </Slider>
            </div>
            <div>
                <Grid container className="p-5">
                    <Grid item xs={12}>
                        <img src="iphonebanner.jpg" />
                    </Grid>
                </Grid>
            </div>
            <div>
                <Grid container className="sm:p-5 md:p-5 lg:p-5 p-2">
                    <Grid item xs={12}>
                        <div>
                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold   sm:text-2xl md:text-2xl lg:text-2xl text-xl italic brand_heads"> offers on iphones</p>

                                <p className="capitalize text-sm font-bold brand_seeall"> see all </p>
                                <div className="slider-arrow">
                                    <ButtonBase className="arrow-btn prev" onClick={() => customSlider.current.slickPrev()}>
                                        <ArrowBackIosIcon style={{ fontSize: 20 }} />
                                    </ButtonBase>
                                    <ButtonBase className="arrow-btn next" onClick={() => customSlider.current.slickNext()}>
                                        <ArrowForwardIosIcon style={{ fontSize: 20 }} />
                                    </ButtonBase>
                                </div>
                            </div>
                        </div>
                    </Grid>
                </Grid>
                <Slider ref={(slider) => (customSlider.current = slider)} {...iphones}>
                    <div className="p-4 brand-iphone">
                        <div>
                            <img src="compare11.png" />
                        </div>
                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)</h1>
                            <div>
                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                    <p className="capitalize text-xs font-bold p-1">
                                        <StarIcon className="brand_staricon" />
                                    </p>
                                    <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                </div>
                            </div>
                            <div>
                                <List component="nav" aria-label="contacts">
                                    <ListItem className="list_datas">
                                        <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                        <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                    </ListItem>
                                    <ListItem className="list_datas">
                                        <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                        <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                    </ListItem>
                                    <ListItem className="list_datas">
                                        <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                        <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                    </ListItem>
                                </List>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-iphone">
                            <div>
                                <img src="compare2.png" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div className="p-4 brand-iphone">
                            <div>
                                <img src="compare3.png" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i3 10th Gen Mac OS Catalina Laptop MWTL2HN/A (8GB RAM, 256GB SSD,13 inch, Gold,1.29 kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-iphone">
                            <div>
                                <img src="compare4.png" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-iphone">
                            <div>
                                <img src="compare11.png" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-iphone">
                            <div>
                                <img src="compare2.png" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-iphone">
                            <div>
                                <img src="compare3.png" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                </Slider>

                <div>
                    <Grid container className="p-1 sm:p-5 md:p-5 lg:p-5">
                        <Grid item xs={12}>
                            <img src="ipadbanner.PNG" />
                        </Grid>
                    </Grid>
                </div>
                <div>
                     <Grid container className="p-1 sm:p-5 md:p-5 lg:p-5">
                    <Grid item xs={12}>
                        <div>
                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold sm:text-2xl md:text-2xl lg:text-2xl text-xl italic  brand_heads"> ipad & ipad accessories</p>

                                <p className="capitalize text-sm font-bold brand_seeall"> see all </p>
                                <div className="slider-arrow">
                                    <ButtonBase className="arrow-btn prev" onClick={() => customipad.current.slickPrev()}>
                                        <ArrowBackIosIcon style={{ fontSize: 20 }} />
                                    </ButtonBase>
                                    <ButtonBase className="arrow-btn next" onClick={() => customipad.current.slickNext()}>
                                        <ArrowForwardIosIcon style={{ fontSize: 20 }} />
                                    </ButtonBase>
                                </div>
                            </div>
                        </div>
                    </Grid>
                </Grid>
                <Slider ref={(sliders) => (customipad.current = sliders)} {...brands}>
                    <div className="p-4 brand-slide">
                        <div>
                            <img src="laptop/laptop1.jpg" />
                        </div>
                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)</h1>
                            <div>
                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                    <p className="capitalize text-xs font-bold p-1">
                                        <StarIcon className="brand_staricon" />
                                    </p>
                                    <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                </div>
                            </div>
                            <div>
                                <List component="nav" aria-label="contacts">
                                    <ListItem className="list_datas">
                                        <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                        <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                    </ListItem>
                                    <ListItem className="list_datas">
                                        <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                        <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                    </ListItem>
                                    <ListItem className="list_datas">
                                        <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                        <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                    </ListItem>
                                </List>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop2.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop3.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i3 10th Gen Mac OS Catalina Laptop MWTL2HN/A (8GB RAM, 256GB SSD,13 inch, Gold,1.29 kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop4.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop5.jpeg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop6.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-slide">
                            <div>
                                <img src="laptop/laptop1.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                <div>
                                    <List component="nav" aria-label="contacts">
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">High Resolution display</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />
                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Fingerprint Scanner</p>
                                        </ListItem>
                                        <ListItem className="list_datas">
                                            <FiberManualRecordIcon style={{ fontSize: 10 }} />

                                            <p className="ml-3 leading-7 text-base whitespace-nowrap overflow-hidden overflow-ellipsis">Withheadphone Jack</p>
                                        </ListItem>
                                    </List>
                                </div>
                            </div>
                        </div>
                    </div>
                </Slider>
                </div>

                <div>
                    <Grid container className="p-2 sm:p-5 md:p-5 lg:p-5">
                        <Grid item xs={12}>
                            <img src="appleaccessories.png" />
                        </Grid>
                    </Grid>
                </div>

                <div>
                     <Grid container className="p-2 sm:p-5 md:p-5 lg:p-5">
                    <Grid item xs={12}>
                        <div>
                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                <p className="flex-grow capitalize inline-flex items-center p-0 brand_heads font-bold sm:text-2xl md:text-2xl lg:text-2xl text-xl italic"> ipad & ipad accessories</p>

                                <p className="capitalize text-sm font-bold brand_seeall"> see all </p>
                                <div className="slider-arrow">
                                    <ButtonBase className="arrow-btn prev" onClick={() => customSliders.current.slickPrev()}>
                                        <ArrowBackIosIcon style={{ fontSize: 20 }} />
                                    </ButtonBase>
                                    <ButtonBase className="arrow-btn next" onClick={() => customSliders.current.slickNext()}>
                                        <ArrowForwardIosIcon style={{ fontSize: 20 }} />
                                    </ButtonBase>
                                </div>
                            </div>
                        </div>
                    </Grid>
                </Grid>
                <Slider ref={(sliders) => (customSliders.current = sliders)} {...brands}>
                    <div className="p-4 brand-ipods">
                        <div>
                            <img src="laptop/laptop1.jpg" />
                        </div>
                        <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                            <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 10th Gen Mac OS Catalina Laptop MVH22HN/A (8GB RAM, 512GB SSD,13 inch, Space Grey,1.29 kg)</h1>
                            <div>
                                <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                    <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                    <p className="capitalize text-xs font-bold p-1">
                                        <StarIcon className="brand_staricon" />
                                    </p>
                                    <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-ipods">
                            <div>
                                <img src="laptop/laptop2.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                                            </div>
                        </div>
                    </div>

                    <div>
                        <div className="p-4 brand-ipods">
                            <div>
                                <img src="laptop/laptop3.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i3 10th Gen Mac OS Catalina Laptop MWTL2HN/A (8GB RAM, 256GB SSD,13 inch, Gold,1.29 kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-ipods">
                            <div>
                                <img src="laptop/laptop4.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-ipods">
                            <div>
                                <img src="laptop/laptop5.jpeg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-ipods">
                            <div>
                                <img src="laptop/laptop6.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="p-4 brand-ipods">
                            <div>
                                <img src="laptop/laptop1.jpg" />
                            </div>
                            <div className="pl-0 sm:pl-7 md:pl-7 lg:pl-7">
                                <h1 className="text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis">Apple Macbook Air Core i5 5th Gen Mac OS Sierra Laptop MQD32HN/A (8GB RAM, 128GB SSD, 13 Inch, Silver, 1.35 Kg)</h1>
                                <div>
                                    <div className="flex justify-between pt-1 pb-1 pr-1 pl-1">
                                        <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs"> Rs.74,999</p>
                                        <p className="capitalize text-xs font-bold p-1">
                                            <StarIcon className="brand_staricon" />
                                        </p>
                                        <p className="capitalize text-xs font-bold p-1"> 4.3</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </Slider>
                </div>
                <div>
                    <Grid container className="p-2 pb-0 sm:p-5 md:p-5 lg:p-5">
                        <Grid item xs={6} className=" p-2 sm:p-2 sm:pl-12 md:p-2 md:pl-12 lg:p-2 lg:pl-12">
                            <img src="watchside.png" className="rounded-xl" />
                        </Grid>

                        <Grid item xs={6} className=" p-2 sm:p-2 sm:pr-12 md:p-2 md:pr-12 lg:p-2 lg:pr-12">
                            <img src="download.jpg" className="rounded-xl"/>
                        </Grid>
                    </Grid>
                </div>

                <div>
                    <Grid container className="p-2 pb-0 sm:p-5 md:p-5 lg:p-5">
                        <Grid item xs={6} className=" p-2 sm:p-2 sm:pl-12 md:p-2 md:pl-12 lg:p-2 lg:pl-12">
                            <img src="sidebanner1.png" className="rounded-xl" />
                        </Grid>

                        <Grid item xs={6} className=" p-2 sm:p-2 sm:pr-12 md:p-2 md:pr-12 lg:p-2 lg:pr-12">
                            <img src="sidebanner2.png" className="rounded-xl"/>
                        </Grid>
                    </Grid>
                </div>
            </div>
        </div>
    );
}

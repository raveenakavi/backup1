import React, { useState, useRef } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import SearchIcon from '@material-ui/icons/Search';
import RemoveIcon from '@material-ui/icons/Remove';
import RangeSlider from 'reactrangeslider';

export default function HeadPhones() {

  const [value, setValue] = React.useState([10, 50]);
  
    const StyledRating = withStyles({
        iconFilled: {
          divor: '#f11a08',
        },
        iconHover: {
          divor: '#ff3d47',
        },
      })(Rating);
    
    const [divShow, setDivShow] = useState(false);
    const [divHide, setDivHide] = useState(false);
    
    const[add,minus]=useState(true);
    
    const selectChange = (element) =>{
      if(element == add){
        setDivShow(false);
        setDivHide(true);
      }else if(element==minus){
        setDivShow(true);
        setDivHide(false);
      }
    }

    // function Example2() {
    //   const [value, setValue] = React.useState([10, 50]);
    //   return (
    //     <div>
    //       <div>
    //         <RangeSlider
    //           progress
    //           style={{ marginTop: 16 }}
    //           value={value}
    //           onChange={value => {
    //             setValue(value);
    //           }}
    //         />
    //       </div>
    //       <div>
    //         <input>
    //           <input
    //             min={0}
    //             max={100}
    //             value={value[0]}
    //             onChange={nextValue => {
    //               const [start, end] = value;
    //               if (nextValue > end) {
    //                 return;
    //               }
    //               setValue([nextValue, end]);
    //             }}
    //           />
    //           <p>to</p>
    //           <input
    //             min={0}
    //             max={100}
    //             value={value[1]}
    //             onChange={nextValue => {
    //               const [start, end] = value;
    //               if (start > nextValue) {
    //                 return;
    //               }
    //               setValue([start, nextValue]);
    //             }}
    //           />
    //         </input>
    //       </div>
    //     </div>
    //   );
    // }

    return (
        <div className="container md:grid grid-cols-4 gap-8 similar-products earbuds py-10 px-6">
            <div className="headphones row-span-6">
            <h3 className="text-lg font-bold border-b border-yellow-500 pb-2 pt-1 px-2">Filters <span className="float-right text-xs font-normal text-gray-500 pt-2 pr-2">Clear All </span></h3>

            {/* <RangeSlider
      value={ value }
      onChange={ onChange }
      min={ 20 }
      max={ 100 }
      step={ 5 }
    /> */}
<div className="py-4 border-b broder-yellow-500">
            <h3 className="text-lg font-bold pb-2 pt-1 px-2">BRAND <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2">47 Brands </span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Apple
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            Celelkon
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Karbonn
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            LAVA
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Micromax
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Samsung
          </label>
        </div>        
                      </form>
            </div>

            <div className="py-4 border-b broder-yellow-500">
            <h3 className="text-lg font-bold pb-2 pt-1 px-2">RAM </h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            6 GB &amp; Above
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            4 GB
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            3 GB
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            2 GB
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            512 MB - 1 GB
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Less than 512 MB
          </label>
        </div>        
                      </form>
            </div>
            
            <div className="py-4 border-b broder-yellow-500">
            <h3 className="text-lg font-bold pb-2 pt-1 px-2">Scrren Size <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Less than 3 inch
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            4 inch - 4.99 inch
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            5 inch - 5.99 inch
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            6 inch &amp; Above
          </label>
        </div>
                      </form>
            </div>

<div className="py-4 border-b broder-yellow-500">
            <h3 className="text-lg font-bold pb-2 pt-1 px-2">Primary Camera <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Less than 2 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            2 Mp - 3.99 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            4 Mp - 11.99 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            12 Mp - 19.99 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            20 Mp - 39.99 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Above 40 MP
          </label>
        </div>        
                      </form>
            </div>   

            <div className="py-4 border-b broder-yellow-500">
            <h3 className="text-lg font-bold pb-2 pt-1 px-2">Secondary Camera <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Less than 2 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            2 Mp - 3.99 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            4 Mp - 11.99 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            12 Mp - 19.99 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            20 Mp - 39.99 MP
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Above 40 MP
          </label>
        </div>        
                      </form>
            </div>  


            <div className="py-4 border-b broder-yellow-500">
            <h3 className="text-lg font-bold pb-2 pt-1 px-2">PROCESSOR <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Dual Core
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            Hexa Core
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Octa Core
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Quad Core
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Single Core
          </label>
        </div>
      
                      </form>
            </div>  


            <div className="py-4 border-b broder-yellow-500">
            <h3 className="text-lg font-bold pb-2 pt-1 px-2">FEATURES <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Bluetooth
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            Wi-Fi
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            NFC
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            FM Radio
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Finger Print Scanner
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            In-Display Finger Print Scanner
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            3.5mm Audio Jack
          </label>
        </div>        
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option3" />
            Without Camera (Front &amp; Back)
          </label>
        </div>                      </form>
            </div>  

            <div className="py-4 border-b broder-yellow-500">
            <h3 className="text-lg font-bold pb-2 pt-1 px-2">OPERATING SYSTEM <span className="float-right text-xs font-normal text-yellow-500 pt-2 pr-2"><RemoveIcon /></span></h3>

{/* <h4 className="py-4 px-10 underline"><SearchIcon /> <span className="px-4">Search</span></h4> */}
<form>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option1" checked={true} />
            Android
          </label>
        </div>
        <div className="radio px-10 py-2">
          <label>
            <input type="radio" value="option2" />
            IOS
          </label>
        </div>
                      </form>
            </div> 



                        </div>

            <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/tab-image-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character  </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/tab-image-2.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/tab-image-3.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/tab-image-4.jpg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

            <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/category-headphone-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>
  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/category-headphone-2.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>
  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/category-headphone-3.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/category-headphone-4.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-2.png'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-3.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-4.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

  <div className="element headphones" id="surround">
        <div className="flex">
        <span className="vc text-xs">New</span>
        <img src={'/asset/earbuds-1.jpeg'} />
            <StyledRating className="mr-3" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        <div className="w-full text-black text-sm pt-4 pb-2 flex relative px-3"><span id="onhover" className="w-1/2 earbuds-icon"><SyncAltIcon /> Compare</span> <span className="w-1/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></div>
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <span className="font-normal text-gray-400 text-xs px-3">(divor and Variant 30 Character)</span>
      <p className="w-full flex text-black text-sm pt-4 px-3">Rs.62,899 <span className="right-3 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <span className="text-gray-600 text-sm py-2 px-3" id="initial"> $ 1,11,999 <span className="text-yellow-600">Save Rs.1,249</span></span>
      <span className="pt-3 ml-auto" id="onhover"><button className="w-full bg-yellow-600 text-white"> <span className="pl-2 py-4 text-md">Add to cart</span></button></span>
  </div>

        </div>
    )
}

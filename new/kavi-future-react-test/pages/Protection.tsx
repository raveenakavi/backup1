import React from 'react'
import GradeIcon from '@material-ui/icons/Grade';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import {Grid, Button, InputBase} from '@material-ui/core';
import SecurityIcon from '@material-ui/icons/Security';
import SimilarProducts from './SimilarProducts';

export default function Protection() {
    return (
        <div>
        <div className="protection w-full p-5">
            <h2 className="py-4 px-3 rounded-t-lg text-yellow-500 text-md md:text-2xl font-bold align-middle"><SecurityIcon /> <span className="px-0 md:px-3">Want to Protect your Phone?</span> <span className="text-xs right-10 absolute pt-2"> know More</span></h2>
            <div className="grid md:grid-flow-col">
                <div className="bg-white shadow-md my-5 mx-5 p-2">
                <div className="md:flex">
                    <div className="flex">
                <input name="isGoing" type="checkbox" />
                <img src={'/asset/protection1.png'} className="" />
                </div>
                <div className="pt-2 pl-5">
                    <h3 className="text-xs md:text-md lg:text-lg font-bold md:h-20">1 Year Accidential, Liquid and Screen Protection Plan</h3>
                    <p className="text-xs md:text-md text-green-300 font-bold pt-2">Rs.3,899 <span className="text-gray-300 pl-5 line-through">Rs. 6,899</span></p>

                </div>
                </div>
                <p className="py-5 text-sm">Brand authorized repair / replacement guarantee for 1 year. Special offer 'Get 6 month Google One trial with Complete Mobile Protection .. <span className="text-xs text-yellow-400">Know More</span></p>
                </div>
                <div className="bg-white shadow-md my-5 mx-5 p-2">
                <div className="md:flex">
                    <div className="flex">
                <input name="isGoing" type="checkbox" />
                <img src={'/asset/protection-2.png'} className="" />
                </div>
                <div className="pt-2 pl-5">
                    <h3 className="text-xs md:text-md lg:text-lg font-bold md:h-20">1 Year Extended Warranty</h3>
                    <p className="text-xs md:text-md text-green-300 font-bold pt-2">Rs.1,299 <span className="text-gray-300 pl-5 line-through">Rs. 1,899</span></p>

                </div>
                </div>
                <p className="py-5 text-sm">Brand authorized repair / replacement guarantee for 1 year. Special offer 'Get 6 month Google One trial with Complete Mobile Protection .. <span className="text-xs text-yellow-400">Know More</span></p>
                </div>
                <div className="my-5 mx-16 leading-10">
                    <table className="relative">
                        <tr>
                            <td className="w-1/2">Product :</td>
                            <td className="right-0 absolute">1,19,249</td>
                        </tr>
                        <tr>
                            <td>Protect Plans :</td>
                            <td className="right-0 absolute">3, 889</td>
                        </tr>
                        <tr className="border-t border-b border-gray-300 leading-12">
                            <td>To be Paid :</td>
                            <td className=" right-0 absolute">1,23,128</td>
                        </tr>
                    </table>
                    <button className="rounded-full protection-button px-10 py-2 my-4 text-white text-xs">Add Protection Plan</button>
                </div>

                

            </div>
           
        </div>

<div className="protection grid md:grid-flow-col border border-gray-300 my-4 py-1 mx-10 text-xs text-gray-400 text-center align-middle">
<div>Product Warranty &nbsp; Service Details <ArrowRightIcon /></div>
<div className="border-l border-r border-gray-300">Manufacturer Information <ArrowRightIcon /></div>
<div><span className="text-black">Sold by :</span> fortune Communications 5 <GradeIcon /> <ArrowRightIcon /></div>
</div>
{/* <SimilarProducts /> */}
</div>


    )
}

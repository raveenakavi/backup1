import React, { useState, Component, useEffect } from 'react'
import GradeIcon from '@material-ui/icons/Grade';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import { Grid, Button } from '@material-ui/core';
import StarRatings from 'react-star-ratings'
import SearchIcon from '@material-ui/icons/Search';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import TextsmsIcon from '@material-ui/icons/Textsms';
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import Icon from '@material-ui/core/Icon';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Avatar from '@material-ui/core/Avatar';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import FileCopyRoundedIcon from '@material-ui/icons/FileCopyRounded';
// import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { RMIUploader } from "react-multiple-image-uploader";

export default function orderstatus(writereviewprops): JSX.Element  {
    console.log(writereviewprops);

   const [name, onChangeText] = React.useState<string>('');
    const [review, changereview] = React.useState<string>('');
    const [showtoast, setShowToast] = useState<boolean>(false)
    const [error, setError] = useState<string>('');
    const [starCount, setRating] = useState<number>(0)
    const [starServicesReviewCount, setServicesReviewRating] = useState<number>(0)
    const [starDeliveryReviewCount, setDeliveryReviewRating] = useState<number>(0)
    const [designRating, setDesignRating] = useState<number>(0);
    const [performanceRating, setPerformanceRating] = useState<number>(0);
    const [cameraRating, setCameraRating] = useState<number>(0);
    const [storageRating, setStorageRating] = useState<number>(0);
    const [costRating, setCostRating] = useState<number>(0);
    const [availRating, setAvailRating] = useState<number>(0);
    const [hover, setHover] = React.useState(-1);
     const [selectedIndex, setSelectedIndex] = useState(0);
    const [starColorChange, setStarColorChange] = useState("#ddd");
    const [starServicesReviewColorChange, setServicesReviewColorChange] = useState("#ddd");
    const [starDeliveryReviewColorChange, setDeliveryReviewColorChange] = useState("#ddd");
    const [designStarColorChange, setDesignStarColorChange] = useState("#ddd");
    const [performanceStarColorChange, setPerformanceStarColorChange] = useState("#ddd");
    const [cameraStarColorChange, setCameraStarColorChange] = useState("#ddd");
    const [storageStarColorChange, setStorageStarColorChange] = useState("#ddd");
    const [costStarColorChange, setCostStarColorChange] = useState("#ddd");
    const [availStarColorChange, setAvailStarColorChange] = useState("#ddd");
    const [backgroundColorChange, setBackgroundColorChange] = useState("#f5f5f5");
    const [showMoreOptions, setShowMoreOptions] = useState(false);
    const [performance1, setperformance1] = useState(false);

    const [showClearText, setShowClearText] = useState(false);
    const [showServicesReviewClearText, setShowServicesReviewClearText] = useState(false);
    const [showDeliveryReviewClearText, setShowDeliveryReviewClearText] = useState(false);
    const [value, setValue] = React.useState('Yes');


    const dataSources = [
      {
        id: 1,
        dataURL: "https://picsum.photos/seed/1/600",
      },

    ];
    const [visible, setVisible] = useState(false);
  const handleSetVisible = () => {
    setVisible(true);
  };
  const hideModal = () => {
    setVisible(false);
  };
  const onUpload = (data) => {
    console.log("Upload files", data);
  };
  const onSelect = (data) => {
    console.log("Select files", data);
  };
  const onRemove = (id) => {
    console.log("Remove image id", id);
  };

    const labels = {
      1: 'Frustrating',
      2: 'Just Bad',
      3: 'Not Bad',
      4: 'Love It !',
      5: 'Apples Master Piece',
    };

 
        const  changeRatingCount = (count) =>{
        setRating(count)
        if(count > 0){
          setShowMoreOptions(true)
          setperformance1(true)
          setShowClearText(true)
        }else{
          setShowMoreOptions(false)
          setperformance1(false)
          setShowClearText(false)
        }
        if(count == 0){
          setStarColorChange("#f9151a")
        }
        if(count == 1){
          setStarColorChange("#f9151a")
        }else if(count == 2){
          setStarColorChange("#ff585d")
        }else if(count == 3){
          setStarColorChange("#fe7b02")
        }else if(count == 4){
          setStarColorChange("#83d564")
        }else if(count == 5){
          setStarColorChange("#2ea000")
        }else{
          setStarColorChange("#ddd")
        }
      }

      
      const selectedServicesReviewStarCount = (count) =>{
        setServicesReviewRating(count)
        if(count > 0){
          setShowServicesReviewClearText(true)
        }else{
          setShowServicesReviewClearText(false)
        }
        if(count == 1){
          setServicesReviewColorChange("#f9151a")
        }else if(count == 2){
          setServicesReviewColorChange("#ff585d")
        }else if(count == 3){
          setServicesReviewColorChange("#fe7b02")
        }else if(count == 4){
          setServicesReviewColorChange("#83d564")
        }else if(count == 5){
          setServicesReviewColorChange("#2ea000")
        }else{
          setServicesReviewColorChange("#ddd")
        }
      }
      const selectedDeliveryReviewStarCount = (count) =>{
        setDeliveryReviewRating(count)
        if(count > 0){
          setShowDeliveryReviewClearText(true)
        }else{
          setShowDeliveryReviewClearText(false)
        }
        if(count == 0){
          setDeliveryReviewColorChange("#ccc")
        }else if(count == 2){
          setDeliveryReviewColorChange("#ff585d")
        }else if(count == 3){
          setDeliveryReviewColorChange("#fe7b02")
        }else if(count == 4){
          setDeliveryReviewColorChange("#83d564")
        }else if(count == 5){
          setDeliveryReviewColorChange("#2ea000")
        }else{
          setDeliveryReviewColorChange("#ddd")
        }
      }
      const selectedDesignStarCount = (designRating) =>{
        setDesignRating(designRating);
        if(designRating == 1){
          setDesignStarColorChange("#f9151a")
        }else if(designRating == 2){
          setDesignStarColorChange("#ff585d")
        }else if(designRating == 3){
          setDesignStarColorChange("#fe7b02")
        }else if(designRating == 4){
          setDesignStarColorChange("#83d564")
        }else if(designRating == 5){
          setDesignStarColorChange("#2ea000")
        }else{
          setDesignStarColorChange("#ddd")
        }
        
      }
      const selectedPerformanceStarCount = (performanceRating) =>{
        setPerformanceRating(performanceRating);
        if(performanceRating == 1){
          setPerformanceStarColorChange("#f9151a")
        }else if(performanceRating == 2){
          setPerformanceStarColorChange("#ff585d")
        }else if(performanceRating == 3){
          setPerformanceStarColorChange("#fe7b02")
        }else if(performanceRating == 4){
          setPerformanceStarColorChange("#83d564")
        }else if(performanceRating == 5){
          setPerformanceStarColorChange("#2ea000")
        }else{
          setPerformanceStarColorChange("#ddd")
        }
        
      }
      const selectedCameraStarCount = (cameraRating) =>{
        setCameraRating(cameraRating);
        if(cameraRating == 1){
          setCameraStarColorChange("#f9151a")
        }else if(cameraRating == 2){
          setCameraStarColorChange("#ff585d")
        }else if(cameraRating == 3){
          setCameraStarColorChange("#fe7b02")
        }else if(cameraRating == 4){
          setCameraStarColorChange("#83d564")
        }else if(cameraRating == 5){
          setCameraStarColorChange("#2ea000")
        }else{
          setCameraStarColorChange("#ddd")
        }
        
      }
      const selectedCostStarCount = (costRating) =>{
        setCostRating(costRating);
        if(costRating == 1){
          setCostStarColorChange("#f9151a")
        }else if(costRating == 2){
          setCostStarColorChange("#ff585d")
        }else if(costRating == 3){
          setCostStarColorChange("#fe7b02")
        }else if(costRating == 4){
          setCostStarColorChange("#83d564")
        }else if(costRating == 5){
          setCostStarColorChange("#2ea000")
        }else{
          setCostStarColorChange("#ddd")
        }
        
      }
      const selectedStorageStarCount = (storageRating) =>{
        setStorageRating(storageRating);
        if(storageRating == 1){
          setStorageStarColorChange("#f9151a")
        }else if(storageRating == 2){
          setStorageStarColorChange("#ff585d")
        }else if(storageRating == 3){
          setStorageStarColorChange("#fe7b02")
        }else if(storageRating == 4){
          setStorageStarColorChange("#83d564")
        }else if(storageRating == 5){
          setStorageStarColorChange("#2ea000")
        }else{
          setStorageStarColorChange("#ddd")
        }
        
      }
      const selectedAvailStarCount = (availRating) =>{
        setAvailRating(availRating);
        if(availRating == 1){
          setAvailStarColorChange("#f9151a")
        }else if(availRating == 2){
          setAvailStarColorChange("#ff585d")
        }else if(availRating == 3){
          setAvailStarColorChange("#fe7b02")
        }else if(availRating == 4){
          setAvailStarColorChange("#83d564")
        }else if(availRating == 5){
          setAvailStarColorChange("#2ea000")
        }else{
          setAvailStarColorChange("#ddd")
        }
        
      }
      const clearCount = () =>{
        setRating(0)
        setShowMoreOptions(false)
      }
      const clearServicesReviewCount = () =>{
        setServicesReviewRating(0)
      }
      const clearDeliveryReviewCount = () =>{
        setDeliveryReviewRating(0)
      }      
    const handleRatingClick= (e, name)=> {

        console.log('You left a ' + name.rating + ' star rating for ' + name.caption);
        
    
    }

  return (
    <Grid>
    <div className="mt-6">
        <div className="md:w-full reviews border border-gray-300">
          <div className="flex bg-gradient-to-r from-gray-400 via-gray-600 to-gray-500">
            <div className="w-1/2">
              <h4 className="text-left font-bold h-12 pl-3 pt-3 text-white">Ratings &amp; Reviews</h4>
            </div>
            <div className="w-1/2 text-right pr-3 pt-3 text-white"> <button>Close</button> <span className="fill-current text-white-600"><button><HighlightOffIcon /></button></span></div>
          </div>

<div className="md:flex">

      <div className="my-order md:flex panel-content md:w-5/12 pl-1 mt-1 border border-grey-300 rounded-lg">

<div className="w-full mb-2">
  <div className="float-right">
  {showClearText ? (<>
                                <Button onClick={() => clearCount()}><span className="float-right capitalize text-xs font-normal text-gray-500 pr-2">Clear</span></Button>
                                </>):null}
  </div>
  <div className="flex mb-2 ml-5">
    <div className="w-2/6">
      <img src="asset/order-1.png" className="m-2 mb-3" />
    </div>

    <div className="m-2 text-left">
      <p className="text-sm text-black-300 leading-loose mb-3">Apple Iphone 11 pro *</p>
      <span className="text-sm text-gray-400 leading-loose">
        <StarRatings disabled={false} numberOfStars={5} rating={starCount} starRatedColor={starColorChange}
                                emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'}
                                starDimension={27} starStyle={{margin:5}}  changeRating={(rating)=>  changeRatingCount(rating)}
                              /></span>

                              
    </div>
  </div>



                        <div className="pl-7">
                        {showMoreOptions ? (<>
                          <p className="flex pb-8 font-bold">Can you tell us more in particular? <span className="ml-2 text-gray-300">(Optional)</span></p>
                          <div className="flex pb-3">
                            <div className="w-3/6">
                              <p>Design</p>
                            </div>
                            <div>
                              <StarRatings starDimension={20} disabled={false} maxStars={5} rating={designRating} starRatedColor={designStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(designRating)=> selectedDesignStarCount(designRating)} />
                            </div>
                          </div>
                          <div className="flex pb-3">
                            <div className="w-3/6">
                              <p>Performance</p>
                            </div>
                            <div>
                              <StarRatings starDimension={20} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(performanceRating)=> selectedPerformanceStarCount(performanceRating)} />
                            </div>
                          </div>
                          <div className="flex pb-3">
                            <div className="w-3/6">
                              <p>Camera</p>
                            </div>
                            <div>
                              <StarRatings starDimension={20} disabled={false} maxStars={5} rating={cameraRating} starRatedColor={cameraStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(cameraRating)=> selectedCameraStarCount(cameraRating)} />
                            </div>
                          </div>
                          <div className="flex pb-3">
                            <div className="w-3/6">
                              <p>Storage</p>
                            </div>
                            <div>
                              <StarRatings starDimension={20} disabled={false} maxStars={5} rating={storageRating} starRatedColor={storageStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(storageRating)=> selectedStorageStarCount(storageRating)} />
                            </div>
                          </div>
                          <div className="flex pb-3">
                            <div className="w-3/6">
                              <p>Cost</p>
                            </div>
                            <div>
                              <StarRatings starDimension={20} disabled={false} maxStars={5} rating={costRating} starRatedColor={costStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(costRating)=> selectedCostStarCount(costRating)} />
                            </div>
                          </div>
                          <div className="flex pb-3">
                            <div className="w-3/6">
                              <p>Available Features</p>
                            </div>
                            <div>
                              <StarRatings starDimension={20} disabled={false} maxStars={5} rating={availRating} starRatedColor={availStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(availRating)=> selectedAvailStarCount(availRating)} />
                            </div>
                          </div>
                          </>): null}
                        </div>
                      
                      <div className="m-7">
                        <p className="text-xs font-bold">Title of your review *</p>
                        <input onBlur={ () => setBackgroundColorChange("#f5f5f5") } onFocus={ () => setBackgroundColorChange("#fff") } className="w-full" style={{backgroundColor:backgroundColorChange,color:"#cbcbcb",borderColor:"#cbcbcb",borderStyle:"dotted",borderWidth:1,borderRadius: 1,padding:8,marginBottom:10}} placeholder='Describe this product in max 3 words'/>
                        <p className="text-xs font-bold">Write your review *</p>
                        <input onBlur={ () => setBackgroundColorChange("#f5f5f5") } onFocus={ () => setBackgroundColorChange("#fff") } className="w-full" style={{backgroundColor:backgroundColorChange,color:"#cbcbcb",borderColor:"#cbcbcb",borderStyle:"dotted",borderWidth:1,borderRadius: 1,padding:8,marginBottom:10}} value={review} placeholder="Give your detail feedback on this product to help other buyers" />
                        <p className="flex">Would Recommened this Product? <p className="pl-5 text-gray-300">(Optional)</p></p>
                        {/* <RadioGroup onValueChange={newValue => setValue(newValue)} value={value}>
                          <div style={{flexDirection:"row"}}>
                            <RadioButton.Item uncheckedColor={Colors.grey10} color={Colors.orange10} value="Yes" label="Yes"/>
                            <RadioButton.Item uncheckedColor={Colors.grey10} color={Colors.orange10}  value="No" label="No"/>  
                          </div>
                        </RadioButton.Group> */}
                      </div>

                      <div className="flex">
              {/* <div><button onClick={handleSetVisible}>Show Me</button></div> */}
      <div className="w-full">
        <RMIUploader
        isOpen={visible}
        hideModal={hideModal}
        onSelect={onSelect}
        onUpload={onUpload}
        onRemove={onRemove}
        dataSources={dataSources}
      />
      </div>
      </div>
                      <Button className="finder-button topbar" variant="contained">
                        <p>Submit the review1</p>
                      </Button>
                      {selectedIndex == 1? (<>
                       <div>
                      <p ><p >Hi,</p> Your first-hand experiences really help other customers. Thanks! </p>
                      <div>
                       <div style={{flexDirection:"row",borderTopWidth:1,borderBottomWidth:1,borderColor:"#ddd"}}>
                         <div>
                            <img style={{width: 110, height:110,margin:"auto"}}  src=""></img>
                          </div>
                         <div style={{}}>
                           <div style={{flexDirection:"row"}}>
                             <div style={{width:"70%"}}>
                                <p  >How do you rate our services for </p>
                              </div>
                             <div style={{width:"30%",flex:1}}>
                                {showServicesReviewClearText ? (<>
                                <Button onClick={() => clearServicesReviewCount()}><p style={{textAlign:"center",color:"#ccc"}}>Clear</p></Button>
                                </>):null}
                              </div>
                            </div>
                           <div >
                              <StarRatings disabled={false} value={Number} maxStars={5} rating={starServicesReviewCount} fullStarColor={starServicesReviewColorChange}
                                emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'}
                                starSize={32} starStyle={{margin:5}} selectedStar={(rating)=> selectedServicesReviewStarCount(rating)}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                     <div >
                        <p >Title of your review *</p>
                        <input onBlur={ () => setBackgroundColorChange("#f5f5f5") } onFocus={ () => setBackgroundColorChange("#fff") } style={{backgroundColor:backgroundColorChange,color:"#cbcbcb",borderColor:"#cbcbcb",borderStyle:"dotted",borderWidth:1,borderRadius: 1,padding:8,marginBottom:10}} value={name}  placeholder='Describe this product in max 3 words' />
                        <p >Write your review *</p>
                        <input onBlur={ () => setBackgroundColorChange("#f5f5f5") } onFocus={ () => setBackgroundColorChange("#fff") } style={{backgroundColor:backgroundColorChange,color:"#cbcbcb",borderColor:"#cbcbcb",borderStyle:"dotted",borderWidth:1,borderRadius: 1,padding:8,marginBottom:10}} value={review} placeholder="Give your detail feedback on this product to help other buyers" />
                      </div>
                      <p className="text-sm flex">Do you have photos to share? <span className="text-gray-200">(Optional)</span></p>

                     <div style={{flexDirection:"row",marginBottom:10}}>
                       <div>
                          <img style={{width: 40, height:40,margin:"auto"}}  src="../../../images/camera.jpg" />
                        </div>
                       <div>
                        </div>
                       <div>
                        </div>
                      </div>
                      <Button >
                        <p style={{backgroundColor:"#606060", color:"#fff",textAlign:"center",padding:10}}>Submit the review</p>
                      </Button>

                    </div>
                    
                    </>):selectedIndex == 2? (<>
                    <div >
                      <p><p>Hi ,</p> Your first-hand experiences really help other customers. Thanks! </p>
                      <div>
                        <div style={{flexDirection:"row",borderTopWidth:1,borderBottomWidth:1,borderColor:"#ddd"}}>
                          <div>
                            <img style={{width: 110, height:110,margin:"auto"}} src="uri: writereviewprops.image" />
                          </div>
                          <div style={{}}>
                            <div style={{flexDirection:"row"}}>
                              <div style={{width:"70%"}}>
                                <p style={{fontSize:14,marginBottom:10}}>How do you rate the delivery process of </p>
                              </div>
                              <div style={{width:"30%",flex:1}}>
                                {showDeliveryReviewClearText ? (<>
                                <Button onClick={() => clearDeliveryReviewCount()}><p style={{textAlign:"center",color:"#ccc"}}>Clear</p></Button>
                                </>):null}
                              </div>
                            </div>
                            <div>
                              <StarRatings disabled={false} maxStars={5} rating={starDeliveryReviewCount} fullStarColor={starDeliveryReviewColorChange}
                                emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'}
                                starSize={32} starStyle={{margin:5}} selectedStar={(rating)=> selectedDeliveryReviewStarCount(rating)}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div>
                        <p>Title of your review *</p>
                       <input onBlur={ () => setBackgroundColorChange("#f5f5f5") } onFocus={ () => setBackgroundColorChange("#fff") } style={{backgroundColor:backgroundColorChange,color:"#cbcbcb",borderColor:"#cbcbcb",borderStyle:"dotted",borderWidth:1,borderRadius: 1,padding:8,marginBottom:10}} value={name}  placeholder='Describe this product in max 3 words' />
                        <p>Write your review *</p>
                       <input onBlur={ () => setBackgroundColorChange("#f5f5f5") } onFocus={ () => setBackgroundColorChange("#fff") } style={{backgroundColor:backgroundColorChange,color:"#cbcbcb",borderColor:"#cbcbcb",borderStyle:"dotted",borderWidth:1,borderRadius: 1,padding:8,marginBottom:10}} value={review} placeholder="Give your detail feedback on this product to help other buyers" />
                      </div>
                      <p>Do you have photos to share? <p style={{color:"#cbcbcb"}}>(Optional)</p></p>

                      <div style={{flexDirection:"row",marginBottom:10}}>
                        <div style={{width:110,height:110,marginRight:10,borderWidth:1,borderColor:"#ddd",borderStyle:"dotted",borderRadius:2}}>
                          <img style={{width: 40, height:40,margin:"auto"}} src="../../../images/camera.jpg"></img>
                        </div>
                        <div style={{width:110,height:110,marginRight:10,borderWidth:1,borderColor:"#ddd",borderStyle:"dotted",borderRadius:2}}>
                        </div>
                        <div style={{width:110,height:110,marginRight:10,borderWidth:1,borderColor:"#ddd",borderStyle:"dotted",borderRadius:2}}>
                        </div>
                      </div>
                      <Button>
                        <p style={{backgroundColor:"#606060", color:"#fff",textAlign:"center",padding:10}}>Submit the review</p>
                      </Button>
                    </div>
                  </>):null}
</div>
</div>

{performance1 ? (<>
            <div className="my-order md:flex panel-content md:w-7/12 pl-1 mt-1 border border-grey-300 rounded-lg">
              
            <div className=" w-full col-span-2 ">
            <div className="review-icon mt-10 pl-10 pr-6">
              <h3 className="text-2xl font-bold">Customer Reviews <span className="float-right text-xs font-normal text-gray-500 pr-2">Filter</span></h3>
              <div className="review pb-3">
                <h4 className="pt-8 text-l font-bold text-green-600 sm:flex">
                <StarRatings starDimension={20} value={value} disabled={false} maxStars={5} rating={designRating} starRatedColor={designStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(designRating)=> selectedDesignStarCount(designRating)} />
                   <span className="float-left pr-56 text-left md:pl-10">{value !== null && <p>{labels[hover !== -1 ? hover : value]}</p>}</span></h4>
                <div className="w-full md:w-2/5 review-status inline-block pt-2">
                  <div className="inline-block"><Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" /> </div>
                  <div className="inline-block pl-5">
                    <h4 className="text-lg font-bold ">Emma Watson</h4>
                    <span className="text-xs text-gray-300">Purchased Customer</span> <span className="text-xs text-gray-500 font-bold pl-6">7 days ago</span>
                  </div>
                </div>
                <div className="w-full md:w-3/5 inline-flex">
                  <div className='camera text-xs flex-1'> <span className="leading-normal">Camera</span><br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li> <li></li></ul>
                  </div>
                  <div className='appearance text-xs flex-1'> Appearance/Design<br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
                  </div>
                  <div className='battery text-xs flex-1'> Battery<br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
                  </div>
                </div>
              </div>
              <p className="mobile-p pt-2 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>

              <div className="w-full md:flex pb-8 border-b border-gray-200"><span className="w-full flex md:w-4/5 flex space-x-2 float-left pr-52"><img src="asset/review2.png" />
                <img src="asset/review2.png" />
                <img src="asset/review3.png" />
                <img src="asset/review4.png" /></span>
                <span className="pl-20 md:pr-0 pt-9 pb-0 inline flex md:space-x-12 text-xs"><span className="flex fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 </span>
              </div>

            </div>

            <div className="review-icon w-full col-span-2 mt-2 pl-10 pr-6">
            <div className="review pb-3">
                <h4 className="pt-8 text-l font-bold text-green-600 sm:flex">
                <StarRatings starDimension={20} value={value} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(designRating)=> selectedDesignStarCount(designRating)} />
                   <span className="float-left pr-56 text-left md:pl-10">{value !== null && <p>{labels[hover !== -1 ? hover : value]}</p>}</span></h4>
                <div className="w-full md:w-2/5 review-status inline-block pt-2">
                  <div className="inline-block"><Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" /> </div>
                  <div className="inline-block pl-5">
                    <h4 className="text-lg font-bold ">Emma Watson</h4>
                    <span className="text-xs text-gray-300">Purchased Customer</span> <span className="text-xs text-gray-500 font-bold pl-6">7 days ago</span>
                  </div>
                </div>
                <div className="w-full md:w-3/5 inline-flex">
                  <div className='camera text-xs flex-1'> <span className="leading-normal">Camera</span><br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li> <li></li></ul>
                  </div>
                  <div className='appearance text-xs flex-1'> Appearance/Design<br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
                  </div>
                  <div className='battery text-xs flex-1'> Battery<br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
                  </div>
                </div>
              </div>
              <p className="mobile-p pt-2 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>
              <div className="w-full md:flex pb-8 border-b border-gray-200"><span className="w-full flex md:w-4/5 flex space-x-2 float-left pr-52"><img src="asset/review2.png" />
                <img src="asset/review2.png" />
                <img src="asset/review3.png" />
                <img src="asset/review4.png" /></span>
                <span className="pl-20 md:pr-0 pt-9 pb-0 inline flex md:space-x-12 text-xs"><span className="flex fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 </span>
              </div>

            </div>
            
            {/* <div className="rounded-full py-3 px-6 pt-3 border border-gray-500 w-2/5">
              <Button className="finder-button">
                247 More Reviews
              </Button>
            </div> */}

          </div>
              </div>
          </>): null}
          </div>

          {/* <Rating
          name="simple-controlled"
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
            
          }}
           changeRating={(rating)=>  changeRatingCount(rating)}
        />   */}
                                      


                    </div>
                              
    </div>
    </Grid>
  )
}


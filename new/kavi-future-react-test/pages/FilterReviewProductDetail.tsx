import React, { Component, useState } from 'react'
import { Rate, Radio } from "antd";
import StarRatings from 'react-star-ratings'
import GradeIcon from '@material-ui/icons/Grade';


function FilterReviewProductDetail({ actionChangeSelectFilter }) {
  const [divShow, setDivShow] = React.useState(false);
const[add,minus]=useState(true);

const selectChange = (element) =>{
  if(element == add){
    setDivShow(false);
  }else if(element==minus){
    setDivShow(true);
  }
}

  const filterMap = [
    {
      name: "All",
      value: "All",
      icon: ""
    },
    {
      name: "Positive",
      value: "Positive",
      icon: ""
    },
    {
      name: "Negative",
      value: "Negative",
      icon: ""
    },
    {
      name: "1",
      value: 1,
      icon: <GradeIcon />
    },
    {
      name: "2",
      value: 2,
      icon: <GradeIcon />
    },
    {
      name: "3",
      value: 3,
      icon: <GradeIcon />
    },
    {
      name: "4",
      value: 4,
      icon: <GradeIcon />
    },
    {
      name: "5",
      value: 5,
      icon: <GradeIcon />
    }
  ];
  return (
    <div>
      <div className="frequent">
<h3 className="text-2xl font-bold">Customer Reviews <span className="float-right text-xs font-normal text-gray-500 pr-2"><button className="filter-button"  onClick={(e)=>{setDivShow(!divShow);minus(!add)}}>Filter</button></span></h3></div>

{(divShow)?(<>
      <div className="filter border border-gray-300 p-5 mt-3">
      <h4 className="text-sm font-bold">Sort By</h4>
                    <ul className="review-list break-normal text-sm pb-2 text-gray-500">
                        <li className="text-green-500">Latest</li>
                        <li className="text-green-500">Oldest</li>
                        <li>By Tech Experts</li>
                        <li>from Purchased Customer</li>
                        <li>By Verified Reviewer</li>
                    </ul>

                    <h4 className="text-sm font-bold">Filter by Rating</h4>
                    {/* <ul className="review-list break-normal text-sm pb-2 text-gray-500">
                        <li className="text-green-500">All</li>
                        <li className="text-green-500">Positive</li>
                        <li>Negative</li>
                        <li>5</li>
                        <li>4</li>
                        <li>3</li>
                        <li>2</li>
                        <li>1</li>
                    </ul>   */}
                    <React.Fragment>
      <Radio.Group
        defaultValue={"All"}
        size="large"
        onChange={e => actionChangeSelectFilter(e.target.value)}
      >
        <ul className="review-list break-normal text-sm pb-2 text-gray-500">
        {filterMap.map(button => {
          return (
            <li><Radio.Button value={button.value}>
              {button.name}
              {button.icon}
            </Radio.Button></li>
          );
        })}
        </ul>
      </Radio.Group>
    </React.Fragment> 

                    <h4 className="text-sm font-bold">Review On</h4>
                    <ul className="review-list break-normal text-sm pb-2 text-gray-500">
                        <li className="text-green-500">Product</li>
                        <li className="text-green-500">Service</li>
                        <li>Delivery</li>
                    </ul>          

                    
      </div>
      </>):null}
    
    </div>
  );
}

export default FilterReviewProductDetail;

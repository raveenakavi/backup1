import React, { Component, useState } from 'react'
import GradeIcon from '@material-ui/icons/Grade';
import StarRatings from 'react-star-ratings'
import {Grid, Button, InputBase} from '@material-ui/core';
// import Rating from '@material-ui/lab/Rating';
import SearchIcon from '@material-ui/icons/Search';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import TextsmsIcon from '@material-ui/icons/Textsms';
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import Icon from '@material-ui/core/Icon';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Avatar from '@material-ui/core/Avatar';
// import WriteReview from '../pages/WriteReview'
import orderstatus from '../pages/WriteReview';

export default function Ratings1() {

  const [name, onChangeText] = React.useState<string>('');
  const [review, changereview] = React.useState<string>('');
  const [showtoast, setShowToast] = useState<boolean>(false)
  const [error, setError] = useState<string>('');
  const [starCount, setRating] = useState<number>(0)
  const [starServicesReviewCount, setServicesReviewRating] = useState<number>(0)
  const [starDeliveryReviewCount, setDeliveryReviewRating] = useState<number>(0)
  const [designRating, setDesignRating] = useState<number>(0);
  const [performanceRating, setPerformanceRating] = useState<number>(0);
  const [performanceStarColorChange, setPerformanceStarColorChange] = useState("#ddd");
  const [starColorChange, setStarColorChange] = useState("#ddd");

  const [gradeIndex, setGradeIndex] = useState();
    const GRADES = ['Poor', 'Fair', 'Good', 'Very good', 'Excellent'];
    const activeStar = {
        fill: 'yellow'
    };

    const changeGradeIndex = ( index ) => {
        setGradeIndex(index);
    }
    

  const  changeRatingCount = (count) =>{
    setRating(count)
    if(count == 0){
      setStarColorChange("#f9151a")
    }
    if(count == 1){
      setStarColorChange("#f9151a")
    }else if(count == 2){
      setStarColorChange("#ff585d")
    }else if(count == 3){
      setStarColorChange("#fe7b02")
    }else if(count == 4){
      setStarColorChange("#83d564")
    }else if(count == 5){
      setStarColorChange("#2ea000")
    }else{
      setStarColorChange("#ddd")
    }
  }


  const selectedPerformanceStarCount = (performanceRating) =>{
    setPerformanceRating(performanceRating);
    if(performanceRating == 1){
      setPerformanceStarColorChange("#f9151a")
    }else if(performanceRating == 2){
      setPerformanceStarColorChange("#ff585d")
    }else if(performanceRating == 3){
      setPerformanceStarColorChange("#fe7b02")
    }else if(performanceRating == 4){
      setPerformanceStarColorChange("#83d564")
    }else if(performanceRating == 5){
      setPerformanceStarColorChange("#2ea000")
    }else{
      setPerformanceStarColorChange("#ddd")
    }
    
  }

  const clearCount = () =>{
    setRating(0)
  }
  const clearServicesReviewCount = () =>{
    setServicesReviewRating(0)
  }
  const clearDeliveryReviewCount = () =>{
    setDeliveryReviewRating(0)
  }      
const handleRatingClick= (e, name)=> {

    console.log('You left a ' + name.rating + ' star rating for ' + name.caption);
    

}

const [divShow, setDivShow] = React.useState(false);
const[add,minus]=useState(true);

const selectChange = (element) =>{
  if(element == add){
    setDivShow(false);
  }else if(element==minus){
    setDivShow(true);
  }
}

    return (
        <div className="mt-6">
                <Grid> 
                <div className="md:w-full reviews-wrp border border-gray-300">
                    <h4 className="text-center font-bold bg-gradient-to-r from-gray-400 via-gray-600 to-gray-500 h-12 pt-3 text-white">Ratings &amp; Reviews</h4>
                    <div className="grid grid-cols-2 md:grid md:grid-cols-3">
                    <div className="mx-0 border-r border-gray-300 md:pb-7">
                        <h2 className="md:flex md:p-10 text-5xl font-bold ml-3 mr-1">
                        <div className="p-8 md:border-r border-gray-500 text-yellow-600 px-sm-20"><span className="flex"><GradeIcon />  4.6 </span>
                        <span className="font-normal text-base text-black px-2 md:pl-3">279 Reviews</span></div>
                        <div className="review-divider p-3 md:p-8 md:pl-4 text-green-600 px-sm-20"><span className="flex">78 % </span>
                        <span className="font-normal text-base text-black px-2 md:float-right pt-6">Recommends</span>
                        </div></h2>
                        <div>
              <Button className="finder-button topbar" variant="contained" href="/WriteReview">
                <span className="font-bold">Write Review</span>
              </Button>
            </div>
                    </div>
                    <div className="p-7">
                        <div className="mb-5 relative">
                        <p className="flex md:w-4/5 text-xs"><span className="float-left w-5/12 md:w-9/12">5 Star</span><span className="float-right text-gray-500 right-0 absolute">124 Reviews</span></p>
                        <div className="p-0 w-full h-2 overflow-hidden bg-gray-200 rounded right-0 absolute">
                        <div className="relative float-left h-full bg-green-400 w-5/6 rounded"></div>
                        </div>
                        </div>

                        <div className="pt-4 mb-4 md:pt-5 md:mb-5 relative">
                        <p className="flex w-4/5 text-xs"><span className="float-left w-7/12 md:w-9/12">4 Star</span><span className="float-right text-gray-500 right-0 absolute">34 Reviews</span></p>
                        <div className="p-0 w-full h-2 overflow-hidden bg-gray-200 rounded right-0 absolute">
                        <div className="relative float-left h-full bg-blue-300 w-4/6 rounded"></div>
                        </div>
                        </div>

                        <div className="pt-4 mb-4 md:pt-5 md:mb-5 relative">
                        <p className="flex w-4/5 text-xs"><span className="float-left w-7/12 md:w-9/12">3 Star</span><span className="float-right text-gray-500 right-0 absolute">18 Reviews</span></p>
                        <div className="p-0 w-full h-2 overflow-hidden bg-gray-200 rounded right-0 absolute">
                        <div className="relative float-left h-full bg-yellow-400 w-3/6 rounded"></div>
                        </div>
                        </div>

                        <div className="pt-4 mb-4 md:pt-5 md:mb-5 relative">
                        <p className="flex w-4/5 text-xs"><span className="float-left w-7/12 md:w-9/12">2 Star</span><span className="float-right text-gray-500 right-0 absolute">8 Reviews</span></p>
                        <div className="p-0 w-full h-2 overflow-hidden bg-gray-200 rounded right-0 absolute">
                        <div className="relative float-left h-full bg-red-300 w-2/6 rounded"></div>
                        </div>
                        </div>

                        <div className="pt-4 mb-4 md:pt-5 md:mb-5 relative">
                        <p className="flex w-4/5 text-xs"><span className="float-left w-7/12 md:w-9/12">1 Star</span><span className="float-right text-gray-500 right-0 absolute">3 Reviews</span></p>
                        <div className="p-0 w-full h-2 overflow-hidden bg-gray-200 rounded right-0 absolute">
                        <div className="relative float-left h-full bg-red-500 w-1/6 rounded"></div>
                        </div>
                        </div>
                    </div>

                    <div className="frequent md:border-l border-gray-300 p-10 w-full"><h4 className="text-sm font-bold">Frequently Mentioned</h4>
                    <ul className="review-list float-left break-normal text-sm pt-6 text-gray-500">
                        <li className="text-green-500">Camera (120)</li>
                        <li className="text-green-500">Value for Money (5)</li>
                        <li>Photography (12)</li>
                        <li>Battery Life (22)</li>
                        <li>Look (10)</li>
                        <li>Performance (34)</li>
                        <li className="text-red-500">Heating Issue (4)</li>
                    </ul>
                    <h3 className="text-center text-base font-bold">Read all Reviews</h3>
                    </div>
                    </div>
                </div> 

                    </Grid>  

                    <Grid>
                    <div className="review-search md:grid md:grid-cols-3">
                        <div className="p-2">
                            <div className="frequent rating-icon border border-gray-200 mt-7 pl-2">
                            <h4 className="text-lg pt-8 font-bold ">Most Recommended Positive Reviews</h4>
                            <span className="text-xs">21 people rated on this review</span>
                            <h4 className="pt-2 text-sm font-bold text-gray-600"><StarRatings starDimension={20} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(performanceRating)=> selectedPerformanceStarCount(performanceRating)} /><span className="pl-5">Fantastic Product !</span></h4>
                            <p className="pt-2 text-sm pb-10">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.</p>
                            <div className="upper-section w-full h-0">
</div>
<div className="w-full h-0 relative border border-gray-300 lower-section section-divider">
</div>
                            <div className="mt-7">
                            <h4 className="text-lg pt-8 font-bold ">Most Recommended Positive Reviews</h4>
                            <span className="text-xs">21 people rated on this review</span>
                            <h4 className="pt-2 text-sm font-bold text-gray-600">                              <StarRatings starDimension={20} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(performanceRating)=> selectedPerformanceStarCount(performanceRating)} />
 <span className="pl-5">Fantastic Product !</span></h4>
                            <p className="pt-2 text-sm pb-10">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.</p>
                        </div>
                        </div>
                       <div className="frequent shadow-2xl p-2"> <h4 className="font-bold bg-gradient-to-r from-gray-400 via-gray-600 to-gray-500 h-12 mt-10 pt-3 pl-5 text-white text-xl">Have a Question?</h4>
                        <div className="review-search pt-2 pb-2 text-xs m-2 border border-gray-300">
                  <SearchIcon />
                <span className="w-full pl-3"><InputBase
                  placeholder="Search or ask your question here"
                  inputProps={{ "aria-label": "Search for Products and More.." }}
                />
                </span>
              </div>
              <div className="pl-1 text-sm text-gray-500 border-b border-gray-200">
                  <h5 className="font-bold pt-2">Q: Which  phone is best iphone XS max or Samsung Galaxy note 9?</h5>
                  <p className="pt-5 pb-5"> A: I'm Currently using both cell phones note 9 and XS max, if you want to go for features then go for note 9, if you just want to click some good pics and show your good status symbol then go fir XS max</p>
                  <span className="mt-5 text-xs">By Tech feedz - Verified tech reviewer</span>
                  <span className="pt-2 pb-3 inline flex space-x-16 text-xs"><span className="fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 <TextsmsIcon /> 20 <SubdirectoryArrowRightIcon /></span>
              </div>
              <div className="pt-6 pl-1 text-sm text-gray-500 border-b border-gray-200">
                  <h5 className="font-bold pt-2">Q: Which  phone is best iphone XS max or Samsung Galaxy note 9?</h5>
                  <p className="pt-5 pb-5"> A: I'm Currently using both cell phones note 9 and XS max, if you want to go for features then go for note 9, if you just want to click some good pics and show your good status symbol then go fir XS max</p>
                  <span className="mt-5 text-xs">By Tech feedz - Verified tech reviewer</span>
                  <span className="pt-2 pb-3 inline flex space-x-16 text-xs"><span className="fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 <TextsmsIcon /> 20 <SubdirectoryArrowRightIcon /></span>
              </div>
              <span className="readmore text-xs capitalize"><Button
        variant="contained"
        // color="disabled"
        endIcon={<Icon>{<ArrowForwardIosIcon />}</Icon>}
      >
        Read all more questions
      </Button></span>
              </div>
                        </div>
                        
                        <div className=" w-full col-span-2 ">
                        <div className="review-icon md:mt-10 pl-5 md:pl-10 pr-6">
                          <div className="frequent">
<h3 className="text-2xl font-bold">Customer Reviews <span className="float-right text-xs font-normal text-gray-500 pr-2"><button className="filter-button"  onClick={(e)=>{setDivShow(!divShow);minus(!add)}}>Filter</button></span></h3></div>

{(divShow)?(<>
      <div className="filter border border-gray-300 p-5 mt-3">
      <h4 className="text-sm font-bold">Sort By</h4>
                    <ul className="review-list break-normal text-sm pb-2 text-gray-500">
                        <li className="text-green-500">Latest</li>
                        <li className="text-green-500">Oldest</li>
                        <li>By Tech Experts</li>
                        <li>from Purchased Customer</li>
                        <li>By Verified Reviewer</li>
                    </ul>

                    <h4 className="text-sm font-bold">Filter by Rating</h4>
                    <ul className="review-list break-normal text-sm pb-2 text-gray-500">
                        <li className="text-green-500">All</li>
                        <li className="text-green-500">Positive</li>
                        <li>Negative</li>
                        <li>5</li>
                        <li>4</li>
                        <li>3</li>
                        <li>2</li>
                        <li>1</li>
                    </ul>   

                    <h4 className="text-sm font-bold">Review On</h4>
                    <ul className="review-list break-normal text-sm pb-2 text-gray-500">
                        <li className="text-green-500">Product</li>
                        <li className="text-green-500">Service</li>
                        <li>Delivery</li>
                    </ul>          

                    
      </div>
      </>):null}

<div className="review pb-3">
<h4 className=" flex pt-8 text-l font-bold text-green-600 sm:flex">
<StarRatings starDimension={20} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(performanceRating)=> selectedPerformanceStarCount(performanceRating)} />
   <span className="float-left w-1/2 text-left pl-10">Loving It !</span></h4>
<div className="w-full md:w-2/5 review-status inline-block pt-2">
<div className="inline-block"><Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" /> </div>
<div className="inline-block align-middle pl-5">
    <h4 className="text-lg font-bold ">Emma Watson</h4>
    <span className="text-xs text-gray-300">Purchased Customer</span> <span className="w-1/2 text-xs text-gray-500 font-bold pl-6">7 days ago</span>
    </div>
</div>
<div className="w-full md:w-3/5 inline-flex">
<div className='camera text-xs flex-1'> <span className="leading-normal">Camera</span><br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li> <li></li></ul>
    </div>
    <div className='appearance text-xs flex-1'> Appearance/Design<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    <div className='battery text-xs flex-1'> Battery<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    </div>
    
    <p className="mobile-p pt-8 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>
    <div className="w-full md:flex pb-8 border-b border-gray-200">
                            <span className="overflow-hidden space-x-4 w-full flex md:w-4/5 flex md:space-x-2 pr-52"><img src="asset/review2.png" />
                            <img src="asset/review2.png" />
                            <img src="asset/review3.png" />
                            <img src="asset/review4.png" /></span>
                            <span className="w-full md:w-1/5 pt-9 pb-0 inline flex md:space-x-16 text-xs"><span className="fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 </span>
                            </div>                        
                            </div>

                            <div className="review pb-3">
<h4 className=" flex pt-8 text-l font-bold text-green-600 sm:flex">
<StarRatings starDimension={20} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(performanceRating)=> selectedPerformanceStarCount(performanceRating)} />
   <span className="float-left w-1/2 text-left pl-10">Loving It !</span></h4>
<div className="w-full md:w-2/5 review-status inline-block pt-2">
<div className="inline-block"><Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" /> </div>
<div className="inline-block align-middle pl-5">
    <h4 className="text-lg font-bold ">Emma Watson</h4>
    <span className="text-xs text-gray-300">Purchased Customer</span> <span className="w-1/2 text-xs text-gray-500 font-bold pl-6">7 days ago</span>
    </div>
</div>
<div className="w-full md:w-3/5 inline-flex">
<div className='camera text-xs flex-1'> <span className="leading-normal">Camera</span><br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li> <li></li></ul>
    </div>
    <div className='appearance text-xs flex-1'> Appearance/Design<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    <div className='battery text-xs flex-1'> Battery<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    </div>
    
    <p className="mobile-p pt-8 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>
    <div className="w-full md:flex pb-8 border-b border-gray-200">
                            <span className="overflow-hidden space-x-4 w-full flex md:w-4/5 flex md:space-x-2 pr-52"><img src="asset/review2.png" />
                            <img src="asset/review2.png" />
                            <img src="asset/review3.png" />
                            <img src="asset/review4.png" /></span>
                            <span className="w-full md:w-1/5 pt-9 pb-0 inline flex md:space-x-16 text-xs"><span className="fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 </span>
                            </div>                        
                            </div>

                        </div>

                        

                        {/* <div className="rounded-full py-3 px-6 pt-3 border border-gray-500 w-2/5">
              <Button className="finder-button">
                247 More Reviews
              </Button>
            </div> */}
                        
                    </div>
                    
</div>

                    </Grid>
        </div>
    )
}

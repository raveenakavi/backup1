import React, { useState, useRef, useCallback } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { render } from 'react-dom';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';

const img1 = '/asset/Frequent-1.png'
const img2 = '/asset/Frequent-2.png'
const img3 = '/asset/Frequent-3.png'
const img4 = '/asset/Frequent-4.png'

const ImageToggleOnMouseOver = ({primaryImg, secondaryImg}) => {
  const imageRef = useRef(null);
  const [divShow, setDivShow] = useState(false);
  const [divHide, setDivHide] = useState(false);
  return (
    <img 
      onMouseOver={() => {
        imageRef.current.src = secondaryImg;
        setDivShow(false);
    setDivHide(true);
      }}
      onMouseOut={() => {
        imageRef.current.src= primaryImg;
        setDivShow(true);
    setDivHide(false);
      }}
      src={primaryImg} 
      alt=""
      ref={imageRef}
    />
  )
}

const TOTAL_SLIDES = 3;

/**
 * It will return the JSX and register the callbacks for next and previous slide.
 * @param prevCallback {function} - Go back to the previous slide
 * @param nextCallback {function} - Go to the next slide
 * @param state {object} - Holds the state of your slider indexes
 * @param totalSlides {number} - Holds the total number of slides
 * @return {*} - Returns the JSX
 */
const renderArrows = (
  prevCallback,
  nextCallback,
  { currentIndex, slidesToScroll },
  totalSlides
) => {
  const cycleDots =
    currentIndex === 0 ? 1 : Math.ceil(totalSlides / slidesToScroll);
  return (
    <div className="mx-2 ...">
      <button disabled={currentIndex === 0} onClick={prevCallback} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button disabled={currentIndex > cycleDots} onClick={nextCallback} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
    </div>
  );
};

const FrequentlyCarousel1 = () => {
  const [state, setState] = useState({ currentIndex: 0, slidesToScroll: 0 });
  const sliderRef = useRef();
  const next = () => {
    sliderRef.current.slickNext();
  };

  const previous = () => {
    sliderRef.current.slickPrev();
  };

  const settings = {
    slidesToShow: 3,
    dots: false,
    draggable: false,
    slidesToScroll: 3,
    arrows: false,
    speed: 1300,
    autoplay: false,
    centerMode: false,
    infinite: false,
    afterChange: indexOfCurrentSlide => {
      setState({
        currentIndex: indexOfCurrentSlide,
        slidesToScroll: 3
      });
    },
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 2
            });
          }
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 1
            });
          }
        }
      }
    ]
  };

  const StyledRating = withStyles({
    iconFilled: {
      color: '#f11a08',
    },
    iconHover: {
      color: '#ff3d47',
    },
  })(Rating);
//   const  [toggleHeart, setToggleHeart] = useState(false)
    
//   changeColor = useCallback(() =>{
//    setToggleHeart(!toggleHeart)
//   },[])
//   <FavoriteIcon className={
//           toggleHeart ? 'heart active' : 'heart'
//         } onClick={changeColor}/>

const [divShow, setDivShow] = useState(false);
const [divHide, setDivHide] = useState(false);

const[add,minus]=useState(true);

const selectChange = (element) =>{
  if(element == add){
    setDivShow(false);
    setDivHide(true);
  }else if(element==minus){
    setDivShow(true);
    setDivHide(false);
  }
}
    
  return (
    <div className="app similar-products">
      {/*  Slider */}

      <h2 className="flex py-4 px-3 border-b border-gray-300 text-black text-md md:text-2xl font-bold">Frequently Bought Together
            <ul className="md:flex similar-list break-normal text-sm pb-2 pt-2 text-gray-500 ml-12">
                        <li className="text-green-500 mx-5 px-2 border border-gray-300 rounded-lg">ALL</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Cases &nbsp; Covers</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">AirPods</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">iWatch</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Pouches</li>
                    </ul> 
                    <span className="text-xs pt-3 text-blue-600"> See all</span>
                    <span className="right-2 absolute">{renderArrows(previous, next, state, TOTAL_SLIDES - 1)}</span>
                    
                    </h2>
      <Slider {...settings} ref={slider => (sliderRef.current = slider)}>
        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img1} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img2} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img3} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img4} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img1} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img2} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img3} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img4} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>

  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img1} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img2} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img3} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>
  <div className="px-2 element" id="surround">
        <div className="flex">
            <ImageToggleOnMouseOver primaryImg={img4} secondaryImg={img1} /> 
            <StyledRating name="customized-color" defaultValue={1} precision={1} max={1} size="large" icon={<FavoriteIcon fontSize="inherit" />} />
        </div>
        
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <span className="font-normal text-gray-400" id="initial">(Green, 128 GB)</span>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <span className="text-gray-600 text-sm py-2" id="initial">Save Rs.1,249 <span className="text-black">(10% OFF)</span></span>
      <span className="tooltip pt-3 ml-auto mx-8" id="onhover"><button className="border border-gray-300 rounded-full px-8 py-1 mx-2 text-yellow-600"><ShoppingCartOutlinedIcon /> <span className="pl-2">Add to cart</span></button></span>
     

  </div>

        {/* })} */}
      </Slider>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
    </div>
  );
};

export default FrequentlyCarousel1;

import React, { useState, useRef } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

const TOTAL_SLIDES = 6;

/**
 * It will return the JSX and register the callbacks for next and previous slide.
 * @param prevCallback {function} - Go back to the previous slide
 * @param nextCallback {function} - Go to the next slide
 * @param state {object} - Holds the state of your slider indexes
 * @param totalSlides {number} - Holds the total number of slides
 * @return {*} - Returns the JSX
 */
const renderArrows = (
  prevCallback,
  nextCallback,
  { currentIndex, slidesToScroll },
  totalSlides
) => {
  const cycleDots =
    currentIndex === 0 ? 1 : Math.ceil(totalSlides / slidesToScroll);
  return (
    <div className="mx-2 ...">
      <button disabled={currentIndex === 0} onClick={prevCallback} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button disabled={currentIndex > cycleDots} onClick={nextCallback} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
    </div>
  );
};

const Carousel = () => {
  const [state, setState] = useState({ currentIndex: 0, slidesToScroll: 0 });
  const sliderRef = useRef();
  const next = () => {
    sliderRef.current.slickNext();
  };

  const previous = () => {
    sliderRef.current.slickPrev();
  };

  const settings = {
    slidesToShow: 3,
    dots: false,
    draggable: false,
    slidesToScroll: 3,
    arrows: false,
    speed: 1300,
    autoplay: false,
    centerMode: false,
    infinite: false,
    afterChange: indexOfCurrentSlide => {
      setState({
        currentIndex: indexOfCurrentSlide,
        slidesToScroll: 3
      });
    },
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 2
            });
          }
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 1
            });
          }
        }
      }
    ]
  };

  return (
    <div className="app similar-products">
      {/*  Slider */}

      <h2 className="flex py-4 px-3 border-b border-gray-300 text-black text-md md:text-2xl font-bold">Compare similar Products 
            <ul className="md:flex similar-list break-normal text-sm pb-2 pt-2 text-gray-500">
                        <li className="text-green-500 mx-5 px-2">Comparing:</li>
                        <li className="text-green-500 mx-5 px-2 border border-gray-300 rounded-lg">Front Camera</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Main Camera</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">RAM</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Battery</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Performance</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">price</li>

                    </ul> 
                    <span className="text-xs pt-3 text-blue-600"> See all</span>
                    <span className="right-2 absolute">{renderArrows(previous, next, state, TOTAL_SLIDES - 1)}</span>
                    
                    </h2>
      <Slider {...settings} ref={slider => (sliderRef.current = slider)}>
        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="px-2">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="px-2"><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
             
             
        {/* })} */}
      </Slider>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
    </div>
  );
};

export default Carousel;

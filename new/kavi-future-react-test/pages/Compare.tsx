import Head from "next/head";
import styles from "../styles/Home.module.css";
import Grid from "@material-ui/core/Grid";
import { useState, useEffect } from "react";
import Rating from "@material-ui/lab/Rating";
import SearchIcon from "@material-ui/icons/Search";
// import "primereact/resources/themes/saga-blue/theme.css";
// import "primereact/resources/primereact.min.css";
// import "primeicons/primeicons.css";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";

const options = [
    { value: "Nokia", label: "Nokia" },
    { value: "Realme", label: "Realme" },
    { value: "Vivo", label: "Vivo" },
];
export default function Compare() {
    const [clientreview, setClientReview] = useState(2);
    const [clientreview1, setClientReview1] = useState(4);
    const [comparereview, setCompareReview] = useState(true);
    const [showModal, setShowModal] = useState(false);
    const [compareMobile, setCompareMobile] = useState(false);
    const [compareMobile1, setCompareMobile1] = useState(false);
    const [compareMobile2, setCompareMobile2] = useState(false);
    const [checked, setChecked] = useState(false);
    const [checked1, setChecked1] = useState(false);
    const [checked2, setChecked2] = useState(true);
    const [checked3, setChecked3] = useState(false);
    const [checked4, setChecked4] = useState(false);
    const [checked5, setChecked5] = useState(false);
    const [checked6, setChecked6] = useState(true);
    const [checked7, setChecked7] = useState(true);
    const handleChange = (nextChecked) => {
        setChecked(nextChecked);
    };
    const handleChange1 = (nextChecked) => {
        setChecked1(nextChecked);
    };
    const handleChange2 = (nextChecked) => {
        setChecked2(nextChecked);
    };
    const handleChange3 = (nextChecked) => {
        setChecked3(nextChecked);
    };
    const handleChange4 = (nextChecked) => {
        setChecked4(nextChecked);
    };
    const handleChange5 = (nextChecked) => {
        setChecked5(nextChecked);
    };
    const handleChange6 = (nextChecked) => {
        setChecked6(nextChecked);
    };
    const handleChange7 = (nextChecked) => {
        setChecked7(nextChecked);
    };
    const [scroll, setScroll] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            setScroll(window.scrollY > 100);
        });
    }, []);

    return (
        <div>
           
            <div className="flex flex-col h-screen">
            <div className="overflow-x-auto sm:overflow-visible md:overflow-visible lg:overflow-visible">
                <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-4">
                    <div className="shadow border-b border-gray-200 sm:rounded-lg">
                        <table className="min-w-full divide-y divide-gray-200 table-auto relative w-full border border-emerald-500">
                            <thead>
                            <tr className={scroll ? "navigation_menuback" : "main-headers"}>
                                    <th className="top-0 px-2 py-2 border border-emerald-500 p-1 bg-gray-100 border-solid border-4 border-light-blue-500">
                                        <div className="flex capitalize text-sm font-normal text-black p-1">
                                            <p>filter by category</p>
                                            <p className="compare_rating">
                                                <ArrowRightIcon />
                                            </p>
                                        </div>
                                    </th>
                                    <th className="top-0 px-2 py-2  border border-emerald-500 p-1 ">
                                        <div className="col">
                                            <div className="relative">
                                                <div>
                                                    <h3 className="flex justify-between capitalize text-sm font-bold text-black p-1">apple iphone 11 pro max</h3>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                    <p>Rs 1,19,249</p>
                                                    <p className="compare_rating">
                                                        <Rating name="read-only" value={clientreview} readOnly />
                                                    </p>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                    <img className="block compare_headimage" src="compare11.png" />
                                                    <div className=" absolute float-right compare_buynowoption">
                                                        <a href="#" className="rounded-full w-full flex p-3 pr-8 pl-8 border border-transparent text-base font-bold rounded-md text-white bg-yellow-500">
                                                            buy now
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th className="top-0 px-2 py-2 border border-emerald-500 p-1 ">
                                        <div className="col">
                                            <div className="relative">
                                                <div>
                                                    <h3 className="flex justify-between capitalize text-sm font-bold text-black p-1">apple iphone 11 pro max</h3>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                    <p>Rs 1,19,249</p>
                                                    <p className="compare_rating">
                                                        <Rating name="read-only" value={clientreview} readOnly />
                                                    </p>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                    <img className="block compare_headimage" src="compare2.png" />
                                                    <div className=" absolute float-right compare_buynowoption">
                                                        <a href="#" className="rounded-full w-full flex p-3 pr-8 pl-8 border border-transparent text-base font-bold rounded-md text-white bg-yellow-500">
                                                            buy now
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th className="top-0 px-2 py-2 border border-emerald-500 p-1 ">
                                        <div className="col ">
                                            <div className="relative">
                                                <div>
                                                    <h3 className="flex justify-between capitalize text-sm font-bold text-black p-1">apple iphone 11 pro max</h3>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                    <p>Rs 1,19,249</p>
                                                    <p className="compare_rating">
                                                        <Rating name="read-only" value={clientreview} readOnly />
                                                    </p>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                    <img className="block compare_headimage" src="compare3.png" />
                                                    <div className=" absolute float-right compare_buynowoption">
                                                        <a href="#" className="rounded-full w-full flex p-3 pr-8 pl-8 border border-transparent text-base font-bold rounded-md text-white bg-yellow-500">
                                                            buy now
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th className=" top-0 px-2 py-2 border border-emerald-500 p-1">
                                        <div className="col">
                                            <div className="relative">
                                                <div>
                                                    <h3 className="flex justify-between capitalize text-sm font-bold text-black p-1">apple iphone 11 pro max</h3>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                    <p>Rs 1,19,249</p>
                                                    <p className="compare_rating">
                                                        <Rating name="read-only" value={clientreview} readOnly />
                                                    </p>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                    <img className="block compare_headimage" src="compare4.png" />
                                                    <div className=" absolute float-right compare_buynowoption">
                                                        <a href="#" className="rounded-full w-full flex p-3 pr-8 pl-8 border border-transparent text-base font-bold rounded-md text-white bg-yellow-500">
                                                            buy now
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="divide-y ">
                                <tr>
                                    <td className="whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="">
                                            <div className="">
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-2 pl-1 pr-1">
                                                    <p>general features</p>

                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-2 pl-1 pr-1">
                                                    <p>design  display</p>
                                                    <div className="card">
                                                        
                                                    </div>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-2 pl-1 pr-1">
                                                    <p>performance storage</p>
                                                    <div className="card">
                                                        
                                                    </div>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-2 pl-1 pr-1">
                                                    <p>camera</p>
                                                    <div className="card">
                                                        
                                                    </div>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-2 pl-1 pr-1">
                                                    <p>battery</p>
                                                    <div className="card">
                                                        
                                                    </div>
                                                </div>
                                                <div className="flex justify-between capitalize text-sm font-normal text-black p-2 pl-1 pr-1">
                                                    <p>network & connectivity</p>
                                                    <div className="card">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                        <div className="flex">
                                            <div className="relative ml-1 p-1 compare_rating" onClick={() => setCompareMobile(true)}>
                                                <div className="compare_addproduct bg-gray-200 rounded-lg mb-2"></div>
                                                <div className="relative  flex w-full flex-wrap items-stretch mb-0">
                                                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-400 absolute bg-transparent text-base items-center justify-center pl-3 py-3 compare_formdata">
                                                        <SearchIcon />
                                                    </span>
                                                    <div className="relative flex w-full flex-wrap items-stretch  rounded-md">
                                                        <input
                                                            type="text"
                                                            placeholder="Search"
                                                            className="px-3 py-3 placeholder-gray-400 text-gray-700 relative  rounded-md text-sm shadow outline-none  w-full pr-10 pl-10 border-solid border-2 border-light-blue-500"
                                                        />
                                                    </div>
                                                </div>

                                                <div>
                                                    <p className="capitalize text-center text-sm text-gray-500 font-semibold">or</p>
                                                </div>

                                                <div>
                                                </div>
                                            </div>
                                            {compareMobile ? (
                                                <div className="justify-center absolute z-50 bg-white compare_productlist">
                                                    <div className="float-right ">
                                                        <button className="compare_closebut bg-transparent text-2xl font-semibo0ld leading-none" onClick={() => setCompareMobile(false)}>
                                                            <span>×</span>
                                                        </button>
                                                    </div>
                                                    <div className="p-0">
                                                        <img className="block p-1 m-auto compare_displayimage" src="compare1.png" />
                                                        <h3 className="flex justify-between capitalize text-sm font-bold text-black p-1">apple iphone 11 pro max</h3>
                                                        <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                            <p>Rs 1,19,249</p>
                                                            <p className="compare_rating">
                                                                <Rating name="read-only" value={clientreview} readOnly />
                                                            </p>
                                                        </div>
                                                        <div className="compare_buynow">
                                                            <button className="font-bold capitalize">buy now</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            ) : null}
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                        <div className="flex">
                                            <div className="relative ml-1 p-1 compare_rating" onClick={() => setCompareMobile1(true)}>
                                                <div className="compare_addproduct bg-gray-200 rounded-lg mb-2"></div>
                                                <div className="relative  flex w-full flex-wrap items-stretch mb-0">
                                                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-400 absolute bg-transparent text-base items-center justify-center pl-3 py-3 compare_formdata">
                                                        <SearchIcon />
                                                    </span>
                                                    <div className="relative flex w-full flex-wrap items-stretch  rounded-md">
                                                        <input
                                                            type="text"
                                                            placeholder="Search"
                                                            className="px-3 py-3 placeholder-gray-400 text-gray-700 relative  rounded-md text-sm shadow outline-none  w-full pr-10 pl-10 border-solid border-2 border-light-blue-500"
                                                        />
                                                    </div>
                                                </div>

                                                <div>
                                                    <p className="capitalize text-center text-sm text-gray-500 font-semibold">or</p>
                                                </div>

                                                <div>
                                                </div>
                                            </div>
                                            {compareMobile1 ? (
                                                <div className="justify-center absolute z-50 bg-white compare_productlist">
                                                    <div className="float-right ">
                                                        <button className="compare_closebut bg-transparent text-2xl font-semibo0ld leading-none" onClick={() => setCompareMobile1(false)}>
                                                            <span>×</span>
                                                        </button>
                                                    </div>
                                                    <div className="p-0">
                                                        <img className="block p-1 m-auto compare_displayimage" src="compare2.png" />
                                                        <h3 className="flex justify-between capitalize text-sm font-bold text-black p-1">apple iphone 11 pro max</h3>
                                                        <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                            <p>Rs 1,19,249</p>
                                                            <p className="compare_rating">
                                                                <Rating name="read-only" value={clientreview} readOnly />
                                                            </p>
                                                        </div>
                                                        <div className="compare_buynow">
                                                            <button className="font-bold capitalize">buy now</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            ) : null}
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                        <div className="flex">
                                            <div className="relative ml-1 p-1 compare_rating" onClick={() => setCompareMobile2(true)}>
                                                <div className="compare_addproduct bg-gray-200 rounded-lg mb-2"></div>
                                                <div className="relative  flex w-full flex-wrap items-stretch mb-0">
                                                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-400 absolute bg-transparent text-base items-center justify-center pl-3 py-3 compare_formdata">
                                                        <SearchIcon />
                                                    </span>
                                                    <div className="relative flex w-full flex-wrap items-stretch  rounded-md">
                                                        <input
                                                            type="text"
                                                            placeholder="Search"
                                                            className="px-3 py-3 placeholder-gray-400 text-gray-700 relative  rounded-md text-sm shadow outline-none  w-full pr-10 pl-10 border-solid border-2 border-light-blue-500"
                                                        />
                                                    </div>
                                                </div>

                                                <div>
                                                    <p className="capitalize text-center text-sm text-gray-500 font-semibold">or</p>
                                                </div>

                                                <div>
                                                </div>
                                            </div>
                                            {compareMobile2 ? (
                                                <div className="justify-center absolute z-50 bg-white compare_productlist">
                                                    <div className="float-right ">
                                                        <button className="compare_closebut bg-transparent text-2xl font-semibo0ld leading-none" onClick={() => setCompareMobile2(false)}>
                                                            <span>×</span>
                                                        </button>
                                                    </div>
                                                    <div className="p-0">
                                                        <img className="block p-1 m-auto compare_displayimage" src="compare3.png" />
                                                        <h3 className="flex justify-between capitalize text-sm font-bold text-black p-1">apple iphone 11 pro max</h3>
                                                        <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                            <p>Rs 1,19,249</p>
                                                            <p className="compare_rating">
                                                                <Rating name="read-only" value={clientreview} readOnly />
                                                            </p>
                                                        </div>
                                                        <div className="compare_buynow">
                                                            <button className="font-bold capitalize">buy now</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            ) : null}
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                        <div className="flex">
                                            <div className="relative ml-1 p-1 compare_rating" onClick={() => setShowModal(true)}>
                                                <div className="compare_addproduct bg-gray-200 rounded-lg mb-2"></div>
                                                <div className="relative  flex w-full flex-wrap items-stretch mb-0">
                                                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-400 absolute bg-transparent text-base items-center justify-center pl-3 py-3 compare_formdata">
                                                        <SearchIcon />
                                                    </span>
                                                    <div className="relative flex w-full flex-wrap items-stretch  rounded-md">
                                                        <input
                                                            type="text"
                                                            placeholder="Search"
                                                            className="px-3 py-3 placeholder-gray-400 text-gray-700 relative  rounded-md text-sm shadow outline-none  w-full pr-10 pl-10 border-solid border-2 border-light-blue-500"
                                                        />
                                                    </div>
                                                </div>

                                                <div>
                                                    <p className="capitalize text-center text-sm text-gray-500 font-semibold">or</p>
                                                </div>

                                                <div>
                                                </div>
                                            </div>
                                            {showModal ? (
                                                <div className="justify-center absolute z-50 bg-white compare_productlist">
                                                    <div className="float-right ">
                                                        <button className="compare_closebut bg-transparent text-2xl font-semibo0ld leading-none" onClick={() => setShowModal(false)}>
                                                            <span>×</span>
                                                        </button>
                                                    </div>
                                                    <div className="p-0">
                                                        <img className="block p-1 m-auto compare_displayimage" src="compare4.png" />
                                                        <h3 className="flex justify-between capitalize text-sm font-bold text-black p-1">apple iphone 11 pro max</h3>
                                                        <div className="flex justify-between capitalize text-sm font-normal text-black p-1">
                                                            <p>Rs 1,19,249</p>
                                                            <p className="compare_rating">
                                                                <Rating name="read-only" value={clientreview} readOnly />
                                                            </p>
                                                        </div>
                                                        <div className="compare_buynow">
                                                            <button className="font-bold capitalize">buy now</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            ) : null}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                            general features
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border-none border border-emerald-500 sm:border sm:border-emerald-500 md:border md:border-emerald-500 lg:border lg:border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic hidden sm:flex md:flex lg:flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic  flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="pl-3 text-sm font-bold text-black p-1  capitalize italic flex">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                {checked6 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line ">launch date</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  whitespace-pre-line capitalize">October 15,2020 (offical)</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">November 30 2019 (offical)</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">May 01,2021 (offical)</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">March 31, 2021 (offical)</div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked6 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize whitespace-pre-line flex">operating system</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">Android 10.0</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">IOS 13.4</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">Android 12.0</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">Android 12.0</div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked6 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim slot</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line ">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked6 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked6 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">network</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked6 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fingerprint sensor</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CancelIcon fontSize="small" color="secondary" className="font-bold mr-2" />
                                                not available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CancelIcon fontSize="small" color="secondary" className="font-bold mr-2" />
                                                not available
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked6 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fast charging</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                <tr>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                            design & display
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic  flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="pl-3 text-sm font-bold text-black p-1  capitalize italic flex">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                {checked7 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">dimension</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked7 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">weight</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  whitespace-pre-line">189 g</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1   whitespace-pre-line">221 g</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1   whitespace-pre-line">178 g</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1   whitespace-pre-line">190 g</div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked7 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">ruggedness</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CancelIcon fontSize="small" color="secondary" className="font-bold mr-2" />
                                                not available
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked7 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">waterproof</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                <tr>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic  flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="pl-3 text-sm font-bold text-black p-1  capitalize italic flex">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                </tr>{checked2 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim slot</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line ">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked2 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked2 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">network</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked2 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fingerprint sensor</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CancelIcon fontSize="small" color="secondary" className="font-bold mr-2" />
                                                not available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CancelIcon fontSize="small" color="secondary" className="font-bold mr-2" />
                                                not available
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked2 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fast charging</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                <tr>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic  flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="pl-3 text-sm font-bold text-black p-1  capitalize italic flex">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                {checked3 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">dimension</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked3 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">weight</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  whitespace-pre-line">189 g</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1   whitespace-pre-line">221 g</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1   whitespace-pre-line">178 g</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1   whitespace-pre-line">190 g</div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked3 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">ruggedness</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CancelIcon fontSize="small" color="secondary" className="font-bold mr-2" />
                                                not available
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked3 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">waterproof</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                 <tr>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic  flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="pl-3 text-sm font-bold text-black p-1  capitalize italic flex">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                </tr>{checked4 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim slot</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line ">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">Dual sim, GSM+GSM</span>
                                                <span className="block">Dual VOLTE</span>
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked4 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">sim size</div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked4 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">network</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">4G available(supports indian bands)</span>
                                                <span className="block">3G available</span>
                                                <span className="block">2G available</span>
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked4 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fingerprint sensor</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CancelIcon fontSize="small" color="secondary" className="font-bold mr-2" />
                                                not available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CancelIcon fontSize="small" color="secondary" className="font-bold mr-2" />
                                                not available
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked4 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">fast charging</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                <tr>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="text-left pl-0 text-sm font-bold text-black p-1 capitalize italic flex">
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic  flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className=" pl-3 text-sm font-bold text-black p-1  capitalize italic flex ">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview1} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                    <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                        <div className="pl-3 text-sm font-bold text-black p-1  capitalize italic flex">
                                            <p className="compare_rating">
                                                <Rating name="read-only" value={clientreview} readOnly />
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                {checked5 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">dimension</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <span className="block">height:155mm</span>
                                                <span className="block">width:77.8mm</span>
                                                <span className="block">thickness:8.1mm</span>
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked5 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">weight</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  whitespace-pre-line">189 g</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1   whitespace-pre-line">221 g</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1   whitespace-pre-line">178 g</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1   whitespace-pre-line">190 g</div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked5 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">ruggedness</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                available
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CancelIcon fontSize="small" color="secondary" className="font-bold mr-2" />
                                                not available
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                                {checked5 ? (
                                    <tr>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500 bg-gray-100">
                                            <div className="text-left pl-16 text-sm font-bold text-black p-1  capitalize italic flex whitespace-pre-line">waterproof</div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                        <td className="px-2 py-2 whitespace-nowrap border border-emerald-500">
                                            <div className=" inline-block pl-3 text-sm font-light text-black p-1  capitalize whitespace-pre-line">
                                                <CheckCircleOutlineIcon fontSize="small" color="primary" className="font-bold mr-2" />
                                                water resistant ip68(upto 30 minutes in a depth of 4 meter)
                                            </div>
                                        </td>
                                    </tr>
                                ) : null}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}

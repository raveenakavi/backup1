import React from 'react'
import ViewCompactIcon from '@material-ui/icons/ViewCompact';
import AppsIcon from '@material-ui/icons/Apps';
import StorageIcon from '@material-ui/icons/Storage';

import Reviews from './Reviews';
import Category from './Category';

export default function BreadCrumb() {
    return (
        <div className="w-full grid grid-flow-col grid-col-2 pt-3 pb-3 tabs">
            <div className="absolute left-6 text-sm">Home &#62; Mobile Accessories &#62; Accessories &#62; HeadPhones</div>
            <div className="absolute right-6 breadcrumb ">
            <input id="tab1" type="radio" name="tabs" checked />
              <label htmlFor="tab1">
                <ViewCompactIcon /></label>
                <input id="tab2" type="radio" name="tabs" checked />
              <label htmlFor="tab2">
                <AppsIcon /> </label>
                <input id="tab2" type="radio" name="tabs" checked />
              <label htmlFor="tab2">
                  <a href=""><StorageIcon /></a></label>
                
                <div className="content w-full">
                <div id="content1">
                    {/* <Category /> */}erere
                    </div>
                    <div id="content2">
                        {/* <Reviews /> */}ewrwerew
                    </div>
                    </div>

                </div>
        </div>
    )
}

import React, { useState, useRef } from "react";
import Slider from "react-slick";


export default function categoryLogo() {

    const [state, setState] = useState({ currentIndex: 0, slidesToScroll: 0 });
    const sliderRef = useRef();
    const next = () => {
      sliderRef.current.slickNext();
    };
  
    const previous = () => {
      sliderRef.current.slickPrev();
    };

    
    const settings = {
        slidesToShow: 2,
        dots: false,
        draggable: false,
        slidesToScroll: 2,
        arrows: false,
        speed: 1300,
        autoplay: false,
        centerMode: false,
        infinite: false,
        afterChange: indexOfCurrentSlide => {
          setState({
            currentIndex: indexOfCurrentSlide,
            slidesToScroll: 3
          });
        },
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              afterChange: indexOfCurrentSlide => {
                setState({
                  currentIndex: indexOfCurrentSlide,
                  slidesToScroll: 2
                });
              }
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              afterChange: indexOfCurrentSlide => {
                setState({
                  currentIndex: indexOfCurrentSlide,
                  slidesToScroll: 1
                });
              }
            }
          }
        ]
      };


    return (
        <div className="bg-white category-series pt-10 pb-5 relative">

            <h3 className="uppercase text-center text-md md:text-lg font-bold">Our Series</h3>
            <h1 className="uppercase text-center text-2xl md:text-4xl font-bold relative"> choosing in one Style</h1>

            <Slider {...settings} ref={slider => (sliderRef.current = slider)}>

                <img src={'/asset/category-logo-1.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-2.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-3.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-4.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-1.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-2.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-3.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-4.png'} className="shadow-xl" />
                <img src={'/asset/category-logo-1.png'} className="shadow-xl" />
            </Slider>
        </div>
    )
}

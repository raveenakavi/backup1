import React from 'react'
import { Grid, Button, InputBase } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import TextsmsIcon from '@material-ui/icons/Textsms';
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import Icon from '@material-ui/core/Icon';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Avatar from '@material-ui/core/Avatar';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default function Reviews() {

  return (
    <div className="mt-6">
      <Grid container>

        <div className="md:w-full reviews border border-gray-300">
          <div className="flex bg-gradient-to-r from-gray-400 via-gray-600 to-gray-500">
            <div className="w-1/2">
              <h4 className="text-left font-bold h-12 pl-3 pt-3 text-white">Ratings &amp; Reviews</h4>
            </div>
            <div className="w-1/2 text-right pr-3 pt-3 text-white"> Close <span className="fill-current text-white-600"></span></div>
          </div>

          <div className="my-order md:flex panel-content w-1/2 ml-10 mt-5 border border-grey-300 rounded-lg">
            <div className="tabs">
              <input id="tab1" type="radio" name="tabs" checked />
              <label htmlFor="tab1">Product review</label>
              <input id="tab2" type="radio" name="tabs" />
              <label htmlFor="tab2">Service Review</label>
              <input id="tab3" type="radio" name="tabs" />
              <label htmlFor="tab3">Delivery Review</label>
              <div className="content w-full">
                <div id="content1">
                  <div className="my-order md:flex panel-content w-full pl-1 mt-1 border border-grey-300 rounded-lg">

                    <div className="w-9/12 mb-2">
                      <div className="flex border-b border-gray-200 mb-2 ml-5">
                        <div className="w-1/6">
                          <img src="asset/order-1.png" className="m-2 mb-3" />
                        </div>
                        <div className="m-2 text-left">
                          <p className="text-sm text-black-300 leading-loose">Apple Iphone 11 pro *</p>
                          <span className="text-sm text-gray-400 leading-loose">Dispatched from Warehouse</span>
                        </div>
                      </div>
                      <div className="flex ml-5">
                        <div className="w-1/6">
                          <img src="asset/order-2.png" className="m-2 mb-3" />
                        </div>
                        <div className="m-2 text-left">
                          <h4 className="text-base text-green-600 font-bold leading-loose">Arriving Tomorrow</h4>
                          <p className="text-sm text-black-300 leading-loose">Apple Iphone 11 pro (Green, 128 GB, 4GB RAM)</p>
                          <span className="text-sm text-gray-400 leading-loose">Dispatched from Warehouse</span>
                        </div>
                      </div>
                    </div>
                    <div className="w-1/5 mt-10 ml-8"><ul className="text-left leading-10 relative">
                      <li className=""><a>Order Details <span className="absolute right-0 pr-0"><ArrowRightIcon fontSize="small" className="absolute right-0 pr-0 pt-3" /></span></a></li>
                      <li>Download Invoice  <span className="absolute right-0 pr-0"><ArrowRightIcon fontSize="small" className="absolute right-0 pr-0 pt-3" /></span></li>
                      <li className="text-gray-400">Cancel this Order  <span className="absolute right-0 pr-0"><ArrowRightIcon fontSize="small" className="absolute right-0 pr-0 pt-3" /></span></li>
                      <li>Share order Details  <span className="absolute right-0 pr-0"><ArrowRightIcon fontSize="small" className="absolute right-0 pr-0 pt-3" /></span></li></ul></div>
                  </div>

                  <div className="my-order md:flex panel-content w-full pl-1 mt-8 border border-grey-300 rounded-lg">

                    <div className="w-full mb-2">
                      <div className="flex border-b border-gray-200 mb-2 relative ml-5">
                        <div className="w-28">
                          <img src="asset/order-3.png" className="m-2" />
                        </div>
                        <div className="m-2 text-left">
                          <h4 className="text-base text-green-600 font-bold leading-loose">Ready to Pickup @ Store</h4>
                          <p className="text-sm text-black-300 leading-loose">Apple Iphone 11 pro (Green, 128 GB, 4GB RAM)</p>
                          <span className="text-sm text-gray-400 leading-loose">Dispatched from Warehouse</span>
                        </div>
                        <div className="absolute mt-10 right-0 w-1/5 rounded-full">
                          <Button className="finder-button" variant="contained">
                            View Order
                          </Button>
                        </div>
                      </div>
                      <div className="flex relative ml-5">
                        <div className="w-28">
                          <img src="asset/order-4.png" className="m-2" />
                        </div>
                        <div className="m-2 text-left">
                          <h4 className="text-base text-yellow-600 font-bold leading-loose">Returned on 28 July, 2020. Refund Processed</h4>
                          <p className="text-sm text-black-300 leading-loose">Apple Iphone 11 pro (Green, 128 GB, 4GB RAM)</p>
                          <span className="text-sm text-gray-400 leading-loose">Dispatched from Warehouse</span>
                        </div>
                        <div className="absolute mt-10 right-0 w-1/5 rounded-full">
                          <Button className="finder-button" variant="contained">
                            View Order
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="my-order md:flex panel-content w-full pl-1 mt-5 border border-grey-300 rounded-lg text-sm bg-gray-200">

                    <div className="w-full flex mb-2 relative">
                      <div className="w-1/2 text-left pl-10 pt-2">
                        Need Help?
        </div>
                      <div className="w-1/2 absolute right-0 text-right pr-10 pt-2">
                        Contact Us <span className="absolute right-0 pr-0 pb-3"><ArrowRightIcon fontSize="small" className="absolute right-0 pr-0 pb-3" /></span>
                      </div>
                    </div>
                  </div>

                  <div className="my-order md:flex panel-content w-full pl-1 mt-5  bg-gray-200">

                    <div className="w-full flex relative text-base pt-5">
                      <div className="w-1/4 text-left pl-10 underline">
                        Completed Order
          </div>
                      <div className="my-order-icon w-1/2 text-center"><ExpandMoreIcon fontSize="large" className="pr-2" /></div>
                      <div className="w-1/4 absolute right-0 text-right pr-10 text-xs text-blue-500 pt-2">
                        See all 23 Orders
              </div>
                    </div>
                  </div>

                  <div className="my-order md:flex panel-content w-full pl-1 pt-5 border border-grey-300 bg-gray-200">

                    <div className="w-full mb-2">
                      <div className="flex mb-2 relative ml-5 mr-2 border border-gray-300 rounded-lg">
                        <div className="w-28">
                          <img src="asset/order-5.png" className="m-2" />
                        </div>
                        <div className="m-2 text-left">
                          <h4 className="text-base text-black-600 font-bold leading-loose">Delivered on 21 Jul, 2020</h4>
                          <p className="text-sm text-black-300 leading-loose">Realme Earpods 2</p>
                          <span className="text-sm text-Black-300 leading-loose uppercase">Rate this Product
                          </span>
                        </div>
                        <div className="absolute mt-10 right-0 w-1/5 rounded-full">
                          <Button className="finder-button" variant="contained">
                            View
                          </Button>
                        </div>
                      </div>

                    </div>
                  </div>

                </div>

                <div id="content2">
                  content2
                </div>

                <div id="content3">
                  content3
                </div>

              </div>

            </div>
          </div>

        </div>

      </Grid>

      <Grid>
        <div className="review-search md:grid md:grid-cols-3">
          <div>
            <div className="rating-icon border border-gray-200 mt-7 pl-2">
              <h4 className="text-lg pt-8 font-bold ">Most Recommended Positive Reviews</h4>
              <span className="text-xs">21 people rated on this review</span>
              <p className="pt-2 text-sm pb-10">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.</p>
              <div className="upper-section w-full h-0">
              </div>
              <div className="w-full h-0 relative border border-gray-300 lower-section section-divider">
              </div>
              <div className="mt-7 pl-2">
                <h4 className="text-lg pt-8 font-bold ">Most Recommended Positive Reviews</h4>
                <span className="text-xs">21 people rated on this review</span>
                <p className="pt-2 text-sm pb-10">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.</p>
              </div>
            </div>
            <div className="shadow-2xl p-2"> <h4 className="font-bold bg-gradient-to-r from-gray-400 via-gray-600 to-gray-500 h-12 mt-10 pt-3 text-white text-xl">Have a Question?</h4>
              <div className="review-search pt-2 pb-2 text-xs m-2 border border-gray-300">
                <SearchIcon />
                <span className="w-full pl-3"><InputBase
                  placeholder="Search or ask your question here"
                  inputProps={{ "aria-label": "Search for Products and More.." }}
                />
                </span>
              </div>
              <div className="pl-1 text-sm text-gray-500 border-b border-gray-200 pl-2 pr-2 ">
                <h5 className="font-bold pt-2">Q: Which  phone is best iphone XS max or Samsung Galaxy note 9?</h5>
                <p className="pt-5 pb-5 "> A: I'm Currently using both cell phones note 9 and XS max, if you want to go for features then go for note 9, if you just want to click some good pics and show your good status symbol then go fir XS max</p>
                <span className="mt-5 text-xs">By Tech feedz - Verified tech reviewer</span>
                <span className="pt-2 pb-3 inline flex space-x-16 text-xs"><span className="fill-current text-yellow-600"> 21</span><ThumbDownIcon /> 1 <TextsmsIcon /> 20 <SubdirectoryArrowRightIcon /></span>
              </div>
              <div className="pt-6 pl-1 text-sm text-gray-500 border-b border-gray-200 pl-2 pr-2 ">
                <h5 className="font-bold pt-2">Q: Which  phone is best iphone XS max or Samsung Galaxy note 9?</h5>
                <p className="pt-5 pb-5"> A: I'm Currently using both cell phones note 9 and XS max, if you want to go for features then go for note 9, if you just want to click some good pics and show your good status symbol then go fir XS max</p>
                <span className="mt-5 text-xs">By Tech feedz - Verified tech reviewer</span>
                <span className="pt-2 pb-3 inline flex space-x-16 text-xs"><span className="fill-current text-yellow-600"> 21</span><ThumbDownIcon /> 1 <TextsmsIcon /> 20 <SubdirectoryArrowRightIcon /></span>
              </div>
              <span className="readmore text-xs capitalize"><Button
                variant="contained"
                endIcon={<Icon>{<ArrowForwardIosIcon />}</Icon>}
              >
                Read all more questions
      </Button></span>
            </div>
          </div>
          <div className=" w-full col-span-2 ">
            <div className="review-icon mt-10 pl-10 pr-6">
              <h3 className="text-2xl font-bold">Customer Reviews <span className="float-right text-xs font-normal text-gray-500 pr-2">Filter</span></h3>
              <div className="review pb-3">
                <div className="w-4/5 md:w-2/5 review-status inline-block pt-2">
                  <div className="inline-block"><Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" /> </div>
                  <div className="inline-block pl-5">
                    <h4 className="text-lg font-bold ">Emma Watson</h4>
                    <span className="text-xs text-gray-300">Purchased Customer</span> <span className="text-xs text-gray-500 font-bold pl-6">7 days ago</span>
                  </div>
                </div>
                <div className="w-4/5 md:w-3/5 inline-flex">
                  <div className='camera text-xs flex-1'> <span className="leading-normal">Camera</span><br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li> <li></li></ul>
                  </div>
                  <div className='appearance text-xs flex-1'> Appearance/Design<br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
                  </div>
                  <div className='battery text-xs flex-1'> Battery<br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
                  </div>
                </div>
              </div>
              <p className="pt-2 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>

              <div className="w-full md:flex pb-8 border-b border-gray-200"><span className="w-3/5 flex md:w-4/5 flex space-x-2 float-left pr-52"><img src="asset/review2.png" />
                <img src="asset/review2.png" />
                <img src="asset/review3.png" />
                <img src="asset/review4.png" /></span>
                <span className="float-right pt-9 pb-0 inline flex md:space-x-16 text-xs"><span className="fill-current text-yellow-600"> 21</span><ThumbDownIcon /> 1 </span>
              </div>

            </div>

            <div className="review-icon w-full col-span-2 mt-2 pl-10 pr-6">
              <div className="review pb-3">
                <div className="w-4/5 md:w-2/5 review-status inline-block pt-2">
                  <div className="inline-block"><Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" /> </div>
                  <div className="inline-block pl-5">
                    <h4 className="text-lg font-bold ">Emma Watson</h4>
                    <span className="text-xs text-gray-300">Purchased Customer</span> <span className="text-xs text-gray-500 font-bold pl-6">7 days ago</span>
                  </div>
                </div>
                <div className="w-4/5 md:w-3/5 inline-flex">
                  <div className='camera text-xs flex-1'> <span className="leading-normal">Camera</span><br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li> <li></li></ul>
                  </div>
                  <div className='appearance text-xs flex-1'> Appearance/Design<br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
                  </div>
                  <div className='battery text-xs flex-1'> Battery<br />
                    <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
                  </div>
                </div>
              </div>
              <p className="pt-2 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>
              <div className="w-full flex pb-8 border-b border-gray-200"><span className="w-3/5 flex md:w-4/5 flex space-x-2 float-left pr-52"><img src="asset/review2.png" />
                <img src="asset/review2.png" />
                <img src="asset/review3.png" />
                <img src="asset/review4.png" /></span>
                <span className="pl-20 md:pr-0 pt-9 pb-0 inline flex md:space-x-12 text-xs"><span className="flex fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 </span>
              </div>

            </div>

            {/* <div className="rounded-full py-3 px-6 pt-3 border border-gray-500 w-2/5">
              <Button className="finder-button">
                247 More Reviews
              </Button>
            </div> */}

          </div>
        </div>

      </Grid>
    </div>
  )
}

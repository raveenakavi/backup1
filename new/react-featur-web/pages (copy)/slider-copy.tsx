import React, { useState, useRef } from "react";
import Slider from "react-slick";
// import "./styles.css";

// Constant Variables
// Slides scroll behavior on different sizes
const TOTAL_SLIDES = 6;
// const DESKTOP_SLIDES_SCROLL = 3;
// const TABLET_SLIDES_SCROLL = 2;
// const MOBILE_SLIDES_SCROLL = 1;

/**
 * It will return the JSX and register the callbacks for next and previous slide.
 * @param prevCallback {function} - Go back to the previous slide
 * @param nextCallback {function} - Go to the next slide
 * @param state {object} - Holds the state of your slider indexes
 * @param totalSlides {number} - Holds the total number of slides
 * @return {*} - Returns the JSX
 */
const renderArrows = (
  prevCallback,
  nextCallback,
  { currentIndex, slidesToScroll },
  totalSlides
) => {
  const cycleDots =
    currentIndex === 0 ? 1 : Math.ceil(totalSlides / slidesToScroll);
  return (
    <div>
      <button disabled={currentIndex === 0} onClick={prevCallback}>
        Previous
      </button>
      <button disabled={currentIndex > cycleDots} onClick={nextCallback}>
        Next
      </button>
    </div>
  );
};

const Carousel = () => {
  // We just have to keep track of the index by keeping it in the state
  const [state, setState] = useState({ currentIndex: 0, slidesToScroll: 0 });

  // To access underlying DOM object for the slider
  const sliderRef = useRef();

  // Trigger next method to show the next slides
  const next = () => {
    sliderRef.current.slickNext();
  };

  // Trigger previous method to show the previous slides
  const previous = () => {
    sliderRef.current.slickPrev();
  };

  // Slider Settings
  const settings = {
    slidesToShow: 3,
    dots: false,
    draggable: false,
    slidesToScroll: 3,
    arrows: false,
    speed: 1300,
    autoplay: false,
    centerMode: false,
    infinite: false,
    afterChange: indexOfCurrentSlide => {
      setState({
        currentIndex: indexOfCurrentSlide,
        slidesToScroll: 3
      });
    },
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 2
            });
          }
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 1
            });
          }
        }
      }
    ]
  };

  return (
    <div className="app">
      <div>
        <h1>Slider Buttons</h1>
        {renderArrows(previous, next, state, TOTAL_SLIDES - 1)}
      </div>

      {/*  Slider */}
      <Slider {...settings} ref={slider => (sliderRef.current = slider)}>
        {[...Array(TOTAL_SLIDES)].map((_, index) => {
          return (
            <div className="card p-5" key={index}>
              <div className="card-img-top">
                <svg
                  className="svg"
                  width="100%"
                  height="225"
                  xmlns="http://www.w3.org/2000/svg"
                  preserveAspectRatio="xMidYMid slice"
                  focusable="false"
                >
                  <rect width="100%" height="100%" fill="#55595c" />
                  {/* The `dy` attribute indicates a shift along the y-axis on the position of
                                  an element or its content. .3em => 4.8px */}
                  <text x="50%" y="50%" fill="#eceeef" dy=".3em">
                    Image {index + 1}
                  </text>
                </svg>
              </div>
              <div className="card-body">
                <p className="card-text">
                  This is a wider card with supporting text below.
                </p>
              </div>
            </div>
          );
        })}
      </Slider>
    </div>
  );
};

export default Carousel;

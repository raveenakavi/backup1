import Head from 'next/head'
import Prevnextslider from './prevnextslider'
import Ratings from './Ratings'
import Ratings1 from './Ratings1'
import Reviews from './Reviews'
import Carousel from './Carousel'
import Orderstatus from './WriteReview'
import Writereview from './WriteReview'
import Compare from './Compare'
import Filtering from './Filtering'
import Protection from './Protection'
import Accessories from './Accessories'
import SimilarProducts from './SimilarProducts'
import MoreProducts from './MoreProducts'
import FrequentlyCarousel from './FrequentlyCarousel'
import Carousel1 from './Carousel1'
import FrequentlyCarousel1 from './FrequentlyCarousel1'

var message:string = "Hello World" 
console.log(message)

export default function Home() {
  return (
    <div>
      {/* <Carousel /> */}
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {/* <div className="text-red-800">Welcome to Next.js!</div> */}
      {/* <Reviews /> */}
      {/* <Ratings /> */}
      {/* <Ratings /> */}
      {/* <Writereview /> */}
      {/* <Orderstatus /> */}
      {/* <Prevnextslider /> */}
      {/* <Compare /> */}
      {/* <Filtering /> */}
      <Protection />
      <br />
      {/* <Carousel /> */}
      <Accessories /><br />
      <Carousel1 /><br />
      <MoreProducts /><br />
      {/* <FrequentlyCarousel1 /><br /><br /> */}
    </div>
  )
}

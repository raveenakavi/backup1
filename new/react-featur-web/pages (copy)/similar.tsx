import React, { Component, useState, useRef } from "react";
import MultiCarousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { Grade, Settings } from '@material-ui/icons';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ReactDOM from "react-dom";
import { Button } from "@material-ui/core";

const renderArrows = (
    prevCallback,
    nextCallback,
    { currentIndex, slidesToScroll },
    totalSlides
  ) => {
    const cycleDots =
      currentIndex === 0 ? 1 : Math.ceil(totalSlides / slidesToScroll);
    return (
      <div>
        <button disabled={currentIndex === 0} onClick={prevCallback}>
          Previous
        </button>
        <button disabled={currentIndex > cycleDots} onClick={nextCallback}>
          Next
        </button>
      </div>
    );
  };

export default function SimilarProducts() {

    const customeSlider = useRef<typeof Slider | null>(null);
    const settings = {
      dots: true
  }
    // setting slider configurations
    const [sliderSettings, setSliderSettings] = useState({
      infinite: true,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 2,
      arrows: true,
      dots: true,
    })
  
    const next = () => {
      Slider.current.slickNext();
    }
    const previous = () => {
      Slider.current.slickPrev();
    }
  
    const gotoNext = () => {
      customeSlider.current.slickNext()
    }
  
    const gotoPrev = () => {
      customeSlider.current.slickPrev()
    }
    const renderSlides = () =>
    [1, 2, 3, 4, 5, 6, 7, 8].map(num => (
      <div>
        <h3>Slide {num}</h3>
      </div>
    ));


    const responsive = {
        superLargeDesktop: {
            breakpoint: { max: 4000, min: 3000 },
            items: 5,
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 3,
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2,
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
        },
    };

    return (
        <div className="similar-products">
            <h2 className="flex py-4 px-3 border-b border-gray-300 text-black text-md md:text-2xl font-bold">Compare similar Products 
            <ul className="md:flex similar-list break-normal text-sm pb-2 pt-2 text-gray-500">
                        <li className="text-green-500 mx-5 px-2">Comparing:</li>
                        <li className="text-green-500 mx-5 px-2 border border-gray-300 rounded-lg">Front Camera</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Main Camera</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">RAM</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Battery</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Performance</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">price</li>

                    </ul> 
                    <span className="text-xs right-10 absolute pt-2"> See all</span>
                    </h2>
<div>
{/* <Slider  {...sliderSettings} dots={true}>{renderSlides()}
        </Slider> */}
<Slider {...Settings}>
  <div className="slick-li items">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="slick-li items"><img src={'/asset/similar-1.png'} />
        <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>

      <div className="slick-li items">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="slick-li items"><img src={'/asset/similar-1.png'} />
        <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="slick-li items">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="slick-li items"><img src={'/asset/similar-1.png'} />
        <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="slick-li items">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="slick-li items"><img src={'/asset/similar-1.png'} />
        <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="slick-li items">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="slick-li items"><img src={'/asset/similar-1.png'} />
        <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="slick-li items">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="slick-li items"><img src={'/asset/similar-1.png'} />
        <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div className="slick-li items">
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="slick-li items"><img src={'/asset/similar-1.png'} />
        <h3 className="text-black text-md">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className=" text-red-500 text-sm pl-5 float-right"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>

</Slider>;
<button className="button" onClick={previous}>
            Previous
          </button>
          <button className="button" onClick={next}>
            Next
          </button>

        <Button type="button" data-role="none" className="slick-arrow slick-prev">Previous</Button>
        <Button type="button" data-role="none" className="slick-arrow slick-prev">Next</Button>

</div>
        </div>
    )
}

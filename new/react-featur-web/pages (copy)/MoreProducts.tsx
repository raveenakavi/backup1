import React, { useState, useRef } from "react";
import Slider from "react-slick";
import { Grade } from '@material-ui/icons';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import AppleIcon from '@material-ui/icons/Apple';

const TOTAL_SLIDES = 6;

/**
 * It will return the JSX and register the callbacks for next and previous slide.
 * @param prevCallback {function} - Go back to the previous slide
 * @param nextCallback {function} - Go to the next slide
 * @param state {object} - Holds the state of your slider indexes
 * @param totalSlides {number} - Holds the total number of slides
 * @return {*} - Returns the JSX
 */
const renderArrows = (
  prevCallback,
  nextCallback,
  { currentIndex, slidesToScroll },
  totalSlides
) => {
  const cycleDots =
    currentIndex === 0 ? 1 : Math.ceil(totalSlides / slidesToScroll);
  return (
    <div className="mx-2 ...">
      <button disabled={currentIndex === 0} onClick={prevCallback} className="previus-button text-bg-gray margin-2 padding-10">
        <ArrowBackIosIcon />
      </button>
      <button disabled={currentIndex > cycleDots} onClick={nextCallback} className="next-button text-bg-gray margin-2 padding-10">
      <ArrowForwardIosIcon />
      </button>
    </div>
  );
};

const MoreProducts = () => {
  const [state, setState] = useState({ currentIndex: 0, slidesToScroll: 0 });
  const sliderRef = useRef();
  const next = () => {
    sliderRef.current.slickNext();
  };

  const previous = () => {
    sliderRef.current.slickPrev();
  };

  const settings = {
    slidesToShow: 3,
    dots: false,
    draggable: false,
    slidesToScroll: 3,
    arrows: false,
    speed: 1300,
    autoplay: false,
    centerMode: false,
    infinite: false,
    afterChange: indexOfCurrentSlide => {
      setState({
        currentIndex: indexOfCurrentSlide,
        slidesToScroll: 3
      });
    },
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 2
            });
          }
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          afterChange: indexOfCurrentSlide => {
            setState({
              currentIndex: indexOfCurrentSlide,
              slidesToScroll: 1
            });
          }
        }
      }
    ]
  };

  return (
    <div className="app similar-products">
      {/*  Slider */}

      <h2 className="flex py-4 px-3 border-b border-gray-300 text-black text-md md:text-2xl font-bold"><AppleIcon style={{ fontSize: 33}} /> <span className="ml-4">More from Apple</span>
            <ul className="px-8 md:flex similar-list break-normal text-sm pb-2 pt-2 text-gray-500">
                        <li className="text-green-500 mx-5 px-2 border border-gray-300 rounded-lg">iPhone</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">iMac</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Airpods</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">iWatch</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">iPad</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">TV</li>

                    </ul> 
                    <span className="text-xs pt-3 text-blue-600"> See all</span>
                    <span className="right-2 absolute">{renderArrows(previous, next, state, TOTAL_SLIDES - 1)}</span>
                    
                    </h2>
      <Slider {...settings} ref={slider => (sliderRef.current = slider)}>
        {/* {[...Array(TOTAL_SLIDES)].map((_, index) => { */}
        <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
      <div className="more-product px-6">
      <img src={'/asset/moreproduct-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"></span> 5000 mah battery</li>

      </ul>
  </div>
  <div className="more-product px-6"><img src={'/asset/moreproduct-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="float-right pr-3 text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"></span> 5000 mah battery</li>

      </ul></div>
             
             
        {/* })} */}
      </Slider>
      {/* {renderArrows(previous, next, state, TOTAL_SLIDES - 1)} */}
    </div>
  );
};

export default MoreProducts;

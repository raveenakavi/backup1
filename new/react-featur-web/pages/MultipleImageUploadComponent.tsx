import React, { useState, Component } from 'react';

export default function MultipleImageUploadComponent() {
    const [state, setState] = useState([]);
    const[fileObj, Setfileobj] = useState([]);
    const[fileArray, SetfileArray] = useState([]);

    uploadMultipleFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setState({ file: fileArray })
    }

    uploadFiles(e) {
        e.preventDefault()
        console.log(state.file)
    }

        return (
            <form>
                <div className="form-group multi-preview">
                    {(fileArray || []).map(url => (
                        <img src={url} alt="..." />
                    ))}
                </div>

                <div className="form-group">
                    <input type="file" className="form-control" onChange={uploadMultipleFiles} multiple />
                </div>
                <button type="button" className="btn btn-danger btn-block" onClick={uploadFiles}>Upload</button>
            </form >
        )
}
import React, { Component, useState, useRef } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ReactDOM from "react-dom";
import { Button } from "@material-ui/core";

const renderArrows = (
  prevCallback,
  nextCallback,
  { currentIndex, slidesToScroll },
  totalSlides
) => {
  const cycleDots =
    currentIndex === 0 ? 1 : Math.ceil(totalSlides / slidesToScroll);
  return (
    <div>
      <button disabled={currentIndex === 0} onClick={prevCallback}>
        Previous
      </button>
      <button disabled={currentIndex > cycleDots} onClick={nextCallback}>
        Next
      </button>
    </div>
  );
};

export default function prevnextslider() {

    //creating the ref
//   customeSlider = React.creatRef<HTMLDivElement>();
  const customeSlider = useRef<typeof Slider | null>(null);
  const settings = {
    dots: true
}
  // setting slider configurations
  const [sliderSettings, setSliderSettings] = useState({
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: true,
    dots: true,
  })

  const next = () => {
    Slider.current.slickNext();
  }
  const previous = () => {
    Slider.current.slickPrev();
  }

  const gotoNext = () => {
    customeSlider.current.slickNext()
  }

  const gotoPrev = () => {
    customeSlider.current.slickPrev()
  }
  const renderSlides = () =>
  [1, 2, 3, 4, 5, 6, 7, 8].map(num => (
    <div>
      <h3>Slide {num}</h3>
    </div>
  ));
    return (
        <div className="container mt-6">
        <div className="md:w-full reviews border border-gray-300">
        <h2>Previous and Next methods</h2>
        {/* <Slider  {...sliderSettings} ref={customeSlider}>
          <div key={1}>
          <img src="http://placekitten.com/g/400/200" />
          </div>
          <div key={2}>
          <img src="http://placekitten.com/g/400/200" />
          </div>

        </Slider> */}
        <Slider  {...sliderSettings} dots={true}>{renderSlides()}
        </Slider>

        {/* <div>
      <button disabled={slider.current === 0} onClick={previous}>
        Previous
      </button>
      <button disabled={current > lastElementsIndex} onClick={next}>
        Next
      </button>
    </div> */}

        <Slider {...settings}>
            <div><img src='http://placekitten.com/g/400/200' /></div>
          <div><img src='http://placekitten.com/g/400/200' /></div>
          <div><img src='http://placekitten.com/g/400/200' /></div>
          <div><img src='http://placekitten.com/g/400/200' /></div>
        </Slider>


        <button className="button" onClick={previous}>
            Previous
          </button>
          <button className="button" onClick={next}>
            Next
          </button>

        <Button type="button" data-role="none" className="slick-arrow slick-prev">Previous</Button>
        <Button type="button" data-role="none" className="slick-arrow slick-prev">Next</Button>

        {/* <div style={{ textAlign: "center" }}>
          <button className="button" onClick={()=>gotoPrev()}>
            Previous
          </button>
          <button className="button" onClick={()=>gotoNext()}>
            Next
          </button>
        </div> */}
      </div>
      </div>
    )
}

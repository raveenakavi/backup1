import React, { useState, useRef } from "react";
import { Grid, Button, InputBase } from '@material-ui/core';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import FavoriteIcon from '@material-ui/icons/Favorite';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import { Grade } from '@material-ui/icons';


export default function CategoryTab() {

  const StyledRating = withStyles({
    iconFilled: {
      color: '#f11a08',
    },
    iconHover: {
      color: '#ff3d47',
    },
  })(Rating);

  return (
    <div className="category-series">
        <h1 className="uppercase text-center text-2xl md:text-4xl font-bold relative"> Choose Your Need</h1>

        <div>

        <div className="my-order md:flex panel-content  mt-5 ">
            <div className="categorytabs">
              <div className="ml-auto mr-auto w-8/12 category-tab bg-black text-white rounded-full px-16">
              <input id="tab1" type="radio" name="categorytabs" checked />
              <label htmlFor="tab1">Call/Work</label>
              <input id="tab2" type="radio" name="categorytabs" />
              <label htmlFor="tab2">Travel</label>
              <input id="tab3" type="radio" name="categorytabs" />
              <label htmlFor="tab3">Gaming</label>
              <input id="tab4" type="radio" name="categorytabs" />
              <label htmlFor="tab4">GYM</label>
              <input id="tab5" type="radio" name="categorytabs" />
              <label htmlFor="tab5">Professional</label>
              <input id="tab6" type="radio" name="categorytabs" />
              <label htmlFor="tab6">Movie</label>                            
                            </div>
              <div className="content w-full">
                <div id="content1">
                  <div className="grid grid-flow-col grid-cols-4 gap-4">
                <div className="element">
        <div className="flex relative">
        <span className="vc text-xs">50% OFF</span>
        </div>
        <img src={'/asset/tab-image-1.jpeg'} />
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <p className="flex font-bold text-black text-lg pt-4 px-3">Rs.62,899 <span className="w-2/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></p>
      <span className="text-gray-600 font-bold text-sm py-2 px-3" id="initial"><span className="text-blue-600">Save Rs.1,249</span></span>
  </div>

  <div className="element">
        <div className="flex relative">
        <span className="vc text-xs">50% OFF</span>
        </div>
        <img src={'/asset/tab-image-2.jpeg'} />
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <p className="flex font-bold text-black text-lg pt-4 px-3">Rs.62,899 <span className="w-2/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></p>
      <span className="text-gray-600 font-bold text-sm py-2 px-3" id="initial"><span className="text-blue-600">Save Rs.1,249</span></span>
  </div>

  <div className="element">
        <div className="flex relative">
        <span className="vc text-xs">50% OFF</span>
        </div>
        <img src={'/asset/tab-image-3.jpeg'} />
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <p className="flex font-bold text-black text-lg pt-4 px-3">Rs.62,899 <span className="w-2/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></p>
      <span className="text-gray-600 font-bold text-sm py-2 px-3" id="initial"><span className="text-blue-600">Save Rs.1,249</span></span>
  </div>

  <div className="element">
        <div className="flex relative">
        <span className="vc text-xs">50% OFF</span>
        </div>
        <img src={'/asset/tab-image-4.jpg'} />
      <h3 className="text-black text-md pt-2 px-3">Product Name in 35 Character </h3>
      <p className="flex font-bold text-black text-lg pt-4 px-3">Rs.62,899 <span className="w-2/6 text-red-500 text-sm right-0 absolute"><Grade /> <span className="text-gray-600">4.5</span></span></p>
      <span className="text-gray-600 font-bold text-sm py-2 px-3" id="initial"><span className="text-blue-600">Save Rs.1,249</span></span>
  </div>
  </div>

                </div>

                {/* <div id="content2">
                  content2
                </div>

                <div id="content3">
                  content3
                </div> */}

              </div>

            </div>
          </div>
        </div>

    </div>
  )
}

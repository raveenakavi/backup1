import React from 'react'
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { Grade } from '@material-ui/icons';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

export default function SimilarProducts({deviceType}) {
    const responsive = {
        superLargeDesktop: {
            breakpoint: { max: 4000, min: 3000 },
            items: 7,
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 5,
        },
        tablet: {
            breakpoint: { max: 900, min: 600 },
            items: 3,
        },
        tablet1: {
            breakpoint: { max: 600, min: 400 },
            items: 2,
        },        mobile: {
            breakpoint: { max: 400, min: 0 },
            items: 1,
        },
    };

    return (
        <div className="similar-products">
            <h2 className="flex py-4 px-3 border-b border-gray-300 text-black text-md md:text-2xl font-bold">Compare similar Products 
            <ul className="md:flex similar-list break-normal text-sm pb-2 pt-2 text-gray-500">
                        <li className="text-green-500 mx-5 px-2">Comparing:</li>
                        <li className="text-green-500 mx-5 px-2 border border-gray-300 rounded-lg">Front Camera</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Main Camera</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">RAM</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Battery</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">Performance</li>
                        <li className="mx-5 px-2 border border-gray-300 rounded-lg">price</li>

                    </ul> 
                    <span className="text-xs right-10 absolute pt-2 text-blue-600"> See all</span>
                    </h2>
<div>
            <Carousel
  swipeable={false}
  draggable={false}
  showDots={false}
  responsive={responsive}
  ssr={true} // means to render carousel on server-side.
  infinite={true}
  autoPlay={false}
  autoPlaySpeed={1000}
  keyBoardControl={true}
  customTransition="all .5"
  transitionDuration={500}
  containerClass="carousel-container"
  removeArrowOnDeviceType={["tablet", "mobile","desktop"]}
//   dotListClass="custom-dot-list-style"
  itemClass="carousel-item-padding-40-px"
  deviceType={deviceType}
>
  <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>

      <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon style={{ color: 'green' }}/></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>
      <div>
      <img src={'/asset/similar-1.png'} />
      <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> 5000 mah battery</li>

      </ul>
  </div>
  <div><img src={'/asset/similar-2.png'} />
        <h3 className="text-black text-md pt-2">Apple Airpods pro </h3>
      <p className="font-normal text-gray-400">(Green, 128 GB)</p>
      <p className="text-black text-sm pt-4">Rs.62,899 <span className="bg-yellow-300 p-1 ml-3 px-3 text-center">4 % Off</span><span className="right-2 absolute text-red-500 text-sm pl-5"><Grade /> <span className="text-gray-600">3.9</span></span></p>
      <ul className="text-sm py-3">
          <li><span className="text-red-600 pr-2"><ArrowDownwardIcon /></span> Front Camera: 16Mp</li>
          <li><span className="text-green-600 pr-2"><ArrowUpwardIcon /></span> Main Camera: 108Mp, Quad c</li>
          <li><span className="text-gray-600 pr-2"><ArrowForwardIcon /></span> 5000 mah battery</li>

      </ul></div>


</Carousel>
</div>
        </div>
    )
}

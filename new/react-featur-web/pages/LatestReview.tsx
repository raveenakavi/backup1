import React, { useState } from "react";
import { Rate, Modal, Carousel, Icon } from "antd";
import StarRatings from 'react-star-ratings'
import Avatar from '@material-ui/core/Avatar';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';


const SampleNextArrow = props => {
  const { className, onClick } = props;
  return (
    <div
      className={`${className} 
            mp-latest-review__next 
            mp-latest-review__button-next-prev`}
      onClick={onClick}
    >
      {/* <Icon type="right" className="mp-latest-review__iconButton" /> */}
    </div>
  );
};

const SamplePrevArrow = props => {
  const { className, onClick } = props;
  return (
    <div
      className={`${className} 
            mp-latest-review__prev 
            mp-latest-review__button-next-prev`}
      onClick={onClick}
    >
    </div>
  );
};

function LatestReview(props) {
  const { rating, message, images, reviewer } = props.review;

  const [visible, setVisible] = useState(false);

  function showModal() {
    setVisible(true);
  }

  function handleClose(e) {
    setVisible(false);
  }
  const [performanceRating, setPerformanceRating] = useState<number>(0);
  const [performanceStarColorChange, setPerformanceStarColorChange] = useState("#ddd");

  const selectedPerformanceStarCount = (performanceRating) =>{
    setPerformanceRating(performanceRating);
    if(performanceRating == 1){
      setPerformanceStarColorChange("#f9151a")
    }else if(performanceRating == 2){
      setPerformanceStarColorChange("#ff585d")
    }else if(performanceRating == 3){
      setPerformanceStarColorChange("#fe7b02")
    }else if(performanceRating == 4){
      setPerformanceStarColorChange("#83d564")
    }else if(performanceRating == 5){
      setPerformanceStarColorChange("#2ea000")
    }else{
      setPerformanceStarColorChange("#ddd")
    }
    
  }

  const settings = {
    slidesToShow: 1,
    arrows: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    appendDots: dots => (
      <div style={{ borderRadius: 10 }}>
        <ul className="dots">{dots}</ul>
      </div>
    )
  };

  const slides = images.map((item, i) => {
    return <img key={i} src={item.largeUrl} alt="" />;
  });

  let checkImages = images.length < 1 ? "" : "mp-latest-review-image";

  function reviewItem(typeShow) {
    return (
        
<div className={`${checkImages} mp-latest-review`} onClick={showModal}>
        <div className="review pb-3">
<h4 className=" flex pt-8 text-l font-bold text-green-600 sm:flex">
<StarRatings starDimension={20} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(performanceRating)=> selectedPerformanceStarCount(performanceRating)} />
   <span className="float-left w-1/2 text-left pl-10">Loving It !</span></h4>
<div className="w-full md:w-2/5 review-status inline-block pt-2">
<div className="inline-block">
<Avatar
   src={
   reviewer.imageUrl
   ? reviewer.imageUrl
   : "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Anonymous_emblem.svg/1200px-Anonymous_emblem.svg.png"
   }
   alt=""
   className="mp-latest-review__profil-image-user"
   />
    
 </div>
<div className="inline-block align-middle pl-5">
    <h4 className="text-lg font-bold ">{reviewer.name}</h4>
    <span className="text-xs text-gray-300">Purchased Customer</span> <span className="w-1/2 text-xs text-gray-500 font-bold pl-6">7 days ago</span>
    </div>
</div>
<div className="w-full md:w-3/5 inline-flex">
<div className='camera text-xs flex-1'> <span className="leading-normal">Camera</span><br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li> <li></li></ul>
    </div>
    <div className='appearance text-xs flex-1'> Appearance/Design<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    <div className='battery text-xs flex-1'> Battery<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    </div>
    
    <p className="mobile-p pt-8 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>
    <div className="w-full md:flex pb-8 border-b border-gray-200">
                            <span className="overflow-hidden space-x-4 w-full flex md:w-4/5 flex md:space-x-2 pr-52"><img src="asset/review2.png" />
                            <img src="asset/review2.png" />
                            <img src="asset/review3.png" />
                            <img src="asset/review4.png" /></span>
                            <span className="w-full md:w-1/5 pt-9 pb-0 inline flex md:space-x-16 text-xs"><span className="fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 </span>
                            </div>                        
                            </div>
                            </div>
    //   <div className={`${checkImages} mp-latest-review`} onClick={showModal}>
    //     <span>
    //       <img
    //         src={
    //           reviewer.imageUrl
    //             ? reviewer.imageUrl
    //             : "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Anonymous_emblem.svg/1200px-Anonymous_emblem.svg.png"
    //         }
    //         alt=""
    //         className="mp-latest-review__profil-image-user"
    //       />
    //       &nbsp;
    //       <StarRatings disabled value={rating} />
    //     </span>
    //     <br />
    //     <div className="mp-latest-review__content">
    //       <p className="mp-latest-review__userNameTime">
    //         Di ulas oleh
    //         <b>{reviewer.name}</b>pada &nbsp;
    //       </p>
    //       <p>{message}</p>
    //       {typeShow === "review" &&
    //         images.map((img, i) => {
    //           return (
    //             <img
    //               key={i}
    //               src={img.mediumUrl}
    //               alt=""
    //               className="mp-latest-review__product-image-order-user"
    //             />
    //           );
    //         })}
    //     </div>
    //   </div>
    );
  }

  return (
    <React.Fragment>
      {reviewItem("review")}
      {/* {images.length >= 1 ? (
        <Modal
          title=""
          visible={visible}
          wrapClassName="mp-latest-review__modal-image"
          onOk={() => handleClose()}
          onCancel={() => handleClose()}
        >
          <Carousel {...settings}>{slides}</Carousel>
          {reviewItem()}
        </Modal>
      ) : (
        ""
      )} */}
    </React.Fragment>
  );
}

export default LatestReview;

import React, { useState, useEffect } from "react";
import { Grade } from '@material-ui/icons';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import SearchIcon from '@material-ui/icons/Search';
import RemoveIcon from '@material-ui/icons/Remove';
import RangeSlider from 'reactrangeslider';
// import Sliders from "@material-ui/core/Slider";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import AddIcon from "@material-ui/icons/Add";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function HeadPhones() {

  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  const [slider1, setSlider1] = useState(null);
  const [slider2, setSlider2] = useState(null);
  const [showText, setShowText] = useState(true);
  const [showFilter, setShowFilter] = useState(true);
  const [showScreen, setShowScreen] = useState(true);
  const [showPrimary, setShowPrimary] = useState(true);
  const [showSecondary, setShowSecondary] = useState(true);
  const [showProcessor, setShowProcessor] = useState(true);

  useEffect(() => {

    setNav1(slider1);
    setNav2(slider2);

  });


  const settingsMain = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.slider-nav'
  };

  const settingsThumbs = {
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    swipeToSlide: true,
    focusOnSelect: true,
    centerPadding: '10px'
  };

  const slidesData = [
    {
      id: 1,
      title: 'repellendus id ullam',
      label: 'Dolorem officiis temporibus.'
    }, {
      id: 2,
      title: 'excepturi consequatur est',
      label: 'Officia non provident dolor esse et neque.'
    }, {
      id: 3,
      title: 'eius doloribus blanditiis',
      label: 'Ut recusandae vel vitae molestiae id soluta.'
    }, {
      id: 4,
      title: 'nihil voluptates delectus',
      label: 'Qui vel consequatur recusandae illo repellendus.'
    }, {
      id: 5,
      title: 'nemo dolorem necessitatibus',
      label: 'Placeat odit velit itaque voluptatem.'
    }, {
      id: 6,
      title: 'dolorem quibusdam quasi',
      label: 'Adipisci officiis repudiandae.'
    },
  ];


  const [values, setValues] = useState([15, 75]);
  const rangeSelector = (event, newValue) => {
      setValues(newValue);
      console.log(newValue);
  };
  const [value, setValue] = React.useState([10, 50]);
  
    const StyledRating = withStyles({
        iconFilled: {
          divor: '#f11a08',
        },
        iconHover: {
          divor: '#ff3d47',
        },
      })(Rating);
    
    const [divShow, setDivShow] = useState(false);
    const [divHide, setDivHide] = useState(false);
    
    const[add,minus]=useState(true);
    
    const selectChange = (element) =>{
      if(element == add){
        setDivShow(false);
        setDivHide(true);
      }else if(element==minus){
        setDivShow(true);
        setDivHide(false);
      }
    }

    // function Example2() {
    //   const [value, setValue] = React.useState([10, 50]);
    //   return (
    //     <div>
    //       <div>
    //         <RangeSlider
    //           progress
    //           style={{ marginTop: 16 }}
    //           value={value}
    //           onChange={value => {
    //             setValue(value);
    //           }}
    //         />
    //       </div>
    //       <div>
    //         <input>
    //           <input
    //             min={0}
    //             max={100}
    //             value={value[0]}
    //             onChange={nextValue => {
    //               const [start, end] = value;
    //               if (nextValue > end) {
    //                 return;
    //               }
    //               setValue([nextValue, end]);
    //             }}
    //           />
    //           <p>to</p>
    //           <input
    //             min={0}
    //             max={100}
    //             value={value[1]}
    //             onChange={nextValue => {
    //               const [start, end] = value;
    //               if (start > nextValue) {
    //                 return;
    //               }
    //               setValue([start, nextValue]);
    //             }}
    //           />
    //         </input>
    //       </div>
    //     </div>
    //   );
    // }

    return (
        <div className="container flex gap-x-3 md:gap-8 similar-products earbuds py-10 px-6">
            <div className="w-2/6 md:w-1/4 headphones row-span-6">
            <h3 className="text-lg font-bold border-b border-yellow-500 pb-2 pt-1 px-2">Filters <span className="float-right text-xs font-normal text-gray-500 pt-2 pr-2">Clear All </span></h3>

            {/* <RangeSlider
      value={ value }
      onChange={ onChange }
      min={ 20 }
      max={ 100 }
      step={ 5 }
    /> */}

<div className="grid grid-cols-6 gap-1">
                <div className={`${showText ? "col-start-1 col-span-2 sm:col-start-1 sm:col-span-1 md:col-start-1 md:col-span-1 lg:col-start-1 lg:col-span-1 ..." : "test"}`}>
                    {showText && (
                        <div className="category_filterheader rounded">
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_background">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">filters</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1">
                                    clear all <FontAwesomeIcon className="category_inputdropdown category_filterclose" icon="times-circle" />
                                </p>
                            </div>
                            <div className="flex justify-between pt-2 pb-2 pr-2 pl-2 category_filterclose">
                                <p className="flex-grow capitalize inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">price range</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1"></p>
                            </div>
                            <div className="flex pl-0 sm:pl-10 md:pl-10 lg:pl-10">
                                <div className="flex-1">Rs. {values[0]}</div>
                                <div className="flex-1 capitalize">to</div>
                                <div className="flex-1">Rs. {values[1]}</div>
                            </div>
                            <div className="pr-4 pl-4 pt-2 pb-2">
                                <Slider value={values} onChange={rangeSelector} valueLabelDisplay="auto" className="category_filterclose" />
                            </div>

                            <div className="flex justify-between pt-1 pb-1 pr-1 pl-1 ">
                                <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">brand</p>

                                <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 category_filterclose">47 brands</p>
                            </div>
                            <div className="pt-2 pb-0 category_background">
                                <div className="pr-3 pl-3 relative flex w-full flex-wrap items-stretch mb-1">
                                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-300 absolute bg-transparent rounded text-base items-center justify-center w-10 pl-1 py-1">
                                        <FontAwesomeIcon icon="search" />
                                    </span>
                                    <input
                                        type="text"
                                        placeholder="Search"
                                        className=" w-28  sm:w-full md:w-full lg:w-full px-2 py-1 category_searchmethod placeholder-blueGray-300 text-blueGray-600 relative bg-white rounded text-sm border border-blueGray-300 outline-none focus:w-full pl-10"
                                    />
                                </div>
                                <p className="mb-1">
                                    <input type="radio" id="apple" name="brand" value="apple" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        apple
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="celkon" name="brand" value="celkon" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        celkon
                                    </label>
                                </p>

                                <p className="mb-1">
                                    <input type="radio" id="karbon" name="brand" value="karbon" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        karbon
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="micromax" name="brand" value="micromax" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        micromax
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="samsung" name="brand" value="samsung" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        samsung
                                    </label>
                                </p>
                                <p className="mb-1">
                                    <input type="radio" id="nokia" name="brand" value="nokia" className="ml-3" />
                                    <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                        nokia
                                    </label>
                                </p>
                            </div>

                            <div className="pt-0 pb-2 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showFilter ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">ram</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowFilter(!showFilter)} />
                                        <RemoveIcon onClick={() => setShowFilter(!showFilter)} className="category_removeicon" />
                                    </p>
                                </div>

                                {showFilter && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="8gb ram" name="ram" value="8gb ram" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                8GB &amp; Above
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="6gb ram" name="ram" value="6gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6GB
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4gb ram" name="ram" value="4gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="3gb ram" name="ram" value="3gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                3GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="2gb ram" name="ram" value="2gb ram" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB-1GB" name="ram" value="512MB-1GB" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                512MB-1GB
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="512MB" name="ram" value="512MB" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than- 512MB
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showScreen ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">screen size</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowScreen(!showScreen)} />
                                        <RemoveIcon onClick={() => setShowScreen(!showScreen)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showScreen && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 3inch" name="screensize" value="lessthan 3inch" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 3 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4inch" name="screensize" value="4inch" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 inch - 4.99 inch
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="5inch" name="screensize" value="5inch" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                5 inch - 5.99 inch
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="6inch" name="screensize" value="6inch" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                6 inch &amp; Above
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showPrimary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">primary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowPrimary(!showPrimary)} />
                                        <RemoveIcon onClick={() => setShowPrimary(!showPrimary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showPrimary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="primarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="primarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="primarycamera" value="12mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="primarycamera" value="20mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="primarycamera" value="40mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showSecondary ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">secondary camera</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowSecondary(!showSecondary)} />
                                        <RemoveIcon onClick={() => setShowSecondary(!showSecondary)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showSecondary && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="lessthan 2mp" name="secondarycamera" value="lessthan 2mp" className="ml-3" />
                                            <label className="ml-3  text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Less than 2MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                2 MP - 3.99 MP
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="4mp" name="secondarycamera" value="4mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                4 MP - 11.99 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="12mp" name="secondarycamera" value="12mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                12 MP -19.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="20mp" name="secondarycamera" value="20mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                20 MP -39.9 MP
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="40mp" name="secondarycamera" value="40mp" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Above 40 MP
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                            <div className="pt-0 pb-0 category_background">
                                <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1  bg-white ${showProcessor ? "on" : "off"}`}>
                                    <p className="flex-grow uppercase inline-flex items-center p-0 cart_flexing font-bold text-xs sm:tex-sm md:text-sm lg:text-sm">Processor</p>

                                    <p className="capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-bold p-1 text-black">
                                        <AddIcon className="category_addicon" onClick={() => setShowProcessor(!showProcessor)} />
                                        <RemoveIcon onClick={() => setShowProcessor(!showProcessor)} className="category_removeicon" />
                                    </p>
                                </div>
                                {showProcessor && (
                                    <div>
                                        <p className="mb-1">
                                            <input type="radio" id="dualcore" name="processor" value="dualcore" className="ml-3" />
                                            <label className="ml-3  text-sm font-medium">
                                                Dual Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="hexacore" name="processor" value="hexacore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Hexa Core
                                            </label>
                                        </p>

                                        <p className="mb-1">
                                            <input type="radio" id="octacore" name="processor" value="octacore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Octa Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="quadcore" name="processor" value="quadcore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Quad Core
                                            </label>
                                        </p>
                                        <p className="mb-1">
                                            <input type="radio" id="singlecore" name="processor" value="singlecore" className="ml-3" />
                                            <label className="ml-3 capitalize text-xs sm:tex-sm md:text-sm lg:text-sm font-medium">
                                                Single Core
                                            </label>
                                        </p>
                                    </div>
                                )}
                            </div>
                        </div>
                    )}
                </div>


                        </div>
<div className="w-4/6 md:w-3/4 mx-2 md:mx-0">
    <div className="md:flex headphonesgrid rounded-lg my-4">
            <div className="w-full md:w-2/5 element py-2 my-2">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<Slider
  {...settingsMain}
  asNavFor={nav2}
  ref={slider => (setSlider1(slider))}
>

  {slidesData.map((slide) =>
    <div className="slick-slide" key={slide.id}>
      <img className="slick-slide-image" src={`/asset/earbuds-2.png?img=${slide.id}`} />
    </div>

  )}

</Slider>
<div className="thumbnail-slider-wrap">
  <Slider
    {...settingsThumbs}
    asNavFor={nav1}
    ref={slider => (setSlider2(slider))}>

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <img className="slick-slide-image" src={`/asset/earbuds-2.png?img=${slide.id}`} />
      </div>

    )}

  </Slider>
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-3/5 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
  </div>


  <div className="headphonesgrid App1">

<div className="slider-wrapper">

  <Slider
    {...settingsMain}
    asNavFor={nav2}
    ref={slider => (setSlider1(slider))}
  >

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <h2 className="slick-slide-title">{slide.title}</h2>
        <img className="slick-slide-image" src={`https://picsum.photos/800/400?img=${slide.id}`} />
        <label className="slick-slide-label">{slide.label}</label>
      </div>

    )}

  </Slider>
  <div className="thumbnail-slider-wrap">
    <Slider
      {...settingsThumbs}
      asNavFor={nav1}
      ref={slider => (setSlider2(slider))}>

      {slidesData.map((slide) =>

        <div className="slick-slide" key={slide.id}>
          <img className="slick-slide-image" src={`https://picsum.photos/800/400?img=${slide.id}`} />
        </div>

      )}

    </Slider>
  </div>
</div>

</div>
  {/* <div className="md:flex headphonesgrid rounded-lg my-4">
            <div className="w-full md:w-2/5 element py-2 my-2" id="surround">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<Sliders
  {...settingsMain}
  asNavFor={nav2}
  ref={slider => (setSlider1(slider))}
>

  {slidesData.map((slide) =>
    <div className="slick-slide" key={slide.id}>
      <img className="slick-slide-image" src={`/asset/earbuds-3.jpeg?img=${slide.id}`} />
    </div>

  )}

</Sliders>
<div className="thumbnail-slider-wrap">
  <Sliders
    {...settingsThumbs}
    asNavFor={nav1}
    ref={slider => (setSlider2(slider))}>

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <img className="slick-slide-image" src={`/asset/earbuds-3.jpeg?img=${slide.id}`} />
      </div>

    )}

  </Sliders>
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-3/5 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
  </div> */}


  {/* <div className="md:flex headphonesgrid rounded-lg my-4">
            <div className="w-full md:w-2/5 element py-2 my-2" id="surround">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<Sliders
  {...settingsMain}
  asNavFor={nav2}
  ref={slider => (setSlider1(slider))}
>

  {slidesData.map((slide) =>
    <div className="slick-slide" key={slide.id}>
      <img className="slick-slide-image" src={`/asset/earbuds-2.png?img=${slide.id}`} />
    </div>

  )}

</Sliders>
<div className="thumbnail-slider-wrap">
  <Sliders
    {...settingsThumbs}
    asNavFor={nav1}
    ref={slider => (setSlider2(slider))}>

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <img className="slick-slide-image" src={`/asset/earbuds-2.png?img=${slide.id}`} />
      </div>

    )}

  </Sliders>
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-3/5 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
  </div>


  <div className="md:flex headphonesgrid rounded-lg my-4">
            <div className="w-full md:w-2/5 element py-2 my-2" id="surround">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<Sliders
  {...settingsMain}
  asNavFor={nav2}
  ref={slider => (setSlider1(slider))}
>

  {slidesData.map((slide) =>
    <div className="slick-slide" key={slide.id}>
      <img className="slick-slide-image" src={`/asset/bestseller-5.jpeg?img=${slide.id}`} />
    </div>

  )}

</Sliders>
<div className="thumbnail-slider-wrap">
  <Sliders
    {...settingsThumbs}
    asNavFor={nav1}
    ref={slider => (setSlider2(slider))}>

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <img className="slick-slide-image" src={`/asset/bestseller-5.jpeg?img=${slide.id}`} />
      </div>

    )}

  </Sliders>
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-3/5 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
  </div>



  <div className="md:flex headphonesgrid rounded-lg my-4">
            <div className="w-full md:w-2/5 element py-2 my-2" id="surround">
        <div className="md:flex">
        <span className="vc text-xs">New</span>
        <div className="slider-wrapper">

<Sliders
  {...settingsMain}
  asNavFor={nav2}
  ref={slider => (setSlider1(slider))}
>

  {slidesData.map((slide) =>
    <div className="slick-slide" key={slide.id}>
      <img className="slick-slide-image" src={`/asset/bestseller-4.jpeg?img=${slide.id}`} />
    </div>

  )}

</Sliders>
<div className="thumbnail-slider-wrap">
  <Sliders
    {...settingsThumbs}
    asNavFor={nav1}
    ref={slider => (setSlider2(slider))}>

    {slidesData.map((slide) =>

      <div className="slick-slide" key={slide.id}>
        <img className="slick-slide-image" src={`/asset/bestseller-4.jpeg?img=${slide.id}`} />
      </div>

    )}

  </Sliders>
</div>
</div> 
                </div>
  </div>
  <div className="w-full md:w-3/5 relative"><h3 className="text-black font-bold text-md pt-6 px-3">Product Name in 35 Character <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 ">Free 2hours Delivery</span></h3>
      <p className="font-normal text-gray-400 text-xs px-3">divor and Variant 30 Character  <span className="invisible md:visible right-3 absolute pr-3 text-xs pl-5 text-yellow-500 "> <Grade /> 4.5  (200 Reviews)</span></p>
      <p className="w-full flex text-black text-md pt-4 px-3 font-bold">Rs.62,899 <span className="right-6 absolute pr-3 text-sm pl-5 text-white bg-black">10% OFF</span></p>
      <p className="w-full flex text-black text-sm pt-4 px-3">EMI from <span className="right-3 absolute pr-3 text-sm pl-5 text-green-400">Save Rs. 1000</span></p>
      <div className="hidden md:block pt-10 px-3">
          <ul className="float-left break-normal text-sm pt-6 text-gray-500">
              <li><span className="text-gray-500">Key Specs :</span></li><li><span className="text-gray-500">Noice Cancelling</span></li><li><span className="text-gray-500">Acclaimed Lifelike, Sound</span></li><li><span className="text-gray-500">Secure &amp; Comfortable fit</span></li><li><span className="text-gray-500">Simple Touch Controls</span></li>
          </ul>
      </div>

</div>
  </div>




 */}












  </div>
        </div>
        </div> 
    )
}

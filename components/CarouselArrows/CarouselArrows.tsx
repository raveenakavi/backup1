import React from 'react'
import { Row, Col, Carousel } from 'antd';
import styles from "./CarouselArrows.module.scss"

const SampleNextArrow = props => {
  const { className, style, onClick } = props
  return (
    <div
      className={className}
      style={{ ...style, display: 'block', background: 'red' }}
      onClick={onClick}
    />
  )
}

const SamplePrevArrow = props => {
  const { className, style, onClick } = props
  return (
    <div
      className={className}
      style={{ ...style, display: 'block', background: 'green' }}
      onClick={onClick}
    />
  )
}

const settings = {
  nextArrow: <SampleNextArrow />,
  prevArrow: <SamplePrevArrow />
}

const CarouselArrows = () => {
  return (
    <>
      <Row justify="center">
        <Col span={16}>
          <Carousel arrows {...settings}>
            <div>
              <h3 className={`styles.Arrows`}>1</h3>
            </div>
            <div>
              <h3>2</h3>
            </div>
            <div>
              <h3>3</h3>
            </div>
            <div>
              <h3>4</h3>
            </div>
          </Carousel>
        </Col>
      </Row>
    </>
  )
}

export default CarouselArrows
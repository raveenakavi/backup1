import React from 'react';
import styles from './ButtonGroup.module.scss';

type Spacing = 'extraTight' | 'tight' | 'loose';

export interface ButtonGroupProps {
  /** Determines the space between button group items */
  spacing?: Spacing;
  /** Join buttons as segmented group */
  segmented?: boolean;
  /** Buttons will stretch/shrink to occupy the full width */
  fullWidth?: boolean;
  /** Remove top left and right border radius */
  connectedTop?: boolean;
  /** Button components */
  children?: React.ReactNode;
  className?: string;
  image?: string;
  title?: string;
}

export function ButtonGroup({
  children,
  segmented,
  fullWidth,
  connectedTop,
  className,
  title,
  image,
}: ButtonGroupProps) {

  return (
    <div
     className={`${styles['ButtonGroup']}`}
      data-buttongroup-segmented={segmented}
      data-buttongroup-connected-top={connectedTop}
      data-buttongroup-full-width={fullWidth}
    >
      {children}
    </div>
  );
}

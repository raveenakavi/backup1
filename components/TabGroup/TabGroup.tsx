import style from './TabGroup.module.scss'

interface TabGroupProps {
  type: TabGroupType
  items: string[]
  onChange: (val: any) => void
  name: string
}

export enum TabGroupType {
  Multiple,
  Single,
}

export const TabGroup = (props: TabGroupProps) => {
  const toggleValue = (e, item) => {
    console.log(e)

    if (props.type == TabGroupType.Single) {
      props.onChange(item)
    } else {
    }
    return
  }

  const element = props.items.map((res, i) => (
    <label key={i} className={style.TabGroup__item}>
      <input
        onChange={(e) => toggleValue(e, res)}
        type={props.type == TabGroupType.Single ? 'radio' : 'checkbox'}
        name={props.name}
      />
      <span>{res}</span>
    </label>
  ))

  return <div className={style.TabGroup}>{element}</div>
}

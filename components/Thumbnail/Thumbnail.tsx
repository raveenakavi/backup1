import React from 'react';

import styles from './Thumbnail.module.scss';

type Size = 'small' | 'medium' | 'large';

export interface ThumbnailProps {
  /**
   * Size of thumbnail
   * @default 'medium'
   */
  size?: Size;
  /** URL for the image */
  source: string;
  /** Alt text for the thumbnail image */
  alt: string;
  label: string;
}

export function Thumbnail({source, alt, size = 'medium', label}: ThumbnailProps) {

  const content =
    typeof source === 'string' ? (
      <image alt={alt} source={source} size={'small'} label={'gray'}/>
    ) : (
      <image  />
    );

  return <span >{content}</span>;
}
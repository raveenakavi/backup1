import React, { useEffect } from 'react'
import styles from './ProductButton.module.scss'
import Button from '../ReviewTop/Button/Button'
import { ButtonGroup } from '../ButtonGroup/ButtonGroup'

export default function ProductButton(props: any)  {

const PostList = [
  {
    id: 1,
    image: "https://picsum.photos/id/134/300/200",
    storage : "8GB + 128GB",
  },
  {
    id: 2,
    image: "https://picsum.photos/id/134/300/200",
    storage : "4GB + 64GB",
  },
  {
    id: 3,
    image: "https://picsum.photos/id/134/300/200",
    storage : "8GB + 128GB",
  }
]

useEffect(() => {
  console.log(PostList)
})

    return (
      <div className="flex">
      {PostList.map((post, id) => (
        <div key={id}>
          <ButtonGroup segmented key={id}>
          {post.storage}
</ButtonGroup>

             <button
             style={{
                  height:'auto',
                  width:'auto',
                  border:'1px solid #ccc',
             }}
             onClick={() => console.log("You clicked on the pink circle!")}
                  key={id}
                  className={`${styles['Product_Button']}`}
                >
        {post.storage}
      </button>
      {/* <Button
                  color="none"
                  height="30px"
                  onClick={() => console.log("You clicked on the pink circle!")}
                  width="98%"
                  border="1px solid #ccc"
                >
        8GB RAM
      </Button>
      <Button
                  color="none"
                  height="30px"
                  onClick={() => console.log("You clicked on the pink circle!")}
                  width="98%"
                  border="1px solid #ccc"
                >
        8GB RAM
      </Button> */}
            
        </div>
                  ))}
</div>
    )
}

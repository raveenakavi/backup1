import React, { FunctionComponent } from "react";
import GradeIcon from '@material-ui/icons/Grade';
import styles from './review-top.module.scss';
import Progressbar from './Progressbar/Progressbar';
import Button from './Button/Button';
import Divider from './Divider/Divider';
import FrequentList from './Frequent/FrequentList';
import { ImageCard } from "../Card/ImageCard/image-card";


interface ReviewConfig {
  source: string;
  ReviewList: ReviewListConfig;
}

interface ReviewListConfig {
  title: string;
  Ratings: string;
  Reviews: string;
  percent: string;
}

const Posts: FunctionComponent<ReviewConfig> = ({
  source,
  ReviewList: { title, Ratings, Reviews, percent }
}) => {
  return (
    <div className={`${styles['reviews-wrp']}`}>
    <h4 className="text-center font-bold bg-gradient-to-r from-gray-400 via-gray-600 to-gray-500 h-12 pt-3 text-white">
       {title}
    </h4>

    <div className="grid grid-cols-2 md:grid md:grid-cols-3">
      <div className="mx-0 border-r border-gray-300 md:pb-7">
        <h2 className="md:flex md:p-10 text-5xl font-bold ml-3 mr-1 h-full justify-center items-center">
          <div className="leading-10 justify-center	items-center text-yellow-600 w-full md:w-1/2">
            <span className="flex items-center pt-6">
              <GradeIcon /> {Ratings}{' '}
            </span>
            <span className="font-normal text-base text-black px-2 md:pl-3">
            {Reviews} Reviews
            </span>
          </div>

          <Divider />

          <div className="review-divider leading-10 w-1/2 justify-center items-center text-green-600">
            <span className="items-center px-2 md:float-right pt-6">{percent} %</span>
            <span className="font-normal text-base text-black px-2 md:float-right pt-6">
              Recommends
            </span>
          </div>
        </h2>

        <div>
          <Button
            border="none"
            color="black"
            height="30px"
            onClick={() => console.log("You clicked on the pink circle!")}
            width="100%"
          >
            Read all more Reviews
            </Button>
        </div>

      </div>


      <div className="p-7">
        <Progressbar />
      </div>

      <div className="col-span-2 md:col-span-1 frequent md:border-l border-gray-300 p-10 w-full">
        <h4 className="text-sm font-bold">Frequently Mentioned</h4>

        <div className="w-full">
          <FrequentList />
        </div>
        <div className="absolute">
          <h3 className="text-center text-base font-bold">
            Read all Reviews
        </h3>
        </div>
      </div>
    </div>
  </div>
  );
};

const ReviewList = {
  title: "Reviews & Ratings",
  Ratings: "4.6",
  Reviews: "279",
  percent: "78",
};

export default function Mpp() {
  return (
    <div className="Mpp">
      <Posts source="sample-test" ReviewList={ReviewList} />
    </div>
  );
}

import React, { useState } from 'react';
import StarRatings from 'react-star-ratings';

export default function Recommanded() {

    const [performanceRating, setPerformanceRating] = useState<number>(0);
    const [performanceStarColorChange, setPerformanceStarColorChange] = useState("#ddd");

    const Recommanded = [
        {
            Rated: "21",
            Ratecontent : "people rated on this review",
            Reviews: "its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.",
            title: "Most Recommended Positive Reviews",
            down: "1",
            sms: "3",
        },

    ]
    return (
        <div>
        {Recommanded.map((datas, i) => (

        <div className="frequent rating-icon border border-gray-200 mt-7 pl-2">
            <h4 className="text-lg pt-8 font-bold ">{datas.title}</h4>
            <span className="text-xs">{datas.Rated} {datas.Ratecontent}</span>
            <h4 className="pt-2 text-sm font-bold text-gray-600"><StarRatings starDimension={20} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{ margin: 5 }} changeRating={(performanceRating) => selectedPerformanceStarCount(performanceRating)} /><span className="pl-5">Fantastic Product !</span></h4>
            <p className="pt-2 text-sm pb-10">{datas.Reviews}</p>
            <div className="upper-section w-full h-0">
            </div>
            <div className="w-full h-0 relative border border-gray-300 lower-section section-divider">
            </div>
        </div>
        ))}
        </div>
    )
}

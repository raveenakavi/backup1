import React from "react";

interface Props {
  border?: string;
  color?: string;
  children?: React.ReactNode;
  height?: string;
  onClick: () => void;
  radius?: string
  width?: string;
  padding?: string;
}

const Button: React.FC<Props> = ({ 
    border,
    color,
    children,
    height,
    onClick, 
    radius,
    width,
    padding
  }) => {
    border = !!border ? border : 'none'
    color = !!color ? color : 'none'
    height = !!height ? height : '30px'
    radius = !!radius ? radius : '0px'
    width  = !!width ? width : '100%'
    padding = !!padding ? padding : '2px'
    children = !!children ? children : 'Submit'
  return (
    <button 
    className={``}
      onClick={onClick}
      style={{
         backgroundColor: color,
         border,
         borderRadius: radius,
         height,
         width,
         padding
      }}
    >
    {children}
    </button>
  );
}

export default Button;
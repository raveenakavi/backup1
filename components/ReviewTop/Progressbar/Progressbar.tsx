import React, { FunctionComponent } from "react";
import styles from "./progressbar.module.scss";

interface ProgressConfig {
    source: string;
    ProgressbarList: ProgressbarListConfig;
  }
  
  interface ProgressbarListConfig {
    Ratings: string;
    Reviews: string;
  }
  
  const Posts: FunctionComponent<ProgressConfig> = ({
    source,
    ProgressbarList: { Ratings, Reviews }
  }) => {
    return (
        
        <div className={`styles.progressbar`}>
            <div>
                <div className="flex mb-2 items-center justify-between">
                    <div>
                        <span className="text-xs inline-block py-1 px-2 uppercase rounded-full text-gray-600 bg-blueGray-200">
                            {Ratings} Stars
                    </span>
                    </div>
                    <div className="text-right">
                        <span className="text-xs inline-block text-gray-600">
                            {Reviews} Reviews
                    </span>
                    </div>
                </div>
                <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-gray-200">
                    <div className="w-5/6 shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-green-500"></div>
                </div>
            </div>

            <div>
                <div className="flex mb-2 items-center justify-between">
                    <div>
                        <span className="text-xs inline-block py-1 px-2 uppercase rounded-full text-gray-600 bg-blueGray-200">
                            4 Stars
                        </span>
                    </div>
                    <div className="text-right">
                        <span className="text-xs inline-block text-gray-600">
                            73 Reviews
                        </span>
                    </div>
                </div>
                <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-gray-200">
                    <div className="w-4/6 shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-blue-500"></div>
                </div>
            </div>

            <div>
                <div className="flex mb-2 items-center justify-between">
                    <div>
                        <span className="text-xs inline-block py-1 px-2 uppercase rounded-full text-gray-600 bg-blueGray-200">
                            3 Stars
        </span>
                    </div>
                    <div className="text-right">
                        <span className="text-xs inline-block text-gray-600">
                            13 Reviews
        </span>
                    </div>
                </div>
                <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-gray-200">
                    <div className="w-3/6 shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-yellow-400"></div>
                </div>
            </div>

            <div>
                <div className="flex mb-2 items-center justify-between">
                    <div>
                        <span className="text-xs inline-block py-1 px-2 uppercase rounded-full text-gray-600 bg-blueGray-200">
                            2 Stars
        </span>
                    </div>
                    <div className="text-right">
                        <span className="text-xs inline-block text-gray-600">
                            8 Reviews
        </span>
                    </div>
                </div>
                <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-gray-200">
                    <div className="w-2/6 shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-red-300"></div>
                </div>
            </div>

            <div>
                <div className="flex mb-2 items-center justify-between">
                    <div>
                        <span className="text-xs inline-block py-1 px-2 uppercase rounded-full text-gray-600 bg-blueGray-200">
                            1 Stars
        </span>
                    </div>
                    <div className="text-right">
                        <span className="text-xs inline-block text-gray-600">
                            3 Reviews
        </span>
                    </div>
                </div>
                <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-gray-200">
                    <div className="w-1/6 shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-red-500"></div>
                </div>
            </div>
        </div>
  );
};

const ProgressbarList = {
  Ratings: "5",
  Reviews: "279",
};

export default function Progressbar() {
  return (
    <div className="Progressbar">
      <Posts source="sample-test" ProgressbarList={ProgressbarList} />
    </div>
  );
}

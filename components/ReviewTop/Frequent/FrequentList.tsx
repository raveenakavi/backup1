import React from 'react'
import style from "./frequentlist.module.scss"


export default function FrequentList() {
    const frequent = ["Camera (120)"," " , "Value for Money (5)", "Photography (12)", "Battery Life (22)", "Look (10)", "Performance (34)"]
    
    return (
        <div>
            {frequent.map((list,index) => (
                <div className={`${style['reviewlist']}` +` `+ 'float-left break-normal text-sm pt-6 text-gray-500'}>{list}</div>
                
))}

        </div>
    )
}

import React, { Component, useState } from 'react'
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import TextsmsIcon from '@material-ui/icons/Textsms';
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import SearchInput from '~/components/SearchInput/SearchInput';
import Button from '../Button/Button';


export default function Questions() {

    const questions = [
        {
            Q: "Which  phone is best iphone XS max or Samsung Galaxy note 9?",
            A: "I'm Currently using both cell phones note 9 and XS max, if you want to go for features then go for note 9, if you just want to click some good pics and show your good status symbol then go fir XS max",
            Reviewer: "By Tech feedz - Verified tech reviewer",
            up: "20",
            down: "1",
            sms: "3",
        }
    ]
    return (
        <div>
            {questions.map((datas, i) => (
                <div className="frequent shadow-2xl"> <h4 className="font-bold bg-gradient-to-r from-gray-400 via-gray-600 to-gray-500 h-12 mt-10 pt-3 pl-5 text-white text-xl">Have a Question?</h4>
                    <div className="review-search pt-2 pb-2 text-xs m-2 mt-4 border border-gray-300 h-12">
                        <SearchInput placeholder="Search or ask your question here" inputProps={{ "aria-label": "Search for Products and More.." }} />

                    </div>
                    <div key={i} className="pl-1 text-sm text-gray-500 border-b border-gray-200">
                        <h5 className="font-bold pt-2">Q: {datas.Q}</h5>
                        <p className="pt-5 pb-5"> A: {datas.A}</p>
                        <span className="mt-5 text-xs">{datas.Reviewer}</span>
                        <span className="pt-2 pb-3 inline flex space-x-16 text-xs"><span className="fill-current text-yellow-600"><ThumbUpIcon /> {datas.up}</span><ThumbDownIcon /> {datas.down} <TextsmsIcon /> {datas.sms} <SubdirectoryArrowRightIcon /></span>
                    </div>
                    <span className="readmore text-xs capitalize">
                        <Button
                            onClick={() => console.log("You clicked on the pink circle!")}
                        >Read all more questions</Button>
                    </span>
                </div>
            ))}
        </div>
    )
}

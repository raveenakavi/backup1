import React, { Component, useState } from 'react'
import StarRatings from 'react-star-ratings'
// import Rating from '@material-ui/lab/Rating';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import Avatar from '@material-ui/core/Avatar';

export default function Filter() {
    const [starCount, setRating] = useState<number>(0)
    const [starServicesReviewCount, setServicesReviewRating] = useState<number>(0)
    const [starDeliveryReviewCount, setDeliveryReviewRating] = useState<number>(0)
    const [designRating, setDesignRating] = useState<number>(0);
    const [performanceRating, setPerformanceRating] = useState<number>(0);
    const [performanceStarColorChange, setPerformanceStarColorChange] = useState("#ddd");
    const [starColorChange, setStarColorChange] = useState("#ddd");

  const  changeRatingCount = (count) =>{
    setRating(count)
    if(count == 0){
      setStarColorChange("#f9151a")
    }
    if(count == 1){
      setStarColorChange("#f9151a")
    }else if(count == 2){
      setStarColorChange("#ff585d")
    }else if(count == 3){
      setStarColorChange("#fe7b02")
    }else if(count == 4){
      setStarColorChange("#83d564")
    }else if(count == 5){
      setStarColorChange("#2ea000")
    }else{
      setStarColorChange("#ddd")
    }
  }


  const selectedPerformanceStarCount = (performanceRating) =>{
    setPerformanceRating(performanceRating);
    if(performanceRating == 1){
      setPerformanceStarColorChange("#f9151a")
    }else if(performanceRating == 2){
      setPerformanceStarColorChange("#ff585d")
    }else if(performanceRating == 3){
      setPerformanceStarColorChange("#fe7b02")
    }else if(performanceRating == 4){
      setPerformanceStarColorChange("#83d564")
    }else if(performanceRating == 5){
      setPerformanceStarColorChange("#2ea000")
    }else{
      setPerformanceStarColorChange("#ddd")
    }
    
  }

  const clearCount = () =>{
    setRating(0)
  }
  const clearServicesReviewCount = () =>{
    setServicesReviewRating(0)
  }
  const clearDeliveryReviewCount = () =>{
    setDeliveryReviewRating(0)
  }      
const handleRatingClick= (e, name)=> {

    console.log('You left a ' + name.rating + ' star rating for ' + name.caption);
    

}

const [divShow, setDivShow] = React.useState(false);
const[add,minus]=useState(true);

const selectChange = (element) =>{
  if(element == add){
    setDivShow(false);
  }else if(element==minus){
    setDivShow(true);
  }
}


    return (
                        <div className="review-icon md:mt-10 pl-5 md:pl-10 pr-6">
                          <div className="frequent">
<h3 className="text-2xl font-bold">Customer Reviews <span className="float-right text-xs font-normal text-gray-500 pr-2"><button className="filter-button"  onClick={(e)=>{setDivShow(!divShow);minus(!add)}}>Filter</button></span></h3></div>

{(divShow)?(<>
      <div className="filter border border-gray-300 p-5 mt-3">
      <h4 className="text-sm font-bold">Sort By</h4>
                    <ul className="review-list break-normal text-sm pb-2 text-gray-500">
                        <li className="text-green-500">Latest</li>
                        <li className="text-green-500">Oldest</li>
                        <li>By Tech Experts</li>
                        <li>from Purchased Customer</li>
                        <li>By Verified Reviewer</li>
                    </ul>

                    <h4 className="text-sm font-bold">Filter by Rating</h4>
                    <ul className="review-list break-normal text-sm pb-2 text-gray-500">
                        <li className="text-green-500">All</li>
                        <li className="text-green-500">Positive</li>
                        <li>Negative</li>
                        <li>5</li>
                        <li>4</li>
                        <li>3</li>
                        <li>2</li>
                        <li>1</li>
                    </ul>   

                    <h4 className="text-sm font-bold">Review On</h4>
                    <ul className="review-list break-normal text-sm pb-2 text-gray-500">
                        <li className="text-green-500">Product</li>
                        <li className="text-green-500">Service</li>
                        <li>Delivery</li>
                    </ul>          

                    
      </div>
      </>):null}

<div className="review pb-3">
<h4 className=" flex pt-8 text-l font-bold text-green-600 sm:flex">
<StarRatings starDimension={20} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(performanceRating)=> selectedPerformanceStarCount(performanceRating)} />
   <span className="float-left w-1/2 text-left pl-10">Loving It !</span></h4>
<div className="w-full md:w-2/5 review-status inline-block pt-2">
<div className="inline-block"><Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" /> </div>
<div className="inline-block align-middle pl-5">
    <h4 className="text-lg font-bold ">Emma Watson</h4>
    <span className="text-xs text-gray-300">Purchased Customer</span> <span className="w-1/2 text-xs text-gray-500 font-bold pl-6">7 days ago</span>
    </div>
</div>
<div className="w-full md:w-3/5 inline-flex">
<div className='camera text-xs flex-1'> <span className="leading-normal">Camera</span><br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li> <li></li></ul>
    </div>
    <div className='appearance text-xs flex-1'> Appearance/Design<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    <div className='battery text-xs flex-1'> Battery<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    </div>
    
    <p className="mobile-p pt-8 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>
    <div className="w-full md:flex pb-8 border-b border-gray-200">
                            <span className="overflow-hidden space-x-4 w-full flex md:w-4/5 flex md:space-x-2 pr-52"><img src="asset/review2.png" />
                            <img src="asset/review2.png" />
                            <img src="asset/review3.png" />
                            <img src="asset/review4.png" /></span>
                            <span className="w-full md:w-1/5 pt-9 pb-0 inline flex md:space-x-16 text-xs"><span className="fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 </span>
                            </div>                        
                            </div>

                            <div className="review pb-3">
<h4 className=" flex pt-8 text-l font-bold text-green-600 sm:flex">
<StarRatings starDimension={20} disabled={false} maxStars={5} rating={performanceRating} starRatedColor={performanceStarColorChange} emptyStar={'star'} fullStar={'star'} halfStar={'star-half'} iconSet={'FontAwesome'} starSize={15} starStyle={{margin:5}}  changeRating={(performanceRating)=> selectedPerformanceStarCount(performanceRating)} />
   <span className="float-left w-1/2 text-left pl-10">Loving It !</span></h4>
<div className="w-full md:w-2/5 review-status inline-block pt-2">
<div className="inline-block"><Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" /> </div>
<div className="inline-block align-middle pl-5">
    <h4 className="text-lg font-bold ">Emma Watson</h4>
    <span className="text-xs text-gray-300">Purchased Customer</span> <span className="w-1/2 text-xs text-gray-500 font-bold pl-6">7 days ago</span>
    </div>
</div>
<div className="w-full md:w-3/5 inline-flex">
<div className='camera text-xs flex-1'> <span className="leading-normal">Camera</span><br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li> <li></li></ul>
    </div>
    <div className='appearance text-xs flex-1'> Appearance/Design<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    <div className='battery text-xs flex-1'> Battery<br />
      <ul className="m-auto absolute list-none p-0"><li></li><li></li><li></li><li></li><li></li></ul>
    </div>
    </div>
    
    <p className="mobile-p pt-8 text-sm pb-6">its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two.its 2020 and I just bought this iphone7 and the phone is amazing with great camera quality and a great screen. it came with earphones, charger and of course the phone. It also came with a sim card. so if ur planning and buying this then just know that it works really good! if I could add more pictures I would but I can only put two. <span className="text-yellow-700"><a href="#">more...</a></span></p>
    <div className="w-full md:flex pb-8 border-b border-gray-200">
                            <span className="overflow-hidden space-x-4 w-full flex md:w-4/5 flex md:space-x-2 pr-52"><img src="asset/review2.png" />
                            <img src="asset/review2.png" />
                            <img src="asset/review3.png" />
                            <img src="asset/review4.png" /></span>
                            <span className="w-full md:w-1/5 pt-9 pb-0 inline flex md:space-x-16 text-xs"><span className="fill-current text-yellow-600"><ThumbUpIcon /> 21</span><ThumbDownIcon /> 1 </span>
                            </div>                        
                            </div>

                        </div>
                        
    )
}

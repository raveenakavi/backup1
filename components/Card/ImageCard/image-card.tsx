import React from 'react'
import style from './Image-card.module.scss'
import GradeIcon from '@material-ui/icons/Grade';


interface ImageCardProps {
  image: string
  title: string
  price: number
  // rating: string
  // description: string
  offer: string
}

export const ImageCard = (props: ImageCardProps) => {
  return (
    <div className={`${style.brand_border} p-4 w-60`}>
      <div>
        <img src={props.image} className={`${style.brand_slide}`} />
      </div>
      <div className={``}>
        <h1
          className={`text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis`}
        >
          {props.title}
        </h1>
        <div>
          <div className={`flex justify-between pt-1 pb-1 pr-1 pl-1`}>
            <p
              className={`flex-grow capitalize inline-flex items-center p-0 font-bold text-xs`}
            >
              {' '}
              Rs. {props.price}
            </p>
            <p className={`capitalize text-xs font-bold p-1`}>
              <GradeIcon className={`${style.brand_staricon}`} />
            </p>
            {/* <p className={`capitalize text-xs font-bold p-1`}>
              {' '}
              {props.rating}
            </p> */}
          </div>
          <div>
            <span className="text-gray-400 text-md">{props.offer}</span>

          </div>
        </div>
      </div>
    </div>
  )
}


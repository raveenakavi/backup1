import style from './video-card.module.scss'

interface VideoCardProps {
  description: string
  play_id: string
  thumbnail_large: string
  thumbnail_medium: string
  thumbnail_small: string
  video_id: number
  video_name: string
  video_type: number
  video_url: string
}

export const VideoCard = (props: { data: VideoCardProps }) => {
  return (
    <div className={style.card}>
      <div className={style.card__video}>
        <div
          className={style.card__video__thumbnail}
          style={{
            backgroundImage: `url(${props.data.thumbnail_large})`,
          }}
        ></div>
        <img src={`assets/icons/play.png`} alt="" />
        {/* <video id={`${props.data.video_id}`} src={props.data.video_url}></video> */}
      </div>
      <div className={style.card__description}>
        <div
          className={style.card__description__thumbnail}
          style={{
            backgroundImage: `url(${props.data.thumbnail_small})`,
          }}
        ></div>
        <div className={style.card__description__text}>
          <b>{props.data.video_name}</b>
          <p>{props.data.description}</p>
          <div className={style.card__description__text__pricing}>
            <span className={style.rating}>Above 4.5</span>
            <img
              className={style['star-icon']}
              src={'assets/icons/star.png'}
              alt=""
            />
            <div className={style.spacer}></div>
            <div className={style.cost}>
              <span
                style={{
                  color: '#1E2D3E',
                }}
              >
                Starting Price
              </span>{' '}
              Rs.16,789
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

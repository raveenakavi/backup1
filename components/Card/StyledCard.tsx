import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledContainer = styled.div`
  border: 1px solid #C16482;
  padding: 25px 12px 18px;
  margin: 25px 20px;
  width:400px;
};
`
const StyledImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border: 1px solid #C16482;
`
const Title = styled.h2`
  color: #C16482;
  font-weight: 300;
  @media (max-width: 500px) {
    font-size: 1rem;
  }
`
const Date = styled.div`
  color: #ccc;
  font-weight: 300;
  margin: 6px 0;
  @media (max-width: 500px) {
    font-size: 0.8rem;
  }
`
const Description = styled.p`
  color: #C16482;
  font-weight: 300;
  padding-left:0;
  @media (max-width: 500px) {
    font-size: 0.75rem;
  }
`
const StyledCard = ({
  image,
  title,
  date,
  price,
  offer,
  description,
  buttonTitle,
  buttonClick
}) => (
  <StyledContainer>
    <StyledImg src={image} />
    <Title>{title}</Title>
    <span>{offer}</span>
    <Date>{date}</Date>
    <Description>{description} {price}</Description>
  </StyledContainer>
)
StyledCard.propTypes = {
  /**
   * post image
   */
  image: PropTypes.string.isRequired,
  /**
   * post title
   */
  title: PropTypes.string.isRequired,
  /**
   * date
   */
  date: PropTypes.string.isRequired,
  /**
   * short description of the post
   */
  price: PropTypes.string.isRequired,
  offer: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  /**
   * Title for the button
   */
  buttonTitle: PropTypes.string.isRequired,
  /**
   * on click function for the button
   */
  buttonClick: PropTypes.func.isRequired,
};
export default StyledCard
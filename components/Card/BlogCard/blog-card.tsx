import React from 'react'
import style from './blog-card.module.scss'

interface BlogCardProps {
  image: string
  heading: string
  timestamp: string
  comments: string
  description: string
  status: string
}

export const BlogCard = (props: BlogCardProps) => {
  return (
    <div className={`${style.blog_border} p-4 w-72`}>
      <div>
        <img src={props.image} className={`${style.blog_slide}`} />
      </div>
      <div className={`${style.blog_text} leading-4`}>
        <h1 className={`text-base text-left overflow-hidden whitespace-nowrap overflow-ellipsis`} >{props.heading}</h1>
        <span>{props.timestamp} / {props.comments} Comments</span>
        <div>
          <div className={`leading-3 flex justify-between pt-1 pb-1 pr-1 my-3`}>
            <p className={`flex-grow capitalize p-0 text-xs`}>{props.description}</p>
          </div>
          <div className="flex my-5">
            <span className="border-dashed border-white font-bold bg-gray-400 text-white text-xs p-1 mr-2 rounded-md">{props.status}</span>
            <span className="border-dashed border-white font-bold bg-gray-400 text-white text-xs p-1 mr-2 rounded-md">{props.status}</span>
            <span className="border-dashed border-white font-bold bg-gray-400 text-white text-xs p-1 mr-2 rounded-md">{props.status}</span>

          </div>
        </div>
      </div>
    </div>
  )
}


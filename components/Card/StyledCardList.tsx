import React from 'react';
import styled from 'styled-components'
import PropTypes from 'prop-types';
import StyledCard from './StyledCard';
const StyledContainer = styled.div`
  max-width: 750px;
  padding: 50px 12px;
  width: 100%;
  margin: auto;
`
const StyledCardList = ({postList}) => {
  return (
    <StyledContainer>
    {postList.map(post => (
        <StyledCard
          key={post.id}
          image={post.image}
          title={post.title}
          date={post.date}
          price={post.price}
          description={post.description}
          buttonTitle={post.buttonTitle}
          buttonClick={post.buttonClick}
          offer={post.offer}
        />
    ))}
    </StyledContainer>
  );
};
StyledCardList.propTypes = {
  /**
   * A list of posts to render
   */
  postList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      image: PropTypes.string,
      title: PropTypes.string,
      date: PropTypes.string,
      price: PropTypes.number,
      description: PropTypes.string,
      buttonTitle: PropTypes.string,
      buttonClick: PropTypes.func,
      offer: PropTypes.string,
    })
  ),
};
export default StyledCardList;
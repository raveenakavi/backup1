import React from 'react'
import { BlogCard } from '~/components/Card/BlogCard/blog-card'
import { HorizontalScrollWrap } from '~/components/Horizontal-scroll-wrap/HorizontalScrollWrap'

export default function TechBlog() {
    const PostList = [
        {
          id: 1,
          image: "https://picsum.photos/id/134/300/200",
          heading : "Best instant Cameras in 2020",
          timestamp : "06/26/2020",
          comments : "124",
          description : "This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!",
          status :"unboxing"
        },
        {
          id: 1,
          image: "https://picsum.photos/id/134/300/200",
          heading : "Best instant Cameras in 2020",
          timestamp : "06/26/2020",
          comments : "124",
          description : "This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!",
          status :"unboxing"
        },
        {
          id: 1,
          image: "https://picsum.photos/id/134/300/200",
          heading : "Best instant Cameras in 2020",
          timestamp : "06/26/2020",
          comments : "124",
          description : "This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!",
          status :"unboxing"
        },
        {
          id: 1,
          image: "https://picsum.photos/id/134/300/200",
          heading : "Best instant Cameras in 2020",
          timestamp : "06/26/2020",
          comments : "124",
          description : "This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!",
          status :"unboxing"
        },
        {
          id: 1,
          image: "https://picsum.photos/id/134/300/200",
          heading : "Best instant Cameras in 2020",
          timestamp : "06/26/2020",
          comments : "124",
          description : "This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!",
          status :"unboxing"
        },
        {
          id: 1,
          image: "https://picsum.photos/id/134/300/200",
          heading : "Best instant Cameras in 2020",
          timestamp : "06/26/2020",
          comments : "124",
          description : "This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!This is my first post. Hello World!",
          status :"unboxing"
        },
    ]
    return (
        <div>
            <HorizontalScrollWrap
            showAllLink="/"
            title={"Shop by Category"}>
            <div className="flex">
          {PostList.map((post, id) => (
            <div key={id} className="m-4">
                  <BlogCard key={post.id}
          image={post.image}
          heading={post.heading}
          timestamp={post.timestamp}
          comments={post.comments}
          description={post.description}
          status={post.status}
          />
            </div>
          ))}
        </div>
            </HorizontalScrollWrap>
            
        </div>
    )
}

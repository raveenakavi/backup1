import { ReactNode, useRef } from 'react'
import Link from 'next/link'
import style from './horizontal-scroll-wrap.module.scss'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { TabGroup } from '../TabGroup/TabGroup';

interface HorizontalScrollWrapProps {
  children?: ReactNode;
  title?: string;
  scrollLength?: number;
  colWidth?: string;
  withFilter?: boolean;
  showAllLink?: string;
}

export const HorizontalScrollWrap = ({
  children,
  title,
  scrollLength,
  colWidth,
  withFilter,
  showAllLink
}: HorizontalScrollWrapProps) => {
  const wrapper = useRef(null)
  const settings = {
    dots: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true
  };
  const scrollL = () => {
    return !!scrollLength 
    ? scrollLength : wrapper.current.clientWidth
  }

  const scrollRight = () => {
    let distance =
      wrapper.current.scrollLeft + scrollL()
    wrapper.current.scrollTo({ left: distance, behavior: 'smooth' })
    return
  }

  const scrollLeft = () => {
    let distance =
      wrapper.current.scrollLeft - scrollL()
    wrapper.current.scrollTo({ left: distance, behavior: 'smooth' })
    return
  }



  return (
    
    <div className={style.scroller}>

        <div className="text-gray-200">

        {/* show all if showAllLink is provided */}
        {!!showAllLink ? <Link href={showAllLink} >
            <span className={style.show_all}>Show all</span>
          </Link> : ''}


          <button onClick={scrollLeft}>
          <ArrowBackIosIcon />
          </button>
          <button onClick={scrollRight}>
          <ArrowForwardIosIcon />
          </button>
        </div>


      <div  {...settings} className={style.scroller__wrapper} 
        style={{
          width: "100%",
            display: "grid",
            overflow: "hidden",
            gridTemplateColumns: `repeat(200, ${!!colWidth ? colWidth : '260px'})`
        }}
        title={title}
        ref={wrapper}>
        {children}
      </div>

    </div>
  )
}

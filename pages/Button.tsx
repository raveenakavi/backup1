import React from "react";
import ReactDOM from "react-dom";

import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import { RadioButtonChecked } from "@material-ui/icons";
import { Checkbox } from "@material-ui/core";

const AModal = props => {
  return (
    <>
      <Button {...props}>A</Button>
      <Modal open={false}>
        <div>Hello Modal</div>
      </Modal>
    </>
  );
};
const OtherModal = ({ buttonText, ...other }) => {
  return (
    <>
      <Button {...other}>{buttonText}</Button>
      <Modal open={false}>
        <div>Hello Modal</div>
      </Modal>
    </>
  );
};
// I don't recommend this approach due to maintainability issues,
// but if you have a lint rule that disallows prop spreading, this is a workaround.
const AvoidPropSpread = ({
  className,
  disabled,
  color,
  disableFocusRipple,
  disableRipple,
  fullWidth,
  size,
  variant
}) => {
  return (
    <>
      <Button
        className={className}
        disabled={disabled}
        color={color}
        disableFocusRipple={disableFocusRipple}
        disableRipple={disableRipple}
        fullWidth={fullWidth}
        type={RadioButtonChecked ? | Checkbox ?}
        size={size}
        variant={variant}
      >
        C
      </Button>
      <Modal open={false}>
        <div>Hello Modal</div>
      </Modal>
    </>
  );
};
function App() {
  return (
    <ButtonGroup>
      <AModal />
      <OtherModal buttonText="B" />
      
    </ButtonGroup>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);

import React from 'react';
import StyledCardList from '../../components/Card/StyledCardList'



export default function index() {
    const postList = [
        {
          id: 1,
          image: "https://picsum.photos/id/134/300/200",
          title : "My First Post",
          date : "06/26/2020",
          description : "This is my first post. Hello World!",
          buttonTitle : "Read this post",
          buttonClick : () => alert("This should take you to the post link")
        },
        {
          id: 2,
          image: "https://picsum.photos/id/5/300/200",
          title : "My Second Post",
          date : "06/27/2020",
          description : "This is my second post. Keep up the rythm!",
          buttonTitle : "Read this post",
          buttonClick : () => alert("This should take you to the post link")
        }
      ]
    return (
        <div>
            <StyledCardList postList={postList} />
        </div>
    )
}

import React from 'react'
import CarouselArrows from '~/components/CarouselArrows/CarouselArrows';
import Questions from '~/components/ReviewTop/Questions/Questions';
import Recommanded from '~/components/ReviewTop/Recommended/Recommanded';
import Filter from '~/components/ReviewTop/ReviewFilter/Filter';
import ReviewTop from '../../components/ReviewTop/ReviewTop';
import Carousel from '../../components/CarouselArrows/Carousel';
import StyledCard from '~/components/Card/StyledCard';
import StyledContainer from 'styled-components';

export default function index() {
  const postList = [
    {
      id: 1,
      image: "https://picsum.photos/id/134/300/200",
      title : "My First Post",
      date : "06/26/2020",
      description : "This is my first post. Hello World!",
      buttonTitle : "Read this post",
      buttonClick : () => alert("This should take you to the post link")
    },
    {
      id: 2,
      image: "https://picsum.photos/id/5/300/200",
      title : "My Second Post",
      date : "06/27/2020",
      description : "This is my second post. Keep up the rythm!",
      buttonTitle : "Read this post",
      buttonClick : () => alert("This should take you to the post link")
    }
  ]
  return (
    <div>      
      <ReviewTop />
      <div className="review-search md:grid md:grid-cols-3">
      <div className="p-2">
        <Recommanded />
          <Questions />
        </div>
        <div className=" w-full col-span-2 ">
          <Filter />
          <CarouselArrows />
        </div>
      </div>
      <div>
      <div className="flex" style={{ maxWidth: 1200, marginLeft: 'auto', marginRight: 'auto', marginTop: 64 }}>
            <Carousel
                show={2}
                className="flex"
            >

<StyledContainer>
    {postList.map(post => (
      <StyledCard />
    ))}
    </StyledContainer>
                <div>
                    <div style={{padding: 8}}>
                        <img src="https://via.placeholder.com/300x300" alt="placeholder" style={{width: '100%'}} />
                    </div>
                </div>
                <div>
                    <div style={{padding: 8}}>
                        <img src="https://via.placeholder.com/300x300" alt="placeholder" style={{width: '100%'}} />
                    </div>
                </div>
                <div>
                    <div style={{padding: 8}}>
                        <img src="https://via.placeholder.com/300x300" alt="placeholder" style={{width: '100%'}} />
                    </div>
                </div>
                <div>
                    <div style={{padding: 8}}>
                        <img src="https://via.placeholder.com/300x300" alt="placeholder" style={{width: '100%'}} />
                    </div>
                </div>
                <div>
                    <div style={{padding: 8}}>
                        <img src="https://via.placeholder.com/300x300" alt="placeholder" style={{width: '100%'}} />
                    </div>
                </div>
                <div>
                    <div style={{padding: 8}}>
                        <img src="https://via.placeholder.com/300x300" alt="placeholder" style={{width: '100%'}} />
                    </div>
                </div>
                <div>
                    <div style={{padding: 8}}>
                        <img src="https://via.placeholder.com/300x300" alt="placeholder" style={{width: '100%'}} />
                    </div>
                </div>
                <div>
                    <div style={{padding: 8}}>
                        <img src="https://via.placeholder.com/300x300" alt="placeholder" style={{width: '100%'}} />
                    </div>
                </div>
            </Carousel>
        </div>
      </div>
    </div>
  )
}
